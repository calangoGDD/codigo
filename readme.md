# Como verificar as Issues no eclipse #

1. No eclipse abra Help -> Install New Software
2. Informe a URL - > http://babelserver.org/mylyn-bitbucket/
3. Marque -> "Mylyn Bitbucket Connector"
4. ctrl + 3 e digite Task Repositories
5. Clique no botão superior direito "Add Task Repository"
6. No campo server informe: "https://bitbucket.org/calangoGDD/codigo"
7. No campo Username informe: Seu email
8. No campo password informe: Seu password
9. Obs.: Para funcionar foi preciso configurar as Issues como publica.
10. Para ver as issues ctrl + 3 e digite Task List sicronize o repositório e pronto!


# Situações de uma Task #

1. NEW - Ao criar
2. OPEN - Em desenvolvimento
3. ON HOLD - Ao finalizar implementação
4. RESOLVED - Quando estiver testada


# Arquitetura Domain Driven Design #

Services come in 3 flavours: **Domain Services, Application Services, and Infrastructure Services** 

**Domain Services** : Encapsulates business logic that doesn't naturally fit within a domain object, and are NOT typical CRUD operations - those would belong to a Repository.

**Application Services** : Used by external consumers to talk to your system (think Web Services). If consumers need access to CRUD operations, they would be exposed here.

**Infrastructure Services** : Used to abstract technical concerns (e.g. MSMQ, email provider, etc)

Keeping Domain Services along with your Domain Objects is sensible - they are all focused on domain logic. And yes, you can inject Repositories into your Services.

Application Services will typically use both Domain Services and Repositories to deal with external requests.


#Configuração do Run#

**Base directory:** ${workspace_loc:/condogreen}
**Goals:** clean install spring-boot:run

#Query creation#
-Table 4. Supported keywords inside method names
-link: http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation