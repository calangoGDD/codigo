package br.com.calango.condogreen.boleto.remessa;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;

import br.com.calango.condogreen.boleto.domain.remessa.HeaderLoteCaixa240;
import br.com.calango.condogreen.boleto.domain.remessa.LeiauteCaixa240;
import br.com.calango.condogreen.pessoa.domain.ContaBancaria;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

/**
 * Classe responsável por testar o cabeçalho do lote de remessa
 * 
 * @author Guilherme Andrade
 *
 */
public class RemessaBancoCaixa240HeaderLotTest {

	PessoaJuridica condominio = null;

	@Before
	public void inicializar() throws UnsupportedEncodingException {
		ContaBancaria contaBancaria = new ContaBancaria();
		contaBancaria.setAgencia("12345");
		contaBancaria.setCodigoBeneficiario("123456");
		contaBancaria.setCodigoBeneficiario("123456");

		condominio = new PessoaJuridica();
		condominio.setNome("Friends");
		condominio.setCnpj("22.666.605/0001-59");
		condominio.setContaBancaria(contaBancaria);
	}

	public void devePossuirOsMesmosValoresDoManual() throws IOException {
		int nrRemessa = 1;
		LeiauteCaixa240 leiaute = new LeiauteCaixa240();
		HeaderLoteCaixa240 headerLote = leiaute.criarHeaderLote(condominio, nrRemessa);

		Assert.assertEquals("1", headerLote.getTipoRegistro());
		Assert.assertEquals("104", headerLote.getCodBanco());
		Assert.assertEquals("0001", headerLote.getLote());
		Character tipoOperacao = new Character('R');
		Assert.assertEquals(tipoOperacao, headerLote.getTipoOperacao());
		Assert.assertEquals(1, headerLote.getTipoServico().intValue());
		Assert.assertEquals(0, headerLote.getUsoExclusivo7());
		Assert.assertEquals(30, headerLote.getLayoutLote());
		Character usoExclusivo8 = new Character(' ');
		Assert.assertEquals(usoExclusivo8, headerLote.getUsoExclusivo8());
		Assert.assertEquals("2", headerLote.getTipoInscricao());
		Assert.assertEquals("22666605000159", headerLote.getNumInscricao());
		Assert.assertEquals("123456", headerLote.getCodCedente());
		Assert.assertEquals(StringUtils.repeat("0", 14), headerLote.getUsoExclusivo9());
		Assert.assertEquals(condominio.getAgencia(), headerLote.getAgencia());
		Assert.assertEquals(condominio.getDigitoAgencia(), headerLote.getAgenciaVerificador());
		Assert.assertEquals(condominio.getCodigoBeneficiario(), headerLote.getCodConvenio());
		Assert.assertEquals(StringUtils.repeat("0", 7), headerLote.getCodModeloPersonalizado());
		Assert.assertEquals("0", headerLote.getUsoExclusivo10());
		Assert.assertEquals("Friends", headerLote.getEmpNome());
	}

}
