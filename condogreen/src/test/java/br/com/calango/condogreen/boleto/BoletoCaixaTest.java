package br.com.calango.condogreen.boleto;

import java.math.BigDecimal;

import org.jrimum.domkee.comum.pessoa.endereco.UnidadeFederativa;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeTitulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo.Aceite;
import org.junit.Before;

import br.com.calango.condogreen.boleto.domain.CodigoDesconto;
import br.com.calango.condogreen.boleto.domain.CodigoDevolucao;
import br.com.calango.condogreen.boleto.domain.CodigoJurosMora;
import br.com.calango.condogreen.boleto.domain.CodigoMulta;
import br.com.calango.condogreen.boleto.domain.CodigoProtesto;
import br.com.calango.condogreen.boleto.domain.ConfiguradorBoleto;
import br.com.calango.condogreen.core.domain.Endereco;
import br.com.calango.condogreen.pessoa.domain.ContaBancaria;
import br.com.calango.condogreen.pessoa.domain.PessoaFisica;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

public class BoletoCaixaTest {

	PessoaJuridica empresa;

	PessoaFisica morador;

	ConfiguradorBoleto configurador;

	@Before
	public void inicializar() {
		Endereco endereco = new Endereco();
		endereco.setRua("RUA FREI MIGUELINHO - INHAM - IGARASSU");
		endereco.setUf(UnidadeFederativa.PE);
		endereco.setCep("53630");
		endereco.setSufixoCep("000");
		endereco.setCidade("Igarassu");
		endereco.setBairro("INHAMA");
		endereco.setNumero("290");
		endereco.setPais("Brasil");

		ContaBancaria contaBancaria = new ContaBancaria();
		contaBancaria.setAgencia("3122");
		contaBancaria.setCodigoBeneficiario("633754");
		contaBancaria.setDigitoCodigoBeneficiario("6");

		empresa = new PessoaJuridica();
		empresa.setNome("Calango");
		empresa.setCnpj("22.666.605/0001-59");
		empresa.setEndereco(endereco);
		empresa.setContaBancaria(contaBancaria);

		morador = new PessoaFisica();
		morador.setNome("Guilherme Andrade Pinto 1");
		morador.setCelular("99650412");
		morador.setCpf("045.144.065-03");
		morador.setEndereco(empresa.getEndereco());

		configurador = new ConfiguradorBoleto();
		configurador.setAceite(Aceite.N);
		configurador.setCodDesconto(CodigoDesconto.VALOR_FIXO);
		configurador.setCodMora(CodigoJurosMora.VALOR_POR_DIA);
		configurador.setCodMulta(CodigoMulta.VALOR_FIXO);
		configurador.setCodProtesto(CodigoProtesto.PROTESTAR);
		configurador.setDiaDeLimiteDesconto(10);
		configurador.setEspecieDoTitulo(TipoDeTitulo.DM_DUPLICATA_MERCANTIL);
		configurador.setInstrucao1("Caso o pagamento seja feito até o 10, você terá 20 reais de desconto");
		configurador.setPrazoProtesto(2);
		configurador.setValor(BigDecimal.valueOf(120.00));
		configurador.setValorDesconto(BigDecimal.valueOf(20.00));
		configurador.setValorMora(BigDecimal.valueOf(0.50));
		configurador.setValorMulta(BigDecimal.valueOf(10.0));
		configurador.setCodDevolucao(CodigoDevolucao.NAO_BAIXAR_NAO_DEVOLVER);
		configurador.setLocalPagamento("PREFERENCIALMENTE NAS CASAS LOTÉRICAS ATÉ O VALOR LIMITE");
	}

}
