package br.com.calango.condogreen.boleto.remessa;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;

import br.com.calango.condogreen.boleto.domain.remessa.HeaderArquivoCaixa240;
import br.com.calango.condogreen.boleto.domain.remessa.LeiauteCaixa240;
import br.com.calango.condogreen.pessoa.domain.ContaBancaria;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

/**
 * Classe responsável por testar o Header do arquivo de remessa CAIXA 240
 * 
 * @author Guilherme Andrade
 *
 */
public class RemessaBancoCaixa240HeaderTest {

	PessoaJuridica condominio = null;

	@Before
	public void inicializar() throws UnsupportedEncodingException {

		ContaBancaria contaBancaria = new ContaBancaria();
		contaBancaria.setAgencia("12345");
		contaBancaria.setCodigoBeneficiario("123456");

		condominio = new PessoaJuridica();
		condominio.setNome("Friends");
		condominio.setCnpj("22.666.605/0001-59");
		condominio.setContaBancaria(contaBancaria);
	}

	public void devePossuirOsMesmosValoresDoManual() throws IOException {
		long nrRemessa = 1;

		LeiauteCaixa240 leiaute = new LeiauteCaixa240();
		HeaderArquivoCaixa240 headerArquivo = leiaute.criarHeader(condominio, nrRemessa);

		Assert.assertEquals("104", headerArquivo.getCodBanco());
		Assert.assertEquals("0000", headerArquivo.getLote());
		Assert.assertEquals("0", headerArquivo.getTipoRegistro());
		Assert.assertEquals(2, headerArquivo.getTipoInscricao().intValue());
		Assert.assertEquals("22666605000159", headerArquivo.getNumInscricao().toString());
		Assert.assertEquals(condominio.getAgencia(), headerArquivo.getAgencia().toString());
		Assert.assertEquals(5, headerArquivo.getAgenciaVerificador());
		Assert.assertEquals(condominio.getCodigoBeneficiario(), headerArquivo.getCodConvenio());
		Assert.assertEquals(0, headerArquivo.getUsoExclusivoCEF2().intValue());
		Assert.assertEquals(0, headerArquivo.getUsoExclusivoCEF3().intValue());
		Assert.assertEquals(true, headerArquivo.getEmpNome().contains(condominio.getNome()));
		Assert.assertEquals(7, headerArquivo.getEmpNome().length());
		Assert.assertEquals(true, headerArquivo.getBancoNome().contains("CAIXA ECONOMICA FEDERAL"));
		Assert.assertEquals(23, headerArquivo.getBancoNome().length());
		Assert.assertEquals(10, headerArquivo.getUsoExclusivo4().length());
		Assert.assertEquals(1, headerArquivo.getArqCodigoRemessa());
		SimpleDateFormat dateFormatter = new SimpleDateFormat("ddMMyyyy");
		Assert.assertEquals(dateFormatter.format(new Date()), headerArquivo.getArqDataGeracao());
		Assert.assertEquals(6, headerArquivo.getArqHoraGeracao().length());
		Assert.assertEquals(nrRemessa, headerArquivo.getArqNumSequencial());
		Assert.assertEquals(5, headerArquivo.getArqDensidade().length());
		Assert.assertEquals(20, headerArquivo.getUsoExclusivoBanco5().length());
		Assert.assertEquals(20, headerArquivo.getUsoExclusivoEmpresa5().length());
		Assert.assertEquals(4, headerArquivo.getVersaoAplicativo().length());
		Assert.assertEquals(25, headerArquivo.getUsoExclusivo6().length());
	}
}
