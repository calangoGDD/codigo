package br.com.calango.condogreen.boleto.remessa;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.jrimum.domkee.comum.pessoa.endereco.UnidadeFederativa;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeTitulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo.Aceite;
import org.junit.Assert;
import org.junit.Before;

import br.com.calango.condogreen.boleto.domain.CodigoDesconto;
import br.com.calango.condogreen.boleto.domain.CodigoDevolucao;
import br.com.calango.condogreen.boleto.domain.CodigoJurosMora;
import br.com.calango.condogreen.boleto.domain.CodigoMulta;
import br.com.calango.condogreen.boleto.domain.CodigoProtesto;
import br.com.calango.condogreen.boleto.domain.ConfiguradorBoleto;
import br.com.calango.condogreen.boleto.domain.InfoBoleto;
import br.com.calango.condogreen.boleto.domain.remessa.Remessa;
import br.com.calango.condogreen.boleto.util.RemessaUtil;
import br.com.calango.condogreen.core.domain.Endereco;
import br.com.calango.condogreen.core.util.LeitorArquivo;
import br.com.calango.condogreen.pessoa.domain.PessoaFisica;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

public class RemessaCaixa240Test {

	private static final int HEADER_ARQUIVO = 0;

	private static final int HEADER_LOTE = 1;

	private static final int SEGMENTO_P_GUILHERME = 2;

	private static final int SEGMENTO_Q_GUILHERME = 3;

	private static final int SEGMENTO_R_GUILHERME = 4;

	private static final int SEGMENTO_P_JESSICA = 5;

	private static final int SEGMENTO_Q_JESSICA = 6;

	private static final int SEGMENTO_R_JESSICA = 7;

	private static final int TRAILER_LOTE = 8;

	private static final int TRAILER_ARQUIVO = 9;

	PessoaJuridica empresa;

	ConfiguradorBoleto configurador;

	private Remessa remessa;

	@Before
	public void inicializar() throws IOException {
		Endereco endereco = new Endereco();
		endereco.setRua("RUA FREI MIGUELINHO");
		endereco.setUf(UnidadeFederativa.PE);
		endereco.setCep("53625");
		endereco.setSufixoCep("813");
		endereco.setCidade("IGARASSU");
		endereco.setBairro("INHAMA");
		endereco.setNumero("299");
		endereco.setPais("Brasil");

		empresa = RemessaUtil.createPessoaJuridica();

		PessoaFisica morador = new PessoaFisica();
		morador.setNome("GUILHERME ANDRADE PINTO");
		morador.setCelular("99650412");
		morador.setCpf("045.144.065-03");
		morador.setEndereco(empresa.getEndereco());

		PessoaFisica morador2 = new PessoaFisica();
		morador2.setNome("JÉSSICA GABRIELE DOS SANTOS");
		morador2.setCelular("99650412");
		morador2.setCpf("386.965.082-62");
		morador2.setEndereco(empresa.getEndereco());

		configurador = new ConfiguradorBoleto();
		configurador.setAceite(Aceite.N);
		configurador.setCodDesconto(CodigoDesconto.VALOR_FIXO);
		configurador.setCodMora(CodigoJurosMora.VALOR_POR_DIA);
		configurador.setCodMulta(CodigoMulta.VALOR_FIXO);
		configurador.setCodProtesto(CodigoProtesto.PROTESTAR);
		configurador.setDiaDeLimiteDesconto(20);
		configurador.setEspecieDoTitulo(TipoDeTitulo.DM_DUPLICATA_MERCANTIL);
		configurador.setPrazoProtesto(30);
		configurador.setValor(BigDecimal.valueOf(120.00));
		configurador.setValorDesconto(BigDecimal.valueOf(20.00));
		configurador.setValorMora(BigDecimal.valueOf(0.04));
		configurador.setValorMulta(BigDecimal.valueOf(2.40));
		configurador.setCodDevolucao(CodigoDevolucao.NAO_BAIXAR_NAO_DEVOLVER);
		configurador.setLocalPagamento("PREFERENCIALMENTE NAS CASAS LOTÉRICAS ATÉ O VALOR LIMITE");
		configurador.setInstrucao1("TAXA CONDOMINIAL JUNHO 2016");
		configurador.setInstrucao2("BLOCO D APT 105");

		LocalDate dataVencimento = LocalDate.of(2016, 06, 20);
		Remessa remessa = new Remessa();
		InfoBoleto boleto = new InfoBoleto(morador, 10000003319L, 100000000003325L, BigDecimal.valueOf(120), dataVencimento, configurador);
		InfoBoleto boleto2 = new InfoBoleto(morador2, 10000003319L, 100000000003325L, BigDecimal.valueOf(120), dataVencimento, configurador);
		remessa.setNumeroRemessa(1L);
		remessa.addBoleto(boleto);
		remessa.addBoleto(boleto2);
		remessa.geraRemessa(empresa, true);
	}

	public void deveEstarComOHeaderDoArquivoConformeManual() throws IOException {
		File arquivo = remessa.getArquivoComoFile();
		LeitorArquivo leitor = new LeitorArquivo(arquivo);
		String linha = leitor.getLinhas().get(HEADER_ARQUIVO);
		CharSequence codigoBanco = linha.subSequence(0, 3);
		Assert.assertEquals("104", codigoBanco.toString());
		CharSequence lote = linha.subSequence(3, 7);
		Assert.assertEquals("0000", lote.toString());
		CharSequence tipoRegistro = linha.subSequence(7, 8);
		Assert.assertEquals("0", tipoRegistro.toString());
		CharSequence usoExclusivo1 = linha.subSequence(8, 17);
		Assert.assertEquals("         ", usoExclusivo1.toString());
		CharSequence inscricaoEmpresa = linha.subSequence(17, 18);
		Assert.assertEquals(empresa.getTipoInscricao(), inscricaoEmpresa.toString());
		CharSequence numeInscricaoEmpresa = linha.subSequence(18, 32);
		Assert.assertEquals(RemessaUtil.retiraCaracteresEspeciais(empresa.getCnpj()), numeInscricaoEmpresa.toString());
		CharSequence usoExclusivo2 = linha.subSequence(32, 52);
		Assert.assertEquals("00000000000000000000", usoExclusivo2.toString());
		CharSequence agencia = linha.subSequence(52, 57);
		Assert.assertEquals("0" + empresa.getAgencia(), agencia.toString());
		CharSequence digitoAgencia = linha.subSequence(57, 58);
		Assert.assertEquals("" + empresa.getDigitoAgencia(), digitoAgencia.toString());
		CharSequence codigoConvenio = linha.subSequence(58, 64);
		Assert.assertEquals(empresa.getCodigoBeneficiario(), codigoConvenio.toString());
		CharSequence usoExclusivo3 = linha.subSequence(64, 71);
		Assert.assertEquals("0000000", usoExclusivo3.toString());
		CharSequence usoExclusivo4 = linha.subSequence(71, 72);
		Assert.assertEquals("0", usoExclusivo4.toString());
		CharSequence nomeEmpresa = linha.subSequence(72, 102);
		Assert.assertEquals("COND CONJ RESIDENCIAL SITIO VI", nomeEmpresa.toString());
		CharSequence nomeBanco = linha.subSequence(102, 132);
		Assert.assertEquals("CAIXA ECONOMICA FEDERAL       ", nomeBanco.toString());
		CharSequence usoExclusivo5 = linha.subSequence(132, 142);
		Assert.assertEquals("          ", usoExclusivo5.toString());
		CharSequence remessaOrRetorno = linha.subSequence(142, 143);
		Assert.assertEquals("1", remessaOrRetorno.toString());
		CharSequence numeroSeq = linha.subSequence(157, 163);
		Assert.assertEquals("000112", numeroSeq.toString());
		CharSequence versao = linha.subSequence(163, 166);
		Assert.assertEquals("050", versao.toString());
		CharSequence densidade = linha.subSequence(166, 171);
		Assert.assertEquals("00000", densidade.toString());
		CharSequence usuBancoRetornoTest = linha.subSequence(171, 191);
		Assert.assertEquals("                    ", usuBancoRetornoTest.toString());
		CharSequence usuReservadoEmpRemessa = linha.subSequence(191, 211);
		Assert.assertEquals("REMESSA-TESTE       ", usuReservadoEmpRemessa.toString());
		CharSequence versaoAppCaixa = linha.subSequence(211, 215);
		// TODO ver se é obrigatório informar a versão da cliente é V032
		Assert.assertEquals("    ", versaoAppCaixa.toString());
		CharSequence usoExclusivoCaixa = linha.subSequence(215, 240);
		Assert.assertEquals("                         ", usoExclusivoCaixa.toString());
	}

	public void deveEstarComOHeaderDoLoteConformeManual() throws IOException {
		File arquivo = remessa.getArquivoComoFile();
		LeitorArquivo leitor = new LeitorArquivo(arquivo);
		String linha = leitor.getLinhas().get(HEADER_LOTE);
		// as datas mudam em todas gerações por isso os testes nunca baterão
		String datas = linha.substring(191, 208);
		// disconsiderando as datas
		linha = linha.replace(datas, "");
		StringBuilder linhaEsperada = new StringBuilder();
		linhaEsperada
		        .append("10400011R0100030 20226666050001596337540000000000000003122463375400000000COND CONJ RESIDENCIAL SITIO VI                                                                                000001123005201630052016                                 ");

		String linhaEsperadaString = linhaEsperada.toString();
		// as datas mudam em todas gerações por isso os testes nunca baterão
		String datasLinhaEsperada = linhaEsperadaString.substring(191, 208);
		// disconsiderando as datas
		linhaEsperadaString = linhaEsperadaString.replace(datasLinhaEsperada, "");
		Assert.assertEquals(linhaEsperadaString, linha);
	}

	public void deveEstarComOSegmentoPConformeManual() throws IOException {
		File arquivo = remessa.getArquivoComoFile();
		LeitorArquivo leitor = new LeitorArquivo(arquivo);
		List<String> linhas = leitor.getLinhas();
		String linhaGuilherme = linhas.get(SEGMENTO_P_GUILHERME);
		String linhaJessica = linhas.get(SEGMENTO_P_JESSICA);
		// as datas mudam em todas gerações por isso os testes nunca baterão
		// disconsiderando as datas
		Date dataEmissao = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("ddMMyyyy");
		String dataEmissaoFormatada = dateFormatter.format(dataEmissao);
		StringBuilder linhaEsperadaGuilherme = new StringBuilder();
		linhaEsperadaGuilherme
		        .append("1040001300001P 0103122463375400000000000141000000000033261122010000003320    2006201600000000001200000000002N")
		        .append(dataEmissaoFormatada)
		        .append("10000000000000000000000412006201600000000000200000000000000000000000000000000010000003320              1302000090000000000 ");

		StringBuilder linhaEsperadaJessica = new StringBuilder();
		linhaEsperadaJessica
		        .append("1040001300004P 0103122463375400000000000141000000000033271122010000003321    2006201600000000001200000000002N")
		        .append(dataEmissaoFormatada)
		        .append("10000000000000000000000412006201600000000000200000000000000000000000000000000010000003321              1302000090000000000 ");

		Assert.assertEquals(linhaEsperadaGuilherme.toString(), linhaGuilherme);
		Assert.assertEquals(linhaEsperadaJessica.toString(), linhaJessica);
	}

	public void deveEstarComOSegmentoQConformeManual() throws IOException {
		File arquivo = remessa.getArquivoComoFile();
		LeitorArquivo leitor = new LeitorArquivo(arquivo);
		List<String> linhas = leitor.getLinhas();
		String linha = linhas.get(SEGMENTO_Q_GUILHERME);
		String linhaEsperada = "1040001300002Q 011000004514406503GUILHERME ANDRADE PINTO                 RUA FREI MIGUELINHO 299                 INHAMA         53625813IGARASSU       PE0000000000000000                                                                       ";
		Assert.assertEquals(linhaEsperada, linha);

		String linhaJessica = linhas.get(SEGMENTO_Q_JESSICA);
		String linhaEsperadaJessica = "1040001300005Q 011000038696508262JESSICA GABRIELE DOS SANTOS             RUA FREI MIGUELINHO 299                 INHAMA         53625813IGARASSU       PE0000000000000000                                                                       ";
		Assert.assertEquals(linhaEsperadaJessica, linhaJessica);
	}

	public void deveEstarComOSegmentoRConformeManual() throws IOException {
		File arquivo = remessa.getArquivoComoFile();
		LeitorArquivo leitor = new LeitorArquivo(arquivo);
		List<String> linhas = leitor.getLinhas();
		String linha = linhas.get(SEGMENTO_R_GUILHERME);
		String linhaEsperada = "1040001300003R 01000000000000000000000000000000000000000000000000120062016000000000000240          TAXA CONDOMINIAL JUNHO 2016             BLOCO D APT 105                                                                                      ";
		Assert.assertEquals(linhaEsperada, linha);

		String linhaJessica = linhas.get(SEGMENTO_R_JESSICA);
		String linhaEsperadaJessica = "1040001300006R 01000000000000000000000000000000000000000000000000120062016000000000000240          TAXA CONDOMINIAL JUNHO 2016             BLOCO D APT 105                                                                                      ";
		Assert.assertEquals(linhaEsperadaJessica, linhaJessica);
	}

	public void deveEstarComOTrailerLoteConformeManual() throws IOException {
		File arquivo = remessa.getArquivoComoFile();
		LeitorArquivo leitor = new LeitorArquivo(arquivo);
		String linha = leitor.getLinhas().get(TRAILER_LOTE);
		String linhaEsperada = "10400015         000008000002000000000000240000000000000000000000000000000000000000000000000                                                                                                                                                    ";
		Assert.assertEquals(linhaEsperada, linha);
	}

	public void deveEstarComOTrailerArquivoConformeManual() throws IOException {
		File arquivo = remessa.getArquivoComoFile();
		LeitorArquivo leitor = new LeitorArquivo(arquivo);
		String linha = leitor.getLinhas().get(TRAILER_ARQUIVO);
		String linhaEsperada = "10499999         000001000010                                                                                                                                                                                                                   ";
		Assert.assertEquals(linhaEsperada, linha);
	}
}
