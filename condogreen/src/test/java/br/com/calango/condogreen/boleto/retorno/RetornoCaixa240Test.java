package br.com.calango.condogreen.boleto.retorno;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.jrimum.texgit.FlatFile;
import org.jrimum.texgit.Record;
import org.jrimum.texgit.Texgit;

import br.com.calango.condogreen.boleto.domain.retorno.HeaderLoteRetorno;
import br.com.calango.condogreen.boleto.domain.retorno.HearderArquivoRetorno;

public class RetornoCaixa240Test {

	public void deveEstaComTodosOsCamposDoHeader() throws IOException {
		FlatFile<Record> ff = Texgit.createFlatFile("LayoutCEFCNAB240Retorno.txg.xml");
		DateFormat df = new SimpleDateFormat("dd/MM/yy");
		ff.read(FileUtils.readLines(new File("R1300287.RET"), "UTF-8"));

		HearderArquivoRetorno header = new HearderArquivoRetorno(ff);

		System.out.println("Empesa: " + header.getNomeEmpresa());
		System.out.println("CNPJ: " + header.getCnpj());
		System.out.println("Data de geração do arquivo: " + df.format(header.getDataGeracao()));

		HeaderLoteRetorno headerLote = new HeaderLoteRetorno(ff);

		System.out.println("Numero sequencial do retorno: " + headerLote.getNumeroRetorno());
		System.out.println("Mensagens:");
		System.out.println("MSG1: " + headerLote.getMensagem1());
		System.out.println("MSG2: " + headerLote.getMensagem2());

		System.out.println("-------------------------------Registros----------------------------------");

		Collection<Record> records = ff.getRecords("Arrecadacao-Segmento-T");
		for (Record r : records) {
			System.out.println("_______________________________");
			System.out.println("> Dados do pagamento");

			Date vencimento = r.getValue("Vencimento");
			BigDecimal valor = r.getValue("Valor");
			BigDecimal tarifas = r.getValue("ValorTarifaCustas");
			System.out.println("Valor Tarifa Custas: " + tarifas);
			String motivoOcorrencia = r.getValue("MotivoOcorrencia");
			System.out.println("Motivo Ocorrência: " + motivoOcorrencia);
			String movimento = r.getValue("CodigoMovimento");
			System.out.println("Movimento:" + movimento);

			// Arrecadacao-Segmento-U
			List<Record> arrecadacoes = r.getInnerRecords();
			for (Record record : arrecadacoes) {
				String nossoNumero = r.getValue("NossoNumero");
				BigDecimal valorMultaJuros = record.getValue("Valor-MultaJuros");
				Date dataPagamento = record.getValue("Data-Ocorrencia");
				BigDecimal valorEfetivoCreditado = record.getValue("Valor-LiquidoCreditado");

				if (movimento.equals("06")) {
					BigDecimal valorPago = record.getValue("Valor-Pago");
					System.out.println("Nosso Número: " + nossoNumero);
					System.out.println("Vencimento do título: " + df.format(vencimento));
					System.out.println("Valor do título: " + valor);
					System.out.println("Valor Pago: " + valorPago);
					System.out.println("Valor Multa Juros:" + valorMultaJuros);
					System.out.println("Data de pagamento:" + df.format(dataPagamento));
					System.out.println("Valor efetivo a ser creditado referente ao Título: " + valorEfetivoCreditado);
				}

			}

		}

		System.out.println("-------------------------------Fim do arquivo ----------------------------------");
		System.out.println("Qtd registros lote: " + ff.getRecord("TraillerLote").getValue("QtdRegistros"));
		System.out.println("Qtd registros totais: " + ff.getRecord("TraillerArquivo").getValue("QtdRegistros"));
	}
}
