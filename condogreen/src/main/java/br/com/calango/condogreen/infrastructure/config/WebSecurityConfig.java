package br.com.calango.condogreen.infrastructure.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// @formatter:off
		auth
			.eraseCredentials(true)
			.jdbcAuthentication()
			.dataSource(dataSource)
			.passwordEncoder(passwordEncoder())
			.usersByUsernameQuery("select nome, senha, ativo from conta where nome=?")
			.authoritiesByUsernameQuery("select p.conta_id, p.nome from perfil p inner join conta c on c.id = p.conta_id where c.nome=?");
		// @formatter:on
	}

	@Bean(name = "passwordEncoder")
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// @formatter:off
		http
			.authorizeRequests()
				.antMatchers("/public/**", "/user", "/", "/*.html").permitAll()
				.antMatchers("#formulario").permitAll()
				.antMatchers("#apartamentos").permitAll()
				.antMatchers("/blocos/**").permitAll()
				.antMatchers("/basic/**").hasAuthority("BASICO")
				.antMatchers("/admin/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.GET, "/**imageBase64**").permitAll()
				.antMatchers(HttpMethod.GET, "/downloads/boleto/**").permitAll()
				.antMatchers(HttpMethod.GET, "/reservas/**").authenticated()
				.antMatchers(HttpMethod.POST, "/reservas/**").authenticated()
				.antMatchers(HttpMethod.PUT, "/reservas/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.DELETE, "/reservas/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.POST, "/fale-conosco/submit").permitAll()
				.antMatchers(HttpMethod.GET, "/noticias/**").permitAll()
				.antMatchers(HttpMethod.POST, "/noticias/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.PUT, "/noticias/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.DELETE, "/noticias/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.GET, "/fornecedores/**").permitAll()
				.antMatchers(HttpMethod.POST, "/fornecedores/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.PUT, "/fornecedores/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.DELETE, "/fornecedores/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.GET, "/despesa-list/**").permitAll()
				.antMatchers(HttpMethod.POST, "/despesas/**").permitAll()
				.antMatchers(HttpMethod.PUT, "/despesas/**").permitAll()
				.antMatchers(HttpMethod.DELETE, "/despesas/**").permitAll()
				.antMatchers(HttpMethod.GET, "/documentos/**").permitAll()
				.antMatchers(HttpMethod.POST, "/documentos/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.PUT, "/documentos/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.DELETE, "/documentos/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.GET, "/categorias-documento/**").permitAll()
				.antMatchers(HttpMethod.GET, "/downloads/documento/**").permitAll()
				.antMatchers(HttpMethod.GET, "/moradores/**").permitAll()
				.antMatchers(HttpMethod.POST, "/moradores/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.PUT, "/moradores/**").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.DELETE, "/moradores/**").hasAuthority("ADMIN")
				.anyRequest().hasAuthority("ADMIN")
		.and()
			.httpBasic()
		.and()
			.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET"))
				.logoutSuccessUrl("/")
				.invalidateHttpSession(true)
				.deleteCookies("JSESSIONID", "principal", "XSRF-TOKEN")
		.and()
			.exceptionHandling()
				.accessDeniedPage("/403")
		.and()
			.csrf()
				.csrfTokenRepository(csrfTokenRepository())
				.and()
				.addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class);
		// @formatter:on
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		// @formatter:off
		web.ignoring().antMatchers("/app.js", "/bower_components/**", "/**/*.js", "/*.css", "/**/*.html");
//		web.ignoring().antMatchers("/app.js",
//				"/**/*.js",
//				"/js/**",
//				"/css/**",
//				"/images/**",
//				"/index.html",
//				"/login.html",
//				"/footer.tpl.html",
//				"/header.tpl.html",
//				"/portal-sitio-viver.html");
		// @formatter:on
	}

	private CsrfTokenRepository csrfTokenRepository() {
		HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
		repository.setHeaderName("X-XSRF-TOKEN");
		return repository;
	}

}
