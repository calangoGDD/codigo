package br.com.calango.condogreen.boleto.aplication.handler;

import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import br.com.calango.condogreen.boleto.domain.ConfiguradorBoleto;

@Component
@RepositoryEventHandler(ConfiguradorBoleto.class)
public class ConfiguradorBoletoEventHandler {

	// private Validator validator;

	// @Autowired
	// public ConfiguradorBoletoEventHandler(Validator validator) {
	// this.validator = validator;
	// }

	@HandleBeforeCreate
	public void validarCreate(ConfiguradorBoleto c) {
		// validator.validate(c, ErrorState.NONE);
		// if (!violations.isEmpty()) {
		// // throw new ConstraintViolationException(violations);
		// }
	}
}
