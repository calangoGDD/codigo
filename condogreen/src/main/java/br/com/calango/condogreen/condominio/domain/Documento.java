package br.com.calango.condogreen.condominio.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

@Entity
@Table(name = "documento")
public class Documento extends AbstractPersistable<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotBlank
	@NotNull
	@Column(name = "nome", length = 100)
	private String nome;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@Column(name = "data_criacao")
	private LocalDateTime dataCriacao;
	
	@Column(name = "versao")
	private long versao;
	
	@Lob
	@JsonIgnore
	@Column(name = "arquivo")
	private byte[] arquivo;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = DocumentoCategoria.FK_ID_CATEGORIA)
	private DocumentoCategoria documentoCategoria;
	
	@NotBlank
	@NotNull
	@Column(name = "descricao", length = 100)
	private String descricao;

	@Transient
	private Long categoriaId;
	
	public Documento() {	
	
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}


	public void setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
	}


	public long getVersao() {
		return versao;
	}


	public void setVersao(long versao) {
		this.versao = versao;
	}

	public byte[] getArquivo() {
		return arquivo;
	}

	public void setArquivo(byte[] arquivo) {
		this.arquivo = arquivo;
	}
	
	public String getNomeCategoriaDocumento() {
		return this.documentoCategoria == null ? "" : this.documentoCategoria.getNome();
	}


	public DocumentoCategoria getDocumentoCategoria() {
		return documentoCategoria;
	}


	public void setDocumentoCategoria(DocumentoCategoria documentoCategoria) {
		this.documentoCategoria = documentoCategoria;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public Long getCategoriaId() {
		return this.categoriaId;
	}


	public void setCategoriaId(Long documentoCategoriaId) {
		this.categoriaId = documentoCategoriaId;
	}
	
	
	
}