package br.com.calango.condogreen.condominio.aplication.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.calango.condogreen.condominio.aplication.repository.CategoriaDocumentoRepository;
import br.com.calango.condogreen.condominio.aplication.repository.DocumentoRepository;
import br.com.calango.condogreen.condominio.domain.Documento;
import br.com.calango.condogreen.condominio.domain.DocumentoCategoria;

@Service
public class DocumentoService {

	private DocumentoRepository documentoRepository;
	private CategoriaDocumentoRepository categoriaDocumentoRepository;

	@Autowired
	public DocumentoService(DocumentoRepository documentoRepository, CategoriaDocumentoRepository categoriaDocumentoRepository) {
		this.documentoRepository = documentoRepository;
		this.categoriaDocumentoRepository = categoriaDocumentoRepository;
	}
	
	public Page<Documento> findAll(Pageable pageable) {
		return documentoRepository.findAll(pageable);
	}

	public void save(String nomeDocumento, byte[] arquivo, Documento documento) {
		documento.setDataCriacao(LocalDateTime.now());
		documento.setArquivo(arquivo);
		documento.setNome(nomeDocumento);
		documento.setDocumentoCategoria(categoriaDocumentoRepository.findOne(documento.getCategoriaId()));
		documentoRepository.saveAndFlush(documento);
	}


	public void delete(long id) {
		documentoRepository.delete(id);
	}

	public Documento findById(long id) {
		return documentoRepository.findOne(id);
	}

	public List<DocumentoCategoria> findCategoriasDocumento() {
		return categoriaDocumentoRepository.findAll();
	}

	public List<Documento> documentsByCategoria(Long id) {
		return documentoRepository.findByDocumentoCategoriaId(id);
	}

}
