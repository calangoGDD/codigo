package br.com.calango.condogreen.condominio.aplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.condominio.domain.Morador;

@RepositoryRestResource
public interface MoradorRepository extends JpaRepository<Morador, Long> {

	Morador findByPessoaContaNome(@Param(value = "nome") String nome);

}
