package br.com.calango.condogreen.core.aplication;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class EmailEnviadorImpl implements Enviador {

	private JavaMailSender mailSender;

	@Autowired
	public EmailEnviadorImpl(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	@Async
	@Override
	public void enviar(String to, String from, String subject, String text, byte[] attachment) throws MailException, MessagingException, IOException {
		mailSender.send(criarMensagemComAnexo(to, from, subject, text, attachment));
	}

	@Async
	@Override
	public void enviar(String to, String from, String subject, String text) {
		mailSender.send(criarMensagem(to, from, subject, text));
	}
	
	@Async
	@Override
	public void enviarWithReplyTo(String to, String replyTo, String from, String subject, String text) {
		mailSender.send(criarMensagem(to, from, replyTo, subject, text));
	}

	private SimpleMailMessage criarMensagem(String to, String from, String replyTo, String subject, String text) {
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(to);
		msg.setFrom(from);
		msg.setSubject(subject);
		msg.setText(text);
		msg.setReplyTo(replyTo);
		return msg;
	}

	private SimpleMailMessage criarMensagem(String to, String from, String subject, String text) {
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(to);
		msg.setFrom(from);
		msg.setSubject(subject);
		msg.setText(text);
		return msg;
	}

	private MimeMessage criarMensagemComAnexo(String to, String from, String subject, String text, byte[] arquivo) throws MessagingException,
			IOException {
		MimeMessage msg = mailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		Resource resource = new ByteArrayResource(arquivo);
		helper.addAttachment("boleto.pdf", resource);
		helper.setTo(to);
		helper.setFrom(from);
		helper.setSubject(subject);
		helper.setText(text);

		return msg;
	}
}
