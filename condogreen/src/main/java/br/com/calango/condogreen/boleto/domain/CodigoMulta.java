package br.com.calango.condogreen.boleto.domain;

/**
 * Código adotado pela FEBRABAN para identificação do critério de pagamento de
 * pena pecuniária, a ser aplicada pelo atraso do pagamento do Título. Pode ser:
 * ‘0’ = Sem Multa '1' = Valor Fixo '2' = Percentual
 * 
 * @author Guilherme Andrade
 *
 */
public enum CodigoMulta {

	SEM_MULTA(0), VALOR_FIXO(1), PERCENTUAL(2);

	/**
	 * Codigo do tipo de pagamento de juros mora.
	 */
	private int codigo;

	CodigoMulta(int codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

}
