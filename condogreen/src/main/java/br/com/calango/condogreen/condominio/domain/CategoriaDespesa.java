package br.com.calango.condogreen.condominio.domain;

/**
 * Categorias para Despesa.
 * 
 * @author Danyllo Araujo
 * @since 15/12/2016
 *
 */
public enum CategoriaDespesa {
	
	MANUTENCAO("man", "Manutenção"), 
	FUNCIONARIO("fun", "Funcionário"),
	SERVICO("ser", "Serviço"),
	OUTROS("out", "Outros");
	
	private String codigo;
	private String nome;
	
	private CategoriaDespesa(String codigo, String nome) {
		this.setCodigo(codigo);
		this.setNome(nome);
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
