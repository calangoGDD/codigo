package br.com.calango.condogreen.condominio.aplication.service;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import br.com.calango.condogreen.condominio.aplication.repository.MoradorRepository;
import br.com.calango.condogreen.condominio.aplication.repository.ReservaRepository;
import br.com.calango.condogreen.condominio.domain.Morador;
import br.com.calango.condogreen.condominio.domain.Reserva;
import br.com.calango.condogreen.condominio.domain.validacao.ValidadorReserva;

/**
 * Esta classe representa um serviço de domínio, responsável pelo gerenciamento de reservas.
 * 
 * @author Guilherme Andrade
 *
 */
@Service
public class ReservaService {

	@Autowired
	private ApplicationContext appContext;


	@Autowired
	private ReservaRepository reservaRepository;
	
	@Autowired
	private MoradorRepository moradorRepository;
	
	
	public void reservar(Reserva reserva, Principal principal){
		ValidadorReserva validadorReserva = (ValidadorReserva) appContext.getBean("validadorReserva");
		validadorReserva.checkBrokenRules(reserva);
		
		Morador morador = moradorRepository.findByPessoaContaNome(principal.getName());
		reserva.setMorador(morador);
		reserva.setSituacao(Reserva.AGUARDANDO_APROVACAO);
		reservaRepository.save(reserva);
	}
	
	public List<Reserva> reservas() throws IOException {
		return reservaRepository.findAll();
	}

	public void remover(long reservaId) {
		reservaRepository.delete(reservaId);
	}

	public void atualizar(Reserva reserva, Principal principal) {
		Morador morador = moradorRepository.findByPessoaContaNome(principal.getName());
		reserva.setMorador(morador);
		reservaRepository.saveAndFlush(reserva);
	}

	public Reserva findById(long id) {
		return reservaRepository.findOne(id);
	}

	public void aprovar(long id) {
		Reserva reserva = reservaRepository.findOne(id);
		reserva.setSituacao(Reserva.APROVADO);
		reservaRepository.saveAndFlush(reserva);
	}
}
