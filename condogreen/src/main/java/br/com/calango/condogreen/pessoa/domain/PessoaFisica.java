package br.com.calango.condogreen.pessoa.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

/**
 * Classe que representa as pessoas físicas.
 * 
 * @author Danyllo Araujo
 *
 */
@Entity
@Table(name = "pessoa_fisica")
@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
public class PessoaFisica extends Pessoa implements IPessoa {

	public static final String TIPO_INSCRICAO_PESSOA_FISICA = "1";
	public static final String FK_ID = "pessoa_fisica_id";
	public static final String FK_ID_SINDICO = "sindico_id";

	@CPF
	@Column(name = "cpf", length = 11)
	private String cpf;

	@JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	@Column(name = "data_nascimento")
	private LocalDate dataNascimento;

	@Override
	public String getTipoInscricao() {
		return TIPO_INSCRICAO_PESSOA_FISICA;
	}

	@Override
	public String getNumeroInscricao() {
		return getCpf();
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		PessoaFisica other = (PessoaFisica) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
		builder.append("INSERT INTO conta").append("(login, senha)").append("VALUES").append("('").append(getCpf())
		        .append("', '").append(bCrypt.encode(getNome().split(" ")[0])).append("',").append("1").append(");").append("\n");

		builder.append("INSERT INTO perfil").append("(login, nome)").append("VALUES ('").append(getCpf()).append("', 'BASICO');\n");

		builder.append("INSERT INTO `endereco` (`bairro`,`cep`,`cidade`,`complemento`,`numero`,`pais`,`rua`,`sufixo_cep`,`uf`,`atualizacao`) ")
		        .append("VALUES ('").append(getBairro().trim()).append("','").append(getCep().trim()).append("','").append(getCidade().trim())
		        .append("','");
		String complemento = getComplemento();
		if (complemento != null) {
			builder.append(complemento.trim()).append("','");
		} else {
			builder.append("','");
		}
		String numero = getNumero();
		if (numero != null) {
			builder.append(numero);
		}

		builder.append("','Brasil','").append(getRua().trim()).append("','").append(getSufixoCep().trim()).append("','").append(getUf().trim())
		        .append("',now());").append("\n");

		builder.append("insert into pessoa (endereco_id, nome,atualizacao, usuario_id)").append("values((select max(id) from endereco),'")
		        .append(getNome().trim()).append("', now(), (select max(id) from usuario));").append("\n");

		builder.append("insert into pessoa_fisica (id,cpf, data_nascimento,atualizacao) ").append(" values((select max(id) from pessoa), '")
		        .append(getCpf()).append("',null, now());").append("\n");

		builder.append("insert into morador (pessoa_id, responsavel_boleto)").append("values ((select max(id) from pessoa), 1); \n");

		return builder.toString();
	}

	public static void main(String[] args) {
		BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
		System.out.println(bCrypt.encode("admin"));
	}

	/**
	 * @see br.com.calango.condogreen.pessoa.domain.IPessoa#accept(br.com.calango.condogreen.pessoa.domain.VisitorPessoa)
	 */
	@Override
	public void accept(VisitorPessoa visitor) {
		visitor.visit(this);
	}

}
