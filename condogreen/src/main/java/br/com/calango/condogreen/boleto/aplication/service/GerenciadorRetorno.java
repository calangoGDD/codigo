package br.com.calango.condogreen.boleto.aplication.service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.jrimum.texgit.FlatFile;
import org.jrimum.texgit.Record;
import org.jrimum.texgit.Texgit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.calango.condogreen.boleto.aplication.repository.BoletoRepository;
import br.com.calango.condogreen.boleto.aplication.repository.RetornoRepository;
import br.com.calango.condogreen.boleto.aplication.repository.SituacaoBoletoRepository;
import br.com.calango.condogreen.boleto.domain.InfoBoleto;
import br.com.calango.condogreen.boleto.domain.InformacaoRetorno;
import br.com.calango.condogreen.boleto.domain.retorno.HeaderLoteRetorno;
import br.com.calango.condogreen.boleto.domain.retorno.Retorno;
import br.com.calango.condogreen.core.util.Conversor;

@Component
public class GerenciadorRetorno {

	private static final String UTF_8 = "UTF-8";

	private static final String LAYOUT_CEFCNAB240_RETORNO_TXG_XML = "./layoutsBancos/LayoutCEFCNAB240Retorno.txg.xml";

	private static final String LAYOUT_CEFCNAB240_MIGRACAO_RETORNO_TXG_XML = "./layoutsBancos/LayoutCEFCNAB240MigracaoRetorno.txg.xml";

	private BoletoRepository boletoRepository;

	private SituacaoBoletoRepository situacaoBoletoRepository;

	private RetornoRepository retornoRepository;

	@Autowired
	public GerenciadorRetorno(BoletoRepository boletoRepository, SituacaoBoletoRepository situacaoBoletoRepository,
			RetornoRepository retornoRepository) {
		this.boletoRepository = boletoRepository;
		this.situacaoBoletoRepository = situacaoBoletoRepository;
		this.retornoRepository = retornoRepository;
	}

	/**
	 * Método responsável por importar retorno.
	 * @param fileName
	 * @param dados
	 * @throws IOException
	 */
	public void importarRetorno(String fileName, byte[] dados) throws IOException {

		File file = File.createTempFile(fileName, ".tmp");
		Files.write(file.toPath(), dados);

		ClassLoader classLoader = this.getClass().getClassLoader();
		File layout = FileUtils.toFile(classLoader.getResource(LAYOUT_CEFCNAB240_RETORNO_TXG_XML));
		FlatFile<Record> ff = Texgit.createFlatFile(layout);
		ff.read(Files.readAllLines(file.toPath(), Charset.forName(UTF_8)));

		HeaderLoteRetorno headerLote = new HeaderLoteRetorno(ff);

		Retorno retorno = new Retorno();
		retorno.setNumeroRetorno(Long.parseLong(headerLote.getNumeroRetorno()));
		retorno.setArquivo(dados);
		retorno.setDataImportacao(LocalDate.now());

		Collection<Record> registrosSegmentoT = ff.getRecords("Arrecadacao-Segmento-T");
		BigDecimal tarifasTotal = BigDecimal.ZERO;

		for (Record segmentoT : registrosSegmentoT) {
			tarifasTotal = processarSegmentosTeU(tarifasTotal, segmentoT, retorno);
		}
		retornoRepository.save(retorno);
	}

	/**
	 * Método usado apenas para migração inicial dos dados de retorno.
	 * 
	 * @param fileName
	 * @param dados
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void gerarRetornoParaMigracao() throws IOException, InterruptedException {

		ClassLoader classLoader = this.getClass().getClassLoader();
		File file =  FileUtils.toFile(classLoader.getResource("./migracoes/retornos.txt"));
		System.out.println("Início importação:");
		File layout = FileUtils.toFile(classLoader.getResource(LAYOUT_CEFCNAB240_MIGRACAO_RETORNO_TXG_XML));
		FlatFile<Record> flatFile = Texgit.createFlatFile(layout);
		flatFile.read(Files.readAllLines(file.toPath(), Charset.forName(UTF_8)));

		processarRetorno(flatFile);
	}

	/**
	 * Método responsável por processar o arquivo retorno.
	 * @param flatFile
	 */
	private void processarRetorno(FlatFile<Record> flatFile) {
		Collection<Record> registrosSegmentoT = flatFile.getRecords("Arrecadacao-Segmento-T");
		BigDecimal tarifasTotal = BigDecimal.ZERO;

		Map<Long, Retorno> mapRetornos = new HashMap<>();
		for (Record segmentoT : registrosSegmentoT) {

			long numeroRetorno = Long.parseLong(segmentoT.getValue("numeroRetorno"));
			Retorno retorno = mapRetornos.get(numeroRetorno);
			if (retorno == null) {
				retorno = new Retorno();
				retorno.setNumeroRetorno(numeroRetorno);
				retorno.setArquivo(null);
				retorno.setDataImportacao(LocalDate.now());
				retorno.setValorTotal(BigDecimal.ZERO);
				retorno.setQuantidadeRegistros(0L);
				mapRetornos.put(numeroRetorno, retorno);
			}

			System.out.println("Processando Retorno: " + retorno.getNumeroRetorno());
			
			tarifasTotal = processarSegmentosTeU(tarifasTotal, segmentoT, retorno);
		}
		
		System.out.println("Iniciar Salvar");
		retornoRepository.save(mapRetornos.values());
		System.out.println("Fim Salvar");
	}

	/**
	 * Métodos responsáveis por processar segmentos T e U.
	 * @param tarifasTotal
	 * @param segmentoT
	 * @param retorno
	 * @return
	 */
	private BigDecimal processarSegmentosTeU(BigDecimal tarifasTotal, Record segmentoT, Retorno retorno) {
		String nossoNumero = segmentoT.getValue("NossoNumero");

		InfoBoleto infoBoleto = boletoRepository.findByNossoNumero(nossoNumero);

		InformacaoRetorno informacaoRetorno = new InformacaoRetorno();

		String codigoMovimento = segmentoT.getValue("CodigoMovimento");

		if (infoBoleto != null) {
			infoBoleto.setSituacaoBoleto(situacaoBoletoRepository.findByCodigo(codigoMovimento));
			informacaoRetorno.setInfoBoleto(infoBoleto);
		}

		informacaoRetorno.setCodigoMovimento(codigoMovimento);

		retorno.addInformacaoRetorno(informacaoRetorno);

		String motivoOcorrencia = segmentoT.getValue("MotivoOcorrencia");
		informacaoRetorno.setMotivoOcorrencia(motivoOcorrencia);

		BigDecimal tarifas = segmentoT.getValue("ValorTarifaCustas");
		tarifasTotal = tarifasTotal.add(tarifas);
		informacaoRetorno.setValorTarifa(tarifas);

		// Arrecadacao-Segmento-U
		List<Record> segmentosU = segmentoT.getInnerRecords();

		if (segmentosU != null) {
			for (Record segmentoU : segmentosU) {
				Date dataOcorrencia = segmentoU.getValue("Data-Ocorrencia");
				informacaoRetorno.setDataOcorrencia(Conversor.deDateParaLocalDate(dataOcorrencia));

				// Liquidação
				if (codigoMovimento.equals("06")) {

					Date dataEfetivacaoCredito = segmentoU.getValue("Data-Efetivacao");
					informacaoRetorno.setDataEfetivacaoCredito(Conversor.deDateParaLocalDate(dataEfetivacaoCredito));

					BigDecimal valorEfetivoCreditado = segmentoU.getValue("Valor-LiquidoCreditado");
					informacaoRetorno.setValorEfetivoCreditado(valorEfetivoCreditado);

					BigDecimal valorMultaJuros = segmentoU.getValue("Valor-MultaJuros");
					informacaoRetorno.setValorMultaJuros(valorMultaJuros);

					BigDecimal valorPago = segmentoU.getValue("Valor-Pago");
					informacaoRetorno.setValorPago(valorPago);

					retorno.setValorTotal(retorno.getValorTotal().add(valorPago));
					
				} else if (codigoMovimento.equals("02")) {// Entrada Confirmada
					// TODO Notificar o sacado por e-mail
				}
				informacaoRetorno.setOrdem(Integer.valueOf(segmentoU.getValue("ordemRetorno").toString().trim()));
				
				retorno.setQuantidadeRegistros(retorno.getQuantidadeRegistros() + 1);
			}
		}
		return tarifasTotal;
	}

}
