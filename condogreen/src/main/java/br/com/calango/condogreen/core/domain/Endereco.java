package br.com.calango.condogreen.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.jrimum.domkee.comum.pessoa.endereco.UnidadeFederativa;

/**
 * Rempresenta a classe de endereços do sistema.
 * 
 * @author Guilherme Andrade
 *
 */
@Entity
@Table(name = "endereco")
public class Endereco extends BasicEntity implements Cloneable {

	public static final String FK_ID = "endereco_id";

	@Column(name = "rua", length = 40)
	private String rua;

	@Column(name = "numero", length = 15)
	private String numero;

	@Column(name = "complemento", length = 100)
	private String complemento;

	@Column(name = "bairro", length = 15)
	private String bairro;

	@Column(name = "cep", length = 5)
	private String cep;

	@Column(name = "sufixo_cep", length = 3)
	private String sufixoCep;

	@Column(name = "cidade", length = 15)
	private String cidade;

	@Column(name = "uf", length = 2)
	@Enumerated(EnumType.STRING)
	private UnidadeFederativa uf;

	@Column(name = "pais", length = 10)
	private String pais;

	/**
	 * @return the rua
	 */
	public String getRua() {
		return rua;
	}

	/**
	 * @param rua
	 *            the rua to set
	 */
	public void setRua(String rua) {
		this.rua = rua;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero
	 *            the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the complemento
	 */
	public String getComplemento() {
		return complemento;
	}

	/**
	 * @param complemento
	 *            the complemento to set
	 */
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	/**
	 * @return the bairro
	 */
	public String getBairro() {
		return bairro;
	}

	/**
	 * @param bairro
	 *            the bairro to set
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	/**
	 * @return the cep
	 */
	public String getCep() {
		return cep;
	}

	/**
	 * @param cep
	 *            the cep to set
	 */
	public void setCep(String cep) {
		this.cep = cep;
	}

	/**
	 * @return the cidade
	 */
	public String getCidade() {
		return cidade;
	}

	/**
	 * @param cidade
	 *            the cidade to set
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	/**
	 * @return the sufixoCep
	 */
	public String getSufixoCep() {
		return sufixoCep;
	}

	/**
	 * @param sufixoCep
	 *            the sufixoCep to set
	 */
	public void setSufixoCep(String sufixoCep) {
		this.sufixoCep = sufixoCep;
	}

	/**
	 * @return the pais
	 */
	public String getPais() {
		return pais;
	}

	/**
	 * @param pais
	 *            the pais to set
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}

	/**
	 * @return the uf
	 */
	public UnidadeFederativa getUf() {
		return uf;
	}

	/**
	 * @param uf
	 *            the uf to set
	 */
	public void setUf(UnidadeFederativa uf) {
		this.uf = uf;
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Endereco clone() {
		Endereco clone = null;
		
		try {
			clone = (Endereco) super.clone();
			clone.setId(null);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return clone;
	}
}
