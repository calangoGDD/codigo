package br.com.calango.condogreen.condominio.aplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.condominio.domain.Condominio;

@RepositoryRestResource(collectionResourceRel = "condominios", path = "condominios")
public interface CondominioRepository extends JpaRepository<Condominio, Long> {

}
