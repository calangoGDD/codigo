package br.com.calango.condogreen.boleto.aplication.resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.calango.condogreen.boleto.aplication.repository.RemessaRepository;
import br.com.calango.condogreen.boleto.aplication.service.GeradorBoleto;
import br.com.calango.condogreen.boleto.domain.remessa.Remessa;
import br.com.calango.condogreen.core.aplication.Notificador;
import br.com.calango.condogreen.infrastructure.Filtro;
import br.com.calango.condogreen.infrastructure.exception.BusinessException;

@RepositoryRestController
@RequestMapping(path = "/remessas")
public class RemessaResource {

	private GeradorBoleto geradorBoleto;
	private RemessaRepository remessaRepository;
	private PagedResourcesAssembler<Remessa> pageAssembler;
	private Notificador notificador;

	@Autowired
	public RemessaResource(GeradorBoleto geradorBoleto, RemessaRepository remessaRepository, PagedResourcesAssembler<Remessa> pageAssembler,
			Notificador notificador) {
		this.geradorBoleto = geradorBoleto;
		this.remessaRepository = remessaRepository;
		this.pageAssembler = pageAssembler;
		this.notificador = notificador;
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "gerar")
	public void gerarRemessa() throws IOException, BusinessException {
		geradorBoleto.gerarRemessa();
	}

	@RequestMapping(path = "/{numeroRemessa}/notificar", method = RequestMethod.GET)
	@ResponseBody
	public PersistentEntityResource notificar(@PathVariable(value = "numeroRemessa") Long numeroRemessa,
			PersistentEntityResourceAssembler resourceAssembler) throws MailException, MessagingException, IOException {
		Filtro filtro = new Filtro();
		filtro.put("numeroRemessa", numeroRemessa);
		RemessaFilterSpecification spec = new RemessaFilterSpecification(filtro);
		Remessa remessa = remessaRepository.findOne(spec);

		notificador.notificarBoletosDisponiveis(remessa);

		return resourceAssembler.toResource(remessa);
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public PagedResources<Resource<Remessa>> search(@RequestParam(value = "nome", required = false) String nome, Pageable pageable) {
		Filtro filtro = new Filtro();
		filtro.put("nome", nome);
		RemessaFilterSpecification spec = new RemessaFilterSpecification(filtro);

		Page<Remessa> page = remessaRepository.findAll(spec, pageable);
		return pageAssembler.toResource(page);
	}

	private class RemessaFilterSpecification implements Specification<Remessa> {

		private Filtro filtro;

		public RemessaFilterSpecification(Filtro filtro) {
			this.filtro = filtro;
		}

		@Override
		public Predicate toPredicate(Root<Remessa> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
			List<Predicate> predicates = new ArrayList<>();

			if (filtro.get("nome") != null) {
				predicates.add(cb.like(cb.lower(root.get("nome")), "%" + filtro.get("nome").toString().toLowerCase() + "%"));
			}

			if (filtro.get("numeroRemessa") != null) {
				predicates.add(cb.equal(root.get("numeroRemessa"), filtro.get("numeroRemessa")));
			}

			return cb.and(predicates.toArray(new Predicate[0]));
		}
	}
}
