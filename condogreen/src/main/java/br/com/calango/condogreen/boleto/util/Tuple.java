package br.com.calango.condogreen.boleto.util;

/**
 * 
 * Esta classe é apenas utilizada para guardar informações que possuam uma chave
 * e valor.
 * 
 * @author Guilherme Andrade
 *
 * @param <S1>
 * @param <S2>
 */
public class Tuple<S1, S2> {

	private S1 key;

	private S2 value;

	/**
	 * @return the key
	 */
	public S1 getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(S1 key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public S2 getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(S2 value) {
		this.value = value;
	}

}
