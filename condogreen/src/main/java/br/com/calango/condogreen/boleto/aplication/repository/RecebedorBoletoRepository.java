package br.com.calango.condogreen.boleto.aplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.boleto.domain.RecebedorBoleto;

@RepositoryRestResource(collectionResourceRel = "recebedoresboleto", path = "recebedoresboleto")
public interface RecebedorBoletoRepository extends JpaRepository<RecebedorBoleto, Long> {

	/**
	 * 
	 * @return A quantidade de apartamentos sem moradores responsáveis por pagar
	 *         boleto
	 */
	@Query(value = " select count(ap.id) from apartamento ap where not exists ( select apm.apartamento_id from apartamento_morador apm inner join morador m on m.id = apm.morador_id where apm.apartamento_id = ap.id and m.responsavel_boleto = 1)", nativeQuery = true)
	Long countRecebedoresBoletoSemResponsavelPorPagar();
}
