package br.com.calango.condogreen.boleto.domain.validacao;

import org.springframework.stereotype.Component;

import br.com.calango.condogreen.boleto.domain.remessa.Remessa;
import br.com.calango.condogreen.infrastructure.specification.ISpecification;

@Component("deveExistirBoletosParaRemessa")
public class DeveExistirBoletosParaRemessa implements ISpecification<Remessa> {

	/**
	 * @see br.com.calango.condogreen.infrastructure.specification.ISpecification#isSatisfiedBy(java.lang.Object)
	 */
	@Override
	public boolean isSatisfiedBy(Remessa remessa) {
		return remessa.getBoletos().size() > 0;
	}

}
