package br.com.calango.condogreen.condominio.aplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.condominio.domain.Documento;

@RepositoryRestResource(collectionResourceRel = "repdocumentos", path = "repdocumentos")
public interface DocumentoRepository extends JpaRepository<Documento, Long>,  JpaSpecificationExecutor<Documento> {

	List<Documento> findByDocumentoCategoriaId(@Param(value = "id") Long id);


}

