package br.com.calango.condogreen.financeiro.aplication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.calango.condogreen.financeiro.aplication.repository.DespesaRepository;
import br.com.calango.condogreen.financeiro.domain.Despesa;

/**
 * Esta classe agrupa os serviços de Despesas do condomínio.
 * 
 * @author Danyllo Araujo
 * @since 15/12/2016
 * 
 */
@Service
public class DespesaService {
	
	private DespesaRepository despesaRepository;
	
	@Autowired
	public DespesaService(DespesaRepository despesaRepository) {
		this.despesaRepository = despesaRepository;
	}
	
	public Page<Despesa> findAll(Pageable pageable) {
		return despesaRepository.findAll(pageable);
	}

	public Despesa findById(long id) {
		return despesaRepository.findOne(id);
	}

	public void save(Despesa despesa) {
		despesaRepository.saveAndFlush(despesa);
	}

	public void delete(long id) {
		despesaRepository.delete(id);
	}

	public Page<Despesa> findByNameContaining(String nome, Pageable pageable) {
		return despesaRepository.findByNomeContaining(nome, pageable);
	}
}
