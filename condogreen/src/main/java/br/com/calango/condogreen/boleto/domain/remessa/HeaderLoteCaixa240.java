package br.com.calango.condogreen.boleto.domain.remessa;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.jrimum.texgit.Record;

import br.com.calango.condogreen.boleto.util.RemessaUtil;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

public class HeaderLoteCaixa240 {

	private Record header;

	public HeaderLoteCaixa240(Record header) {
		this.header = header;
	}

	public String getCodBanco() {
		return header.getValue("locodigoBanco");
	}

	public String getLote() {
		return header.getValue("loloteServico");
	}

	public String getTipoRegistro() {
		return header.getValue("loTipoRegistro");
	}

	public void setCodCedente(String loempCodCedente) {
		header.setValue("loempCodCedente", loempCodCedente);
	}

	public void setUsoExclusivo9() {
		header.setValue("lousoExclusivo9", StringUtils.repeat("0", 14));
	}

	public void setCodConvenio(String codBeneficiario) {
		header.setValue("loempCodConvenio", codBeneficiario);
	}

	public void setCodModeloPersonalizado() {
		header.setValue("loempCodModPer", StringUtils.repeat("0", 7));
	}

	public void setUsoExclusivo10() {
		header.setValue("lousoExclusivo10", StringUtils.repeat("0", 1));
	}

	public void setEmpNome(String loempNome) {
		header.setValue("loempNome", RemessaUtil.retiraCaracteresEspeciais(StringUtils.substring(loempNome, 0, 30)));
	}

	public void setGeralMen1() {
		header.setValue("logeralMen1", StringUtils.repeat(" ", 40));
	}

	public void setGeralMen2() {
		header.setValue("logeralMen2", StringUtils.repeat(" ", 40));
	}

	public void setNumRemRet(long nrRemessa) {
		header.setValue("lonumRemRet", nrRemessa);
	}

	public void setDataGravacaoRet() {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("ddMMyyyy");
		header.setValue("lodataGravacaoRet", dateFormatter.format(new Date()));
	}

	public void setDataCredito() {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("ddMMyyyy");
		header.setValue("lodataCredito", dateFormatter.format(new Date()));
	}

	public void setUsoExclusivo11() {
		header.setValue("lousoExclusivo11", StringUtils.repeat(" ", 33));
	}

	public Record getHeader() {
		return this.header;
	}

	public Character getTipoOperacao() {
		return header.getValue("lotipoOperacao");
	}

	public Integer getTipoServico() {
		return header.getValue("lotipoServico");
	}

	public int getUsoExclusivo7() {
		return header.getValue("lousoExclusivo7");
	}

	public int getLayoutLote() {
		return header.getValue("lolayoutLote");
	}

	public Character getUsoExclusivo8() {
		return header.getValue("lousoExclusivo8");
	}

	// daqui para baixo tem no headerArquivo mudando apaenas o prefixo

	public void setTipoInscricao(String tipoInscricao) {
		header.setValue("loempTipoInscricao", tipoInscricao);
	}

	public void setNumInscricao(String numInscricao) {
		header.setValue("loempNumInscricao", RemessaUtil.retiraCaracteresEspeciais(numInscricao));
	}

	public void setAgencia(String agenciaConta) {
		header.setValue("loempAgenciaConta", agenciaConta);
	}

	public void setAgenciaVerificador(int loempAgenciaVerificador) {
		header.setValue("loempAgenciaVerificador", loempAgenciaVerificador);
	}

	public String getCodConvenio() {
		return header.getValue("loempCodConvenio");
	}

	public Integer getAgenciaVerificador() {
		return header.getValue("loempAgenciaVerificador");
	}

	public String getCodModeloPersonalizado() {
		return header.getValue("loempCodModPer");
	}

	public String getTipoInscricao() {
		return header.getValue("loempTipoInscricao");
	}

	public String getNumInscricao() {
		return header.getValue("loempNumInscricao");
	}

	public String getCodCedente() {
		return header.getValue("loempCodCedente");
	}

	public String getUsoExclusivo9() {
		return header.getValue("lousoExclusivo9");
	}

	public String getAgencia() {
		return header.getValue("loempAgenciaConta");
	}

	public String getUsoExclusivo10() {
		return header.getValue("lousoExclusivo10");
	}

	public String getEmpNome() {
		return header.getValue("loempNome");
	}

	public Record contruirHeaderLote(PessoaJuridica empresa, long nrRemessa) {

		System.out.println("************* header do lote ****************");
		// tipo de inscrição CPF ou CNPJ
		this.setTipoInscricao(empresa.getTipoInscricao());
		this.setNumInscricao(empresa.getNumeroInscricao());
		this.setCodCedente(empresa.getCodigoBeneficiario());
		this.setUsoExclusivo9();
		this.setAgencia(empresa.getAgencia());
		this.setAgenciaVerificador(empresa.getDigitoAgencia());
		this.setCodConvenio(empresa.getCodigoBeneficiario());

		// Código Modelo Personalizado - Código fornecido pela CAIXA utilizado
		// somente quando o modelo do bloqueto for personalizado
		this.setCodModeloPersonalizado();
		this.setUsoExclusivo10();
		this.setEmpNome(empresa.getNome());
		this.setGeralMen2();
		this.setNumRemRet(nrRemessa);
		this.setDataGravacaoRet();
		this.setDataCredito();
		this.setUsoExclusivo11();

		return this.getHeader();
	}
}
