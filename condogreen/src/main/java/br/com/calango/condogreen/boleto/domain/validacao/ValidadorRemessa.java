package br.com.calango.condogreen.boleto.domain.validacao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.calango.condogreen.boleto.domain.remessa.Remessa;
import br.com.calango.condogreen.infrastructure.exception.AbstractValidator;
import br.com.calango.condogreen.infrastructure.exception.BusinessException;

@Component("validadorRemessa")
public class ValidadorRemessa extends AbstractValidator<Remessa> {

	@Autowired
	private DeveExistirBoletosParaRemessa deveExistirBoletosParaRemessa;

	@Override
	public void registerRulesSpecification() throws BusinessException {
		rules.put(deveExistirBoletosParaRemessa, new String[] { "Não existe boletos para geração de remessa!" });
	}
}
