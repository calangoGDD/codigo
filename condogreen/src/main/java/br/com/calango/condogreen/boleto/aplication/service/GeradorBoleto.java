package br.com.calango.condogreen.boleto.aplication.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import br.com.calango.condogreen.boleto.aplication.repository.BoletoRepository;
import br.com.calango.condogreen.boleto.aplication.repository.ConfiguradorBoletoRepository;
import br.com.calango.condogreen.boleto.aplication.repository.RecebedorBoletoRepository;
import br.com.calango.condogreen.boleto.aplication.repository.RemessaRepository;
import br.com.calango.condogreen.boleto.aplication.repository.SituacaoBoletoRepository;
import br.com.calango.condogreen.boleto.domain.BoletoDTO;
import br.com.calango.condogreen.boleto.domain.ConfiguradorBoleto;
import br.com.calango.condogreen.boleto.domain.InfoBoleto;
import br.com.calango.condogreen.boleto.domain.RecebedorBoleto;
import br.com.calango.condogreen.boleto.domain.SituacaoBoleto;
import br.com.calango.condogreen.boleto.domain.ValorCustomizado;
import br.com.calango.condogreen.boleto.domain.remessa.Remessa;
import br.com.calango.condogreen.boleto.domain.validacao.ValidadorBoletoAvulso;
import br.com.calango.condogreen.boleto.domain.validacao.ValidadorGeracaoBoletoEmMassa;
import br.com.calango.condogreen.boleto.domain.validacao.ValidadorRemessa;
import br.com.calango.condogreen.boleto.util.Tuple;
import br.com.calango.condogreen.condominio.aplication.resource.ApartamentoMorador;
import br.com.calango.condogreen.infrastructure.exception.BusinessException;
import br.com.calango.condogreen.pessoa.aplication.PessoaJurdicaRepository;
import br.com.calango.condogreen.pessoa.domain.IPessoa;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

/**
 * Esta classe representa um serviço de domínio, responsável pela geração de
 * boleto
 * 
 * @author Guilherme Andrade
 *
 */
@Service
public class GeradorBoleto {

	@Autowired
	private ApplicationContext appContext;

	@Autowired
	private RecebedorBoletoRepository recebeBoletoRepository;

	@Autowired
	private PessoaJurdicaRepository pessoaJurdicaRepository;

	@Autowired
	private ConfiguradorBoletoRepository configuradorBoletoRepository;

	@Autowired
	private RemessaRepository remessaRepository;

	@Autowired
	private BoletoRepository boletoRepository;

	@Autowired
	private SituacaoBoletoRepository situacaoBoletoRepository;

	/**
	 * Método responsável por gerar boletos avulso que estão com a situação
	 * aguardando geração de remessa.
	 * 
	 * @throws IOException
	 */
	public void gerarRemessa() throws IOException, BusinessException {
		long numeroUltimaRemessa = getNumeroUltimaRemessa();

		SituacaoBoleto aguardandoConfirmacaoEnvio = situacaoBoletoRepository.findByCodigo(SituacaoBoleto.SITUACAO_AGUARDANDO_CONFIRMACAO_ENVIO);

		List<InfoBoleto> boletos = boletoRepository.findBySituacaoBoletoCodigo(SituacaoBoleto.SITUACAO_AGUARDANDO_GERACAO_REMESSA);

		Remessa remessa = new Remessa(++numeroUltimaRemessa, new ArrayList<InfoBoleto>(boletos));
		ValidadorRemessa validarRemessa = (ValidadorRemessa) appContext.getBean("validadorRemessa");
		validarRemessa.checkBrokenRules(remessa);

		// TODO alterar quando existir outro sistema
		PessoaJuridica empresa = pessoaJurdicaRepository.findTopByOrderByIdAsc();
		for (InfoBoleto infoBoleto : boletos) {
			infoBoleto.setSituacaoBoleto(aguardandoConfirmacaoEnvio);
		}
		boletoRepository.save(boletos);

		remessa.geraRemessa(empresa, true);
		remessa.setQuantidadeBoletos(boletos.size());
		remessaRepository.save(remessa);
	}

	/**
	 * Método responsável por cancelar boleto
	 * 
	 * @param boleto
	 */
	public void cancelarBoleto(Long id) {
		InfoBoleto boletoParaCancelar = boletoRepository.findOne(id);
		SituacaoBoleto aguardandoGeracaoRemessa = situacaoBoletoRepository.findByCodigo(SituacaoBoleto.SITUACAO_AGUARDANDO_GERACAO_REMESSA);
		boletoParaCancelar.setSituacaoBoleto(aguardandoGeracaoRemessa);
		boletoParaCancelar.setCodigoMovimento(InfoBoleto.MOVIMENTO_PEDIDO_BAIXA);
		boletoRepository.save(boletoParaCancelar);
	}

	/**
	 * Método responsável por gerar remessa para o condomínio.
	 * 
	 * @param remessa
	 * @param parametrosGeracao
	 * @throws IOException
	 * @throws BusinessException
	 * @throws InterruptedException
	 */
	@Async
	@Transactional
	public void gerarBoletosEmMassa(BoletoDTO boletoAvulsoDTO) throws IOException, BusinessException, InterruptedException {

		ValidadorGeracaoBoletoEmMassa validadorGeracaoBoletoEmMassa = (ValidadorGeracaoBoletoEmMassa) appContext
		        .getBean("validadorGeracaoBoletoEmMassa");
		validadorGeracaoBoletoEmMassa.checkBrokenRules(this);

		String uriConfiguradorBoleto = boletoAvulsoDTO.getUriConfiguradorBoleto();
		ConfiguradorBoleto configuradorBoleto = configuradorBoletoRepository.findOne(Long.valueOf(getId(uriConfiguradorBoleto)));
		Tuple<String, String> tuple = getInfoUltimoBoleto();
		Long nossoNumeroUltimoBoleto = Long.valueOf(tuple.getKey());
		Long numeroDocumentoUltimoBoleto = Long.valueOf(tuple.getValue());

		SituacaoBoleto aguardandoGeracaoRemessa = situacaoBoletoRepository.findByCodigo(SituacaoBoleto.SITUACAO_AGUARDANDO_GERACAO_REMESSA);

		List<InfoBoleto> infoBoletos = new ArrayList<>();
		List<RecebedorBoleto> recebedoresBoleto = recebeBoletoRepository.findAll();
		for (RecebedorBoleto recebedorBoleto : recebedoresBoleto) {
			 ApartamentoMorador responsavelPorPagarBoleto = recebedorBoleto.getResponsavelPorPagarBoleto();
			if (responsavelPorPagarBoleto != null) {
				BigDecimal valorCustomizado = getValorCustomizado(configuradorBoleto, recebedorBoleto);
				IPessoa sacado = responsavelPorPagarBoleto.getPessoa();
				InfoBoleto boleto = new InfoBoleto(sacado, ++nossoNumeroUltimoBoleto, ++numeroDocumentoUltimoBoleto, valorCustomizado,
				        boletoAvulsoDTO.getDataVencimento(), configuradorBoleto);

				boleto.setSituacaoBoleto(aguardandoGeracaoRemessa);
				boleto.setCodigoMovimento(InfoBoleto.MOVIMENTO_ENTRADA_TITULO);
				infoBoletos.add(boleto);
			}
		}
		boletoRepository.save(infoBoletos);
	}

	/**
	 * Método
	 * 
	 * @param remessaAvulsaDTO
	 * @return
	 * @throws IOException
	 */
	public void gerarBoletoAvulso(BoletoDTO boletoAvulsoDTO) throws IOException {
		String uriRecebedorBoleto = boletoAvulsoDTO.getUriRecebedorBoleto();
		String uriConfiguradorBoleto = boletoAvulsoDTO.getUriConfiguradorBoleto();
		ConfiguradorBoleto configuradorBoleto = configuradorBoletoRepository.findOne(Long.valueOf(getId(uriConfiguradorBoleto)));
		RecebedorBoleto recebedorBoleto = recebeBoletoRepository.findOne(Long.valueOf(getId(uriRecebedorBoleto)));

		ValidadorBoletoAvulso validadorGeracaoBoletoAvulso = (ValidadorBoletoAvulso) appContext.getBean("validadorBoletoAvulso");
		validadorGeracaoBoletoAvulso.checkBrokenRules(recebedorBoleto);

		ApartamentoMorador responsavelPorPagarBoleto = recebedorBoleto.getResponsavelPorPagarBoleto();

		InfoBoleto boleto = null;
		if (responsavelPorPagarBoleto != null) {
			Tuple<String, String> tuple = getInfoUltimoBoleto();
			Long nossoNumeroUltimoBoleto = Long.valueOf(tuple.getKey());
			Long numeroDocumentoUltimoBoleto = Long.valueOf(tuple.getValue());
			boleto = new InfoBoleto(responsavelPorPagarBoleto.getPessoa(), ++nossoNumeroUltimoBoleto, ++numeroDocumentoUltimoBoleto,
			        boletoAvulsoDTO.getValor(), boletoAvulsoDTO.getDataVencimento(), configuradorBoleto);

			SituacaoBoleto aguardandoGeracaoRemessa = situacaoBoletoRepository.findByCodigo(SituacaoBoleto.SITUACAO_AGUARDANDO_GERACAO_REMESSA);

			boleto.setSituacaoBoleto(aguardandoGeracaoRemessa);
			boleto.setCodigoMovimento(InfoBoleto.MOVIMENTO_ENTRADA_TITULO);
		}

		boletoRepository.save(boleto);
	}

	private Tuple<String, String> getInfoUltimoBoleto() {
		InfoBoleto ultimoBoleto = boletoRepository.findTopByOrderByNossoNumeroDesc();
		Tuple<String, String> tuple = new Tuple<String, String>();
		if (ultimoBoleto != null) {
			tuple.setKey(ultimoBoleto.getNossoNumero());
			tuple.setValue(ultimoBoleto.getNumeroDocumento());
		} else {
			tuple.setKey("0");
			tuple.setValue("0");
		}
		return tuple;
	}

	private long getNumeroUltimaRemessa() {
		Remessa ultimaRemessa = remessaRepository.findTopByOrderByNumeroRemessaDesc();
		long numeroUltimaRemessa = 0l;
		if (ultimaRemessa != null) {
			numeroUltimaRemessa = ultimaRemessa.getNumeroRemessa();
		}
		return numeroUltimaRemessa;
	}

	/**
	 * Método responsável por recuperar o valor customizado.
	 * 
	 * @param configurador
	 * @param recebedorBoleto
	 * @return
	 */
	private BigDecimal getValorCustomizado(ConfiguradorBoleto configurador, RecebedorBoleto recebedorBoleto) {
		ValorCustomizado valorCustomizado = configurador.getValoresCustomizados().stream()
		        .filter(v -> v.getRecebedorBoleto().equals(recebedorBoleto)).findFirst().orElse(null);
		return valorCustomizado == null ? null : valorCustomizado.getValor();
	}

	public Long getQtdRecebedoresBoletoSemResponsavelPorPagar() {
		return recebeBoletoRepository.countRecebedoresBoletoSemResponsavelPorPagar();
	}

	private String getId(String uri) {
		return uri.substring((uri.lastIndexOf("/") + 1));
	}

	public List<InfoBoleto> meusBoletos(Principal principal) {
		List<String> situacoes = Arrays.asList(SituacaoBoleto.SITUACAO_ENTRADA_CONFIRMADA, SituacaoBoleto.SITUACAO_LIQUIDADO);
		return boletoRepository.findBySituacaoBoletoCodigoInAndSacadoContaNomeOrderByNossoNumeroDesc(situacoes, principal.getName());
	}

}
