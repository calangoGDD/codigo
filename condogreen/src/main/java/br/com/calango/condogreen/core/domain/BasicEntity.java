package br.com.calango.condogreen.core.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@MappedSuperclass
@JsonIgnoreProperties(ignoreUnknown = true, value = { "hibernateLazyInitializer", "handler" })
public class BasicEntity {

	public static final String PK_ID = "id";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = PK_ID)
	@JsonIdentityReference(alwaysAsId = true)
	private Long id;

	@Version
	@Column(name = "atualizacao", nullable = false)
	protected Timestamp atualizacao;

	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getAtualizacao() {
		return atualizacao;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(atualizacao).build();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BasicEntity other = (BasicEntity) obj;
		return new EqualsBuilder().append(other.id, id).isEquals();
	}

}
