package br.com.calango.condogreen.condominio.aplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.condominio.domain.Bloco;

@RepositoryRestResource(collectionResourceRel = "blocos", path = "blocos")
public interface BlocoRepository extends JpaRepository<Bloco, Long> {

}
