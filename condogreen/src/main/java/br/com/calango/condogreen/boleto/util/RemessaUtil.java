package br.com.calango.condogreen.boleto.util;

import java.math.BigDecimal;
import java.text.NumberFormat;

import org.jrimum.domkee.comum.pessoa.endereco.UnidadeFederativa;

import br.com.calango.condogreen.core.domain.Endereco;
import br.com.calango.condogreen.pessoa.domain.ContaBancaria;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

public class RemessaUtil {

	public static String retiraCaracteresEspeciais(String stringFonte) {
		String passa = stringFonte;
		passa = passa.replaceAll("[ÂÀÁÄÃ]", "A");
		passa = passa.replaceAll("[âãàáä]", "a");
		passa = passa.replaceAll("[ÊÈÉË]", "E");
		passa = passa.replaceAll("[êèéë]", "e");
		passa = passa.replaceAll("ÎÍÌÏ", "I");
		passa = passa.replaceAll("îíìï", "i");
		passa = passa.replaceAll("[ÔÕÒÓÖ]", "O");
		passa = passa.replaceAll("[ôõòóö]", "o");
		passa = passa.replaceAll("[ÛÙÚÜ]", "U");
		passa = passa.replaceAll("[ûúùü]", "u");
		passa = passa.replaceAll("Ç", "C");
		passa = passa.replaceAll("ç", "c");
		passa = passa.replaceAll("[ýÿ]", "y");
		passa = passa.replaceAll("Ý", "Y");
		passa = passa.replaceAll("ñ", "n");
		passa = passa.replaceAll("Ñ", "N");
		passa = passa.replaceAll("[-+=*;%$#@!_]", "");
		passa = passa.replaceAll("['\"]", "");
		passa = passa.replaceAll("[<>()\\{\\}]", "");
		passa = passa.replaceAll("['\\\\.,()|/]", "");
		passa = passa.replaceAll("[^!-ÿ]{1}[^ -ÿ]{0,}[^!-ÿ]{1}|[^!-ÿ]{1}", " ");
		return passa;
	}

	public static String currencyFormat(BigDecimal n) {
		return NumberFormat.getCurrencyInstance().format(n);
	}

	public static String padLeftSpaces(String str, int n) {
		return String.format("%1$" + n + "s", str);
	}

	public static String formatarNumeroComDuasCasasDecimais(BigDecimal valor) {
		valor = valor.setScale(2);
		return valor.toString().replace(".", "");
	}

	public static PessoaJuridica createPessoaJuridica() {
		Endereco endereco = new Endereco();
		endereco.setRua("RUA FREI MIGUELINHO");
		endereco.setUf(UnidadeFederativa.PE);
		endereco.setCep("53625");
		endereco.setSufixoCep("813");
		endereco.setCidade("IGARASSU");
		endereco.setBairro("INHAMA");
		endereco.setNumero("299");
		endereco.setPais("Brasil");

		ContaBancaria contaBancaria = new ContaBancaria();
		contaBancaria.setAgencia("3122");
		contaBancaria.setCodigoBeneficiario("633754");
		contaBancaria.setDigitoCodigoBeneficiario("6");

		PessoaJuridica empresa = new PessoaJuridica();
		empresa.setNome("COND CONJ RESIDENCIAL SITIO VIVER");
		empresa.setCnpj("22.666.605/0001-59");
		empresa.setEndereco(endereco);
		empresa.setContaBancaria(contaBancaria);

		return empresa;
	}
}
