package br.com.calango.condogreen.boleto.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.jrimum.bopepo.Boleto;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeCobranca;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeTitulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo.Aceite;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import br.com.calango.condogreen.boleto.util.BoletoUtil;
import br.com.calango.condogreen.core.domain.BasicEntity;
import br.com.calango.condogreen.pessoa.domain.IPessoa;
import br.com.calango.condogreen.pessoa.domain.Pessoa;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

/**
 * Está classe representa os boletos gerados para os moradores.
 * 
 * @author Guilherme Andrade
 * @since 24/06/2015
 */
@Entity
@Table(name = "boleto")
public class InfoBoleto extends BasicEntity {

	private static final long serialVersionUID = 1L;

	public static final String DATA_DDMMYYYY = "ddMMyyyy";

	public static final String FK_ID = "boleto_id";

	/**
	 * Código de movimento da remessa - entrada de título
	 */
	public static final String MOVIMENTO_ENTRADA_TITULO = "01";

	/**
	 * Código de movimento da remessa - pedido de baixa
	 */
	public static final String MOVIMENTO_PEDIDO_BAIXA = "02";

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = ConfiguradorBoleto.FK_ID)
	private ConfiguradorBoleto configuradorBoleto;

	/**
	 * Morador
	 */
	@NotNull
	@ManyToOne(fetch = FetchType.EAGER, targetEntity = Pessoa.class)
	@JoinColumn(name = Pessoa.FK_ID_SACADO)
	private IPessoa sacado;

	/**
	 * Número adotado pelo Banco Cedente para identificar o Título. Para Código
	 * de Movimento (posições 16-17 do Segmento P) igual a '01' (Entrada de
	 * Títulos): 2) Quando informado pelo Cliente/Cedente, o Nosso Número deverá
	 * obedecer o seguinte formato: CCNNNNNNNNNNNNNNN, onde: CC = 11 (título
	 * Registrado emissão CAIXA) CC = 14 (título Registrado emissão Cedente) CC
	 * = 21 (título Sem Registro emissão CAIXA) NNNNNNNNNNNNNNN = Número livre
	 * do Cliente/Cedente Obs.: O númer
	 */
	@Column(name = "nosso_numero")
	private String nossoNumero;

	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@Column(name = "dt_emissao")
	private LocalDateTime dataEmissao;

	/**
	 * Número utilizado e controlado pelo Cliente, para identificar o título de
	 * cobrança. Poderá conter número de duplicata, no caso de cobrança de
	 * duplicatas; número da apólice, no caso de cobrança de seguros, etc. Campo
	 * de preenchimento obrigatório
	 */
	@Column(name = "numero_documento")
	private String numeroDocumento;

	/**
	 * Número adotado pelo Banco Cedente para identificar o Título. Os dois
	 * primeiros número do nossonúmero
	 */
	@Column(name = "modalidade")
	@Enumerated(EnumType.STRING)
	private TipoDeCobranca modalidade;

	/**
	 * Código adotado pela FEBRABAN, para identificar a característica dos
	 * títulos dentro das modalidades de cobrança existentes no banco. ‘1’ =
	 * Cobrança Simples ‘3’ = Cobrança Caucionada ‘4’ = Cobrança Descontada
	 */
	@Column(name = "cd_carteira")
	@Enumerated(EnumType.STRING)
	private CodigoCarteira codigoDaCarteira;

	/**
	 * O valor real do boleto é guardado nesta coluna devido aos motivos já
	 * relatados @see InfoBoleto#valorDescontoPorBoleto
	 */
	@Column(name = "vl_boleto")
	private BigDecimal valor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = SituacaoBoleto.FK_ID)
	private SituacaoBoleto situacaoBoleto;

	@JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	@Column(name = "data_vencimento")
	private LocalDate dataVencimento;

	/**
	 * Código movimento da remessa
	 */
	@Column(name = "cd_movimento")
	@NotNull
	private String codMovimento;

	public InfoBoleto() {
	}

	public InfoBoleto(IPessoa pessoa, Long nossoNumero, Long numeroDocumento, BigDecimal valorCustomizado, LocalDate dataVencimento,
			ConfiguradorBoleto configuradorBoleto) {
		this();
		this.configuradorBoleto = configuradorBoleto;
		this.sacado = pessoa;
		this.nossoNumero = nossoNumero.toString();
		this.modalidade = TipoDeCobranca.COM_REGISTRO;
		this.numeroDocumento = numeroDocumento.toString();
		this.dataEmissao = LocalDateTime.now();
		this.codigoDaCarteira = CodigoCarteira.SIMPLES;
		setValor(valorCustomizado);
		this.dataVencimento = dataVencimento;
	}

	public void setValor(BigDecimal valorCustomizado) {
		if (valorCustomizado != null && valorCustomizado.compareTo(BigDecimal.ZERO) != 0) {
			this.valor = valorCustomizado;
		} else {
			this.valor = getValorDaConfiguracao();
		}
	}

	private BigDecimal getValorDaConfiguracao() {
		return this.configuradorBoleto.getValor();
	}

	/**
	 * @return the pessoa
	 */
	public IPessoa getSacado() {
		return sacado;
	}

	/**
	 * @param pessoa
	 *            the pessoa to set
	 */
	public void setSacado(IPessoa sacado) {
		this.sacado = sacado;
	}

	/**
	 * @return the nossoNumero
	 */
	@JsonIgnore
	public String getNossoNumeroFormatado() {
		return BoletoUtil.geraNossoNumeroSIGCB(Long.valueOf(nossoNumero), CodigoModalidade.COM_REGISTRO);
	}

	/**
	 * @param nossoNumero
	 *            the nossoNumero to set
	 */
	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	/**
	 * 
	 * @return nossoNumero
	 */
	public String getNossoNumero() {
		return nossoNumero;
	}

	/**
	 * @return the dataEmissao
	 */
	public LocalDateTime getDataEmissao() {
		return dataEmissao;
	}

	/**
	 * @param dataEmissao
	 *            the dataEmissao to set
	 */
	public void setDataEmissao(LocalDateTime dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * @param numeroDocumento
	 *            the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the identificacaoDeModalidade
	 */
	public TipoDeCobranca getModalidade() {
		return modalidade;
	}

	/**
	 * @param identificacaoDeModalidade
	 *            the identificacaoDeModalidade to set
	 */
	public void setModalidade(TipoDeCobranca identificacaoDeModalidade) {
		this.modalidade = identificacaoDeModalidade;
	}

	/**
	 * @return the codigoDaCarteira
	 */
	public CodigoCarteira getCodigoDaCarteira() {
		return codigoDaCarteira;
	}

	/**
	 * @param codigoDaCarteira
	 *            the codigoDaCarteira to set
	 */
	public void setCodigoDaCarteira(CodigoCarteira codigoDaCarteira) {
		this.codigoDaCarteira = codigoDaCarteira;
	}

	public BigDecimal getValor() {
		return valor;
	}

	@JsonIgnore
	public String getDataVencimentoFormatada() {
		return this.dataVencimento.format(DateTimeFormatter.ofPattern(DATA_DDMMYYYY));
	}

	/**
	 * Método responsável por calcular o valor de juros de mora.
	 * 
	 * Se os juros de mora são de 1% ao mês, considerando um mês de 30 dias,
	 * esse percentual deve ser divididos pelos 30 dias. Assim, 1% de R$ 100 é
	 * igual a R$ 1, que dividido por dia fica R$ 0,03. Esse valor será
	 * multiplicado por cinco (quantidade de dias em atraso), resultado em R$
	 * 0,15. TODO criar teste
	 * 
	 * @return
	 */
	public BigDecimal calcularValorMora() {
		BigDecimal resultado = BigDecimal.ZERO.setScale(2);
		BigDecimal diaMaximoDoMes = new BigDecimal(LocalDate.now().lengthOfMonth());
		BigDecimal valorMoraConfigurada = this.configuradorBoleto.getValorMora();
		CodigoJurosMora codMora = this.configuradorBoleto.getCodMora();
		if (CodigoJurosMora.TAXA_MENSAL.equals(codMora)) {
			BigDecimal percentualConfigurado = valorMoraConfigurada.divide(new BigDecimal("100"));
			resultado = percentualConfigurado.multiply(getValor()).divide(diaMaximoDoMes, 2, RoundingMode.HALF_DOWN);
		} else if (CodigoJurosMora.VALOR_POR_DIA.equals(codMora)) {
			resultado = valorMoraConfigurada;
		}
		return resultado;
	}

	/**
	 * @return intrução 1 do boleto.
	 */
	public String getInstrucao1() {
		return this.configuradorBoleto.getInstrucao1();
	}

	/**
	 * @return intrução 1 do boleto.
	 */
	public String getInstrucao2() {
		return this.configuradorBoleto.getInstrucao2();
	}

	/**
	 * @return instrução ao Sacado
	 */
	public String getInstrucaoAoSacado() {
		return this.configuradorBoleto.getInstrucaoAoSacado();
	}

	/**
	 * 
	 * @return local de pagamento.
	 */
	public String getLocalPagamento() {
		return this.configuradorBoleto.getLocalPagamento();
	}

	/**
	 * 
	 * @return O aceite do boleto.
	 */
	public Aceite getAceite() {
		return this.configuradorBoleto.getAceite();
	}

	/**
	 * 
	 * @return O tipo do documento
	 */
	public TipoDeTitulo getEspecieDoTitulo() {
		return this.configuradorBoleto.getEspecieDoTitulo();
	}

	public Date getDataVencimentoComoDate() {
		return Date.from(this.dataVencimento.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public String getDataLimiteDescontoPelaDataVencimento(int diaDeLimiteDesconto) {
		LocalDate date = this.dataVencimento;
		LocalDate dataDesconto = date;
		if(diaDeLimiteDesconto <=31){
			dataDesconto = LocalDate.of(date.getYear(), date.getMonth(), diaDeLimiteDesconto);
		} 
		return dataDesconto.format(DateTimeFormatter.ofPattern("ddMMyyyy"));
	}

	/**
	 * Se possuir valor customizado no boleto será desconsiderado o valor de
	 * desconto do configurador.
	 */
	public BigDecimal getValorDesconto() {
		return this.configuradorBoleto.getValorDesconto();
	}

	/**
	 * 
	 * @return Nome do boleto
	 */
	public String getNomeBoleto() {
		String nome = sacado.getNome().replace(" ", "_");
		StringBuilder nomeBoleto = new StringBuilder();
		nomeBoleto.append(nome).append("_").append(this.getDataVencimentoFormatada()).append(".pdf");
		return nomeBoleto.toString();
	}

	/**
	 * Método responsável por criar Arquivo de Boleto
	 * 
	 * @param empresa
	 * @param infoBoleto
	 * @return
	 */
	public byte[] getArquivo(PessoaJuridica empresa) {
		Boleto boleto = new BoletoBuilder(empresa, this).build();
		return BoletoUtil.getPdfBoleto(boleto);
	}

	/**
	 * @return the configuradorBoleto
	 */
	public ConfiguradorBoleto getConfiguradorBoleto() {
		return configuradorBoleto;
	}

	/**
	 * @param configuradorBoleto
	 *            the configuradorBoleto to set
	 */
	public void setConfiguradorBoleto(ConfiguradorBoleto configuradorBoleto) {
		this.configuradorBoleto = configuradorBoleto;
	}

	/**
	 * @param dataVencimento
	 *            the dataVencimento to set
	 */
	public void setDataVencimento(LocalDate dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	/**
	 * @return the dataVencimento
	 */
	public LocalDate getDataVencimento() {
		return dataVencimento;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("INSERT INTO `boleto`(id, configurador_id, sacado_id, nosso_numero, dt_emissao, numero_documento, modalidade, cd_carteira, vl_boleto, situacao_boleto_id, data_vencimento, cd_movimento) VALUES('");
		builder.append(getId()).append("','");
		builder.append(configuradorBoleto.getId()).append("',");
		builder.append("(select pf.id from pessoa_fisica pf where pf.cpf='" + sacado.getNumeroInscricao() + "')").append(",'");
		builder.append(nossoNumero).append("',");
		builder.append("STR_TO_DATE('").append(dataEmissao.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))).append("',  '%d/%m/%Y'),'");
		builder.append(numeroDocumento).append("','");
		builder.append(modalidade).append("','");
		builder.append(codigoDaCarteira).append("','");
		builder.append(valor).append("',");
		builder.append("(select id from situacao_boleto where codigo ='" + situacaoBoleto.getCodigo() + "')").append(", STR_TO_DATE('");
		builder.append(dataVencimento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))).append("',  '%d/%m/%Y'),");
		builder.append(codMovimento).append("); \n");

		return builder.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((codigoDaCarteira == null) ? 0 : codigoDaCarteira.hashCode());
		result = prime * result + ((configuradorBoleto == null) ? 0 : configuradorBoleto.hashCode());
		result = prime * result + ((dataEmissao == null) ? 0 : dataEmissao.hashCode());
		result = prime * result + ((dataVencimento == null) ? 0 : dataVencimento.hashCode());
		result = prime * result + ((modalidade == null) ? 0 : modalidade.hashCode());
		result = prime * result + ((nossoNumero == null) ? 0 : nossoNumero.hashCode());
		result = prime * result + ((numeroDocumento == null) ? 0 : numeroDocumento.hashCode());
		result = prime * result + ((sacado == null) ? 0 : sacado.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		InfoBoleto other = (InfoBoleto) obj;
		if (nossoNumero == null) {
			if (other.nossoNumero != null)
				return false;
		} else if (!nossoNumero.equals(other.nossoNumero))
			return false;
		return true;
	}

	/**
	 * Método responsável por retornar a descrição da situação.
	 * 
	 * @return
	 */
	public String getDescricaoSituacao() {
		return situacaoBoleto.getDescricao();
	}

	/**
	 * @return the codMovimento
	 */
	public String getCodMovimento() {
		return codMovimento;
	}

	/**
	 * @param codMovimento
	 *            the codMovimento to set
	 */
	public void setCodigoMovimento(String codMovimento) {
		this.codMovimento = codMovimento;
	}

	public boolean isEscondeBotaoCancelar() {
		return situacaoBoleto.isSituacaoCancelamentoSolicitado() || situacaoBoleto.isSituacaoLiquidado()
				|| situacaoBoleto.isSituacaoAguardandoGeracaoRemessa() || situacaoBoleto.isSituacaoAguardandoConfirmacaoEnvio()
				|| situacaoBoleto.isSituacaoEnviadoParaRegistro() || situacaoBoleto.isSituacaoCancelado() || situacaoBoleto.isSituacaoCancelado() 
				|| situacaoBoleto.isSituacaoEntradaRejeitada() || situacaoBoleto.isSituacaoProtestadoBaixado() ;
	}

	public boolean isEscondeBotaoExcluir() {
		return !situacaoBoleto.isSituacaoAguardandoGeracaoRemessa();
	}

	public String getNumeroDocumentoFormatado() {
		return BoletoUtil.formatarNumeroDocumento(this.numeroDocumento);
	}

	public SituacaoBoleto getSituacaoBoleto() {
		return situacaoBoleto;
	}

	public void setSituacaoBoleto(SituacaoBoleto situacaoBoleto) {
		this.situacaoBoleto = situacaoBoleto;
	}

	public String getDataJurosMora() {
		return this.dataVencimento.plusDays(1).format(DateTimeFormatter.ofPattern(DATA_DDMMYYYY));
	}
	
	public boolean isShowEmMeusBoletos() {
		return SituacaoBoleto.SITUACAO_ENTRADA_CONFIRMADA.equals(this.situacaoBoleto.getCodigo());
	}
}
