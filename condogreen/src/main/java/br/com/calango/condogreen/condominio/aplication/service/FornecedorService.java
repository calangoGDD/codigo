package br.com.calango.condogreen.condominio.aplication.service;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.calango.condogreen.condominio.aplication.repository.FornecedorRepository;
import br.com.calango.condogreen.condominio.domain.Fornecedor;

@Service
public class FornecedorService {

	private FornecedorRepository fornecedorRepository;

	@Autowired
	public FornecedorService(FornecedorRepository fornecedorRepository) {
		this.fornecedorRepository = fornecedorRepository;
	}
	
	public Page<Fornecedor> findAll(Pageable pageable) {
		return fornecedorRepository.findAll(pageable);
	}

	public void save(String nomeImagem, byte[] imagem, Fornecedor noticia) {
		noticia.setDataCriacao(LocalDate.now());
		noticia.setImagem(imagem);
		noticia.setNomeImagem(nomeImagem);
		fornecedorRepository.saveAndFlush(noticia);
	}


	public void delete(long id) {
		fornecedorRepository.delete(id);
	}


	public void ativarDesativar(long id) {
		Fornecedor noticia = fornecedorRepository.findOne(id);
		noticia.ativarOuDesativar();
		fornecedorRepository.saveAndFlush(noticia);
	}

	public Fornecedor findById(long id) {
		return fornecedorRepository.findOne(id);
	}

}
