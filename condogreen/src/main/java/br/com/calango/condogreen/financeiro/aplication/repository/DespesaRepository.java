package br.com.calango.condogreen.financeiro.aplication.repository;

import java.math.BigDecimal;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.financeiro.domain.Despesa;

/**
 * Esta classe representa o Repositório de Despesas do condomínio.
 * 
 * @author Danyllo Araujo
 * @since 15/12/2016
 * 
 */
@RepositoryRestResource(collectionResourceRel = "despesas", path = "despesas")
public interface DespesaRepository extends JpaRepository<Despesa, Long> , JpaSpecificationExecutor<Despesa> {
	
	Page<Despesa> findByNomeContaining(@Param(value = "nome") String nome, Pageable pePageable);
	
	@Query(value = "select sum(d.valor) from Despesa d where d.data_vencimento between str_to_date(:from , '%d/%m/%Y') and str_to_date( :to, '%d/%m/%Y')",  nativeQuery = true)
	BigDecimal sumDespesasByDataVencimento(@Param("from") String from, @Param("to") String to);
}
