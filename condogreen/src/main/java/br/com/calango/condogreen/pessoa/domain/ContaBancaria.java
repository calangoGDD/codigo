package br.com.calango.condogreen.pessoa.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

import br.com.calango.condogreen.boleto.util.BoletoUtil;
import br.com.calango.condogreen.core.domain.BasicEntity;

@Entity
@Table(name = "conta_bancaria")
public class ContaBancaria extends BasicEntity {

	public static final String FK_ID = "conta_bancaria_id";

	@NotBlank
	@Column(name = "agencia")
	private String agencia;

	@Column(name = "codigo_beneficiario")
	private String codigoBeneficiario;

	@Column(name = "digito_codigo_beneficiario")
	private String digitoCodigoBeneficiario;

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getCodigoBeneficiario() {
		return codigoBeneficiario;
	}

	public void setCodigoBeneficiario(String codigoBeneficiario) {
		this.codigoBeneficiario = codigoBeneficiario;
	}

	public String getDigitoCodigoBeneficiario() {
		return digitoCodigoBeneficiario;
	}

	public void setDigitoCodigoBeneficiario(String digitoCodigoBeneficiario) {
		this.digitoCodigoBeneficiario = digitoCodigoBeneficiario;
	}

	public Integer getDigitoAgencia() {
		return BoletoUtil.calculeDVModulo11(agencia);
	}

}
