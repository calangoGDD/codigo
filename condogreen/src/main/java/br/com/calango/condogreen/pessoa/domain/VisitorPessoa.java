package br.com.calango.condogreen.pessoa.domain;

public interface VisitorPessoa {

	void visit(PessoaFisica pessoaFisica);
	
	void visit(PessoaJuridica pessoaJuridica);
}
