package br.com.calango.condogreen.boleto.aplication.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.boleto.domain.InfoBoleto;

@RepositoryRestResource(collectionResourceRel = "boletos", path = "boletos")
public interface BoletoRepository extends JpaRepository<InfoBoleto, Long> {

	/**
	 * 
	 * @return Último boleto pelo nosso número.
	 */
	@Query(value = "select * from boleto order by nosso_numero *1  desc limit 1", nativeQuery = true)
	InfoBoleto findTopByOrderByNossoNumeroDesc();

	InfoBoleto findByNossoNumero(@Param(value = "nosso_numero") String nossoNumero);

	List<InfoBoleto> findBySituacaoBoletoCodigo(@Param(value = "situacaoBoleto_codigo") String situacaoBoletoCodigo);

	Page<InfoBoleto> findAllByOrderByAtualizacaoDesc(Pageable pePageable);

	List<InfoBoleto> findBySacadoId(@Param(value = "id") Long id);

	List<InfoBoleto> findBySacadoContaNome(@Param(value = "nome") String nome);

	List<InfoBoleto> findBySituacaoBoletoCodigoInAndSacadoContaNomeOrderByNossoNumeroDesc(@Param(value = "situacoes") List<String> situacoes,
	        @Param(value = "nome") String nome);

	/*+ "where not exists ( "
	+ "select ir.id "
	+ "from informacao_retorno ir "
	+ "where ir.boleto_id = b.id "
	+ "and ir.codigo_movimento in ('06','03','09') "
+ ") and "*/
	
	@Query(value ="select sum(b.vl_boleto) from boleto b where "
			
			+ " b.data_vencimento < now() "
			+ "and  b.dt_emissao between str_to_date(:from , '%d/%m/%Y') and str_to_date( :to, '%d/%m/%Y')", nativeQuery = true)
	BigDecimal getTotalInadimplentes(@Param("from") String from, @Param("to") String to);
}
