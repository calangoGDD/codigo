package br.com.calango.condogreen.condominio.domain;

import java.time.LocalDate;
import java.util.Base64;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

@Entity
@Table(name = "noticia")
public class Noticia extends AbstractPersistable<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotBlank
	@NotNull
	@Column(name = "titulo", length = 40)
	private String titulo;
	
	@Column(name = "conteudo", length = 4000)
	private String conteudo;
	
	@JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	@Column(name = "data_criacao")
	private LocalDate dataCriacao;
	
	@Column(name = "situacao")
	private boolean situacao;
	
	@Column(name = "versao")
	private long versao;
	
	@Lob
	@JsonIgnore
	@Column(name = "imagem")
	private byte[] imagem;

	@Column(name = "nomeimagem")
	private String nomeImagem;
	
	public Noticia() {	
	
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public boolean isSituacao() {
		return situacao;
	}

	public void setSituacao(boolean situacao) {
		this.situacao = situacao;
	}

	public long getVersao() {
		return versao;
	}

	public void setVersao(long versao) {
		this.versao = versao;
	}
	
	public byte[] getImagem() {
		return imagem;
	}

	public void setImagem(byte[] imagem) {
		this.imagem = imagem;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LocalDate getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDate dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public void setNomeImagem(String nomeImagem) {
		this.nomeImagem = nomeImagem;
	}
	
	public String getNomeImagem() {
		return nomeImagem;
	}
	
	public String getDescricaoSituacao() {
		String result = "Desativada";
		if(Boolean.TRUE.equals(situacao)) {
			result = "Ativa";
		}
		
		return result;
	}
	
	public String getImageBase64() {
		String picture = "";
		if (imagem != null) {
			picture = "data:image/png;base64," + Base64.getEncoder().encodeToString(imagem);
		} else {
			picture = "images/no-image-box.png";
		}
		return picture;
	}

	public boolean isTamanhoValido() {
		return true;
	}

	public void ativarOuDesativar() {
		if(this.situacao){
			this.situacao = false;
		} else {
			this.situacao = true;
		}
	}
}