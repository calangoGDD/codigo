package br.com.calango.condogreen.financeiro.domain;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

/**
 * Esta classe representa Despesa do condomínio.
 * 
 * @author Danyllo Araujo
 * @since 15/12/2016
 * 
 */
@Entity
@Table(name = "despesa")
public class Despesa extends AbstractPersistable<Long> {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "versao")
	private long versao;
	
	@Column(name = "categoria")
	private String categoria;
	
	@Column(name = "nome", nullable = false)
	private String nome;
	
	@Column(name = "valor", nullable = false)
	private BigDecimal valor;
	
	@JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	@Column(name = "data_vencimento", nullable = false)
	private LocalDate dataVencimento;

	@JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	@Column(name = "data_pagamento")
	private LocalDate dataPagamento;
	
	@Lob
	@JsonIgnore   
	@Column(name = "comprovante")
	private byte[] comprovante;
	
	@Column(name = "nome_arquivo_comprovante")
	private String nomeArquivoComprovante;
	
	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public LocalDate getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(LocalDate dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public LocalDate getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(LocalDate dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public byte[] getComprovante() {
		return comprovante;
	}

	public void setComprovante(byte[] comprovante) {
		this.comprovante = comprovante;
	}

	public boolean estaPaga(){
		return dataPagamento != null;
	}

	public long getVersao() {
		return versao;
	}

	public void setVersao(long versao) {
		this.versao = versao;
	}

	public String getNomeArquivoComprovante() {
		return nomeArquivoComprovante;
	}

	public void setNomeArquivoComprovante(String nomeArquivoComprovante) {
		this.nomeArquivoComprovante = nomeArquivoComprovante;
	}
	
}
