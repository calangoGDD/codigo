package br.com.calango.condogreen.financeiro.aplication.resource;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import br.com.calango.condogreen.financeiro.aplication.service.DespesaService;
import br.com.calango.condogreen.financeiro.domain.Despesa;


@RepositoryRestController
@RequestMapping("/despesas")
public class DespesaResource {

	private DespesaService despesaService;
	
	@Autowired
	public DespesaResource(DespesaService despesaService) {
		this.despesaService = despesaService;
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public byte[] salvar(@RequestPart(value = "comprovante") MultipartFile comprovante, @RequestPart(value = "despesa") Despesa despesa) throws IOException {
		try {
			despesa.setComprovante(comprovante.getBytes());
			despesa.setNomeArquivoComprovante(comprovante.getOriginalFilename());
			despesaService.save(despesa);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return comprovante.getBytes();
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.PUT, value = "/update")
	public void atualizar(@Valid @RequestBody Despesa despesa) throws IOException {
		despesaService.save(despesa);
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "/get/{id:\\d+}")
	public Despesa get(@PathVariable("id") long id) {
		return despesaService.findById(id);
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "{id:\\d+}/delete")
	public void delete(@PathVariable("id") long id) throws IOException {
		despesaService.delete(id);
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET)
	public Page<Despesa> search(@RequestParam(value = "nome", required = false) String nome, Pageable pageable) {
		return despesaService.findByNameContaining(nome, pageable);
	}
	
}
