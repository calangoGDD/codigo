package br.com.calango.condogreen.boleto.aplication.resource;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.calango.condogreen.boleto.aplication.service.GeradorBoleto;
import br.com.calango.condogreen.boleto.domain.InfoBoleto;

@RestController
@RequestMapping("/basic/boleto")
public class BoletoBasicResource {

	@Autowired
	private GeradorBoleto geradorBoleto;

	@RequestMapping(method = RequestMethod.GET, value = "meusBoletos")
	public List<InfoBoleto> meusBoletos(Principal principal) throws IOException {
		return geradorBoleto.meusBoletos(principal);
	}

}