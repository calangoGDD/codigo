package br.com.calango.condogreen.infrastructure;

import java.security.Principal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.cloud.aws.context.config.annotation.EnableContextCredentials;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = "br.com.calango.condogreen", basePackageClasses = Jsr310JpaConverters.class)
@Order(1)
@RestController
@SpringBootApplication
@EnableContextCredentials(accessKey = "AKIAI5FADWYX5GJ7BHMA", secretKey = "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")
public class AppConfig extends SpringBootServletInitializer {

	// "/" para produção
	// "/condogreen" para desenvolvimento
	public static final String CONTEXT_ROOT = "/condogreen";

	@RequestMapping("/user")
	public Principal user(Principal user) {
		return user;
	}

	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AppConfig.class);
	}
}
