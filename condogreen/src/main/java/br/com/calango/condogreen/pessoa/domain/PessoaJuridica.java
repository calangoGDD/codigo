package br.com.calango.condogreen.pessoa.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CNPJ;

/**
 * Classe que representa as empresas cadastradas no sistema
 * 
 * @author Guilherme Andrade
 *
 */
@Entity
@Table(name = "pessoa_juridica")
@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
public class PessoaJuridica extends Pessoa implements IPessoa {

	public static final String TIPO_INSCRICAO_PESSOA_JURIDICA = "2";

	public static final String FK_ID = "pessoa_juridica_id";
	public static final String FK_EMPRESA_ID = "empresa_id";

	@NotBlank
	@Column(name = "nome_fantasia", length = 30)
	private String nomeFantasia;

	@CNPJ
	@Column(name = "cnpj", length = 14)
	private String cnpj;

	@Override
	public String getTipoInscricao() {
		return TIPO_INSCRICAO_PESSOA_JURIDICA;
	}

	@Override
	public String getNumeroInscricao() {
		return getCnpj();
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	
	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	/**
	 * @see br.com.calango.condogreen.pessoa.domain.IPessoa#accept(br.com.calango.condogreen.pessoa.domain.VisitorPessoa)
	 */
	@Override
	public void accept(VisitorPessoa visitor) {
		visitor.visit(this);
	}

}
