package br.com.calango.condogreen.condominio.domain;

import java.util.List;

import br.com.calango.condogreen.condominio.aplication.resource.ApartamentoMorador;
import br.com.calango.condogreen.pessoa.domain.IPessoa;

public interface IMorador {

	String getApartamentosFormatados();

	void setApartamentos(List<ApartamentoMorador> apartamentos);

	List<ApartamentoMorador> getApartamentos();

	String getNome();

	void setPessoa(IPessoa pessoa);

	IPessoa getPessoa();

}
