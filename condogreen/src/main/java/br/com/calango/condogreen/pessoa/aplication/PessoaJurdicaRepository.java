package br.com.calango.condogreen.pessoa.aplication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.pessoa.domain.Pessoa;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

@RepositoryRestResource(collectionResourceRel = "pessoasjuridicas", path = "pessoasjuridicas")
public interface PessoaJurdicaRepository extends JpaRepository<Pessoa, Long> {

	PessoaJuridica findTopByOrderByIdAsc();

}
