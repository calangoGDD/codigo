package br.com.calango.condogreen.condominio.aplication.resource;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.calango.condogreen.condominio.aplication.service.ReservaService;
import br.com.calango.condogreen.condominio.domain.Reserva;

@RestController
@RequestMapping("/reservas")
public class ReservaResource {

	@Autowired
	ReservaService reservaService;
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET)
	public List<Reserva> reservas() throws IOException {
		return reservaService.reservas();
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "reservar")
	public void reservar(@Valid @RequestBody Reserva reserva, Principal principal) throws IOException {
		reservaService.reservar(reserva, principal);
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "{id:\\d+}/remover")
	public void remover(@PathVariable("id") long id) throws IOException {
		reservaService.remover(id);
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "{id:\\d+}/aprovar")
	public void aprovar(@PathVariable("id") long id) throws IOException {
		reservaService.aprovar(id);
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.PUT, value = "atualizar")
	public void atualizar(@Valid @RequestBody Reserva reserva, Principal principal) throws IOException {
		reservaService.atualizar(reserva, principal);
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "{id:\\d+}/get")
	public Reserva getReserva(@PathVariable("id") long id) throws IOException {
		return reservaService.findById(id);
	}
}
