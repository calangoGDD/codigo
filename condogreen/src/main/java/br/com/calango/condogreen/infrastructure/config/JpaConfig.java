package br.com.calango.condogreen.infrastructure.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import br.com.calango.condogreen.boleto.domain.remessa.Remessa;
import br.com.calango.condogreen.condominio.domain.Apartamento;
import br.com.calango.condogreen.condominio.domain.Reserva;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "br.com.calango.condogreen" })
public class JpaConfig extends RepositoryRestMvcConfiguration implements TransactionManagementConfigurer {

	@Autowired
	private LocalContainerEntityManagerFactoryBean entityManagerFactory;

	@Autowired
	private ComboPooledDataSource dataSource;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigIn() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable("condogreen");
	}

	@Override
	@Bean(name = "transactionManager")
	public PlatformTransactionManager annotationDrivenTransactionManager() {
		entityManagerFactory.setPackagesToScan("br.com.calango.condogreen");
		entityManagerFactory.setDataSource(dataSource);
		entityManagerFactory.afterPropertiesSet();

		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory.getObject());
		return txManager;
	}

	@Override
	protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.exposeIdsFor(Remessa.class);
		config.exposeIdsFor(Reserva.class);
		config.exposeIdsFor(Apartamento.class);
	}
}
