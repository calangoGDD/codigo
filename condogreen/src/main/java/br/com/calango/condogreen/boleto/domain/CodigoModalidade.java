package br.com.calango.condogreen.boleto.domain;

/**
 * Indica o tipo de desconto que deseja conceder ao Pagador do título: ‘0’ (Sem
 * Desconto); ou ‘1’ (Valor Fixo até a Data do Desconto informada); ou ‘2’
 * (Percentual até a Data do Desconto informada)
 * 
 * @author Guilherme Andrade
 *
 */
public enum CodigoModalidade {

	COM_REGISTRO(14), SEM_REGISTRO(24);

	/**
	 * Codigo do tipo de pagamento de juros mora.
	 */
	private int codigo;

	CodigoModalidade(int codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

}
