package br.com.calango.condogreen.boleto.domain;

import java.awt.Image;

import org.jrimum.bopepo.Boleto;
import org.jrimum.domkee.financeiro.banco.febraban.Carteira;
import org.jrimum.domkee.financeiro.banco.febraban.ContaBancaria;
import org.jrimum.domkee.financeiro.banco.febraban.NumeroDaConta;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeMoeda;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo;

import br.com.calango.condogreen.boleto.util.BoletoUtil;
import br.com.calango.condogreen.pessoa.domain.IPessoa;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

/**
 * Classe responsável po criar o boleto com informações do título e do
 * configurador.
 * 
 * @author Guilherme Andrade
 *
 */
public class BoletoBuilder {
	private Boleto boleto;
	private InfoBoleto infoBoleto;
	private PessoaJuridica empresa;

	public BoletoBuilder(PessoaJuridica empresa, InfoBoleto infoBoleto) {
		this.infoBoleto = infoBoleto;
		this.empresa = empresa;
		Titulo titulo = new TituloBuilder(empresa, infoBoleto).build();
		ContaBancaria contaBancaria = titulo.getContaBancaria();
		NumeroDaConta numeroDaConta = contaBancaria.getNumeroDaConta();
		// Parametros: Numero do cedente com 6 digitos, Digito verificador do
		// código do cedente, Nosso Número
		// se o digito verificador não for zero deve-se usar o modolo 11
		CampoLivreCefSIGCB campoLivreCefSIGCB = new CampoLivreCefSIGCB(numeroDaConta.getCodigoDaConta().toString(), numeroDaConta.getDigitoDaConta(),
		        titulo.getNossoNumero() + titulo.getDigitoDoNossoNumero());
		this.boleto = new Boleto(titulo, campoLivreCefSIGCB);
		comInfoConfiguraveis();
		comTextosExtras();
	}

	public BoletoBuilder comInfoConfiguraveis() {
		boleto.setLocalPagamento(infoBoleto.getLocalPagamento());
		boleto.setInstrucaoAoSacado(infoBoleto.getInstrucaoAoSacado());
		boleto.setInstrucao1(infoBoleto.getInstrucao1());
		boleto.setInstrucao2(infoBoleto.getInstrucao2());
		return this;
	}

	public BoletoBuilder comTextosExtras() {
		Titulo titulo = boleto.getTitulo();
		IPessoa sacado = infoBoleto.getSacado();
		ContaBancaria contaBancaria = titulo.getContaBancaria();
		NumeroDaConta numeroDaConta = contaBancaria.getNumeroDaConta();
		StringBuilder agenciaCodigoCedente = new StringBuilder();
		agenciaCodigoCedente.append(contaBancaria.getAgencia().getCodigo()).append("/").append(numeroDaConta.getCodigoDaConta()).append("-")
		        .append(numeroDaConta.getDigitoDaConta());
		Carteira carteira = contaBancaria.getCarteira();
		// TODO quando precisar colocar o balancete
		// boleto.addTextosExtras("txtRsProtocolo", "teste");
		boleto.addTextosExtras("txtFcAgenciaCodigoCedente", agenciaCodigoCedente.toString());
		boleto.addTextosExtras("txtRsAgenciaCodigoCedente", agenciaCodigoCedente.toString());
		boleto.addTextosExtras("txtRsSacado", sacado.getNome());
		String numeroInscricaoFormatado = BoletoUtil.formatarNumeroInscricao(sacado.getNumeroInscricao());
		boleto.addTextosExtras("txtRsSacadoCpfCnpj", numeroInscricaoFormatado);
		boleto.addTextosExtras("txtFcSacadoL1", sacado.getNome());
		boleto.addTextosExtras("txtFcSacadoCpfCnpj", numeroInscricaoFormatado);
		String cnpjEmpresa = BoletoUtil.formatarNumeroInscricao(empresa.getCnpj());
		boleto.addTextosExtras("txtRsCpfCnpj", cnpjEmpresa);
		boleto.addTextosExtras("txtFcCpfCnpj", cnpjEmpresa);
		boleto.addTextosExtras("txtRsEnderecoCedente", empresa.getRua());
		boleto.addTextosExtras("txtFcEnderecoCedente", empresa.getRua());
		boleto.addTextosExtras("txtFcEspecie", TipoDeMoeda.REAL.write());
		boleto.addTextosExtras("txtFcCarteira", "0" + carteira.getCodigo());
		String nossoNumeroFormatado = getNossoNumeroFormatado(titulo);
		boleto.addTextosExtras("txtRsNossoNumero", nossoNumeroFormatado);
		boleto.addTextosExtras("txtFcNossoNumero", nossoNumeroFormatado);
		Image logo = new javax.swing.ImageIcon(getClass().getResource("/imagens/caixa_nome.gif")).getImage();
		boleto.addImagensExtras("txtRsLogoBanco", logo);
		boleto.addImagensExtras("txtFcLogoBanco", logo);
		return this;
	}

	/**
	 * Método responsável por retorna o campo nosso número formatado.
	 * 
	 * Quando informado pelo Cliente/Cedente, o Nosso Número deverá obedecer o
	 * seguinte formato: CCNNNNNNNNNNNNNNN, onde: CC = 11 (título Registrado
	 * emissão CAIXA) CC = 14 (título Registrado emissão Cedente) CC = 21
	 * (título Sem Registro emissão CAIXA) NNNNNNNNNNNNNNN = Número livre
	 * 
	 * @param titulo
	 * @return
	 */
	private String getNossoNumeroFormatado(Titulo titulo) {
		String nossoNumero = titulo.getNossoNumero();
		String CC = nossoNumero.substring(0, 2);
		String NNNNNNNNNNNNNNN = nossoNumero.substring(2, nossoNumero.length());
		StringBuilder nossoNumeroFormatado = new StringBuilder();
		nossoNumeroFormatado.append(CC).append("/").append(NNNNNNNNNNNNNNN).append("-").append(titulo.getDigitoDoNossoNumero());
		return nossoNumeroFormatado.toString();
	}

	public Boleto build() {
		return this.boleto;
	}
}
