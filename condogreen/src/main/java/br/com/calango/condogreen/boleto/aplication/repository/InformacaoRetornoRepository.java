package br.com.calango.condogreen.boleto.aplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.boleto.domain.InformacaoRetorno;

@RepositoryRestResource(collectionResourceRel = "informacoesRetorno", path = "informacoesRetorno")
public interface InformacaoRetornoRepository extends JpaRepository<InformacaoRetorno, Long>, JpaSpecificationExecutor<InformacaoRetorno> {

}
