package br.com.calango.condogreen.condominio.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;

import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.calango.condogreen.condominio.aplication.resource.ApartamentoMorador;
import br.com.calango.condogreen.core.domain.BasicEntity;
import br.com.calango.condogreen.pessoa.domain.IPessoa;
import br.com.calango.condogreen.pessoa.domain.Pessoa;

/**
 * 
 * Representa os moradores do condomínio.
 * 
 * @author Guilherme Andrade
 *
 */
@Entity
@Table(name = "morador")
@Proxy(proxyClass = IMorador.class)
public class Morador extends BasicEntity implements IMorador {
 
	public static final String FK_ID = "morador_id";

	@Valid
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Pessoa.class)
	@JoinColumn(name = Pessoa.FK_ID)
	private IPessoa pessoa;

	@Type(type = "org.hibernate.type.BinaryType")
	@Column(name = "imagem_perfil")
	private byte[] imagemPerfil;

	@JsonManagedReference
	@OneToMany(mappedBy = "morador", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<ApartamentoMorador> apartamentos;
	
	public Morador() {
		this.apartamentos = new ArrayList<>();
	}

	public Morador(IPessoa pessoa) {
		this.pessoa = pessoa;
	}

	/**
	 * @param pessoa
	 *            the pessoa to set
	 */
	public void setPessoa(IPessoa pessoa) {
		this.pessoa = pessoa;
	}

	/**
	 * @return the imagemPerfil
	 */
	public byte[] getImagemPerfil() {
		return imagemPerfil;
	}

	/**
	 * @param imagemPerfil
	 *            the imagemPerfil to set
	 */
	public void setImagemPerfil(byte[] imagemPerfil) {
		this.imagemPerfil = imagemPerfil;
	}

	/**
	 * @return Nome do morador
	 */
	public String getNome() {
		return this.pessoa.getNome();
	}

	/**
	 * @see br.com.calango.condogreen.boleto.domain.ResponsavelPorPagarBoleto#getPessoa()
	 */
	public IPessoa getPessoa() {
		return this.pessoa;
	}

	public String getApartamentosFormatados() {
		StringBuilder bd = new StringBuilder();
		apartamentos.sort((ApartamentoMorador a1, ApartamentoMorador a2)-> 
		a1.getIdentificacaoRecebedorBoleto().compareTo(a2.getIdentificacaoRecebedorBoleto()));
		apartamentos.forEach((apartamentoMorador)-> 
		bd.append(apartamentoMorador.getIdentificacaoRecebedorBoleto()).append(" <br> "));
		
		return bd.toString();
	}

	public List<ApartamentoMorador> getApartamentos() {
		return apartamentos;
	}

	public void setApartamentos(List<ApartamentoMorador> apartamentos) {
		this.apartamentos = apartamentos;
	}

	public void addApartamento(List<ApartamentoMorador> apartamentos) {
		this.apartamentos.clear();
		this.apartamentos.addAll(apartamentos);
	}

	public String getEmail() {
		return this.pessoa.getEmail();
	}

	public String getCelular() {
		return this.pessoa.getCelularWithCodigoArea();
	}

	public String getTipoPessoa() {
		return this.pessoa.getTipoInscricao();
	}
	
}
