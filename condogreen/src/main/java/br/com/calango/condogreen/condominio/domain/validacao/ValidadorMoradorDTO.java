package br.com.calango.condogreen.condominio.domain.validacao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.calango.condogreen.condominio.aplication.resource.MoradorDTO;
import br.com.calango.condogreen.infrastructure.exception.AbstractValidator;
import br.com.calango.condogreen.infrastructure.exception.BusinessException;

@Component("validadorMoradorDTO")
public class ValidadorMoradorDTO extends AbstractValidator<MoradorDTO> {

	@Autowired
	private NaoExisteMoradorResponsavelPorPagamentoParaOApartamento naoExisteMoradorResponsavelPorPagamentoParaOApartamento;

	@Override
	public void registerRulesSpecification() throws BusinessException {
		rules.put(naoExisteMoradorResponsavelPorPagamentoParaOApartamento, new String[] { "Existem apartamentos que já possui responsável pelo boleto, por favor verifique." });
	}

}
