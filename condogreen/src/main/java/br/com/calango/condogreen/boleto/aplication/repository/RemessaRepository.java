package br.com.calango.condogreen.boleto.aplication.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.boleto.domain.remessa.Remessa;

@RepositoryRestResource(collectionResourceRel = "remessas", path = "remessas")
public interface RemessaRepository extends JpaRepository<Remessa, Long>, JpaSpecificationExecutor<Remessa> {

	/**
	 * 
	 * @return Última remessa pelo número.
	 */
	Remessa findTopByOrderByNumeroRemessaDesc();

	Page<Remessa> findAllByOrderByAtualizacaoDesc(Pageable pePageable);

}
