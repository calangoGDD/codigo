package br.com.calango.condogreen.boleto.domain.remessa;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeCobranca;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeTitulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo.Aceite;
import org.jrimum.texgit.FlatFile;
import org.jrimum.texgit.Record;

import br.com.calango.condogreen.boleto.domain.CodigoCarteira;
import br.com.calango.condogreen.boleto.domain.CodigoDesconto;
import br.com.calango.condogreen.boleto.domain.CodigoDevolucao;
import br.com.calango.condogreen.boleto.domain.CodigoJurosMora;
import br.com.calango.condogreen.boleto.domain.CodigoMulta;
import br.com.calango.condogreen.boleto.domain.CodigoProtesto;
import br.com.calango.condogreen.boleto.domain.ConfiguradorBoleto;
import br.com.calango.condogreen.boleto.domain.InfoBoleto;
import br.com.calango.condogreen.boleto.util.RemessaUtil;
import br.com.calango.condogreen.pessoa.domain.IPessoa;
import br.com.calango.condogreen.pessoa.domain.PessoaFisica;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

public class SegmentoPCaixa240 {

	private Record segmentoP;
	private ConfiguradorBoleto configurador;

	public SegmentoPCaixa240(Record segmentoP, ConfiguradorBoleto configurador) {
		this.segmentoP = segmentoP;
		this.configurador = configurador;
	}

	public SegmentoPCaixa240(Record record) {
		this.segmentoP = record;
	}

	/**
	 * @return the segmentoP
	 */
	public Record getRecordSegmentoP() {
		return segmentoP;
	}

	public void setDetNumReg(int seq) {
		segmentoP.setValue("detNumReg", seq);
	}

	public void setDetAgenciaConta(String agencia) {
		segmentoP.setValue("detAgenciaConta", agencia);
	}

	public void setDetAgenciaVerificador(int agenciaVerificador) {
		segmentoP.setValue("detAgenciaVerificador", agenciaVerificador);
	}

	public void setDetCodConvenio(String codBeneficiario) {
		segmentoP.setValue("detCodConvenio", codBeneficiario);
	}

	public void setNossoNumero(Long nossoNumero) {
		segmentoP.setValue("NossoNumero", nossoNumero);
	}

	public String getNossoNumero() {
		return segmentoP.getValue("NossoNumero");
	}

	public void setNumeroDocumento(Long numeroDocumento) {
		segmentoP.setValue("NumDocumento", numeroDocumento);
	}

	public String getNumeroDocumento() {
		return segmentoP.getValue("NumDocumento");
	}

	public void setDetUsoExclusivo4() {
		segmentoP.setValue("detUsoExclusivo4", StringUtils.repeat(" ", 4));
	}

	public void setDataVencimento(LocalDate dataVencimento) {
		segmentoP.setValue("DataVencimento", dataVencimento.format(DateTimeFormatter.ofPattern(InfoBoleto.DATA_DDMMYYYY)));
	}

	public LocalDate getDataVencimento() {
		DateTimeFormatter df = DateTimeFormatter.ofPattern(InfoBoleto.DATA_DDMMYYYY);
		return LocalDate.parse(segmentoP.getValue("DataVencimento"), df);
	}

	public void setValor(BigDecimal valor) {
		segmentoP.setValue("valor", RemessaUtil.formatarNumeroComDuasCasasDecimais(valor));
	}

	public BigDecimal getValor() {
		return segmentoP.getValue("valor");
	}

	public void setAceite(String aceite) {
		segmentoP.setValue("Aceite", aceite);
	}

	public Aceite getAceite() {
		String aceite = segmentoP.getValue("Aceite");
		if (Aceite.A.equals(aceite)) {
			return Aceite.A;
		} else {
			return Aceite.N;
		}
	}

	public void setEmissaoTitulo(Date dateEmissao) {
		segmentoP.setValue("EmissaoTitulo", dateEmissao);
	}

	public LocalDate getEmissaoTitulo() {
		DateTimeFormatter df = DateTimeFormatter.ofPattern(InfoBoleto.DATA_DDMMYYYY);
		return LocalDate.parse(segmentoP.getValue("EmissaoTitulo"), df);
	}

	public void setMora(Integer codMora) {
		segmentoP.setValue("Mora", codMora);
	}

	private int getMora() {
		return segmentoP.getValue("Mora");
	}

	public void setJurosMora(BigDecimal valorMora) {
		segmentoP.setValue("JurosMora", RemessaUtil.formatarNumeroComDuasCasasDecimais(valorMora));
	}

	public BigDecimal getJurosMora() {
		return segmentoP.getValue("JurosMora");
	}

	public void setUsoEmpCedente(String numeroDocumento) {
		segmentoP.setValue("UsoEmpCedente", numeroDocumento);
	}

	public void setCodProtesto(String codProtesto) {
		segmentoP.setValue("codProtesto", codProtesto);
	}

	private int getCodProtesto() {
		return segmentoP.getValue("codProtesto");
	}

	public void setPrazoProtesto(Integer prazoProtesto) {
		segmentoP.setValue("prazoProtesto", prazoProtesto);
	}

	public Integer getPrazoProtesto() {
		return segmentoP.getValue("prazoProtesto");
	}

	public Integer getDetCodMovRem() {
		return segmentoP.getValue("detCodMovRem");
	}

	public void setDetCodMovRem(String detCodMovRem) {
		segmentoP.setValue("detCodMovRem", detCodMovRem);
	}

	public void setEspecie(String especieDoTitulo) {
		segmentoP.setValue("Especie", especieDoTitulo);
	}

	public Integer getEspecie() {
		return segmentoP.getValue("Especie");
	}

	public void setCodDevolucao(String codDevolucao) {
		segmentoP.setValue("codDevolucao", codDevolucao);
	}

	private int getCodDevolucao() {
		return segmentoP.getValue("codDevolucao");
	}

	public void setPrazoDevolucao(int prazoDevolucao) {
		segmentoP.setValue("prazoDevolucao", prazoDevolucao);
	}

	private int getPrazoDevolucao() {
		return segmentoP.getValue("prazoDevolucao");
	}

	private void setCodDesconto(int codigo) {
		segmentoP.setValue("codDesconto", codigo);
	}

	private int getCodDesconto() {
		return segmentoP.getValue("codDesconto");
	}

	private void setValorDesconto(BigDecimal valorDesconto) {
		segmentoP.setValue("ValorDesconto", RemessaUtil.formatarNumeroComDuasCasasDecimais(valorDesconto));
	}

	private BigDecimal getValorDesconto() {
		return segmentoP.getValue("ValorDesconto");
	}

	private long getNuRemessa() {
		return Long.valueOf(segmentoP.getValue("nuRemessa").toString().replace(" ", ""));
	}

	private void setDataDesconto(InfoBoleto boleto, ConfiguradorBoleto configurador2) {
		String dataLimite = boleto.getDataLimiteDescontoPelaDataVencimento(configurador.getDiaDeLimiteDesconto());
		segmentoP.setValue("DataDesconto", dataLimite);
	}

	private Integer getDiaDesconto() {
		String valueFromXml = segmentoP.getValue("DataDesconto").toString();
		int resultado = 0;
		if (valueFromXml.length() >= 3) {
			resultado = Integer.valueOf(valueFromXml.substring(0, 2));
		}
		return resultado;
	}

	public int criarDetalheSegmentoP(final FlatFile<Record> ff, final InfoBoleto boleto, int sequencia, PessoaJuridica empresa,
	        LocalDate dataVencimento, boolean comSegmentoR) {
		System.out.println("**************** SEGMENTOP " + sequencia + " ****************");
		this.setDetNumReg(sequencia);
		this.setDetCodMovRem(boleto.getCodMovimento());
		this.setDetAgenciaConta(empresa.getAgencia());
		this.setDetAgenciaVerificador(empresa.getDigitoAgencia());
		this.setEspecie(configurador.getEspecieDoTitulo().getCodigo() + "");
		this.setDetCodConvenio(empresa.getCodigoBeneficiario());
		this.setNossoNumero(Long.valueOf(boleto.getNossoNumero()));
		this.setNumeroDocumento(Long.valueOf(boleto.getNumeroDocumento()));
		this.setDetUsoExclusivo4();
		this.setDataVencimento(dataVencimento);
		this.setValor(boleto.getValor());
		this.setAceite(boleto.getAceite().toString());
		this.setEmissaoTitulo(Date.from(boleto.getDataEmissao().atZone(ZoneId.systemDefault()).toInstant()));
		this.setMora(configurador.getCodMora().getCodigo());
		this.setJurosMora(boleto.calcularValorMora());
		this.setDateJurosMora(boleto.getDataJurosMora());
		CodigoDesconto codDesconto = configurador.getCodDesconto();
		this.setCodDesconto(codDesconto.getCodigo());
		if (!CodigoDesconto.SEM_DESCONTO.equals(codDesconto)) {
			this.setDataDesconto(boleto, configurador);
		}
		this.setValorDesconto(boleto.getValorDesconto());
		this.setUsoEmpCedente(boleto.getNumeroDocumentoFormatado());
		this.setCodProtesto(configurador.getCodProtesto().getCodigo().toString());
		this.setPrazoProtesto(configurador.getPrazoProtesto());
		this.setCodDevolucao(configurador.getCodDevolucao().getCodigo().toString());
		this.setPrazoDevolucao(configurador.getPrazoDevolucao());

		sequencia++;

		SegmentoQCaixa240 segmentoQ = createDetailSegmentoQ(ff, boleto, sequencia);
		segmentoP.addInnerRecord(segmentoQ.getSegmento());

		if (comSegmentoR) {
			sequencia++;
			SegmentoRCaixa240 segmentoR = createDetailSegmentoR(ff, boleto, sequencia);
			segmentoP.addInnerRecord(segmentoR.getSegmento());
		}

		return sequencia;
	}

	private void setDateJurosMora(String dataVencimentoFormatada) {
		segmentoP.setValue("DataMora", dataVencimentoFormatada);
	}

	public ConfiguradorBoleto extrairConfiguradorSegmentoP() {
		ConfiguradorBoleto configuradorBoleto = new ConfiguradorBoleto();
		configuradorBoleto.setAceite(getAceite());
		configuradorBoleto.setCodDesconto(CodigoDesconto.values()[this.getCodDesconto()]);
		configuradorBoleto.setCodDevolucao(CodigoDevolucao.getEnum(this.getCodDevolucao()));
		configuradorBoleto.setCodMora(CodigoJurosMora.getEnum(this.getMora()));
		configuradorBoleto.setCodProtesto(CodigoProtesto.getEnum(this.getCodProtesto()));
		configuradorBoleto.setDiaDeLimiteDesconto(getDiaDesconto());
		configuradorBoleto.setEspecieDoTitulo(TipoDeTitulo.valueOf(getEspecie()));
		configuradorBoleto.setValor(getValor());
		configuradorBoleto.setValorMora(getJurosMora());
		configuradorBoleto.setValorDesconto(getValorDesconto());
		configuradorBoleto.setPrazoDevolucao(getPrazoDevolucao());
		configuradorBoleto.setPrazoProtesto(getPrazoProtesto());
		configuradorBoleto.setCodMulta(CodigoMulta.VALOR_FIXO);
		configuradorBoleto.setValorMulta(BigDecimal.valueOf(2.40));
		return configuradorBoleto;
	}

	private SegmentoRCaixa240 createDetailSegmentoR(final FlatFile<Record> ff, final InfoBoleto boleto, int sequencia) {
		SegmentoRCaixa240 segmentoR = new SegmentoRCaixa240(ff.createRecord("SegmentoR"));
		System.out.println("************* segmento r ****************");
		segmentoR.setNumRegistro(sequencia);
		segmentoR.setCodMulta(configurador.getCodMulta().getCodigo());
		segmentoR.setDataMulta(boleto.getDataVencimentoFormatada());
		segmentoR.setMulta(configurador.getValorMulta());
		segmentoR.setInstrucaoAoSacado(configurador.getInstrucaoAoSacado());
		segmentoR.setInstrucao1(configurador.getInstrucao1());
		segmentoR.setInstrucao2(configurador.getInstrucao2());
		segmentoR.setCodMovimento(boleto.getCodMovimento());
		return segmentoR;
	}

	public SegmentoQCaixa240 createDetailSegmentoQ(final FlatFile<Record> ff, final InfoBoleto boleto, int sequencia) {
		SegmentoQCaixa240 segmentoQ = new SegmentoQCaixa240(ff.createRecord("SegmentoQ"));
		IPessoa pessoa = boleto.getSacado();
		System.out.println("************* segmento q ****************");
		segmentoQ.setNumRegistro(sequencia);
		segmentoQ.setTipoInscricao(pessoa.getTipoInscricao());
		segmentoQ.setNumInscricao(pessoa.getNumeroInscricao());
		segmentoQ.setNome(pessoa.getNome());
		segmentoQ.setEndereco(pessoa.getRua() + " " + pessoa.getNumero());
		segmentoQ.setBairro(pessoa.getBairro());
		segmentoQ.setCep(pessoa.getCep());
		segmentoQ.setSufixoCep(pessoa.getSufixoCep());
		segmentoQ.setCidade(pessoa.getCidade());
		segmentoQ.setUf(pessoa.getUf());
		segmentoQ.setCodMovimento(boleto.getCodMovimento());
		return segmentoQ;

	}

	public Remessa extrairRemessa() {
		Remessa remessa = new Remessa();
		long nuRemessa = getNuRemessa();
		remessa.setNumeroRemessa(nuRemessa);
		StringBuilder nomeRemessa = new StringBuilder();
		nomeRemessa.append("E").append(nuRemessa).append(".REM");
		remessa.setNome(nomeRemessa.toString());
		return remessa;
	}

	public InfoBoleto extrairBoleto(PessoaFisica sacado, ConfiguradorBoleto configuradorBoletoDaLista) {
		LocalDate dataVencimento = getDataVencimento();
		InfoBoleto infoBoleto = new InfoBoleto();
		infoBoleto.setConfiguradorBoleto(configuradorBoletoDaLista);
		infoBoleto.setCodigoDaCarteira(CodigoCarteira.SIMPLES);
		infoBoleto.setModalidade(TipoDeCobranca.COM_REGISTRO);
		infoBoleto.setDataEmissao(LocalDateTime.of(getEmissaoTitulo(), LocalTime.now()));
		infoBoleto.setNossoNumero(getNossoNumero());
		infoBoleto.setNumeroDocumento(getNumeroDocumento());
		infoBoleto.setSacado(sacado);
		infoBoleto.setValor(configuradorBoletoDaLista.getValor());
		infoBoleto.setDataVencimento(dataVencimento);

		return infoBoleto;
	}
}
