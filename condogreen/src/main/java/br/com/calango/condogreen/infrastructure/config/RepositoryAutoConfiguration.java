package br.com.calango.condogreen.infrastructure.config;

import java.util.EnumSet;
import java.util.concurrent.TimeUnit;

import javax.servlet.DispatcherType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.MimeMappings;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import br.com.calango.condogreen.infrastructure.AppConfig;
import br.com.calango.condogreen.infrastructure.LocalDateConverter;
import br.com.calango.condogreen.infrastructure.LocalDateTimeConverter;

@Configuration
@EnableAsync
@ComponentScan(basePackages = { "br.com.calango.condogreen" })
public class RepositoryAutoConfiguration extends RepositoryRestConfigurerAdapter implements EmbeddedServletContainerCustomizer {

	@Autowired
	private MessageSource messageSource;

	@Bean
	public Validator validator() {
		LocalValidatorFactoryBean validatorFactory = new LocalValidatorFactoryBean();
		validatorFactory.setValidationMessageSource(messageSource);
		return validatorFactory;
	}

	@Override
	public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
		validatingListener.addValidator("afterCreate", validator());
		validatingListener.addValidator("beforeCreate", validator());
		validatingListener.addValidator("afterSave", validator());
		validatingListener.addValidator("beforeSave", validator());
	}

	@Override
	public void customize(ConfigurableEmbeddedServletContainer container) {
		MimeMappings mappings = new MimeMappings(MimeMappings.DEFAULT);
		mappings.add("html", "text/html;charset=UTF-8");
		container.setMimeMappings(mappings);
		container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, AppConfig.CONTEXT_ROOT + "/notfound.html"));
		container.setSessionTimeout(10, TimeUnit.MINUTES);
	}

	@Override
	public void configureConversionService(ConfigurableConversionService conversionService) {
		conversionService.addConverter(new LocalDateConverter("yyyy-MM-dd"));
		conversionService.addConverter(new LocalDateTimeConverter("dd/MM/yyyy HH:mm:ss"));
	}

	@Bean
	public FilterRegistrationBean commonsRequestLoggingFilter() {
		final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new br.com.calango.condogreen.infrastructure.config.SimpleCORSFilter());
		registrationBean.setDispatcherTypes(EnumSet.allOf(DispatcherType.class));
		return registrationBean;
	}
}
