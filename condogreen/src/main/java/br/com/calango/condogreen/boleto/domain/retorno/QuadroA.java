package br.com.calango.condogreen.boleto.domain.retorno;

import java.util.HashMap;
import java.util.Map;

public class QuadroA implements QuadroDetalhamento {

	private static  Map<String, String> quadroA =  new HashMap<>();
	
	static{
		quadroA.put("01", 	"Código do Banco Inválido");
		quadroA.put("02", 	"Código do Registro Inválido");
		quadroA.put("03", 	"Código do Segmento Inválido");
		quadroA.put("04", 	"Código do Movimento não Permitido p/ Carteira");
		quadroA.put("05", 	"Código do Movimento Inválido");
		quadroA.put("06", 	"Tipo Número Inscrição Cedente Inválido");
		quadroA.put("07", 	"Agencia/Conta/DV Inválidos");
		quadroA.put("08", 	"Nosso Número Inválido");
		quadroA.put("09", 	"Nosso Número Duplicado");
		quadroA.put("10", 	"Carteira Inválida");
		quadroA.put("11", 	"Data de Geração Inválida");
		quadroA.put("12", 	"Tipo de Documento Inválido");
		quadroA.put("13", 	"Identif. Da Emissão do Bloqueto Inválida");
		quadroA.put("14", 	"Identif. Da Distribuição do Bloqueto Inválida");
		quadroA.put("15", 	"Características Cobrança Incompatíveis");
		quadroA.put("16", 	"Data de Vencimento Inválida");
		quadroA.put("17", 	"Data de Vencimento Anterior a Data de Emissão");
		quadroA.put("18", 	"Vencimento fora do prazo de operação");
		quadroA.put("19", 	"Título a Cargo de Bco Correspondentes c/ Vencto Inferior a XX Dias");
		quadroA.put("20", 	"Valor do Título Inválido");
		quadroA.put("21", 	"Espécie do Título Inválida");
		quadroA.put("22", 	"Espécie do Título Não Permitida para a Carteira");
		quadroA.put("23", 	"Aceite Inválido");
		quadroA.put("24", 	"Data da Emissão Inválida");
		quadroA.put("25", 	"Data da Emissão Posterior a Data de Entrada");
		quadroA.put("26", 	"Código de Juros de Mora Inválido");
		quadroA.put("27", 	"Valor/Taxa de Juros de Mora Inválido");
		quadroA.put("28", 	"Código do Desconto Inválido");
		quadroA.put("29", 	"Valor do Desconto Maior ou Igual ao Valor do Título");
		quadroA.put("30", 	"Desconto a Conceder Não Confere");
		quadroA.put("31", 	"Concessão de Desconto - Já Existe Desconto Anterior");
		quadroA.put("32",	"Valor do IOF Inválido ");
		quadroA.put("33",	"Valor do Abatimento Inválido");
		quadroA.put("34",	"Valor do Abatimento Maior ou Igual ao Valor do Título");
		quadroA.put("35",	"Valor Abatimento a Conceder Não Confere");
		quadroA.put("36",	"Concessão de Abatimento - Já Existe Abatimento Anterior ");
		quadroA.put("37",	"Código para Protesto Inválido ");
		quadroA.put("38",	"Prazo para Protesto Inválido");
		quadroA.put("39",	"Pedido de Protesto Não Permitido para o Título");
		quadroA.put("40",	"Título com Ordem de Protesto Emitida");
		quadroA.put("41",	"Pedido Cancelamento/Sustação p/ Títulos sem Instrução Protesto");
		quadroA.put("42",	"Código para Baixa/Devolução Inválido");
		quadroA.put("43",	"Prazo para Baixa/Devolução Inválido ");
		quadroA.put("44",	"Código da Moeda Inválido");
		quadroA.put("45",	"Nome do Sacado Não Informado");
		quadroA.put("46",	"Tipo/Número de Inscrição do Sacado Inválidos ");
		quadroA.put("47",	"Endereço do Sacado Não Informado ");
		quadroA.put("48",	"CEP Inválido ");
		quadroA.put("49",	"CEP Sem Praça de Cobrança (Não Localizado)");
		quadroA.put("50",	"CEP Referente a um Banco Correspondente");
		quadroA.put("51",	"CEP incompatível com a Unidade da Federação");
		quadroA.put("52",	"Unidade da Federação Inválida ");
		quadroA.put("53",	"Tipo/Número de Inscrição do Sacador/Avalista Inválidos");
		quadroA.put("54",	"Sacador/Avalista Não Informado");
		quadroA.put("55",	"Nosso número no Banco Correspondente Não Informado");
		quadroA.put("56",	"Código do Banco Correspondente Não Informado ");
		quadroA.put("57",	"Código da Multa Inválido");
		quadroA.put("58",	"Data da Multa Inválida");
		quadroA.put("59",	"Valor/Percentual da Multa Inválido");
		quadroA.put("60",	"Movimento para Título Não Cadastrado");
		quadroA.put("61",	"Alteração da Agência Cobradora/DV Inválida");
		quadroA.put("62",	"Tipo de Impressão Inválido ");
		quadroA.put("63",	"Entrada para Título já Cadastrado");
		quadroA.put("64",	"Entrada Inválida para Cobrança Caucionada ");
		quadroA.put("65",	"CEP do Sacado não encontrado");
		quadroA.put("66",	"Agencia Cobradora não encontrada ");
		quadroA.put("67",	"Agencia Cedente não encontrada");
		quadroA.put("68",	"Movimentação inválida para título");
		quadroA.put("69",	"Alteração de dados inválida");
		quadroA.put("70",	"Apelido do cliente não cadastrado");
		quadroA.put("71",	"Erro na composição do arquivo ");
		quadroA.put("72",	"Lote de serviço inválido");
		quadroA.put("73",	"Código do Cedente inválido ");
		quadroA.put("74",	"Cedente não pertencente a Cobrança Eletrônica");
		quadroA.put("75",	"Nome da Empresa inválido");
		quadroA.put("76",	"Nome do Banco inválido");
		quadroA.put("77",	"Código da Remessa inválido ");
		quadroA.put("78",	"Data/Hora Geração do arquivo inválida");
		quadroA.put("79",	"Número Sequencial do arquivo inválido");
		quadroA.put("80",	"Versão do Lay out do arquivo inválido");
		quadroA.put("81",	"Literal REMESSA-TESTE - Válido só p/ fase testes");
		quadroA.put("82",	"Literal REMESSA-TESTE - Obrigatório p/ fase testes");
		quadroA.put("83",	"Tp Número Inscrição Empresa inválido");
		quadroA.put("84",	"Tipo de Operação inválido");
		quadroA.put("85",	"Tipo de serviço inválido");
		quadroA.put("86",	"Forma de lançamento inválido");
		quadroA.put("87",	"Número da remessa inválido ");
		quadroA.put("88",	"Número da remessa menor/igual remessa anterior");
		quadroA.put("89",	"Lote de serviço divergente ");
		quadroA.put("90",	"Número sequencial do registro inválido ");
		quadroA.put("91",	"Erro seq de segmento do registro detalhe");
		quadroA.put("92",	"Cod movto divergente entre grupo de segm");
		quadroA.put("93",	"Qtde registros no lote inválido");
		quadroA.put("94",	"Qtde registros no lote divergente");
		quadroA.put("95",	"Qtde lotes no arquivo inválido");
		quadroA.put("96",	"Qtde lotes no arquivo divergente ");
		quadroA.put("97",	"Qtde registros no arquivo inválido");
		quadroA.put("98",	"Qtde registros no arquivo divergente");
		quadroA.put("99",	"Código de DDD inválido");
	}

	/**
	 * @return
	 */
	@Override
	public Map<String, String> getQuadro() {
		return quadroA;
	}

}
