package br.com.calango.condogreen.condominio.aplication.resource;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import br.com.calango.condogreen.condominio.aplication.service.NoticiaService;
import br.com.calango.condogreen.condominio.domain.Noticia;

@RepositoryRestController
@RequestMapping("/noticias")
public class NoticiaResource {

	@Autowired
	NoticiaService noticiaService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ResponseBody
	public byte[] importar(@RequestPart(value = "arquivo") MultipartFile arquivo, @RequestPart(value = "noticia") Noticia noticia) throws IOException {
		try {
			noticiaService.save(arquivo.getOriginalFilename(), arquivo.getBytes(), noticia);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arquivo.getBytes();
	}
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public Page<Noticia> search(Pageable pageable) {
		return noticiaService.findAll(pageable);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/get/{id:\\d+}")
	@ResponseBody
	public Noticia get(@PathVariable("id") long id) {
		return noticiaService.findById(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getQuatroPrimeiras")
	@ResponseBody
	public List<Noticia> getQuatroPrimeiras() {
		return noticiaService.getQuatroPrimeiras();
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "{id:\\d+}/delete")
	public void delete(@PathVariable("id") long id) throws IOException {
		noticiaService.delete(id);
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.PUT, value = "/ativar-desativar/{id:\\d+}")
	public void ativarDesativar(@PathVariable("id") long id) throws IOException {
		noticiaService.ativarDesativar(id);
	}
}
