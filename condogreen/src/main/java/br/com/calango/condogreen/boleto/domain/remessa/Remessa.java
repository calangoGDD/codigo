package br.com.calango.condogreen.boleto.domain.remessa;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.commons.io.FileUtils;
import org.jrimum.bopepo.Boleto;
import org.jrimum.bopepo.view.BoletoViewer;
import org.jrimum.texgit.FlatFile;
import org.jrimum.texgit.Record;

import br.com.calango.condogreen.boleto.domain.BoletoBuilder;
import br.com.calango.condogreen.boleto.domain.InfoBoleto;
import br.com.calango.condogreen.core.domain.BasicEntity;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.primitives.Bytes;

/**
 * 
 * Classe responsável pela geração da remessa do banco CAIXA 240
 * 
 * @author Guilherme Andrade
 *
 */
@Entity
@Table(name = "remessa")
public class Remessa extends BasicEntity {

	public static final String FK_ID = "remessa_id";

	@Column(name = "nome", length = 30)
	private String nome;

	@Column(name = "numero_remessa")
	private Long numeroRemessa;

	@Column(name = "qtd_boletos", nullable = false)
	private int quantidadeBoletos;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "remessa_boleto", joinColumns = { @JoinColumn(name = Remessa.FK_ID, nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = InfoBoleto.FK_ID, nullable = false, updatable = false) })
	private List<InfoBoleto> boletos;

	@Lob
	@Column(name = "arquivo")
	@JsonIgnore
	private byte[] arquivo;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = PessoaJuridica.FK_EMPRESA_ID)
	private PessoaJuridica empresa;

	public Remessa() {
		this.boletos = new ArrayList<InfoBoleto>();

	}

	public Remessa(Long numeroRemessa, List<InfoBoleto> boletos) {
		this();
		this.numeroRemessa = numeroRemessa;
		this.boletos = boletos;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getNumeroRemessa() {
		return numeroRemessa;
	}

	public void setNumeroRemessa(Long numeroRemessa) {
		this.numeroRemessa = numeroRemessa;
	}

	public int getQuantidadeBoletos() {
		return quantidadeBoletos;
	}

	public void setQuantidadeBoletos(int quantidadeBoletos) {
		this.quantidadeBoletos = quantidadeBoletos;
	}

	public List<InfoBoleto> getBoletos() {
		return boletos;
	}

	public void setBoletos(List<InfoBoleto> boletos) {
		this.boletos = boletos;
	}

	@JsonIgnore
	public byte[] getArquivo() {
		return arquivo;
	}

	@JsonIgnore
	public File getArquivoComoFile() throws IOException {
		File file = null;
		if (arquivo != null) {
			file = File.createTempFile(String.format("%s_%d", "rem", this.numeroRemessa), ".txt");
			FileUtils.writeByteArrayToFile(file, arquivo);
		}
		return file;
	}

	public void addBoleto(InfoBoleto boleto) {
		this.boletos.add(boleto);
	}

	/**
	 * Método responsável pela geração da remessa
	 * 
	 * @return
	 * @throws IOException
	 */
	public LeiauteCaixa240 geraRemessa(final PessoaJuridica empresa, boolean comSegmentoR) throws IOException {
		LeiauteCaixa240 leiaute = new LeiauteCaixa240();
		if (boletos.size() != 0) {
			this.empresa = empresa;
			leiaute.criarHeader(empresa, numeroRemessa);
			leiaute.criarHeaderLote(empresa, numeroRemessa);
			leiaute.processarLoteDeBoletos(empresa, boletos, comSegmentoR);
			leiaute.criarTrailerLote(boletos.size());
			leiaute.criarTrailerArquivo();
			gerarArquivo(leiaute);
		}
		return leiaute;
	}

	/**
	 * Método responsável por gerar arquivo.
	 * 
	 * @param leiaute
	 * @throws IOException
	 */
	private void gerarArquivo(LeiauteCaixa240 leiaute) throws IOException {
		FlatFile<Record> flatFile = leiaute.getFlatFile();
		StringBuilder nomeRemessa = new StringBuilder();
		nomeRemessa.append("E").append(numeroRemessa).append(".REM");
		List<String> linhas = flatFile.write();
		byte[] linhasBytes = new byte[0];
		for (String linha : linhas) {
			linha = linha + "\n";
			linhasBytes = Bytes.concat(linhasBytes, linha.getBytes("UTF8"));
		}
		nome = nomeRemessa.toString();
		this.arquivo = linhasBytes;
	}

	public byte[] getBoletosConcatenados(PessoaJuridica empresa) {
		List<Boleto> boletosConcatenados = new ArrayList<>();
		for (InfoBoleto infoBoleto : boletos) {
			Boleto boleto = new BoletoBuilder(empresa, infoBoleto).build();
			boletosConcatenados.add(boleto);
		}

		ClassLoader classLoader = getClass().getClassLoader();
		File templatePersonalizado = FileUtils.toFile(classLoader.getResource("./templates/templateCaixa.pdf"));
		return BoletoViewer.groupInOnePdfWithTemplate(boletosConcatenados, templatePersonalizado);
	}

	/**
	 * @return the fkId
	 */
	public static String getFkId() {
		return FK_ID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((numeroRemessa == null) ? 0 : numeroRemessa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Remessa other = (Remessa) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (numeroRemessa == null) {
			if (other.numeroRemessa != null)
				return false;
		} else if (!numeroRemessa.equals(other.numeroRemessa))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("INSERT INTO `remessa`").append("(`id`,`nome`, `numero_remessa`)").append("VALUES ('").append(getId()).append("', '")
		        .append(nome).append("', '").append(numeroRemessa).append("'); \n");

		for (InfoBoleto boleto : boletos) {
			builder.append("INSERT INTO `remessa_boleto` (`remessa_id`, `boleto_id`)").append("VALUES ('").append(getId())
			        .append("',(select b.id from boleto b where b.nosso_numero ='").append(boleto.getNossoNumero()).append("')); \n");
		}

		return builder.toString();
	}

	/**
	 * @return the empresa
	 */
	public PessoaJuridica getEmpresa() {
		return empresa;
	}

	/**
	 * @param empresa
	 *            the empresa to set
	 */
	public void setEmpresa(PessoaJuridica empresa) {
		this.empresa = empresa;
	}
}
