package br.com.calango.condogreen.condominio.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.calango.condogreen.boleto.domain.RecebedorBoleto;
import br.com.calango.condogreen.condominio.aplication.resource.ApartamentoMorador;

@Entity
@Table(name = "apartamento")
@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Apartamento extends RecebedorBoleto {

	public static final String FK_ID = "apartamento_id";

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = Bloco.FK_ID)
	private Bloco bloco;

	@NotBlank
	@Column(name = "nome", length = 30)
	private String nome;

	/**
	 * Donos do apartamento
	 */
	@JsonManagedReference
	@OneToMany(mappedBy = "apartamento", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ApartamentoMorador> moradores;

	@OneToMany(mappedBy = "apartamento", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Transporte> transportes;

	
	public Apartamento() {
		this.moradores = new ArrayList<>();
		this.transportes = new HashSet<>();
	}

	public Apartamento(String name, Bloco bloco) {
		this();
		this.nome = name;
		this.bloco = bloco;
	}

	/**
	 * @return the bloco
	 */
	public Bloco getBloco() {
		return bloco;
	}

	/**
	 * @param bloco
	 *            the bloco to set
	 */
	public void setBloco(Bloco bloco) {
		this.bloco = bloco;
	}

	/**
	 * @return the name
	 */
	@Override
	public String getNome() {
		return nome;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setNome(String name) {
		this.nome = name;
	}

	public List<ApartamentoMorador> getMoradores() {
		return moradores;
	}

	public void setMoradores(List<ApartamentoMorador> moradores) {
		this.moradores = moradores;
	}

	/**
	 * @return the transportes
	 */
	public Set<Transporte> getTransportes() {
		return transportes;
	}

	/**
	 * @param transportes
	 *            the transportes to set
	 */
	public void setTransportes(Set<Transporte> transportes) {
		this.transportes = transportes;
	}

	/**
	 * 
	 * @return O morador responsável por receber boleto.
	 */
	public ApartamentoMorador getResponsavelPorPagarBoleto() {
		return moradores.stream().filter(morador -> morador.isResponsavelBoleto()).findFirst().orElse(null);
	}

	/**
	 * @see br.com.calango.condogreen.boleto.domain.RecebedorBoleto#getIdentificacaoRecebedorBoleto()
	 */
	@Override
	public String getIdentificacaoRecebedorBoleto() {
		StringBuilder builder = new StringBuilder();
		builder.append(bloco.getNome()).append(" - ").append(this.getNome());
		return builder.toString();
	}

}
