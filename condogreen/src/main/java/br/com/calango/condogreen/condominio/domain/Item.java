package br.com.calango.condogreen.condominio.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.calango.condogreen.core.domain.BasicEntity;

/**
 * Representa os itens do condomínio. Ex.: Piscina, Salão de festas
 * 
 * @author Guilherme Andrade
 *
 */
@Entity
@Table(name = "item")
public class Item extends BasicEntity {

	/**
	 * 
	 */
	@Column(name = "nome")
	public String nome;

	@ManyToOne
	@JoinColumn(name = Condominio.FK_ID)
	private Condominio condominio;

	public Item(String nome) {
		this.nome = nome;
	}

	public Item() {
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

}
