package br.com.calango.condogreen.core.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Classe responsável por extrair as linhas do arquivo.
 * 
 * @author Guilherme Andrade
 *
 */
public class LeitorArquivo {

	private List<String> listLines = new ArrayList<>();
	private Scanner scanner;

	public LeitorArquivo(String path) {
		File file = new File(path);
		try {
			scanner = new Scanner(file);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public LeitorArquivo(File file) {
		try {
			scanner = new Scanner(file);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Método responsável por ler as linhas do arquivo e colocar na lista.
	 * 
	 * @return uma lista com as linhas que estão no arquivo.
	 */
	public List<String> getLinhas() {
		return lerArquivo();
	}

	/**
	 * 
	 * @return As linhas processadas no arquivo
	 */
	private List<String> lerArquivo() {
		try {
			scanner.useDelimiter(Pattern.compile("[\\r\\n;]+"));
			while (scanner.hasNextLine()) {
				listLines.add(scanner.next());
			}

		}
		catch (NoSuchElementException e) {
			e.printStackTrace();
		}
		finally {
			scanner.close();
		}

		return listLines;
	}

	/**
	 * 
	 * @return Apenas a primeira linha do arquivo, usado para arquivos que não
	 *         possuem mais de uma linha.
	 */
	public String getLinha() {
		String linha = "";
		Iterator<String> iterator = lerArquivo().iterator();
		if (iterator.hasNext()) {
			linha = iterator.next();
		}
		return linha;
	}
}
