package br.com.calango.condogreen.boleto.domain.validacao;

import org.springframework.stereotype.Component;

import br.com.calango.condogreen.boleto.domain.InfoBoleto;
import br.com.calango.condogreen.infrastructure.specification.ISpecification;

@Component("deveNotificarApenasBoletosComSituacaoEntradaConfirmada")
public class DeveNotificarApenasBoletosComSituacaoEntradaConfirmada implements ISpecification<InfoBoleto> {

	/**
	 * @see br.com.calango.condogreen.infrastructure.specification.ISpecification#isSatisfiedBy(java.lang.Object)
	 */
	@Override
	public boolean isSatisfiedBy(InfoBoleto boleto) {
		return boleto.getSituacaoBoleto().isSituacaoEntradaConfirmada();
	}

}
