package br.com.calango.condogreen.condominio.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import br.com.calango.condogreen.core.domain.BasicEntity;

/**
 * Representa os blocos do condomínio
 * 
 * @author Guilherme Andrade
 *
 */
@Entity
@Table(name = "bloco")
public class Bloco extends BasicEntity {

	public static final String FK_ID = "bloco_id";

	@NotBlank
	@NotNull
	@Column(name = "nome", length = 30)
	private String nome;

	@ManyToOne
	@JoinColumn(name = Condominio.FK_ID)
	private Condominio condominio;

	@OneToMany(mappedBy = "bloco", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Apartamento> apartamentos;

	public Bloco() {
		this.apartamentos = new ArrayList<Apartamento>();
	}

	public Bloco(String nome, int quantidadeDeApartamentosPorTorre) {
		this();
		this.nome = nome;
		buildApartaments(quantidadeDeApartamentosPorTorre);
	}

	public List<Apartamento> getApartamentos() {
		return apartamentos;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @param apartamentos
	 *            the apartamentos to set
	 */
	public void setApartamentos(List<Apartamento> apartamentos) {
		this.apartamentos = apartamentos;
	}

	/**
	 * Método responsável por construir os aparamentos.
	 * 
	 * @param quantityTowers
	 * @return
	 */
	public List<Apartamento> buildApartaments(
			int quantidadeDeApartamentosPorTorre) {
		int numeroAp = 0;
		for (int i = 0; i < quantidadeDeApartamentosPorTorre; i++) {
			numeroAp = numeroAp + 1;
			if (numeroAp == 6) {
				numeroAp = (1 * 100) + 1;
			} else if (numeroAp == 106) {
				numeroAp = (2 * 100) + 1;
			} else if (numeroAp == 206) {
				numeroAp = (3 * 100) + 1;
			}
			apartamentos.add(new Apartamento("AP " + numeroAp, this));
		}

		return apartamentos;
	}

}
