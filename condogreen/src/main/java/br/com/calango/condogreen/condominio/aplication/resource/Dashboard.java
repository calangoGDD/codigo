package br.com.calango.condogreen.condominio.aplication.resource;

import java.math.BigDecimal;

public class Dashboard {
	
	private BigDecimal valorTotalDespesas;
	
	private BigDecimal valorTotalReceitas;

	private BigDecimal valorTotalInadimplentes;
	
	public Dashboard(BigDecimal valorTotalDespesas, BigDecimal valorTotalReceitas, BigDecimal valorTotalInadimplentes) {
		super();
		this.valorTotalDespesas = valorTotalDespesas;
		this.valorTotalReceitas = valorTotalReceitas;
		this.valorTotalInadimplentes = valorTotalInadimplentes;
	}

	public BigDecimal getValorTotalDespesas() {
		return valorTotalDespesas;
	}

	public void setValorTotalDespesas(BigDecimal valorTotalDespesas) {
		this.valorTotalDespesas = valorTotalDespesas;
	}

	public BigDecimal getValorTotalReceitas() {
		return valorTotalReceitas;
	}

	public void setValorTotalReceitas(BigDecimal valorTotalReceitas) {
		this.valorTotalReceitas = valorTotalReceitas;
	}

	public BigDecimal getValorTotalInadimplentes() {
		return valorTotalInadimplentes;
	}

	public void setValorTotalInadimplentes(BigDecimal valorTotalInadimplentes) {
		this.valorTotalInadimplentes = valorTotalInadimplentes;
	}
	
	
}
