package br.com.calango.condogreen.boleto.domain.remessa;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.jrimum.texgit.Record;

import br.com.calango.condogreen.boleto.util.RemessaUtil;

public class SegmentoRCaixa240 {

	private Record segmentoR;

	public SegmentoRCaixa240(Record segmentoR) {
		this.segmentoR = segmentoR;
	}

	public void setNumRegistro(int seq) {
		segmentoR.setValue("rNumRegistro", seq);
	}

	public void setCodMulta(int codigo) {
		segmentoR.setValue("rCodMulta", codigo);
	}

	public void setDataMulta(String dataVencimentoFormatada) {
		segmentoR.setValue("rDataMulta", dataVencimentoFormatada);

	}

	public void setMulta(BigDecimal valorMora) {
		segmentoR.setValue("rMulta", valorMora);
	}

	public void setInstrucaoAoSacado(String instrucaoAoSacado) {
		if (instrucaoAoSacado != null) {
			segmentoR.setValue("rSacado", RemessaUtil.retiraCaracteresEspeciais(StringUtils.substring(instrucaoAoSacado, 0, 10)));
		}
	}

	public void setInstrucao1(String instrucao1) {
		if (instrucao1 != null) {
			segmentoR.setValue("rInfo", RemessaUtil.retiraCaracteresEspeciais(StringUtils.substring(instrucao1, 0, 40)));
		}
	}

	public void setInstrucao2(String instrucao2) {
		if (instrucao2 != null) {
			segmentoR.setValue("rInfo2", RemessaUtil.retiraCaracteresEspeciais(StringUtils.substring(instrucao2, 0, 40)));
		}
	}

	public Record getSegmento() {
		return segmentoR;
	}

	public void setCodMovimento(String codMovimento) {
		this.segmentoR.setValue("rCodMovimento", codMovimento);
	}

}
