package br.com.calango.condogreen.infrastructure.exception;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.rest.core.RepositoryConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class ExceptionHandlerController {

	@Autowired
	private MessageSource messageSource;

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public List<String> handle(Exception ex) {
		ex.printStackTrace();
		return Arrays.asList("Erro inesperado");
	}

	@ExceptionHandler(RepositoryConstraintViolationException.class)
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	public List<String> handle(RepositoryConstraintViolationException ex) {
		ex.printStackTrace();
		List<FieldError> fieldErrors = ex.getErrors().getFieldErrors();

		return fieldErrors.stream().map(f -> formatarMensagem(f.getDefaultMessage(), f.getField())).collect(Collectors.toList());
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	public List<String> handle(DataIntegrityViolationException ex) {
		ex.printStackTrace();

		return Arrays.asList(messageSource.getMessage("org.springframework.dao.DataIntegrityViolationException.message", new String[0],
				LocaleContextHolder.getLocale()));
	}

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public List<String> handle(ConstraintViolationException ex) {
		ex.printStackTrace();
		return parseAsListString(ex.getConstraintViolations());
	}

	@ExceptionHandler(BusinessException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public List<String> handle(BusinessException ex) {
		ex.printStackTrace();
		return parseMessagesAsListString(ex.getMessageExceptions());
	}

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public List<String> handle(AccessDeniedException ex) {
		ex.printStackTrace();
		return Arrays.asList("Autorização negada!");
	}

	private List<String> parseMessagesAsListString(Set<Message> messageExceptions) {
		return messageExceptions.stream().map(m -> m.getMessage()).collect(Collectors.toList());
	}

	private String formatarMensagem(String msg, String field) {
		return msg.replace("{field}", getLabel(field));
	}

	private List<String> parseAsListString(Set<ConstraintViolation<?>> constraintViolations) {
		return constraintViolations.stream().map(c -> formatarMensagem(c.getMessage(), c.getPropertyPath().toString())).collect(Collectors.toList());
	}

	private String getLabel(String field) {
		return messageSource.getMessage(field, new Object[0], LocaleContextHolder.getLocale());
	}
}
