package br.com.calango.condogreen.boleto.domain.retorno;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Guilherme
 *
 */
public enum DetalheRetornoEntrada {

	ENTRADA_CONFIRMADA("02","Entrada Confirmada", new QuadroA()), 
	ENTRADA_REJEITADA("03", "Entrada Rejeitada", new QuadroA()), 
	INSTRUCAO_REJEITADA("26","Intrução Rejeitada", new QuadroA()), 
	ALTERACAO_DADOS_REJEITADA("30","Alteração de Dados Rejeitada", new QuadroA()),
	DEBITO_DE_TARIFAS_CUSTAS("28","Débito de Tarifas/Custas", new QuadroB()),
	LIQUIDACAO("06","Liquidação", new QuadroC()),
	BAIXA("09","Baixa", new QuadroD());
	
	private String codigo;
	private String descricao;
	private static  Map<String, String> quadroE = new HashMap<>();
	private QuadroDetalhamento detalhamento;
	
	DetalheRetornoEntrada(String codigo, String descricao, QuadroDetalhamento detalhamento){
		this.setCodigo(codigo);
		this.descricao = descricao;
		this.detalhamento = detalhamento;
	}
	
	static {
		quadroE.put("01", "Dinheiro");
		quadroE.put("02", "Cheque");
	}

	private static Map<String, DetalheRetornoEntrada> map = new HashMap<String, DetalheRetornoEntrada>();

	static {
		for (DetalheRetornoEntrada detalhe : values()) {
			map.put(detalhe.getCodigo(), detalhe);
		}
	}

	public static DetalheRetornoEntrada getEnum(String codigo) {
		return map.get(codigo);
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public QuadroDetalhamento getDetalhamento() {
		return detalhamento;
	}

	public void setDetalhamento(QuadroDetalhamento detalhamento) {
		this.detalhamento = detalhamento;
	}

	/**
	 * Método responsável por retornar o detalhamento do retorno pelo código do enum passado e 
	 * @param codigo
	 * @param codigoDetalhe
	 * @return
	 */
	public static String getDetalheFormatado(String codigoMotivo, String codigosDetalhe){
		StringBuilder sb = new StringBuilder();
		DetalheRetornoEntrada enumDetalhe = getEnum(codigoMotivo);
		if(enumDetalhe!= null){
			sb.append(enumDetalhe.getDescricao());
			String codigoQuadro = codigosDetalhe.substring(0, 2);
			sb.append("/");
			String descricaoDetalhamento = enumDetalhe.getDetalhamento().getQuadro().get(codigoQuadro);
			
			if(descricaoDetalhamento != null){
				sb.append(descricaoDetalhamento);
			}
			
			if(LIQUIDACAO.equals(enumDetalhe)) {
				String dinheiroOuCheque = codigosDetalhe.substring(2, 4);
				sb.append("/");
				sb.append(quadroE.get(dinheiroOuCheque));
			}
		}
		return sb.toString();
	}
	
}
