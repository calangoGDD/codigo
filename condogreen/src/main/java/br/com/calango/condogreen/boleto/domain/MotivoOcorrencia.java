package br.com.calango.condogreen.boleto.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.calango.condogreen.core.domain.BasicEntity;

@Entity
@Table(name = "motivo_ocorrencia")
public class MotivoOcorrencia extends BasicEntity {

	@Column(name = "quadro")
	private String quadro;

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "descricao")
	private String descricao;

	public String getQuadro() {
		return quadro;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

}
