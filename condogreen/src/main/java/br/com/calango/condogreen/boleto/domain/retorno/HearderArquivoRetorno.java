package br.com.calango.condogreen.boleto.domain.retorno;

import java.util.Date;

import org.jrimum.texgit.FlatFile;
import org.jrimum.texgit.Record;

public class HearderArquivoRetorno {

	private Record record;

	public HearderArquivoRetorno(FlatFile<Record> flatFile) {
		record = flatFile.getRecord("HeaderArquivo");
	}

	public String getNomeEmpresa() {
		return record.getValue("Empresa-Nome");
	}

	public String getCnpj() {
		return record.getValue("Empresa-CPRF");
	}

	public Date getDataGeracao() {
		return record.getValue("Arquivo-GeracaoData");
	}
}
