package br.com.calango.condogreen.boleto.domain.retorno;

import java.util.HashMap;
import java.util.Map;

public class QuadroB implements QuadroDetalhamento {
	
	private static  Map<String, String> quadroB = new HashMap<>();
	
	static {
		quadroB.put("01", "Tarifa de Emissão de Extrato de Posição");
		quadroB.put("02", "Tarifa de Manutenção de Título Vencido");
		quadroB.put("03",  "Tarifa de Sustação");
		quadroB.put("04", "Tarifa de Protesto");
		quadroB.put("05", "Tarifa de Outras Instruções");
		quadroB.put("06", "Tarifa de Outras Ocorrências");
		quadroB.put("07", "Tarifa de Envio de Duplicata ao Sacado");
		quadroB.put("08", "Custas de Protesto");
		quadroB.put("09", "Custas de Sustação de Protesto");
		quadroB.put("10", "Custas de Cartório Distribuidor");
		quadroB.put("11", "Custas de Edital");
		quadroB.put("12", "Redisponibilização de Arquivo Retorno Eletrônico");
		quadroB.put("13", "Tarifa Sobre Registro Cobrada na Baixa/Liquidação");
		quadroB.put("14", "Tarifa Sobre Reapresentação Automática");
		quadroB.put("15", "Banco de Sacados");
		quadroB.put("16", "Tarifa Sobre Informações Via Fax");
		quadroB.put("17", "Entrega Aviso Disp Bloqueto via e-amail ao sacado (s/ emissão Bloqueto)");
		quadroB.put("18", "Emissão de Bloqueto Pré-impresso CAIXA matricial");
		quadroB.put("19", "Emissão de Bloqueto Pré-impresso CAIXA A4");
		quadroB.put("20", "Emissão de Bloqueto Padrão CAIXA");
	}

	public Map<String, String> getQuadro() {
		return quadroB;
	}

}
