package br.com.calango.condogreen.infrastructure.exception;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import br.com.calango.condogreen.infrastructure.specification.ISpecification;

public abstract class AbstractValidator<T> implements IValidator<T> {

	protected Map<ISpecification<T>, String[]> rules = new HashMap<ISpecification<T>, String[]>();

	@Override
	public void checkBrokenRules(T entity) throws BusinessException {

		registerRulesSpecification();

		BusinessException businessException = new BusinessException();
		for (Entry<ISpecification<T>, String[]> rule : rules.entrySet()) {
			if (!rule.getKey().isSatisfiedBy(entity)) {
				String[] params = rule.getValue();
				if (params.length > 1) {
					String[] args = Arrays.copyOfRange(rule.getValue(), 1, params.length);
					businessException.addException(new Message(params[0], Message.Type.DANGER, Arrays.toString(args)));
				} else {
					businessException.addException(new Message(params[0], Message.Type.DANGER));
				}
			}
		}
		if (businessException.hasChainOfMessageExceptions()) {
			throw businessException;
		}
	}

}