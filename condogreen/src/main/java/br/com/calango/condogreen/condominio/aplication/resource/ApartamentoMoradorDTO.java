package br.com.calango.condogreen.condominio.aplication.resource;

import org.hibernate.validator.constraints.NotBlank;

import com.sun.istack.NotNull;

public class ApartamentoMoradorDTO {
	
	@NotNull
	private Long apartamentoId;
	
	@NotBlank
	private String tipo;
	
	@NotNull
	private Boolean responsavelBoleto;

	private String nome;

	public ApartamentoMoradorDTO() {

	}
	
	public ApartamentoMoradorDTO(String nome, Long apartamentoId, String tipo, boolean responsavelBoleto) {
		this.nome = nome;
		this.apartamentoId = apartamentoId;
		this.tipo = tipo;
		this.responsavelBoleto = responsavelBoleto;
	}

	public Long getApartamentoId() {
		return apartamentoId;
	}

	public void setApartamentoId(Long apartamentoId) {
		this.apartamentoId = apartamentoId;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Boolean getResponsavelBoleto() {
		return responsavelBoleto;
	}

	public void setResponsavelBoleto(Boolean responsavelBoleto) {
		this.responsavelBoleto = responsavelBoleto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
