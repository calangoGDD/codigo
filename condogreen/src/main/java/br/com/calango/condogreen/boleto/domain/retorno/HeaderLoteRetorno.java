package br.com.calango.condogreen.boleto.domain.retorno;

import org.jrimum.texgit.FlatFile;
import org.jrimum.texgit.Record;

public class HeaderLoteRetorno {

	private Record record;

	public HeaderLoteRetorno(FlatFile<Record> ff) {
		record = ff.getRecord("HeaderLote");
	}

	public String getNumeroRetorno() {
		return record.getValue("Retorno-Numero").toString();
	}

	public String getMensagem1() {
		return record.getValue("Mensagem1");
	}

	public String getMensagem2() {
		return record.getValue("Mensagem2");
	}

}
