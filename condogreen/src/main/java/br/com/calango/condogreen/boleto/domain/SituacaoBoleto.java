package br.com.calango.condogreen.boleto.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.calango.condogreen.boleto.domain.retorno.DetalheRetornoEntrada;
import br.com.calango.condogreen.core.domain.BasicEntity;

@Entity
@Table(name = "situacao_boleto")
public class SituacaoBoleto extends BasicEntity {

	public static final String FK_ID = "situacao_boleto_id";

	/**
	 * Cacelamento Solicitado
	 */
	public static final String SITUACAO_CANCELAMENTO_SOCILICITADO = "CS";

	/**
	 * Enviado para registro
	 */
	public static final String SITUACAO_ENVIADO_PARA_REGISTRO = "ER";
	
	/**
	 * Enviado para registro - Alt Venc
	 */
	public static final String SITUACAO_ENVIADO_PARA_REGISTRO_ALT_VENC = "EV";

	/**
	 * Aguardando confirmação de envio.
	 */
	public static final String SITUACAO_AGUARDANDO_CONFIRMACAO_ENVIO = "AC";

	/**
	 * Aguardando geração de remessa.
	 */
	public static final String SITUACAO_AGUARDANDO_GERACAO_REMESSA = "GR";

	/**
	 * Liguidado
	 */
	public static final String SITUACAO_LIQUIDADO = "06";

	/**
	 * Entrada confirmada
	 */
	public static final String SITUACAO_ENTRADA_CONFIRMADA = "02";
	
	/**
	 * Entrada rejeitada
	 */
	public static final String SITUACAO_ENTRADA_REJEITADA = "03";
	
	/**
	 * Protestado e Baixado (Baixa por Ter Sido Protestado)
	 */
	public static final String SITUACAO_PROTESTADO_BAIXADO = "25";

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "descricao")
	private String descricao;

	public String getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public boolean isSituacaoCancelamentoSolicitado() {
		return SITUACAO_CANCELAMENTO_SOCILICITADO.equals(codigo);
	}

	public boolean isSituacaoLiquidado() {
		return SITUACAO_LIQUIDADO.equals(codigo);
	}

	public boolean isSituacaoAguardandoGeracaoRemessa() {
		return SITUACAO_AGUARDANDO_GERACAO_REMESSA.equals(codigo);
	}

	public boolean isSituacaoAguardandoConfirmacaoEnvio() {
		return SITUACAO_AGUARDANDO_CONFIRMACAO_ENVIO.equals(codigo);
	}

	public boolean isSituacaoEnviadoParaRegistro() {
		return SITUACAO_ENVIADO_PARA_REGISTRO.equals(codigo);
	}
	
	public boolean isSituacaoEntradaRejeitada() {
		return SITUACAO_ENTRADA_REJEITADA.equals(codigo);
	}
	
	public boolean isSituacaoProtestadoBaixado() {
		return SITUACAO_PROTESTADO_BAIXADO.equals(codigo);
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isSituacaoEntradaConfirmada() {
		return SITUACAO_ENTRADA_CONFIRMADA.equals(this.codigo);
	}

	public boolean isSituacaoCancelado() {
		return DetalheRetornoEntrada.BAIXA.getCodigo().equals(this.codigo);
	}

}
