package br.com.calango.condogreen.condominio.aplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.condominio.domain.Noticia;

@RepositoryRestResource(collectionResourceRel = "repnoticias", path = "repnoticias")
public interface NoticiaRepository extends JpaRepository<Noticia, Long>,  JpaSpecificationExecutor<Noticia> {

	@Query(value = "select * from noticia n where n.situacao = 1 order by n.data_criacao desc limit 4", nativeQuery = true)
	List<Noticia> getQuatroPrimeiras();


}

