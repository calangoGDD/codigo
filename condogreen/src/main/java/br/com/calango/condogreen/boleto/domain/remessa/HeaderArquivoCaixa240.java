package br.com.calango.condogreen.boleto.domain.remessa;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.jrimum.texgit.Record;

import br.com.calango.condogreen.boleto.util.RemessaUtil;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

public class HeaderArquivoCaixa240 {

	private static final String TIPO_REMESSA = "REMESSA-PRODUCAO";
	private Record header;

	public HeaderArquivoCaixa240(Record header) {
		this.header = header;
	}

	public String getCodBanco() {
		return header.getValue("codigoBanco");
	}

	public String getLote() {
		return header.getValue("loteServico");
	}

	public String getTipoRegistro() {
		return header.getValue("TipoRegistro");
	}

	public void setUsoExclusivo1() {
		header.setValue("UsoExclusivo1", StringUtils.repeat(" ", 9));
	}

	public void setTipoInscricao(String tipo) {
		header.setValue("empTipoInscricao", tipo);
	}

	public void setNumInscricao(String numInscricao) {
		header.setValue("empNumInscricao", Long.valueOf(RemessaUtil.retiraCaracteresEspeciais(numInscricao)));
	}

	public void setUsoExclusivoCEF() {
		header.setValue("usoExclusivoCEF", StringUtils.repeat("0", 20));
	}

	public void setAgencia(String agencia) {
		header.setValue("empAgenciaConta", Integer.valueOf(agencia));
	}

	public void setAgenciaVerificador(int digitoAgencia) {
		header.setValue("empAgenciaVerificador", digitoAgencia);
	}

	public void setCodConvenio(String codBeneficiario) {
		header.setValue("empCodConvenio", codBeneficiario);
	}

	public void setEmpNome(String nome) {
		String empNome = RemessaUtil.retiraCaracteresEspeciais(StringUtils.substring(nome, 0, 30));
		header.setValue("empNome", empNome);
	}

	public void setUsoExclusivo4() {
		header.setValue("UsoExclusivo4", StringUtils.repeat(" ", 10));
	}

	public void setArqDataGeracao() {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("ddMMyyyy");
		header.setValue("arqDataGeracao", dateFormatter.format(new Date()));
	}

	public void setArqHoraGeracao() {
		SimpleDateFormat df = new SimpleDateFormat("HHmmss");
		Date now = new Date();
		header.setValue("arqHoraGeracao", df.format(now));
	}

	public void setArqNumSequencial(long nrRemessa) {
		header.setValue("arqNumSequencial", nrRemessa);
	}

	public void setArqDensidade() {
		header.setValue("arqDensidade", StringUtils.repeat("0", 5));
	}

	public void setUsoExclusivoEmpresa5() {
		header.setValue("usoExclusivoEmpresa5", TIPO_REMESSA);
	}

	public void setVersaoAplicativo() {
		header.setValue("versaoAplicativo", StringUtils.repeat(" ", 4));
	}

	public void setUsoExclusivo6() {
		header.setValue("usoExclusivo6", StringUtils.repeat(" ", 25));
	}

	public String getUsoExclusivo1() {
		return header.getValue("UsoExclusivo1");
	}

	public Integer getTipoInscricao() {
		return Integer.valueOf(header.getValue("empTipoInscricao"));
	}

	public Long getNumInscricao() {
		return header.getValue("empNumInscricao");
	}

	public String getUsoExclusivoCEF() {
		return header.getValue("usoExclusivoCEF");
	}

	public Integer getAgencia() {
		return header.getValue("empAgenciaConta");
	}

	public int getAgenciaVerificador() {
		return header.getValue("empAgenciaVerificador");
	}

	public String getCodConvenio() {
		return header.getValue("empCodConvenio");
	}

	public String getEmpNome() {
		return header.getValue("empNome");
	}

	public String getUsoExclusivo4() {
		return header.getValue("UsoExclusivo4");
	}

	public String getArqDataGeracao() {
		return header.getValue("arqDataGeracao");
	}

	public String getArqHoraGeracao() {
		return header.getValue("arqHoraGeracao");
	}

	public long getArqNumSequencial() {
		return header.getValue("arqNumSequencial");
	}

	public String getArqDensidade() {
		return header.getValue("arqDensidade");
	}

	public String getUsoExclusivoBanco5() {
		return header.getValue("usoExclusivoBanco5");
	}

	public String getUsoExclusivoEmpresa5() {
		return header.getValue("usoExclusivoEmpresa5");
	}

	public String getVersaoAplicativo() {
		return header.getValue("versaoAplicativo");
	}

	public String getUsoExclusivo6() {
		return header.getValue("usoExclusivo6");
	}

	public Integer getUsoExclusivoCEF2() {
		return header.getValue("usoExclusivoCEF2");
	}

	public Integer getUsoExclusivoCEF3() {
		return header.getValue("usoExclusivoCEF3");
	}

	public String getBancoNome() {
		return header.getValue("bancoNome");
	}

	public int getArqCodigoRemessa() {
		return header.getValue("arqCodigoRemessa");
	}

	public Record contruirHeader(PessoaJuridica empresa, long nrRemessa) {
		System.out.println("************* header ****************");
		this.setUsoExclusivo1();
		this.setTipoInscricao(empresa.getTipoInscricao());
		this.setNumInscricao(empresa.getNumeroInscricao());
		this.setUsoExclusivoCEF();
		this.setAgencia(empresa.getAgencia());
		this.setAgenciaVerificador(empresa.getDigitoAgencia());
		this.setCodConvenio(empresa.getCodigoBeneficiario());
		this.setEmpNome(empresa.getNome());
		this.setUsoExclusivo4();
		this.setArqDataGeracao();
		this.setArqHoraGeracao();
		this.setArqNumSequencial(nrRemessa);
		this.setArqDensidade();
		this.setUsoExclusivoEmpresa5();
		this.setVersaoAplicativo();
		this.setUsoExclusivo6();

		return header;
	}
}
