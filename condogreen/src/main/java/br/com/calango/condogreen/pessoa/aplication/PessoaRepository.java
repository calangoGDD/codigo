package br.com.calango.condogreen.pessoa.aplication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.pessoa.domain.Pessoa;

@RepositoryRestResource(collectionResourceRel = "pessoas", path = "pessoas")
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

}
