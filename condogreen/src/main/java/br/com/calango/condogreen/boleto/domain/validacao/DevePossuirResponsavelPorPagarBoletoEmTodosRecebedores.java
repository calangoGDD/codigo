package br.com.calango.condogreen.boleto.domain.validacao;

import org.springframework.stereotype.Component;

import br.com.calango.condogreen.boleto.aplication.service.GeradorBoleto;
import br.com.calango.condogreen.infrastructure.specification.ISpecification;

@Component("devePossuirResponsavelPorPagarBoletoEmTodosRecebedores")
public class DevePossuirResponsavelPorPagarBoletoEmTodosRecebedores implements ISpecification<GeradorBoleto> {

	/**
	 * @see br.com.calango.condogreen.infrastructure.specification.ISpecification#isSatisfiedBy(java.lang.Object)
	 */
	@Override
	public boolean isSatisfiedBy(GeradorBoleto geradorRemessa) {
		Long recebedoresBoletoSemResponsavelPorPagar = geradorRemessa.getQtdRecebedoresBoletoSemResponsavelPorPagar();
		return recebedoresBoletoSemResponsavelPorPagar == null || recebedoresBoletoSemResponsavelPorPagar == 0;
	}

}
