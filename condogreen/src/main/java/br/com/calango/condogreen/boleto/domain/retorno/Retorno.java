package br.com.calango.condogreen.boleto.domain.retorno;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.calango.condogreen.boleto.domain.InformacaoRetorno;
import br.com.calango.condogreen.core.domain.BasicEntity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

/**
 * 
 * Classe responsável pelo Retorno de remessa pela CAIXA.
 * 
 * @author Danyllo Araujo
 *
 */
@Entity
@Table(name = "retorno")
public class Retorno extends BasicEntity {

	private static final String NOME_ARQUIVO = "Arquivo_";

	public static final String FK_ID = "retorno_id";

	@Column(name = "numero_retorno")
	private Long numeroRetorno;

	@Lob
	@Column(name = "arquivo")
	private byte[] arquivo;

	@JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	@Column(name = "data_importacao")
	private LocalDate dataImportacao;

	@Column(name = "quantidade_registros")
	private Long quantidadeRegistros;

	@Column(name = "valor_total")
	private BigDecimal valorTotal = BigDecimal.ZERO;

	/**
	 * Informações de Retorno da CAIXA referentes ao boleto.
	 */
	@OneToMany(mappedBy = "retorno", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<InformacaoRetorno> informacoesRetorno;

	public Retorno() {
		this.informacoesRetorno = new ArrayList<InformacaoRetorno>();
	}

	public Long getNumeroRetorno() {
		return numeroRetorno;
	}

	public void setNumeroRetorno(Long numeroRetorno) {
		this.numeroRetorno = numeroRetorno;
	}

	public byte[] getArquivo() {
		return arquivo;
	}

	public void setArquivo(byte[] arquivo) {
		this.arquivo = arquivo;
	}

	public LocalDate getDataImportacao() {
		return dataImportacao;
	}

	public void setDataImportacao(LocalDate dataImportacao) {
		this.dataImportacao = dataImportacao;
	}

	public Long getQuantidadeRegistros() {
		return quantidadeRegistros;
	}

	public void setQuantidadeRegistros(Long quantidadeRegistros) {
		this.quantidadeRegistros = quantidadeRegistros;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getNomeDoArquivo() {
		return NOME_ARQUIVO + getNumeroRetorno();
	}

	/**
	 * @return the informacoesRetorno
	 */
	public List<InformacaoRetorno> getInformacoesRetorno() {
		return informacoesRetorno;
	}

	/**
	 * @param informacoesRetorno
	 *            the informacoesRetorno to set
	 */
	public void setInformacoesRetorno(List<InformacaoRetorno> informacoesRetorno) {
		this.informacoesRetorno = informacoesRetorno;
	}

	public void addInformacaoRetorno(InformacaoRetorno informacaoRetorno) {
		informacaoRetorno.setRetorno(this);
		this.informacoesRetorno.add(informacaoRetorno);
	}
}
