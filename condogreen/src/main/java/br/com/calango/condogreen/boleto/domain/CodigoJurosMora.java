package br.com.calango.condogreen.boleto.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Este Enum representa o tipo de pagamento de juros de mora; Tipo de
 * preferência: ‘1’ (Valor por Dia); ou ‘2’ (Taxa Mensal); ou ‘3’ (Isento)
 * 
 * @author Guilherme Andrade
 *
 */
public enum CodigoJurosMora {

	ISENTO(3), VALOR_POR_DIA(1), TAXA_MENSAL(2);

	private static Map<Integer, CodigoJurosMora> map = new HashMap<Integer, CodigoJurosMora>();

	static {
		for (CodigoJurosMora codigoJurosMora : CodigoJurosMora.values()) {
			map.put(codigoJurosMora.getCodigo(), codigoJurosMora);
		}
	}

	/**
	 * Codigo do tipo de pagamento de juros mora.
	 */
	private int codigo;

	CodigoJurosMora(int codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public static CodigoJurosMora getEnum(int mora) {
		return map.get(mora);
	}
}
