package br.com.calango.condogreen.condominio.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.calango.condogreen.core.domain.BasicEntity;

@Entity
@Table(name = "convidado")
public class Convidado extends BasicEntity {
	
	@Column(name = "nome", length = 40)
	private String nome;
	
	@OneToOne(fetch = FetchType.LAZY, targetEntity= Morador.class)
	@JoinColumn(name = Morador.FK_ID)
	private IMorador morador;

	@JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	@Column(name = "data_convite")
	private LocalDate dataConvite;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public IMorador getMorador() {
		return morador;
	}

	public void setMorador(IMorador morador) {
		this.morador = morador;
	}

	public LocalDate getDataConvite() {
		return dataConvite;
	}

	public void setDataConvite(LocalDate dataConvite) {
		this.dataConvite = dataConvite;
	}
	
}
