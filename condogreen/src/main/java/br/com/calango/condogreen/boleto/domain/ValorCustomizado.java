package br.com.calango.condogreen.boleto.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.calango.condogreen.core.domain.BasicEntity;

@Entity
@Table(name = "valor_customizado")
public class ValorCustomizado extends BasicEntity {

	@ManyToOne
	@JoinColumn(name = ConfiguradorBoleto.FK_ID)
	private ConfiguradorBoleto configuradorBoleto;

	@ManyToOne
	@JoinColumn(name = RecebedorBoleto.FK_ID)
	private RecebedorBoleto recebedorBoleto;

	private BigDecimal valor = BigDecimal.ZERO;

	public ValorCustomizado() {
	}

	/**
	 * @return the configuradorBoleto
	 */
	public ConfiguradorBoleto getConfiguradorBoleto() {
		return configuradorBoleto;
	}

	/**
	 * @param configuradorBoleto
	 *            the configuradorBoleto to set
	 */
	public void setConfiguradorBoleto(ConfiguradorBoleto configuradorBoleto) {
		this.configuradorBoleto = configuradorBoleto;
	}

	/**
	 * @return the recebedorBoleto
	 */
	public RecebedorBoleto getRecebedorBoleto() {
		return recebedorBoleto;
	}

	/**
	 * @param recebedorBoleto
	 *            the recebedorBoleto to set
	 */
	public void setRecebedorBoleto(RecebedorBoleto recebedorBoleto) {
		this.recebedorBoleto = recebedorBoleto;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getNomeConfigurador() {
		return this.configuradorBoleto.getNome();
	}

	public String getIdentificacaoRecebedorBoleto() {
		return this.recebedorBoleto.getIdentificacaoRecebedorBoleto();
	}
}
