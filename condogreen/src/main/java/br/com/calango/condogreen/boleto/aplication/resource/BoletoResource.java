package br.com.calango.condogreen.boleto.aplication.resource;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.calango.condogreen.boleto.aplication.service.GeradorBoleto;
import br.com.calango.condogreen.boleto.domain.BoletoDTO;
import br.com.calango.condogreen.boleto.domain.InfoBoleto;
import br.com.calango.condogreen.infrastructure.exception.BusinessException;

@RepositoryRestController
@RequestMapping("/boleto")
public class BoletoResource {

	@Autowired
	private GeradorBoleto geradorBoleto;

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "gerar")
	public void gerarBoleto(@Valid @RequestBody BoletoDTO boletoAvulsoDTO) throws IOException {
		geradorBoleto.gerarBoletoAvulso(boletoAvulsoDTO);
	}

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "gerarBoletosEmMassa")
	public void gerarBoletosEmMassa(@Valid @RequestBody BoletoDTO boletoEmMassaDTO) throws IOException, BusinessException, InterruptedException {
		geradorBoleto.gerarBoletosEmMassa(boletoEmMassaDTO);
	}

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "cancelar")
	public void cacelarBoleto(@Valid @RequestBody InfoBoleto boleto) throws IOException {
		geradorBoleto.cancelarBoleto(boleto.getId());
	}
}