package br.com.calango.condogreen.condominio.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

@Entity
@Table(name = "reserva")
public class Reserva extends AbstractPersistable<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String FK_ID = "reserva_id";

	public static final String APROVADO = "AP";

	public static final String AGUARDANDO_APROVACAO = "AAP";

	@NotBlank
	@NotNull
	@Column(name = "titulo", length = 40)
	private String titulo;
	
	@Column(name = "descricao", length = 100)
	private String descricao;
	
	@JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	@Column(name = "inicio")
	private LocalDate inicio;
	
	@JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	@Column(name = "fim")
	private LocalDate fim;
	
	@Column(name = "diatodo")
	private boolean diaTodo;

	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, targetEntity= Morador.class)
	@JoinColumn(name = Morador.FK_ID)
	private IMorador morador;

	@Column(name = "situacao", nullable = false)
	private String situacao;

	
	public Reserva() {	
	}
	
	public Reserva(String titulo, String descricao, LocalDate inicio, LocalDate fim, boolean diaTodo) {
		this();
		this.titulo = titulo;
		this.descricao = descricao;
		this.inicio = inicio;
		this.fim = fim;
		this.diaTodo = diaTodo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public LocalDate getInicio() {
		return inicio;
	}

	public void setInicio(LocalDate inicio) {
		this.inicio = inicio;
	}

	public LocalDate getFim() {
		return fim;
	}

	public void setFim(LocalDate fim) {
		this.fim = fim;
	}

	public boolean isDiaTodo() {
		return diaTodo;
	}

	public void setDiaTodo(boolean diaTodo) {
		this.diaTodo = diaTodo;
	}

	public IMorador getMorador() {
		return morador;
	}

	public void setMorador(IMorador morador) {
		this.morador = morador;
	}
	
	public String getTituloFormatado(){
		StringBuilder sb = new StringBuilder(this.titulo);
		IMorador morador = this.getMorador();
		if(morador != null) {
			sb.append("\n (");
			sb.append(morador.getApartamentosFormatados());
			sb.append(")");
			sb.append("\n");
		}
		
		sb.append(getDescriptionSituation(this.situacao).replaceAll(" ", "\n"));
		return sb.toString();
	}

	private String getDescriptionSituation(String situacao) {
		String result = "";
		if(APROVADO.equals(situacao)) {
			result = "Aprovado";
		} else if(AGUARDANDO_APROVACAO.equals(situacao)) {
			result = "Aguardando Aprovação";
		}
		
		return result;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getSituacao() {
		return situacao;
	}
}
