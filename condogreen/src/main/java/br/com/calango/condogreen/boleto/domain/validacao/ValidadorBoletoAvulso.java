package br.com.calango.condogreen.boleto.domain.validacao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.calango.condogreen.boleto.domain.RecebedorBoleto;
import br.com.calango.condogreen.infrastructure.exception.AbstractValidator;
import br.com.calango.condogreen.infrastructure.exception.BusinessException;

@Component("validadorBoletoAvulso")
public class ValidadorBoletoAvulso extends AbstractValidator<RecebedorBoleto> {

	@Autowired
	private DevePossuirResponsavelPorPagarBoleto devePossuirResponsavelPorPagarBoleto;

	@Override
	public void registerRulesSpecification() throws BusinessException {
		rules.put(devePossuirResponsavelPorPagarBoleto, new String[] { "Não foi encontrado responsável por pagamento!" });
	}

}
