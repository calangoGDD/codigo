package br.com.calango.condogreen.condominio.aplication.resource;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import br.com.calango.condogreen.condominio.aplication.service.DocumentoService;
import br.com.calango.condogreen.condominio.domain.Documento;

@RepositoryRestController
@RequestMapping("/documentos")
public class DocumentoResource {

	@Autowired
	DocumentoService documentoService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/create", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ResponseBody
	public byte[] importar(@RequestPart(value = "arquivo") MultipartFile arquivo, @RequestPart(value = "documento") Documento documento) throws IOException {
		try {
			documentoService.save(arquivo.getOriginalFilename(), arquivo.getBytes(), documento);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arquivo.getBytes();
	}
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public Page<Documento> search(Pageable pageable) {
		return documentoService.findAll(pageable);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/byCategoria/{id:\\d+}")
	@ResponseBody
	public List<Documento> documentsByCategoria(@PathVariable("id") long id) {
		return documentoService.documentsByCategoria(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/get/{id:\\d+}")
	@ResponseBody
	public Documento get(@PathVariable("id") long id) {
		return documentoService.findById(id);
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "{id:\\d+}/delete")
	public void delete(@PathVariable("id") long id) throws IOException {
		documentoService.delete(id);
	}
}
