package br.com.calango.condogreen.condominio.aplication.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.calango.condogreen.condominio.aplication.repository.ApartamentoRepository;
import br.com.calango.condogreen.condominio.aplication.repository.MoradorRepository;
import br.com.calango.condogreen.condominio.aplication.resource.ApartamentoMorador;
import br.com.calango.condogreen.condominio.aplication.resource.ApartamentoMoradorDTO;
import br.com.calango.condogreen.condominio.aplication.resource.MoradorDTO;
import br.com.calango.condogreen.condominio.domain.Apartamento;
import br.com.calango.condogreen.condominio.domain.Morador;
import br.com.calango.condogreen.condominio.domain.validacao.ValidadorMoradorDTO;
import br.com.calango.condogreen.pessoa.aplication.PessoaJurdicaRepository;
import br.com.calango.condogreen.pessoa.domain.IPessoa;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;
import br.com.calango.condogreen.pessoa.domain.VisitorConstrutorPessoa;

@Service
public class MoradorService {

	private MoradorRepository moradorRepository;

	private PessoaJurdicaRepository pessoaJurdicaRepository;
	
	private ApartamentoRepository apartamentoRepository;
	
	private ApplicationContext appContext;
	
	@Autowired
	public MoradorService(MoradorRepository moradorRepository, PessoaJurdicaRepository pessoaJurdicaRepository, ApartamentoRepository apartamentoRepository, ApplicationContext appContext) {
		this.moradorRepository = moradorRepository;
		this.pessoaJurdicaRepository = pessoaJurdicaRepository;
		this.apartamentoRepository = apartamentoRepository;
		this.appContext = appContext;
	}
	
	public Page<Morador> findAll(Pageable pageable) {
		return moradorRepository.findAll(pageable);
	}

	public void save(MoradorDTO moradorDTO) {
		ValidadorMoradorDTO validadorMoradorDTO = (ValidadorMoradorDTO) appContext.getBean("validadorMoradorDTO");
		validadorMoradorDTO.checkBrokenRules(moradorDTO);
		
		Morador morador = null;
		//TODO criar futuramente aba de endereco
		PessoaJuridica condominio = pessoaJurdicaRepository.findTopByOrderByIdAsc();
		moradorDTO.setEndereco(condominio.getEndereco().clone());
		
		VisitorConstrutorPessoa construtorPessoa = new VisitorConstrutorPessoa();
		if(moradorDTO.getId() == null) {
			morador = new Morador();
			IPessoa pessoa = construtorPessoa.construtorPessoa(moradorDTO);
			morador.setPessoa(pessoa);
		} else {
			morador = moradorRepository.findOne(moradorDTO.getId());
			construtorPessoa.atualizarPessoa(moradorDTO, morador);
		}
		
		List<ApartamentoMoradorDTO> apartamentos = moradorDTO.getApartamentos();
		List<ApartamentoMorador> apartamentoMoradorList = new ArrayList<>();
		for (ApartamentoMoradorDTO apartamentoMoradorDTO : apartamentos) {
			Boolean responsavelBoleto = apartamentoMoradorDTO.getResponsavelBoleto();
			Apartamento apartamento = apartamentoRepository.findOne(apartamentoMoradorDTO.getApartamentoId());
			String tipo = apartamentoMoradorDTO.getTipo();
			ApartamentoMorador apartamentoMorador = new ApartamentoMorador(apartamento, morador, tipo, responsavelBoleto);
			apartamentoMoradorList.add(apartamentoMorador);
		}
		
		morador.addApartamento(apartamentoMoradorList);
		
		moradorRepository.saveAndFlush(morador);
	}


	public void delete(long id) {
		moradorRepository.delete(id);
	}

	public Morador findById(long id) {
		return moradorRepository.findOne(id);
	}

}
