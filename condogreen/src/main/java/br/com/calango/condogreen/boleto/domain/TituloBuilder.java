package br.com.calango.condogreen.boleto.domain;

import java.util.Date;

import org.jrimum.bopepo.BancosSuportados;
import org.jrimum.domkee.comum.pessoa.endereco.Endereco;
import org.jrimum.domkee.comum.pessoa.endereco.UnidadeFederativa;
import org.jrimum.domkee.financeiro.banco.febraban.Agencia;
import org.jrimum.domkee.financeiro.banco.febraban.Carteira;
import org.jrimum.domkee.financeiro.banco.febraban.Cedente;
import org.jrimum.domkee.financeiro.banco.febraban.ContaBancaria;
import org.jrimum.domkee.financeiro.banco.febraban.NumeroDaConta;
import org.jrimum.domkee.financeiro.banco.febraban.Sacado;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeCobranca;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo;

import br.com.calango.condogreen.boleto.util.BoletoUtil;
import br.com.calango.condogreen.pessoa.domain.IPessoa;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

/**
 * Classe responsável por construir o título bancário.
 * 
 * @author Guilherme Andrade
 *
 */
public class TituloBuilder {

	private Titulo titulo;
	private InfoBoleto infoBoleto;
	private PessoaJuridica empresa;

	public TituloBuilder(PessoaJuridica empresa, InfoBoleto infoBoleto) {
		this.infoBoleto = infoBoleto;
		this.empresa = empresa;
		Sacado sacado = criarSacado();
		Cedente cedente = criarCedente();
		ContaBancaria contaBancaria = criarContaBancaria();
		this.titulo = new Titulo(contaBancaria, sacado, cedente);
		comInfoBoleto();
	}

	private ContaBancaria criarContaBancaria() {
		CodigoCarteira codigoCarteira = infoBoleto.getCodigoDaCarteira();
		TipoDeCobranca modalidade = infoBoleto.getModalidade();
		ContaBancaria contaBancaria = new ContaBancaria(BancosSuportados.CAIXA_ECONOMICA_FEDERAL.create());
		String codBeneficiario = empresa.getCodigoBeneficiario();
		String digitoCodBeneficiario = empresa.getDigitoCodBeneficiario();
		contaBancaria.setNumeroDaConta(new NumeroDaConta(Integer.valueOf(codBeneficiario), digitoCodBeneficiario));
		contaBancaria.setCarteira(new Carteira(codigoCarteira.getCodigo(), modalidade));
		contaBancaria.setAgencia(new Agencia(Integer.valueOf(empresa.getAgencia())));
		return contaBancaria;
	}

	private Sacado criarSacado() {
		IPessoa sacado = infoBoleto.getSacado();
		Endereco enderecoSac = new Endereco();
		enderecoSac.setUF(UnidadeFederativa.PE);
		enderecoSac.setLocalidade(sacado.getCidade());
		enderecoSac.setCep(sacado.getCep());
		enderecoSac.setBairro(sacado.getBairro());
		enderecoSac.setLogradouro(sacado.getRua());
		enderecoSac.setNumero(sacado.getNumero());
		enderecoSac.setComplemento(sacado.getComplemento());
		Sacado sacadoJ = new Sacado(sacado.getNome(), sacado.getNumeroInscricao());
		sacadoJ.addEndereco(enderecoSac);
		return sacadoJ;
	}

	public Cedente criarCedente() {
		Endereco endereco = new Endereco();
		endereco.setUF(UnidadeFederativa.PE);
		endereco.setLocalidade(empresa.getCidade());
		endereco.setCep(empresa.getCep());
		endereco.setBairro(empresa.getBairro());
		endereco.setLogradouro(empresa.getRua());
		endereco.setNumero(empresa.getNumero());
		endereco.setComplemento(empresa.getComplemento());
		Cedente cedente = new Cedente(empresa.getNome(), empresa.getCnpj());
		cedente.addEndereco(endereco);
		return cedente;
	}

	public TituloBuilder comInfoBoleto() {
		// O banco não tem controle sobre este campo, costumam colocar
		// identificação de notas fiscais.
		titulo.setNumeroDoDocumento(infoBoleto.getNumeroDocumento());
		titulo.setNossoNumero(infoBoleto.getNossoNumeroFormatado());
		titulo.setDigitoDoNossoNumero(BoletoUtil.calculeDVModulo11(titulo.getNossoNumero()) + "");
		titulo.setDataDoVencimento(infoBoleto.getDataVencimentoComoDate());
		titulo.setValor(infoBoleto.getValor());
		titulo.setDataDoDocumento(new Date());
		titulo.setTipoDeDocumento(infoBoleto.getEspecieDoTitulo());
		titulo.setAceite(infoBoleto.getAceite());
		return this;
	}

	public Titulo build() {
		assert titulo.getContaBancaria() != null;
		assert titulo.getCedente() != null;
		assert titulo.getSacado() != null;
		return this.titulo;
	}
}
