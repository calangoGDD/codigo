package br.com.calango.condogreen.infrastructure.config;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.ClassUtils;

/**
 * @author Guilherme Andrade
 *
 */
public class SchemaGenerator {
	private Configuration cfg;

	@SuppressWarnings("unchecked")
	public SchemaGenerator(String packageName) throws Exception {
		cfg = new Configuration();
		cfg.setProperty("hibernate.hbm2ddl.auto", "create");

		cfg.setProperty("dataSource.driverClassName", "com.mysql.jdbc.Driver");
		cfg.setProperty("dataSource.url", "jdbc:mysql://us-cdbr-iron-east-04.cleardb.net/heroku_179c254ca4e39db");
		cfg.setProperty("dataSource.username", "b146d27e94e55a");
		cfg.setProperty("dataSource.password", "ae428b79");
		cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		cfg.setProperty("hibernate.jdbc.use_streams_for_binary", "false");
		for (Class<Object> clazz : getClasses(packageName)) {
			cfg.addAnnotatedClass(clazz);
		}
	}

	/**
	 * Method that actually creates the file.
	 * 
	 * @param dbDialect
	 *            to use
	 */
	private void generate(Dialect dialect) {
		SchemaExport export = new SchemaExport(cfg);
		export.setDelimiter(";");
		export.setOutputFile("ddl_" + dialect.name().toLowerCase() + ".sql");
		export.execute(true, false, false, false);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		SchemaGenerator gen = new SchemaGenerator("br.com.calango.condogreen.*");
		gen.generate(Dialect.MYSQL);

	}

	/**
	 * Utility method used to fetch Class list based on a package name.
	 * 
	 * @param packageName
	 *            (should be the package containing your annotated beans.
	 */
	/**
	 * Utility method used to fetch Class list based on a package name.
	 * 
	 * @param packageName
	 *            (should be the package containing your annotated beans.
	 */
	@SuppressWarnings("rawtypes")
	private List<Class> getClasses(String packageName) throws Exception {
		List<Class> classes = new ArrayList<Class>();
		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
		scanner.addIncludeFilter(new AnnotationTypeFilter(Entity.class));
		scanner.addIncludeFilter(new AnnotationTypeFilter(MappedSuperclass.class));
		for (BeanDefinition bd : scanner.findCandidateComponents(packageName)) {
			String className = bd.getBeanClassName();
			Class<?> type = ClassUtils.resolveClassName(className, Thread.currentThread().getContextClassLoader());
			classes.add(type);
		}
		return classes;
	}

	/**
	 * Holds the classnames of hibernate dialects for easy reference.
	 */
	private static enum Dialect {
		ORACLE("org.hibernate.dialect.Oracle10gDialect"), MYSQL("org.hibernate.dialect.MySQLDialect"), HSQL("org.hibernate.dialect.HSQLDialect"), POSTGRE(
		        "org.hibernate.dialect.PostgreSQLDialect");

		private String dialectClass;

		private Dialect(String dialectClass) {
			this.dialectClass = dialectClass;
		}

		@SuppressWarnings("unused")
		public String getDialectClass() {
			return dialectClass;
		}
	}
}
