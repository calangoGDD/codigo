package br.com.calango.condogreen.boleto.aplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.boleto.domain.ValorCustomizado;

@RepositoryRestResource(collectionResourceRel = "valorescustomizados", path = "valorescustomizados")
public interface ValoresCustomizadosRepository extends JpaRepository<ValorCustomizado, Long> {

}
