package br.com.calango.condogreen.pessoa.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Proxy;
import org.hibernate.validator.constraints.NotBlank;

import br.com.calango.condogreen.core.domain.BasicEntity;
import br.com.calango.condogreen.core.domain.Conta;
import br.com.calango.condogreen.core.domain.Endereco;

@Entity
@Table(name = "pessoa")
@Proxy(proxyClass = IPessoa.class)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Pessoa extends BasicEntity {

	public static final String FK_ID = "pessoa_id";
	public static final String FK_ID_SACADO = "sacado_id";
	
	@NotBlank
	@Column(name = "nome", length = 30)
	private String nome;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = Conta.FK_ID)
	private Conta conta;

	@Column(name = "celular")
	private String celular;

	@Column(name = "codigo_area_celular", length = 3)
	private String codigoAreaCelular;

	/**
	 * Deverá apontar para o endereço do condomínio.
	 */
	@NotNull
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = Endereco.FK_ID)
	private Endereco endereco;

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name = ContaBancaria.FK_ID)
	private ContaBancaria contaBancaria;

	public Pessoa() {
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the conta
	 */
	public Conta getConta() {
		return conta;
	}

	/**
	 * @param conta
	 *            the conta to set
	 */
	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getCodigoAreaCelular() {
		return codigoAreaCelular;
	}

	public void setCodigoAreaCelular(String codigoAreaCelular) {
		this.codigoAreaCelular = codigoAreaCelular;
	}

	public String getRua() {
		return endereco.getRua();
	}

	public String getBairro() {
		return endereco.getBairro();
	}

	public String getCep() {
		return endereco.getCep();
	}

	public String getSufixoCep() {
		return endereco.getSufixoCep();
	}

	public String getCidade() {
		return endereco.getCidade();
	}

	public String getUf() {
		return endereco.getUf().getSigla();
	}

	public String getNumero() {
		return endereco.getNumero();
	}

	public String getComplemento() {
		return endereco.getComplemento();
	}

	public abstract String getTipoInscricao();

	public abstract String getNumeroInscricao();

	public ContaBancaria getContaBancaria() {
		return contaBancaria;
	}

	public void setContaBancaria(ContaBancaria contaBancaria) {
		this.contaBancaria = contaBancaria;
	}

	public String getCodigoBeneficiario() {
		return contaBancaria == null ? null : contaBancaria.getCodigoBeneficiario();
	}

	public String getDigitoCodBeneficiario() {
		return contaBancaria == null ? null : contaBancaria.getDigitoCodigoBeneficiario();
	}

	public String getAgencia() {
		return contaBancaria == null ? null : contaBancaria.getAgencia();
	}

	public Integer getDigitoAgencia() {
		return contaBancaria == null ? null : contaBancaria.getDigitoAgencia();
	}

	public String getEmail() {
		return this.conta.getEmail();
	}

	public String getCelularWithCodigoArea() {
		return this.codigoAreaCelular + this.celular;
	}
}
