package br.com.calango.condogreen.boleto.aplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.boleto.domain.SituacaoBoleto;

@RepositoryRestResource(collectionResourceRel = "situacoes_boleto", path = "situacoes_boleto")
public interface SituacaoBoletoRepository extends JpaRepository<SituacaoBoleto, Long> {

	SituacaoBoleto findByCodigo(@Param(value = "codigo") String codigo);
}
