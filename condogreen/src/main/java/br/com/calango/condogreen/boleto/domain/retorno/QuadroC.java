package br.com.calango.condogreen.boleto.domain.retorno;

import java.util.HashMap;
import java.util.Map;

public class QuadroC implements QuadroDetalhamento {

	private static  Map<String, String> quadroC = new HashMap<>();
	
	static {
		quadroC.put("02", "Casa Lotérica");
		quadroC.put("03", "Agências CAIXA");
		quadroC.put("04", "Compensação Eletrônica");
		quadroC.put("05", "Compensação Convencional");
		quadroC.put("06", "Internet Banking");
		quadroC.put("07", "Correspondente Bancário");
		quadroC.put("08", "Em Cartório");
	}

	public Map<String, String> getQuadro() {
		return quadroC;
	}
	
}
