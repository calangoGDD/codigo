package br.com.calango.condogreen.boleto.domain.validacao;

import org.springframework.stereotype.Component;

import br.com.calango.condogreen.boleto.domain.RecebedorBoleto;
import br.com.calango.condogreen.condominio.aplication.resource.ApartamentoMorador;
import br.com.calango.condogreen.infrastructure.specification.ISpecification;

@Component("devePossuirResponsavelPorPagarBoleto")
public class DevePossuirResponsavelPorPagarBoleto implements ISpecification<RecebedorBoleto> {

	/**
	 * @see br.com.calango.condogreen.infrastructure.specification.ISpecification#isSatisfiedBy(java.lang.Object)
	 */
	@Override
	public boolean isSatisfiedBy(RecebedorBoleto recebedorBoleto) {
		 ApartamentoMorador responsavelPorPagarBoleto = recebedorBoleto.getResponsavelPorPagarBoleto();
		return responsavelPorPagarBoleto != null;
	}

}
