package br.com.calango.condogreen.boleto.domain.remessa;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.jrimum.texgit.FlatFile;
import org.jrimum.texgit.Record;
import org.jrimum.texgit.Texgit;

import br.com.calango.condogreen.boleto.domain.ConfiguradorBoleto;
import br.com.calango.condogreen.boleto.domain.InfoBoleto;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

public class LeiauteCaixa240 {

	public static final String LEIAUTE_CEF_240 = "./layoutsBancos/LayoutCEFCNAB240Envio.txg.xml";

	private FlatFile<Record> flatFile;

	private HeaderArquivoCaixa240 headerArquivo;

	private HeaderLoteCaixa240 headerLote;

	private int sequencia = 1;

	private BigDecimal valorTotal;

	public LeiauteCaixa240() {
		ClassLoader classLoader = this.getClass().getClassLoader();
		File layout = FileUtils.toFile(classLoader.getResource(LEIAUTE_CEF_240));
		flatFile = Texgit.createFlatFile(layout);
	}

	public HeaderArquivoCaixa240 criarHeader(PessoaJuridica empresa, long nrRemessa) {
		headerArquivo = new HeaderArquivoCaixa240(flatFile.createRecord("HeaderArquivo"));
		Record headerRecord = headerArquivo.contruirHeader(empresa, nrRemessa);
		flatFile.addRecord(headerRecord);
		return headerArquivo;
	}

	public HeaderLoteCaixa240 criarHeaderLote(PessoaJuridica empresa, long nrRemessa) {
		headerLote = new HeaderLoteCaixa240(flatFile.createRecord("HeaderLote"));
		Record headerLoteRecord = headerLote.contruirHeaderLote(empresa, nrRemessa);
		flatFile.addRecord(headerLoteRecord);

		return headerLote;
	}

	/**
	 * Método responsável por processar os boletos dentro do lote.
	 * 
	 * @param empresa
	 * @param boletos
	 * @param configurador
	 * @param dataVencimento
	 * @param flatFile
	 * 
	 * @return
	 */
	public void processarLoteDeBoletos(PessoaJuridica empresa, List<InfoBoleto> boletos, boolean comSegmentoR) {
		valorTotal = BigDecimal.ZERO;
		for (InfoBoleto boleto : boletos) {
			ConfiguradorBoleto configurador = boleto.getConfiguradorBoleto();
			SegmentoPCaixa240 segmentoP = new SegmentoPCaixa240(flatFile.createRecord("SegmentoP"), configurador);
			valorTotal = valorTotal.add(boleto.getValor());
			sequencia = segmentoP.criarDetalheSegmentoP(flatFile, boleto, sequencia, empresa, boleto.getDataVencimento(), comSegmentoR);
			flatFile.addRecord(segmentoP.getRecordSegmentoP());
			sequencia++;
		}
	}

	public void criarTrailerLote(int qtdBoletos) {
		sequencia++;
		TrailerLote trailerLote = new TrailerLote(flatFile.createRecord("TrailerLote"));
		System.out.println("************* trailer do lote ****************");
		trailerLote.construir(valorTotal, sequencia, qtdBoletos);
		sequencia++;
		flatFile.addRecord(trailerLote.geTrailerLote());
	}

	public void criarTrailerArquivo() {
		// Soma mais um porque tem que contar o cabeçalho do arquivo.
		sequencia++;
		flatFile.addRecord(criarTrailerArquivo(flatFile, sequencia));
	}

	public Record criarTrailerArquivo(final FlatFile<Record> ff, final int seq) {
		Record trailerArquivo = ff.createRecord("TrailerArquivo");
		System.out.println("************* trailer do arquivo ****************");
		trailerArquivo.setValue("tlQtdLotes", "1");
		trailerArquivo.setValue("tlQtdRegistros", seq);
		return trailerArquivo;
	}

	public HeaderArquivoCaixa240 getHeaderArquivo() {
		return this.headerArquivo;
	}

	public FlatFile<Record> getFlatFile() {
		return flatFile;
	}
}
