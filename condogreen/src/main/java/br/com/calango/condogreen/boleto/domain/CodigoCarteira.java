package br.com.calango.condogreen.boleto.domain;

/**
 * Código adotado pela FEBRABAN, para identificar a característica dos títulos
 * dentro das modalidades de cobrança existentes no banco. ‘1’ = Cobrança
 * Simples ‘3’ = Cobrança Caucionada ‘4’ = Cobrança Descontada
 * 
 * @author Guilherme Andrade
 *
 */
public enum CodigoCarteira {

	SIMPLES(1), CAUCIONADA(3), DESCONTADA(4);

	/**
	 * Codigo do tipo de pagamento de juros mora.
	 */
	private Integer codigo;

	CodigoCarteira(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the codigo
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

}
