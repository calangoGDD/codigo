package br.com.calango.condogreen.condominio.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.calango.condogreen.core.domain.BasicEntity;

/**
 * 
 * Classe que representa os carros e motos do condomínio.
 * 
 * @author Guilherme Andrade
 *
 */
@Entity
@Table(name = "transporte")
public class Transporte extends BasicEntity {

	@Column(name = "placa")
	private String placa;

	/**
	 * Carro ou Moto
	 */
	@Column(name = "tipo")
	private String tipo;

	@ManyToOne
	@JoinColumn(name = Apartamento.FK_ID)
	private Apartamento apartamento;

	/**
	 * @return the placa
	 */
	public String getPlaca() {
		return placa;
	}

	/**
	 * @param placa
	 *            the placa to set
	 */
	public void setPlaca(String placa) {
		this.placa = placa;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Apartamento getApartamento() {
		return apartamento;
	}

	public void setApartamento(Apartamento apartamento) {
		this.apartamento = apartamento;
	}

}
