package br.com.calango.condogreen.boleto.aplication.service;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.jrimum.domkee.comum.pessoa.endereco.UnidadeFederativa;
import org.jrimum.texgit.FlatFile;
import org.jrimum.texgit.Record;
import org.jrimum.texgit.Texgit;

import br.com.calango.condogreen.boleto.domain.ConfiguradorBoleto;
import br.com.calango.condogreen.boleto.domain.InfoBoleto;
import br.com.calango.condogreen.boleto.domain.SituacaoBoleto;
import br.com.calango.condogreen.boleto.domain.remessa.Remessa;
import br.com.calango.condogreen.boleto.domain.remessa.SegmentoPCaixa240;
import br.com.calango.condogreen.boleto.domain.remessa.SegmentoQCaixa240;
import br.com.calango.condogreen.core.domain.Endereco;
import br.com.calango.condogreen.pessoa.domain.ContaBancaria;
import br.com.calango.condogreen.pessoa.domain.PessoaFisica;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

public class MigradorRemessa {

	private static final String DADOS_REMESSAS_TXT = "./migracoes/sitioviver03/dadosRemessas.txt";

	private static final String SEGMENTO_P = "SegmentoP";

	private static final String UTF_8 = "UTF-8";

	private static final String LAYOUT_CEFCNAB240_ENVIO_TXG_XML = "./layoutsBancos/LayoutCEFCNAB240Migracao.txg.xml";

	public static void main(String[] args) throws IOException {
		Endereco endereco = new Endereco();
		endereco.setRua("RUA FREI MIGUELINHO - INHAM - IGARASSU");
		endereco.setUf(UnidadeFederativa.PE);
		endereco.setCep("53630");
		endereco.setSufixoCep("000");
		endereco.setCidade("Igarassu");
		endereco.setBairro("INHAMA");
		endereco.setNumero("290");
		endereco.setPais("Brasil");

		ContaBancaria contaBancaria = new ContaBancaria();
		contaBancaria.setAgencia("3122");
		contaBancaria.setCodigoBeneficiario("633754");
		contaBancaria.setDigitoCodigoBeneficiario("6");

		PessoaJuridica empresa = new PessoaJuridica();
		empresa.setNome("Residencial Sítio Viver Igarassu");
		empresa.setCnpj("22.666.605/0001-59");
		empresa.setEndereco(endereco);
		empresa.setContaBancaria(contaBancaria);

		ClassLoader classLoader = empresa.getClass().getClassLoader();
		File layout = FileUtils.toFile(classLoader.getResource(LAYOUT_CEFCNAB240_ENVIO_TXG_XML));
		FlatFile<Record> ff = Texgit.createFlatFile(layout);
		File dadosRemessas = FileUtils.toFile(classLoader.getResource(DADOS_REMESSAS_TXT));
		ff.read(FileUtils.readLines(dadosRemessas, Charset.forName(UTF_8)));

		List<ConfiguradorBoleto> configuradores = new ArrayList<>();
		List<PessoaFisica> sacados = new ArrayList<>();
		List<Remessa> remessas = new ArrayList<>();
		List<InfoBoleto> boletos = new ArrayList<>();

		// Registros
		Collection<Record> linhasBoleto = ff.getRecords(SEGMENTO_P);
		for (Record boleto : linhasBoleto) {
			SegmentoPCaixa240 segmentoP = new SegmentoPCaixa240(boleto);
			ConfiguradorBoleto configuradorExtraido = segmentoP.extrairConfiguradorSegmentoP();
			ConfiguradorBoleto configuradorBoletoDaLista = configuradores.stream().filter(c -> c.equals(configuradorExtraido)).findFirst()
			        .orElse(null);

			if (configuradorBoletoDaLista == null) {
				configuradorBoletoDaLista = configuradorExtraido;
				long configuradorId = configuradores.size() + 1l;
				configuradorBoletoDaLista.setNome("Configurador MIGRADO " + configuradorId);
				configuradorBoletoDaLista.setId(configuradorId);
				configuradores.add(configuradorBoletoDaLista);
			}

			Remessa remessaExtraida = segmentoP.extrairRemessa();
			Remessa remessaDaLista = remessas.stream().filter(r -> r.equals(remessaExtraida)).findFirst().orElse(null);
			if (remessaDaLista == null) {
				remessaDaLista = remessaExtraida;
				long idRemessa = remessas.size() + 1l;
				remessaDaLista.setId(idRemessa);
				remessas.add(remessaDaLista);
			}

			Record linhaRecebedor = boleto.getInnerRecords().stream().findFirst().get();
			SegmentoQCaixa240 segmentoQ = new SegmentoQCaixa240(linhaRecebedor);

			PessoaFisica sacadoExtraido = segmentoQ.extrairSacado();
			PessoaFisica sacadoDaLista = sacados.stream().filter(s -> s.equals(sacadoExtraido)).findFirst().orElse(null);
			if (sacadoDaLista == null) {
				sacadoDaLista = sacadoExtraido;
				long idSacado = sacados.size() + 1l;
				sacadoDaLista.setId(idSacado);
				sacados.add(sacadoDaLista);
			}

			InfoBoleto boletoExtraido = segmentoP.extrairBoleto(sacadoDaLista, configuradorBoletoDaLista);
			InfoBoleto boletoDaLista = boletos.stream().filter(s -> s.equals(boletoExtraido)).findFirst().orElse(null);
			if (boletoDaLista == null) {
				boletoDaLista = boletoExtraido;
				boletoDaLista.setId(boletos.size() + 1l);
				boletos.add(boletoDaLista);
			}

			remessaDaLista.addBoleto(boletoDaLista);

			SituacaoBoleto situacaoEntradaParaRegistro = new SituacaoBoleto();
			situacaoEntradaParaRegistro.setCodigo(SituacaoBoleto.SITUACAO_ENVIADO_PARA_REGISTRO);

			SituacaoBoleto situacaoCancelamentoSolicitado = new SituacaoBoleto();
			situacaoCancelamentoSolicitado.setCodigo(SituacaoBoleto.SITUACAO_CANCELAMENTO_SOCILICITADO);
			
			SituacaoBoleto situacaoEnviadoParaRegistroAltVencimento = new SituacaoBoleto();
			situacaoEnviadoParaRegistroAltVencimento.setCodigo(SituacaoBoleto.SITUACAO_ENVIADO_PARA_REGISTRO_ALT_VENC);
			// TODO: Atualizar para SituacaoBoleto
			SituacaoBoleto situacaoBoleto = boletoDaLista.getSituacaoBoleto();
			if (situacaoBoleto == null || (segmentoP.getDetCodMovRem() == 1 && !situacaoBoleto.isSituacaoCancelamentoSolicitado())) {
				boletoDaLista.setSituacaoBoleto(situacaoEntradaParaRegistro);
				boletoDaLista.setCodigoMovimento(InfoBoleto.MOVIMENTO_ENTRADA_TITULO);
			} else if (segmentoP.getDetCodMovRem() == 2) {
				boletoDaLista.setSituacaoBoleto(situacaoCancelamentoSolicitado);
				boletoDaLista.setCodigoMovimento(InfoBoleto.MOVIMENTO_PEDIDO_BAIXA);
			} else if (segmentoP.getDetCodMovRem() == 6) {
				boletoDaLista.setSituacaoBoleto(situacaoEnviadoParaRegistroAltVencimento);
			} else {
				boletoDaLista.setSituacaoBoleto(null);
			}
		}

		// Daqui para baixo é apenas relatório
		/**
		 * int quantidadeRemessas = 0; int quantidadeBoletos = 0; for
		 * (ConfiguradorBoleto configuradorBoleto : configuradores) {
		 * List<Remessa> remessas = configuradorBoleto.getRemessas();
		 * quantidadeRemessas = quantidadeRemessas + remessas.size();
		 * System.out.println(configuradorBoleto); for (Remessa remessa :
		 * remessas) { // Na mesma remessa esta vindo boletos configurados para
		 * // protestos e outros não. // Por este motivo temos a mesma remessa
		 * em vários // configuradores System.out.println(remessa);
		 * List<InfoBoleto> boletos = remessa.getBoletos(); quantidadeBoletos =
		 * quantidadeBoletos + boletos.size();
		 * System.out.println("Quantidade de Boletos da remessa: " +
		 * boletos.size()); // for (InfoBoleto boleto : boletos) { //
		 * System.out.println(boleto); // } } }
		 * 
		 * System.out.println("Quantidade de Configuradores: " +
		 * configuradores.size()); System.out.println("Quantidade de Remessas: "
		 * + quantidadeRemessas); System.out.println("Quantidade de boletos: " +
		 * quantidadeBoletos); System.out.println("Quantidade de sacados: " +
		 * sacados.size());
		 */

		PrintWriter writer = new PrintWriter("remessas-para-migracao.sql", "UTF-8");
		for (PessoaFisica sacado : sacados) {
			writer.println(sacado.toString());
		}

		for (ConfiguradorBoleto conf : configuradores) {
			writer.println(conf.toString());
		}

		for (InfoBoleto boleto : boletos) {
			writer.println(boleto.toString());
		}

		for (Remessa remessa : remessas) {
			remessa.geraRemessa(empresa, false);
			writer.println(remessa.toString());
		}

		writer.close();
	}

}
