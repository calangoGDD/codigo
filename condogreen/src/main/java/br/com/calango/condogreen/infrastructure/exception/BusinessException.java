package br.com.calango.condogreen.infrastructure.exception;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class BusinessException extends RuntimeException {

	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Exceções agrupadas
	 */
	protected Set<Message> messageExceptions;

	/**
	 * Mensagem de erro.
	 */
	private Message messageBusiness;

	public BusinessException() {

	}

	public BusinessException(Message message) {
		super();
		this.setMessageBusiness(message);
	}

	/**
	 * Gets the root exception represented by this higher level exception.
	 *
	 * @return The root exception.
	 */
	public Throwable getRootException() {
		return super.getCause();
	}

	/**
	 * Prints this exception and its backtrace to the standard error stream.
	 */
	@Override
	public void printStackTrace() {
		this.printStackTrace(System.out);
	}

	/**
	 * Description of the Method
	 *
	 * @return Description of the Return Value
	 */
	public boolean hasChainOfMessageExceptions() {
		return ((messageExceptions != null) && (messageExceptions.size() > 0));
	}

	/**
	 * Adds a feature to the Exception attribute of the BusinessException object
	 *
	 * @param ex
	 *            The feature to be added to the Exception attribute
	 */
	public void addException(final Message ex) {
		getMessageExceptions().add(ex);
	}

	/**
	 * Adds a feature to the AllExceptions attribute of the BusinessException
	 * object
	 *
	 * @param exptions
	 *            The feature to be added to the AllExceptions attribute
	 */
	public void addAllExceptions(final Collection<? extends Message> exptions) {
		getMessageExceptions().addAll(exptions);
	}

	/**
	 * Gets the exceptions attribute of the BusinessException object
	 *
	 * @return The exceptions value
	 */
	public Set<Message> getMessageExceptions() {
		if (messageExceptions == null) {
			messageExceptions = new HashSet<Message>();
		}
		return messageExceptions;
	}

	public Message getMessageBusiness() {
		return messageBusiness;
	}

	public void setMessageBusiness(Message messageBusiness) {
		this.messageBusiness = messageBusiness;
	}

	public void setMessageExceptions(Set<Message> messageExceptions) {
		this.messageExceptions = messageExceptions;
	}
}
