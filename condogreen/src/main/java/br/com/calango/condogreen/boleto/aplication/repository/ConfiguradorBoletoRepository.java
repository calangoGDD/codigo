package br.com.calango.condogreen.boleto.aplication.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.boleto.domain.ConfiguradorBoleto;

@RepositoryRestResource(collectionResourceRel = "configuradorboletos", path = "configuradorboletos")
public interface ConfiguradorBoletoRepository extends JpaRepository<ConfiguradorBoleto, Long> {

	Page<ConfiguradorBoleto> findByNomeContaining(@Param(value = "nome") String nome, Pageable pePageable);

}
