package br.com.calango.condogreen.condominio.aplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.condominio.domain.Apartamento;

@RepositoryRestResource(collectionResourceRel = "apartamentos", path = "apartamentos")
public interface ApartamentoRepository extends JpaRepository<Apartamento, Long> {

}
