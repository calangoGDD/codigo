package br.com.calango.condogreen.condominio.aplication.resource;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.calango.condogreen.condominio.domain.Apartamento;
import br.com.calango.condogreen.condominio.domain.Morador;
import br.com.calango.condogreen.core.domain.Endereco;

public class MoradorDTO {

	private Long id;
	
	@NotBlank
	private String nome;
	
	@Email
	private String email;
	
	@NotBlank
	private String celular;
	
	private String tipo;
	
	@NotBlank
	private String tipoPessoa;
	
	private boolean responsavelBoleto;
	
	@CPF
	@NotBlank
	private String cpf;
	
	@JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dataNascimento;
	
	@CNPJ
	@NotBlank
	private String cnpj;
	
	private String nomeFantasia;
	
	private List<ApartamentoMoradorDTO> apartamentos;

	private Endereco endereco;
	
	public MoradorDTO() {
		this.apartamentos = new ArrayList<>();
	}

	public MoradorDTO(Morador morador) {
		this();
		this.id = morador.getId();
		this.nome = morador.getNome();
		this.email = morador.getEmail();
		this.celular = morador.getCelular();
		this.tipoPessoa = morador.getTipoPessoa();
		VisitorPessoaConstrutorMoradorDTO visitor = new VisitorPessoaConstrutorMoradorDTO();
		visitor.preencherInformacoesPessoa(morador, this);
		
		List<ApartamentoMorador> apartamentosMorador = morador.getApartamentos();
		for (ApartamentoMorador apartamentoMorador : apartamentosMorador) {
			Apartamento apartamento = apartamentoMorador.getApartamento();
			apartamentos.add(new ApartamentoMoradorDTO(apartamentoMorador.getIdentificacaoRecebedorBoleto(), apartamento.getId(), apartamentoMorador.getTipo(), apartamentoMorador.isResponsavelBoleto()) );
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public boolean isResponsavelBoleto() {
		return responsavelBoleto;
	}

	public void setResponsavelBoleto(boolean responsavelBoleto) {
		this.responsavelBoleto = responsavelBoleto;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public List<ApartamentoMoradorDTO> getApartamentos() {
		return apartamentos;
	}

	public void setApartamentos(List<ApartamentoMoradorDTO> apartamentos) {
		this.apartamentos = apartamentos;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Endereco getEndereco() {
		return this.endereco;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
}
