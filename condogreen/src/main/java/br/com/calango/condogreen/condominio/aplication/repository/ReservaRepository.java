package br.com.calango.condogreen.condominio.aplication.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.condominio.domain.Reserva;

@RepositoryRestResource(collectionResourceRel = "represervas", path = "represervas")
public interface ReservaRepository extends JpaRepository<Reserva, Long>,  JpaSpecificationExecutor<Reserva> {

	Reserva findByInicio(LocalDate inicio);

}
