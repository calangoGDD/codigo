package br.com.calango.condogreen.pessoa.aplication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.pessoa.domain.PessoaFisica;

@RepositoryRestResource(collectionResourceRel = "pessoasfisicas", path = "pessoasfisicas")
public interface PessoaFisicaRepository extends JpaRepository<PessoaFisica, Long> {

	PessoaFisica findByCpf(@Param("cpf") String cpf);

}
