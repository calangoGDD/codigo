package br.com.calango.condogreen.boleto.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.calango.condogreen.boleto.domain.retorno.DetalheRetornoEntrada;
import br.com.calango.condogreen.boleto.domain.retorno.Retorno;
import br.com.calango.condogreen.core.domain.BasicEntity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

/**
 * Esta classe representa as informações do boleto oriundas do Retorno da CAIXA.
 * 
 * @author Danyllo Araujo
 * @since 28/06/2016
 */
@Entity
@Table(name = "informacao_retorno")
public class InformacaoRetorno extends BasicEntity {

	public static final String DATA_DDMMYYYY = "ddMMyyyy";

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = Retorno.FK_ID)
	private Retorno retorno;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = InfoBoleto.FK_ID)
	private InfoBoleto infoBoleto;

	@Column(name = "valor_pago")
	private BigDecimal valorPago;

	@Column(name = "valor_multa_juros")
	private BigDecimal valorMultaJuros;

	@Column(name = "valor_efetivo_creditado")
	private BigDecimal valorEfetivoCreditado;

	@Column(name = "valor_tarifa")
	private BigDecimal valorTarifa;

	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@Column(name = "data_ocorrencia")
	private LocalDateTime dataOcorrencia;

	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@Column(name = "data_efeticacao_credito")
	private LocalDateTime dataEfetivacaoCredito;

	@Column(name = "codigo_movimento")
	private String codigoMovimento;

	@Column(name = "motivo_ocorrencia")
	private String motivoOcorrencia;

	@Column(name = "ordem")
	private int ordem;

	public InformacaoRetorno() {
	}

	public BigDecimal getValorPago() {
		return valorPago;
	}

	public void setValorPago(BigDecimal valorPago) {
		this.valorPago = valorPago;
	}

	public BigDecimal getValorMultaJuros() {
		return valorMultaJuros;
	}

	public void setValorMultaJuros(BigDecimal valorMultaJuros) {
		this.valorMultaJuros = valorMultaJuros;
	}

	public BigDecimal getValorEfetivoCreditado() {
		return valorEfetivoCreditado;
	}

	public void setValorEfetivoCreditado(BigDecimal valorEfetivoCreditado) {
		this.valorEfetivoCreditado = valorEfetivoCreditado;
	}

	public BigDecimal getValorTarifa() {
		return valorTarifa;
	}

	public void setValorTarifa(BigDecimal valorTarifa) {
		this.valorTarifa = valorTarifa;
	}

	/**
	 * @return the retorno
	 */
	public Retorno getRetorno() {
		return retorno;
	}

	/**
	 * @return the infoBoleto
	 */
	public InfoBoleto getInfoBoleto() {
		return infoBoleto;
	}

	public String getMotivoOcorrencia() {
		return motivoOcorrencia;
	}

	public void setMotivoOcorrencia(String motivoOcorrencia) {
		this.motivoOcorrencia = motivoOcorrencia;
	}

	public String getMotivoOcorrenciaDescricao() {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < 10; i += 2) {
			result.append(motivoOcorrencia.substring(i, i + 2)).append(", ");
		}
		return result.substring(0, result.length() - 2);
	}

	public String getCodigoMovimento() {
		return codigoMovimento;
	}

	public void setCodigoMovimento(String codigoMovimento) {
		this.codigoMovimento = codigoMovimento;
	}

	public LocalDateTime getDataOcorrencia() {
		return dataOcorrencia;
	}

	public void setDataOcorrencia(LocalDateTime dataOcorrencia) {
		this.dataOcorrencia = dataOcorrencia;
	}

	public LocalDateTime getDataEfetivacaoCredito() {
		return dataEfetivacaoCredito;
	}

	public void setDataEfetivacaoCredito(LocalDateTime dataEfetivacaoCredito) {
		this.dataEfetivacaoCredito = dataEfetivacaoCredito;
	}

	/**
	 * @param retorno
	 *            the retorno to set
	 */
	public void setRetorno(Retorno retorno) {
		this.retorno = retorno;
	}

	/**
	 * @param infoBoleto
	 *            the infoBoleto to set
	 */
	public void setInfoBoleto(InfoBoleto infoBoleto) {
		this.infoBoleto = infoBoleto;
	}

	public String getNossoNumero() {
		String nossoNumero = "";
		if (this.infoBoleto != null) {
			nossoNumero = this.infoBoleto.getNossoNumero();
		}
		return nossoNumero;
	}

	public String getNomeBoleto() {
		String nome = "";
		if (this.infoBoleto != null) {
			nome = this.infoBoleto.getNomeBoleto();
		}
		return nome;
	}
	
	/**
	 * Método responsável por retornar o detalhamento de ocorrências do retorno
	 * @return
	 */
	public String getDetalhe(){
		return DetalheRetornoEntrada.getDetalheFormatado(codigoMovimento, motivoOcorrencia);
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}
	
	public int getOrdem() {
		return ordem;
	}
	
}
