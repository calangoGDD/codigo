package br.com.calango.condogreen.infrastructure.exception;

public interface IValidator<T> {

	void checkBrokenRules(T entity) throws BusinessException, Throwable;

	void registerRulesSpecification() throws BusinessException;
}