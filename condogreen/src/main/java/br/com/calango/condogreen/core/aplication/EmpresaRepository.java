package br.com.calango.condogreen.core.aplication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

@RepositoryRestResource(collectionResourceRel = "empresas", path = "empresas")
public interface EmpresaRepository extends JpaRepository<PessoaJuridica, Long> {

}
