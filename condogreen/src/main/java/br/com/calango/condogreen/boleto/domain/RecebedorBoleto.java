package br.com.calango.condogreen.boleto.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.calango.condogreen.condominio.aplication.resource.ApartamentoMorador;
import br.com.calango.condogreen.core.domain.BasicEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "recebe_boleto")
@Inheritance(strategy = InheritanceType.JOINED)
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public abstract class RecebedorBoleto extends BasicEntity {

	public static final String FK_ID = "recebedor_boleto_id";

	@OneToMany(mappedBy = "recebedorBoleto", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ValorCustomizado> valoresCustomizados;

	public RecebedorBoleto() {
		this.valoresCustomizados = new ArrayList<>();
	}

	public abstract ApartamentoMorador getResponsavelPorPagarBoleto();

	public abstract String getNome();

	/**
	 * @return the valoresCustomizados
	 */
	public List<ValorCustomizado> getValoresCustomizados() {
		return valoresCustomizados;
	}

	/**
	 * @param valoresCustomizados
	 *            the valoresCustomizados to set
	 */
	public void setValoresCustomizados(List<ValorCustomizado> valoresCustomizados) {
		this.valoresCustomizados = valoresCustomizados;
	}

	public String getIdentificacaoRecebedorBoleto() {
		return null;
	}
}
