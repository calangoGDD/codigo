package br.com.calango.condogreen.boleto.domain;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

public class BoletoDTO {

	@NotNull
	@JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dataVencimento;

	@NotNull
	private BigDecimal valor;

	@NotBlank
	private String uriConfiguradorBoleto;

	@NotBlank
	private String uriRecebedorBoleto;

	public BoletoDTO() {
	}

	/**
	 * @return the dataVencimento
	 */
	public LocalDate getDataVencimento() {
		return dataVencimento;
	}

	/**
	 * @param dataVencimento
	 *            the dataVencimento to set
	 */
	public void setDataVencimento(LocalDate dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	/**
	 * @return the uriConfiguradorBoleto
	 */
	public String getUriConfiguradorBoleto() {
		return uriConfiguradorBoleto;
	}

	/**
	 * @param uriConfiguradorBoleto
	 *            the uriConfiguradorBoleto to set
	 */
	public void setUriConfiguradorBoleto(String uriConfiguradorBoleto) {
		this.uriConfiguradorBoleto = uriConfiguradorBoleto;
	}

	/**
	 * @return the uriRecebedorBoleto
	 */
	public String getUriRecebedorBoleto() {
		return uriRecebedorBoleto;
	}

	/**
	 * @param uriRecebedorBoleto
	 *            the uriRecebedorBoleto to set
	 */
	public void setUriRecebedorBoleto(String uriRecebedorBoleto) {
		this.uriRecebedorBoleto = uriRecebedorBoleto;
	}

}
