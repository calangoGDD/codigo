package br.com.calango.condogreen.condominio.aplication.resource;

import br.com.calango.condogreen.condominio.domain.Morador;
import br.com.calango.condogreen.pessoa.domain.IPessoa;
import br.com.calango.condogreen.pessoa.domain.PessoaFisica;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;
import br.com.calango.condogreen.pessoa.domain.VisitorPessoa;

public class VisitorPessoaConstrutorMoradorDTO {

	public void preencherInformacoesPessoa(Morador morador, MoradorDTO moradorDTO) {
		IPessoa pessoa = morador.getPessoa();
		
		VisitorPessoa visitor = new VisitorPessoa() {
			
			@Override
			public void visit(PessoaJuridica pessoaJuridica) {
				moradorDTO.setCnpj(pessoaJuridica.getCnpj());
				moradorDTO.setNomeFantasia(pessoaJuridica.getNomeFantasia());
			}
			
			@Override
			public void visit(PessoaFisica pessoaFisica) {
				moradorDTO.setCpf(pessoaFisica.getCpf());
				moradorDTO.setDataNascimento(pessoaFisica.getDataNascimento());
			}
		};
		
		pessoa.accept(visitor);
		
	}
}
