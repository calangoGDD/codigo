package br.com.calango.condogreen.infrastructure.specification;

public interface ISpecification<T> {

	boolean isSatisfiedBy(T entity);
}
