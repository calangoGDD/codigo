package br.com.calango.condogreen.core.aplication;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.mail.MailException;

import br.com.calango.condogreen.boleto.domain.InfoBoleto;
import br.com.calango.condogreen.boleto.domain.remessa.Remessa;
import br.com.calango.condogreen.portal.aplication.resource.FormularioContato;

public interface Notificador {

	/**
	 * Método responsável por notificar o responsável pelo pagamento que o
	 * boleto já está disponível.
	 * 
	 * @param boleto
	 * @throws MessagingException
	 * @throws MailException
	 * @throws IOException
	 */
	void notificarBoletoDisponivel(InfoBoleto boleto, byte[] arquivo) throws MailException, MessagingException, IOException;

	/**
	 * Método responsável por notificar o responsáveis pelo pagamento que o
	 * respectivo boleto já está disponível.
	 * 
	 * @param boleto
	 * @throws MessagingException
	 * @throws MailException
	 * @throws IOException
	 */
	void notificarBoletosDisponiveis(List<InfoBoleto> boletos) throws MailException, MessagingException, IOException;

	/**
	 * Método responsável por notificar o responsáveis pelo pagamento que o
	 * respectivo boleto já está disponível.
	 * 
	 * @param remessa
	 * @throws MailException
	 * @throws MessagingException
	 * @throws IOException
	 */
	void notificarBoletosDisponiveis(Remessa remessa) throws MailException, MessagingException, IOException;

	/**
	 * Método responsável por notificar administrador do condomínio - Fale conosco
	 * @param formularioContato 
	 * @param boleto
	 * @param arquivo
	 * @throws MailException
	 * @throws MessagingException
	 */
	void notificarFaleConosco(FormularioContato formularioContato) throws MailException, MessagingException;

}
