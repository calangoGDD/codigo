package br.com.calango.condogreen.condominio.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name = "categoria_documento")
public class DocumentoCategoria extends AbstractPersistable<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String FK_ID_CATEGORIA = "categoria_id";
	
	@NotBlank
	@NotNull
	@Column(name = "nome", length = 100)
	private String nome;
	
	@NotBlank
	@NotNull
	@Column(name = "simbolo", length = 2)
	private String simbolo;
	
	@OneToMany(mappedBy = "documentoCategoria", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Documento> documentos;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSimbolo() {
		return simbolo;
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}

	public List<Documento> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<Documento> documentos) {
		this.documentos = documentos;
	}

}
