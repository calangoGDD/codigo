package br.com.calango.condogreen.boleto.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Código adotado pela FEBRABAN para identificar o tipo de prazo a ser
 * considerado para o protesto. ‘1’ = Protestar ‘3’ = Não Protestar ‘9’ =
 * Cancelamento Protesto Automático (Somente válido p/ Código Movimento Remessa
 * = ‘31’ - Alteração de Outros Dados), vide nota C004.
 * 
 * @author Guilherme Andrade
 *
 */
public enum CodigoProtesto {

	PROTESTAR(1), NAO_PROTESTAR(3), CANCELAMENTO_AUTOMATICO(9), ALTERACAO_DADOS(31);

	/**
	 * Codigo do tipo de pagamento de juros mora.
	 */
	private Integer codigo;

	CodigoProtesto(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the codigo
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	private static Map<Integer, CodigoProtesto> map = new HashMap<Integer, CodigoProtesto>();

	static {
		for (CodigoProtesto codigoProtesto : CodigoProtesto.values()) {
			map.put(codigoProtesto.getCodigo(), codigoProtesto);
		}
	}

	public static CodigoProtesto getEnum(int mora) {
		return map.get(mora);
	}

}
