package br.com.calango.condogreen.boleto.domain.remessa;

import org.apache.commons.lang.StringUtils;
import org.jrimum.domkee.comum.pessoa.endereco.UnidadeFederativa;
import org.jrimum.texgit.Record;

import br.com.calango.condogreen.boleto.util.RemessaUtil;
import br.com.calango.condogreen.core.domain.Endereco;
import br.com.calango.condogreen.pessoa.domain.PessoaFisica;

/**
 * Classe responsável por setar os valores do Segmento "Q"
 * 
 * Este segmento representa as informações do Sacado.
 * 
 * @author Guilherme Andrade
 *
 */
public class SegmentoQCaixa240 {

	private Record segmentoQ;

	public SegmentoQCaixa240(Record segmentoQ) {
		this.segmentoQ = segmentoQ;
	}

	public void setNumRegistro(int seq) {
		segmentoQ.setValue("qNumRegistro", seq);
	}

	public int getNumRegistro() {
		return segmentoQ.getValue("qNumRegistro");
	}

	public void setTipoInscricao(String tipoInscricao) {
		segmentoQ.setValue("qTipoInscricao", tipoInscricao);
	}

	public void setNumInscricao(String numInscricao) {
		segmentoQ.setValue("qNumInscricao", RemessaUtil.retiraCaracteresEspeciais(numInscricao));
	}

	public String getNumInscricao() {
		return segmentoQ.getValue("qNumInscricao");
	}

	public void setNome(String nome) {
		String empNome = RemessaUtil.retiraCaracteresEspeciais(StringUtils.substring(nome, 0, 30));
		segmentoQ.setValue("qNome", empNome);
	}

	public String getNome() {
		return segmentoQ.getValue("qNome");
	}

	public void setEndereco(String rua) {
		String endereco = RemessaUtil.retiraCaracteresEspeciais(StringUtils.substring(rua, 0, 40));
		segmentoQ.setValue("qEndereco", endereco);
	}

	public String getEndereco() {
		return segmentoQ.getValue("qEndereco");
	}

	public void setBairro(String bairro) {
		String qBairro = RemessaUtil.retiraCaracteresEspeciais(StringUtils.substring(bairro, 0, 15));
		segmentoQ.setValue("qBairro", qBairro);
	}

	public String getBairro() {
		return segmentoQ.getValue("qBairro");
	}

	public void setCep(String cep) {
		segmentoQ.setValue("qCEP", StringUtils.left(cep, 5));
	}

	public String getCep() {
		return segmentoQ.getValue("qCEP");
	}

	public void setSufixoCep(String sufixoCep) {
		segmentoQ.setValue("qCEPSuf", StringUtils.right(sufixoCep, 3));
	}

	public String getSufixoCep() {
		return segmentoQ.getValue("qCEPSuf");
	}

	public void setCidade(String cidade) {
		String qCidade = RemessaUtil.retiraCaracteresEspeciais(StringUtils.substring(cidade, 0, 15));
		segmentoQ.setValue("qCidade", qCidade);
	}

	public String getCidade() {
		return segmentoQ.getValue("qCidade");
	}

	public void setUf(String uf) {
		segmentoQ.setValue("qUF", uf);
	}

	public String getUf() {
		return segmentoQ.getValue("qUF");
	}

	public Record getSegmento() {
		return this.segmentoQ;
	}

	public PessoaFisica extrairSacado() {
		PessoaFisica pessoa = new PessoaFisica();
		String cpf = getNumInscricao();
		pessoa.setCpf(cpf.substring(4, cpf.length()));
		pessoa.setNome(getNome());
		Endereco endereco = new Endereco();
		endereco.setComplemento(getEndereco());
		endereco.setRua(getEndereco());
		endereco.setBairro(getBairro());
		endereco.setCep(getCep());
		endereco.setSufixoCep(getSufixoCep());
		endereco.setCidade(getCidade());
		endereco.setUf(UnidadeFederativa.valueOfSigla(getUf()));
		pessoa.setEndereco(endereco);

		return pessoa;
	}

	public void setCodMovimento(String codMovimento) {
		segmentoQ.setValue("qCodMovimento", codMovimento);
	}
}
