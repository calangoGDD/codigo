package br.com.calango.condogreen.boleto.domain.retorno;

import java.util.Map;

public interface QuadroDetalhamento {

	Map<String, String> getQuadro();

}
