package br.com.calango.condogreen.portal.aplication.resource;

import javax.mail.MessagingException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.calango.condogreen.portal.aplication.service.PortalService;

@RepositoryRestController
@RequestMapping("/fale-conosco")
public class FaleConoscoResource {

	private PortalService portalService;

	@Autowired
	public FaleConoscoResource(PortalService portalService) {
		this.portalService = portalService;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/submit")
	@ResponseBody
	public void submit(@Valid @RequestBody FormularioContato formularioContato ) throws MailException, MessagingException {
		portalService.enviarEmailFaleConosco(formularioContato);
	}
}
