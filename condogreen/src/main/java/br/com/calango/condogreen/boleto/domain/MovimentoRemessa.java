package br.com.calango.condogreen.boleto.domain;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.calango.condogreen.boleto.domain.remessa.Remessa;

public class MovimentoRemessa {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = Remessa.FK_ID)
	private Remessa remessa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = InfoBoleto.FK_ID)
	private InfoBoleto infoBoleto;
}
