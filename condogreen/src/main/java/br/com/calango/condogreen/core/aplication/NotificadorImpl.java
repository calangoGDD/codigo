package br.com.calango.condogreen.core.aplication;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Component;

import br.com.calango.condogreen.boleto.aplication.repository.BoletoRepository;
import br.com.calango.condogreen.boleto.domain.InfoBoleto;
import br.com.calango.condogreen.boleto.domain.remessa.Remessa;
import br.com.calango.condogreen.boleto.domain.validacao.DeveNotificarApenasBoletosComSituacaoEntradaConfirmada;
import br.com.calango.condogreen.core.domain.Conta;
import br.com.calango.condogreen.pessoa.aplication.PessoaJurdicaRepository;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;
import br.com.calango.condogreen.portal.aplication.resource.FormularioContato;

@Component
public class NotificadorImpl implements Notificador {

	@Value("${spring.mail.username}")
	private String from;

	@Value("${spring.mail.faleconosco}")
	private String faleconosco;
	
	private Enviador enviador;

	private BoletoRepository boletoRepository;

	@Autowired
	private PessoaJurdicaRepository pessoaJurdicaRepository;

	@Autowired
	private DeveNotificarApenasBoletosComSituacaoEntradaConfirmada deveNotificarApenasBoletosComSituacaoEntradaConfirmada;

	@Autowired
	public NotificadorImpl(Enviador enviador, BoletoRepository boletoRepository) {
		this.enviador = enviador;
		this.boletoRepository = boletoRepository;
	}

	/*
	 * @see br.com.calango.condogreen.core.aplication.Notificador#
	 * notificarBoletosDisponiveis(java.util.List)
	 */
	@Override
	public void notificarBoletosDisponiveis(List<InfoBoleto> boletos) throws MailException, MessagingException, IOException {
		PessoaJuridica empresa = pessoaJurdicaRepository.findTopByOrderByIdAsc();
		for (InfoBoleto boleto : boletos) {
			if (deveNotificarApenasBoletosComSituacaoEntradaConfirmada.isSatisfiedBy(boleto)) {
				notificarBoletoDisponivel(boleto, boleto.getArquivo(empresa));
				boletoRepository.save(boleto);
			}
		}
	}

	/*
	 * @see br.com.calango.condogreen.core.aplication.Notificador#
	 * notificarBoletoDisponivel
	 * (br.com.calango.condogreen.boleto.domain.geracao.InfoBoleto)
	 */
	@Override
	public void notificarBoletoDisponivel(InfoBoleto boleto, byte[] arquivo) throws MailException, MessagingException, IOException {
		Conta conta = boleto.getSacado().getConta();
		// TODO: remover esta verificação pois todos os sacados deverão ter uma
		// conta.
		if (conta != null) {
			String email = conta.getEmail();
			String vencimento = boleto.getDataVencimentoFormatada();
			String assunto = String.format("Boleto %s disponível", vencimento);
			String corpoMensagem = String.format("Boleto referent à taxa de condomínio com o vencimento %s já está disponível pra baixar!",
			        vencimento);

			if (arquivo == null) {
				enviador.enviar(email, from, assunto, corpoMensagem);
			} else {
				enviador.enviar(email, from, assunto, corpoMensagem, arquivo);
			}
		}
	}

	/*
	 * @see br.com.calango.condogreen.core.aplication.Notificador#
	 * notificarBoletosDisponiveis
	 * (br.com.calango.condogreen.boleto.domain.remessa.Remessa)
	 */
	@Override
	public void notificarBoletosDisponiveis(Remessa remessa) throws MailException, MessagingException, IOException {
		notificarBoletosDisponiveis(remessa.getBoletos());
	}
	
	/*
	 * @see br.com.calango.condogreen.core.aplication.Notificador#notificarFaleConosco(br.com.calango.condogreen.boleto.domain.geracao.InfoBoleto, byte[])
	 */
	@Override
	public void notificarFaleConosco(FormularioContato formularioContato) throws MailException, MessagingException {
		String replyTo = formularioContato.getEmail();
		String assunto = formularioContato.getAssunto();
		String corpoMensagem = formularioContato.getMensagem();
		enviador.enviarWithReplyTo(faleconosco, replyTo, from, assunto, corpoMensagem);
	}
}
