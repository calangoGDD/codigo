package br.com.calango.condogreen.condominio.domain.validacao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.calango.condogreen.condominio.aplication.repository.ApartamentoRepository;
import br.com.calango.condogreen.condominio.aplication.resource.ApartamentoMorador;
import br.com.calango.condogreen.condominio.aplication.resource.ApartamentoMoradorDTO;
import br.com.calango.condogreen.condominio.aplication.resource.MoradorDTO;
import br.com.calango.condogreen.condominio.domain.Apartamento;
import br.com.calango.condogreen.infrastructure.specification.ISpecification;

@Component("naoExisteMoradorResponsavelPorPagamentoParaOApartamento")
public class NaoExisteMoradorResponsavelPorPagamentoParaOApartamento implements ISpecification<MoradorDTO> {

	@Autowired
	private ApartamentoRepository apartamentoRepository;
	
	/**
	 * @see br.com.calango.condogreen.infrastructure.specification.ISpecification#isSatisfiedBy(java.lang.Object)
	 */
	@Override
	public boolean isSatisfiedBy(MoradorDTO moradorDTO) {
		boolean result = true;
		List<ApartamentoMoradorDTO> apartamentos = moradorDTO.getApartamentos();
		for (ApartamentoMoradorDTO apartamentoMoradorDTO : apartamentos) {
			if(apartamentoMoradorDTO.getResponsavelBoleto()) {
				Apartamento apartamento = apartamentoRepository.findOne(apartamentoMoradorDTO.getApartamentoId());
				ApartamentoMorador responsavelPorPagarBoleto = apartamento.getResponsavelPorPagarBoleto();
				if(responsavelPorPagarBoleto != null) {
					//TODO ajustar infra para aceitar parâmetros
					result = false;
				}
			}
		}
		
		return result;
	}

}
