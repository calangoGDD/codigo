package br.com.calango.condogreen.infrastructure;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.stereotype.Component;

@Component
public class FlywayExecutor implements FlywayMigrationStrategy {

	@Value("${flyway.clean}")
	private boolean cleanDB;

	@Override
	public void migrate(Flyway flyway) {
		if (cleanDB) {
			flyway.clean();
		}
		flyway.migrate();
	}
}
