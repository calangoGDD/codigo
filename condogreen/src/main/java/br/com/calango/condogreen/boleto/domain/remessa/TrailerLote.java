package br.com.calango.condogreen.boleto.domain.remessa;

import java.math.BigDecimal;

import org.jrimum.texgit.Record;

import br.com.calango.condogreen.boleto.util.RemessaUtil;

public class TrailerLote {

	private Record trailerLote;

	public TrailerLote(Record trailerLote) {
		this.trailerLote = trailerLote;
	}

	public void setQtdRegistro(int sequencia) {
		trailerLote.setValue("tQtdRegistros", sequencia);
	}

	public void setTituloCobranca(int sequencia) {
		trailerLote.setValue("tTitCobranca", sequencia);

	}

	public void setValorTotalTitulo(BigDecimal valorTotal) {
		trailerLote.setValue("tValTotTit", RemessaUtil.formatarNumeroComDuasCasasDecimais(valorTotal));
	}

	public void construir(BigDecimal valorTotal, int sequencia, int qtdBoletos) {
		this.setQtdRegistro(sequencia);
		this.setTituloCobranca(qtdBoletos);
		this.setValorTotalTitulo(valorTotal);
	}

	public Record geTrailerLote() {
		return trailerLote;
	}

}
