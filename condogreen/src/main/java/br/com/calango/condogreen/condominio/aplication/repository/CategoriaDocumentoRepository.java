package br.com.calango.condogreen.condominio.aplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.condominio.domain.DocumentoCategoria;

@RepositoryRestResource(collectionResourceRel = "repcategoriasdocumento", path = "repcategoriasdocumento")
public interface CategoriaDocumentoRepository extends JpaRepository<DocumentoCategoria, Long>,  JpaSpecificationExecutor<DocumentoCategoria> {


}

