package br.com.calango.condogreen.pessoa.domain;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.calango.condogreen.condominio.aplication.resource.MoradorDTO;
import br.com.calango.condogreen.condominio.domain.Morador;
import br.com.calango.condogreen.core.domain.Conta;

public class VisitorConstrutorPessoa {

	Map<String, IPessoa> instancia = new HashMap<>();
	{
		instancia.put(PessoaFisica.TIPO_INSCRICAO_PESSOA_FISICA, new PessoaFisica());
		instancia.put(PessoaJuridica.TIPO_INSCRICAO_PESSOA_JURIDICA, new PessoaJuridica());
	}
	
	//TODO colocar em um builder, dessa forma o construtor de pessoa está ligado ao VO de morador?
	public IPessoa construtorPessoa(MoradorDTO moradorDTO) {
		IPessoa pessoa = instancia.get(moradorDTO.getTipoPessoa());
		
		preencheInfoPessoa(moradorDTO, pessoa);
		
		BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
		pessoa.setConta(new Conta(moradorDTO.getCpf(), moradorDTO.getEmail(), bCrypt.encode(pessoa.getNome().split(" ")[0].toUpperCase())));
		
		return pessoa;
	}

	public IPessoa atualizarPessoa(MoradorDTO moradorDTO, Morador morador) {
		IPessoa pessoa = morador.getPessoa();
		preencheInfoPessoa(moradorDTO, pessoa);
		
		BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
		Conta conta = pessoa.getConta();
		conta.setEmail(moradorDTO.getEmail());
		conta.setNome(moradorDTO.getCpf());
		conta.setSenha(bCrypt.encode(pessoa.getNome().split(" ")[0].toUpperCase()));
		
		return pessoa;
	}
	
	private void preencheInfoPessoa(MoradorDTO moradorDTO, IPessoa pessoa) {
		VisitorPessoa visitor = new VisitorPessoa() {
			
			@Override
			public void visit(PessoaJuridica pessoaJuridica) {
				pessoaJuridica.setCnpj(moradorDTO.getCnpj());
				pessoaJuridica.setNomeFantasia(moradorDTO.getNomeFantasia());
			}
			
			@Override
			public void visit(PessoaFisica pessoaFisica) {
				pessoaFisica.setCpf(moradorDTO.getCpf());
				
				pessoaFisica.setDataNascimento(moradorDTO.getDataNascimento());
			}
		};
		
		pessoa.accept(visitor);
		
		String celular = moradorDTO.getCelular();
		pessoa.setCodigoAreaCelular(celular.substring(0, 2));
		pessoa.setCelular(celular.substring(2, 11));
		pessoa.setNome(moradorDTO.getNome());
		pessoa.setEndereco(moradorDTO.getEndereco());
	}
}
