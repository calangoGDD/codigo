package br.com.calango.condogreen.condominio.aplication.resource;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.calango.condogreen.condominio.aplication.service.MoradorService;
import br.com.calango.condogreen.condominio.domain.Morador;

@RepositoryRestController
@RequestMapping("/moradores")
public class MoradorResource {

	@Autowired
	MoradorService moradorService;
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public Page<Morador> search(Pageable pageable) {
		return moradorService.findAll(pageable);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/get/{id:\\d+}")
	@ResponseBody
	public MoradorDTO get(@PathVariable("id") long id) {
		Morador morador = moradorService.findById(id);
		return new MoradorDTO(morador);
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "/delete/{id:\\d+}")
	public void delete(@PathVariable("id") long id) throws IOException {
		moradorService.delete(id);
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public void salvar(@Valid @RequestBody MoradorDTO moradorDTO) throws IOException {
		moradorService.save(moradorDTO);
	}
}
