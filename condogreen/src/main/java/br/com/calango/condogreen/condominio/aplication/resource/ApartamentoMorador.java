package br.com.calango.condogreen.condominio.aplication.resource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.data.rest.core.annotation.RestResource;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.calango.condogreen.condominio.domain.Apartamento;
import br.com.calango.condogreen.condominio.domain.IMorador;
import br.com.calango.condogreen.condominio.domain.Morador;
import br.com.calango.condogreen.pessoa.domain.IPessoa;

@Entity
@Table(name = "apartamento_morador")
public class ApartamentoMorador  extends AbstractPersistable<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonBackReference
	@RestResource(rel = "apartamentoM")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = Apartamento.FK_ID)
	private Apartamento apartamento;
	
	@JsonBackReference
	@RestResource(rel = "moradorA")
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Morador.class)
	@JoinColumn(name = Morador.FK_ID)
	private IMorador morador;
	
	@NotBlank
	@Column(name = "tipo")
	private String tipo;

	@Column(name = "responsavel_boleto")
	private boolean responsavelBoleto;

	
	public ApartamentoMorador() {}
	
	public ApartamentoMorador(Apartamento apartamento, Morador morador, String tipo, Boolean responsavelBoleto) {
		this.apartamento = apartamento;
		this.morador = morador;
		this.tipo = tipo;
		this.responsavelBoleto = responsavelBoleto;
	}

	public Apartamento getApartamento() {
		return apartamento;
	}

	public void setApartamento(Apartamento apartamento) {
		this.apartamento = apartamento;
	}

	public IMorador getMorador() {
		return morador;
	}

	public void setMorador(IMorador morador) {
		this.morador = morador;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public boolean isResponsavelBoleto() {
		return responsavelBoleto;
	}

	public void setResponsavelBoleto(boolean responsavelBoleto) {
		this.responsavelBoleto = responsavelBoleto;
	}

	public IPessoa getPessoa() {
		return morador.getPessoa();
	}
	
	public String getIdentificacaoRecebedorBoleto() {
		return this.apartamento.getIdentificacaoRecebedorBoleto();
	}
	
}
