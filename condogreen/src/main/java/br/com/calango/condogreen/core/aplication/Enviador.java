package br.com.calango.condogreen.core.aplication;

import java.io.IOException;

import javax.mail.MessagingException;

import org.springframework.mail.MailException;

public interface Enviador {

	void enviar(String to, String from, String subject, String text);

	void enviar(String to, String from, String subject, String text, byte[] attachment) throws MailException, MessagingException, IOException;

	void enviarWithReplyTo(String to, String replyTo, String from, String subject, String text);

}
