package br.com.calango.condogreen.core.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.AbstractPersistable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Conta")
@NamedQuery(name = Conta.FIND_BY_EMAIL, query = "select u from Conta u where u.email = :email")
@JsonIgnoreProperties(ignoreUnknown = true, value = { "hibernateLazyInitializer", "handler" })
public class Conta extends AbstractPersistable<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String FIND_BY_EMAIL = "Conta.findByEmail";

	public static final String FK_ID = "conta_id";

	@Email(message = "{email.message}")
	@Column(name = "email", unique = true)
	private String email;

	@Column(name = "nome")
	private String nome;

	@NotBlank
	@Column(name = "senha")
	@JsonIgnore
	private String senha;

	@JsonIgnore
	@OneToMany(mappedBy = "conta", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Perfil> perfis;

	public Conta() {
		this.perfis = new ArrayList<>();
	}

	public Conta(String nome, String email, String senha) {
		this();
		this.nome = nome;
		this.email = email;
		this.senha = senha;
		Perfil perfil = new Perfil();
		perfil.setConta(this);
		perfil.setNome(Perfil.BASICO);
		this.perfis.add(perfil);
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * @param senha
	 *            the senha to set
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * @return the perfis
	 */
	public List<Perfil> getPerfis() {
		return perfis;
	}

	/**
	 * @param perfis
	 *            the perfis to set
	 */
	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

}
