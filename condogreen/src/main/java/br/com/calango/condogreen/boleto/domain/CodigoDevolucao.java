package br.com.calango.condogreen.boleto.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Código adotado pela FEBRABAN para identificar qual o procedimento a ser
 * adotado com o Título. ‘1’ = Baixar / Devolver ‘2’ = Não Baixar / Não Devolver
 * 
 * @author Guilherme Andrade
 *
 */
public enum CodigoDevolucao {

	BAIXAR_DEVOLVER(1), NAO_BAIXAR_NAO_DEVOLVER(2);

	/**
	 * Codigo do tipo de pagamento de juros mora.
	 */
	private Integer codigo;

	CodigoDevolucao(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the codigo
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	private static Map<Integer, CodigoDevolucao> map = new HashMap<Integer, CodigoDevolucao>();

	static {
		for (CodigoDevolucao codigoDevolucao : CodigoDevolucao.values()) {
			map.put(codigoDevolucao.getCodigo(), codigoDevolucao);
		}
	}

	public static CodigoDevolucao getEnum(int codigo) {
		return map.get(codigo);
	}
}
