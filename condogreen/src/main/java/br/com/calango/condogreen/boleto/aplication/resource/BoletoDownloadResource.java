package br.com.calango.condogreen.boleto.aplication.resource;

import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.calango.condogreen.boleto.aplication.repository.BoletoRepository;
import br.com.calango.condogreen.boleto.domain.InfoBoleto;
import br.com.calango.condogreen.core.aplication.DownloadsResource;
import br.com.calango.condogreen.pessoa.aplication.PessoaJurdicaRepository;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

@RestController
@RequestMapping("downloads/boleto")
public class BoletoDownloadResource extends DownloadsResource {

	private BoletoRepository boletoRepository;

	private PessoaJurdicaRepository pessoaJurdicaRepository;

	@Autowired
	public BoletoDownloadResource(BoletoRepository boletoRepository, PessoaJurdicaRepository pessoaJurdicaRepository) {
		this.boletoRepository = boletoRepository;
		this.pessoaJurdicaRepository = pessoaJurdicaRepository;
	}

	@RequestMapping(path = "{nossoNumero}/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public HttpEntity<byte[]> arquivoBoleto(@PathVariable("nossoNumero") String nossoNumero, HttpResponse response) {
		InfoBoleto boleto = boletoRepository.findByNossoNumero(nossoNumero);
		PessoaJuridica empresa = pessoaJurdicaRepository.findTopByOrderByIdAsc();
		return download(String.format("boleto_%s.pdf", nossoNumero), boleto.getArquivo(empresa));
	}
}
