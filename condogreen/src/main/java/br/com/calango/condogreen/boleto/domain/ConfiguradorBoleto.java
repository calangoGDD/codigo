package br.com.calango.condogreen.boleto.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeTitulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo.Aceite;

import br.com.calango.condogreen.core.domain.BasicEntity;

/**
 * Classe responsável por Configurar os boletos.
 * 
 * @author Guilherme Andrade
 *
 */
@Entity
@Table(name = "configurador_boleto")
public class ConfiguradorBoleto extends BasicEntity {

	public static final String FK_ID = "configurador_id";

	@NotBlank
	@Column(name = "nome")
	private String nome;

	/**
	 * Valor Nominal do Título
	 */
	@Column(name = "valor", columnDefinition = "Decimal(15,2) default '0.00'")
	private BigDecimal valor;

	/**
	 * Código adotado pela FEBRABAN para identificar o tipo de título de
	 * cobrança. Cód ID Descrição 01 CH Cheque 02 DM Duplicata Mercantil 03 DMI
	 * Duplicata Mercantil p/ Indicação 04 DS Duplicata de Serviço 05 DSI
	 * Duplicata de Serviço p/ Indicação 06 DR Duplicata Rural 07 LC Letra de
	 * Câmbio 08 NCC Nota de Crédito Comercial 09 NCE Nota de Crédito a
	 * Exportação 10 NCI Nota de Crédito Industrial 11 NCR Nota de Crédito Rural
	 * 12 NP Nota Promissória 13 NPR Nota Promissória Rural 14 TM Triplicata
	 * Mercantil 15 TS Triplicata de Serviço 16 NS Nota de Seguro 17 RC Recibo
	 * 18 FAT Fatura 19 ND Nota de Débito 20 AP Apólice de Seguro 21 ME
	 * Mensalidade Escolar 22 PC Parcela de Consórcio 23 NF Nota Fiscal 24 DD
	 * Documento de Dívida 25 CPR Cédula de Produto Rural 99 OU Outros
	 */
	@NotNull
	@Column(name = "tp_titulo")
	@Enumerated(EnumType.STRING)
	private TipoDeTitulo especieDoTitulo;

	/**
	 * Código adotado pela FEBRABAN para identificar se o título de cobrança foi
	 * aceito (reconhecimento da dívida pelo Sacado). ‘A’ = Aceite ‘N’ = Não
	 * Aceite
	 */
	@NotNull
	@Column(name = "aceite")
	@Enumerated(EnumType.STRING)
	private Aceite aceite;

	/**
	 * Indica o tipo de pagamento de juros de mora; preencher com o tipo de
	 * preferência: ‘1’ (Valor por Dia); ou ‘2’ (Taxa Mensal); ou ‘3’ (Isento)
	 */
	@NotNull
	@Column(name = "cod_mora")
	@Enumerated(EnumType.STRING)
	private CodigoJurosMora codMora;

	/**
	 * Juros é a monta que você paga, percentualmente sobre o principal, por dia
	 * de atraso.
	 * 
	 * Valor ou porcentagem sobre o valor do título a ser cobrado de juros de
	 * mora. Se Código do Juros de Mora (nota C018): = ‘1’ Informar valor = ‘2’
	 * Informar percentual. Caso a data seja inválida ou não informada o sistema
	 * assumirá a data de vencimento + 1.
	 * 
	 * Estamos deixando em branco o campo de data na remessa. Logo a data a ser
	 * considerada no calculo é a data de vencimento +1
	 * 
	 * NÃO COLOCAMOS A DATA DO JUROS PORQUE CASO NÃO COLOQUE O SISTEMA DA CAIXA
	 * ASSUME O DIA DO VENCIMENTO +1
	 */
	@Column(name = "vl_mora", columnDefinition = "Decimal(15,2) default '0.00'")
	private BigDecimal valorMora;

	/**
	 * Código adotado pela FEBRABAN para identificação do critério de pagamento
	 * de pena pecuniária, a ser aplicada pelo atraso do pagamento do Título.
	 * Pode ser: ‘0’ = Sem Multa '1' = Valor Fixo '2' = Percentual
	 */
	@NotNull
	@Column(name = "cod_multa")
	@Enumerated(EnumType.STRING)
	private CodigoMulta codMulta;

	/**
	 * Valor ou percentual de multa a ser aplicado sobre o valor do Título, por
	 * atraso no pagamento. Se Código da Multa (Nota G073): = ‘1’ Informar valor
	 * = ‘2’ Informar percentual
	 */
	@Column(name = "vl_multa", columnDefinition = "Decimal(15,2) default '0.00'")
	private BigDecimal valorMulta;

	/**
	 * Indica o tipo de desconto que deseja conceder ao Pagador do título: ‘0’
	 * (Sem Desconto); ou ‘1’ (Valor Fixo até a Data do Desconto informada); ou
	 * ‘2’ (Percentual até a Data do Desconto informada)
	 */
	@NotNull
	@Column(name = "cod_descont")
	@Enumerated(EnumType.STRING)
	private CodigoDesconto codDesconto;

	/**
	 * Campo que informa até que dia de pagamento o boleto possui desconto
	 */
	@Column(name = "dia_desconto")
	private int diaDeLimiteDesconto;

	/**
	 * Preencher de acordo com a informação do campo 30.3P (Código do Desconto),
	 * no formato DDMMAAAA (Dia, Mês e Ano); Se 30.3P = ‘0’, preencher com
	 * zeros; Se = '1' ou '2', informar a data limite do desconto
	 */
	@Column(name = "vl_desc", columnDefinition = "Decimal(15,2) default '0.00'")
	private BigDecimal valorDesconto;

	/**
	 * Código adotado pela FEBRABAN para identificar o tipo de prazo a ser
	 * considerado para o protesto. ‘1’ = Protestar ‘3’ = Não Protestar ‘9’ =
	 * Cancelamento Protesto Automático (Somente válido p/ Código Movimento
	 * Remessa = ‘31’ - Alteração de Outros Dados), vide nota C004.
	 */
	@Column(name = "cod_protesto", length = 100)
	@Enumerated(EnumType.STRING)
	private CodigoProtesto codProtesto;

	/**
	 * Número de dias decorrentes após a data de vencimento para inicialização
	 * do processo de cobrança via protesto. Pode ser de 02 a 90 dias, sendo: De
	 * 02 a 05 = dias úteis Acima de 05 = dias corridos
	 */
	@Column(name = "prazo_protesto", length = 2)
	private int prazoProtesto;

	/**
	 * Código adotado pela FEBRABAN para identificar qual o procedimento a ser
	 * adotado com o Título. ‘1’ = Baixar / Devolver ‘2’ = Não Baixar / Não
	 * Devolver
	 */
	@NotNull
	@Column(name = "cd_devolucao", length = 100)
	@Enumerated(EnumType.STRING)
	private CodigoDevolucao codDevolucao;

	/**
	 * Número de dias corridos após a data de vencimento de um Título não pago,
	 * que deverá ser baixado e devolvido para o Cedente. Pode ser: - de 05 a
	 * 120 dias corridos.
	 */
	@Column(name = "prazo_devolucao", length = 3)
	private int prazoDevolucao;

	/**
	 * Mensagem 1 / 2 Texto referente a mensagens que serão impressas no campo
	 * instruções da Ficha de Compensação e na parte Recibo do Sacado, em todos
	 * os bloquetos referentes ao mesmo lote. Utilizar somente se o código de
	 * movimento for ‘01’ - Entrada/Solicitação de títulos. Estes campos não
	 * serão enviados no arquivo retorno.
	 * 
	 * ex.: "Não aceitar pagamento após vencimento"
	 */
	@NotNull
	@Column(name = "instrucao1", length = 40)
	private String instrucao1;

	/**
	 * Mensagem 1 / 2 Texto referente a mensagens que serão impressas no campo
	 * instruções da Ficha de Compensação e na parte Recibo do Sacado, em todos
	 * os bloquetos referentes ao mesmo lote. Utilizar somente se o código de
	 * movimento for ‘01’ - Entrada/Solicitação de títulos. Estes campos não
	 * serão enviados no arquivo retorno.
	 */
	@Column(name = "instrucao2", length = 40)
	private String instrucao2;

	/**
	 * Instrução do local de pagamento. (Utilizado apenas no PDF)
	 * 
	 * ex.: Pagável preferencialmente na Rede da Caixa ou em qualquer Banco até
	 * o Vencimento.
	 */
	@NotNull
	@Column(name = "local_pagamento")
	private String localPagamento;

	/**
	 * Instrução ao sacado. (Utilizado apenas no PDF)
	 * "Senhor condômino evite multa pagando antes do vencimento."
	 */
	@Column(name = "instrucao_ao_sacado")
	private String instrucaoAoSacado;

	@OneToMany(mappedBy = "configuradorBoleto", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ValorCustomizado> valoresCustomizados;

	public ConfiguradorBoleto() {
		this.valoresCustomizados = new ArrayList<>();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public TipoDeTitulo getEspecieDoTitulo() {
		return especieDoTitulo;
	}

	public void setEspecieDoTitulo(TipoDeTitulo especieDoTitulo) {
		this.especieDoTitulo = especieDoTitulo;
	}

	public Aceite getAceite() {
		return aceite;
	}

	public void setAceite(Aceite aceite) {
		this.aceite = aceite;
	}

	public CodigoJurosMora getCodMora() {
		return codMora;
	}

	public void setCodMora(CodigoJurosMora codMora) {
		this.codMora = codMora;
	}

	public BigDecimal getValorMora() {
		return valorMora;
	}

	public void setValorMora(BigDecimal valorMora) {
		this.valorMora = valorMora;
	}

	public CodigoMulta getCodMulta() {
		return codMulta;
	}

	public void setCodMulta(CodigoMulta codMulta) {
		this.codMulta = codMulta;
	}

	public BigDecimal getValorMulta() {
		return valorMulta;
	}

	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}

	public CodigoDesconto getCodDesconto() {
		return codDesconto;
	}

	public void setCodDesconto(CodigoDesconto codDesconto) {
		this.codDesconto = codDesconto;
	}

	public int getDiaDeLimiteDesconto() {
		return diaDeLimiteDesconto;
	}

	public void setDiaDeLimiteDesconto(int diaDeLimiteDesconto) {
		this.diaDeLimiteDesconto = diaDeLimiteDesconto;
	}

	public BigDecimal getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(BigDecimal valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public CodigoProtesto getCodProtesto() {
		return codProtesto;
	}

	public void setCodProtesto(CodigoProtesto codProtesto) {
		this.codProtesto = codProtesto;
	}

	public int getPrazoProtesto() {
		return prazoProtesto;
	}

	public void setPrazoProtesto(int prazoProtesto) {
		this.prazoProtesto = prazoProtesto;
	}

	public CodigoDevolucao getCodDevolucao() {
		return codDevolucao;
	}

	public void setCodDevolucao(CodigoDevolucao codDevolucao) {
		this.codDevolucao = codDevolucao;
	}

	public int getPrazoDevolucao() {
		return prazoDevolucao;
	}

	public void setPrazoDevolucao(int prazoDevolucao) {
		this.prazoDevolucao = prazoDevolucao;
	}

	public String getInstrucao1() {
		return instrucao1;
	}

	public void setInstrucao1(String instrucao1) {
		this.instrucao1 = instrucao1;
	}

	public String getInstrucao2() {
		return instrucao2;
	}

	public void setInstrucao2(String instrucao2) {
		this.instrucao2 = instrucao2;
	}

	public String getLocalPagamento() {
		return localPagamento;
	}

	public void setLocalPagamento(String localPagamento) {
		this.localPagamento = localPagamento;
	}

	public String getInstrucaoAoSacado() {
		return instrucaoAoSacado;
	}

	public void setInstrucaoAoSacado(String instrucaoAoSacado) {
		this.instrucaoAoSacado = instrucaoAoSacado;
	}

	/**
	 * @return the valoresCustomizados
	 */
	public List<ValorCustomizado> getValoresCustomizados() {
		return valoresCustomizados;
	}

	/**
	 * @param valoresCustomizados
	 *            the valoresCustomizados to set
	 */
	public void setValoresCustomizados(List<ValorCustomizado> valoresCustomizados) {
		this.valoresCustomizados = valoresCustomizados;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		ConfiguradorBoleto other = (ConfiguradorBoleto) obj;
		if (aceite != other.aceite)
			return false;
		if (codDesconto != other.codDesconto)
			return false;
		if (codDevolucao != other.codDevolucao)
			return false;
		if (codMora != other.codMora)
			return false;
		if (codMulta != other.codMulta)
			return false;
		if (codProtesto != other.codProtesto)
			return false;
		if (diaDeLimiteDesconto != other.diaDeLimiteDesconto)
			return false;
		if (especieDoTitulo != other.especieDoTitulo)
			return false;
		if (instrucao1 == null) {
			if (other.instrucao1 != null)
				return false;
		} else if (!instrucao1.equals(other.instrucao1))
			return false;
		if (instrucao2 == null) {
			if (other.instrucao2 != null)
				return false;
		} else if (!instrucao2.equals(other.instrucao2))
			return false;
		if (instrucaoAoSacado == null) {
			if (other.instrucaoAoSacado != null)
				return false;
		} else if (!instrucaoAoSacado.equals(other.instrucaoAoSacado))
			return false;
		if (localPagamento == null) {
			if (other.localPagamento != null)
				return false;
		} else if (!localPagamento.equals(other.localPagamento))
			return false;
		if (prazoDevolucao != other.prazoDevolucao)
			return false;
		if (prazoProtesto != other.prazoProtesto)
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		if (valorDesconto == null) {
			if (other.valorDesconto != null)
				return false;
		} else if (!valorDesconto.equals(other.valorDesconto))
			return false;
		if (valorMora == null) {
			if (other.valorMora != null)
				return false;
		} else if (!valorMora.equals(other.valorMora))
			return false;
		if (valorMulta == null) {
			if (other.valorMulta != null)
				return false;
		} else if (!valorMulta.equals(other.valorMulta))
			return false;
		if (valoresCustomizados == null) {
			if (other.valoresCustomizados != null)
				return false;
		} else if (!valoresCustomizados.equals(other.valoresCustomizados))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("INSERT INTO `condogreen`.`configurador_boleto` (`id`, `nome`, `aceite`, `cod_descont`, `cd_devolucao`, `cod_mora`, `cod_multa`, `cod_protesto`, `dia_desconto`, `tp_titulo`, `instrucao1`, `instrucao2`, `instrucao_ao_sacado`, `local_pagamento`, `prazo_devolucao`, `prazo_protesto`, `valor`, `vl_desc`, `vl_mora`, `vl_multa`, `atualizacao`) ");
		builder.append("VALUES (" + getId() + " , " + getNullOuValor(nome) + ", " + getNullOuValor(aceite) + ", " + getNullOuValor(codDesconto)
				+ ", " + getNullOuValor(codDevolucao) + ", " + getNullOuValor(codMora) + ", " + getNullOuValor(codMulta) + ", "
				+ getNullOuValor(codProtesto) + ", " + getNullOuValor(diaDeLimiteDesconto) + ", " + getNullOuValor(especieDoTitulo) + ", "
				+ getNullOuValor(instrucao1) + ", " + getNullOuValor(instrucao2) + ", " + getNullOuValor(instrucaoAoSacado) + ", "
				+ getNullOuValor(localPagamento) + ", " + getNullOuValor(prazoDevolucao) + ", " + getNullOuValor(prazoProtesto) + ", "
				+ getNullOuValor(valor) + ", " + getNullOuValor(valorDesconto) + ", " + getNullOuValor(valorMora) + ", " + getNullOuValor(valorMulta)
				+ ", now());");
		return builder.toString();
	}

	private String getNullOuValor(Object o) {
		if (o == null) {
			return "null";
		}
		return "'" + o.toString() + "'";
	}
}
