package br.com.calango.condogreen.boleto.domain.retorno;

import java.util.HashMap;
import java.util.Map;

public class QuadroD implements QuadroDetalhamento {

	private static  Map<String, String> quadroD = new HashMap<>();
	
	static {
		quadroD.put("09", "Comandada Banco");
		quadroD.put("10", "Comandada Cliente via Arquivo");
		quadroD.put("11", "Comandada Cliente On-line");
		quadroD.put("12", "Decurso Prazo - Cliente");
		quadroD.put("13", "Decurso Prazo - Banco");
		quadroD.put("14", "Protestado");
	}

	public Map<String, String> getQuadro() {
		return quadroD;
	}
	
}
