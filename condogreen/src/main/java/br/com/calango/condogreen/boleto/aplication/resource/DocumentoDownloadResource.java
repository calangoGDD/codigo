package br.com.calango.condogreen.boleto.aplication.resource;

import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.calango.condogreen.condominio.aplication.repository.DocumentoRepository;
import br.com.calango.condogreen.condominio.domain.Documento;
import br.com.calango.condogreen.core.aplication.DownloadsResource;

@RestController
@RequestMapping("downloads/documento")
public class DocumentoDownloadResource extends DownloadsResource {

	private DocumentoRepository documentoRepository;

	@Autowired
	public DocumentoDownloadResource(DocumentoRepository documentoRepository) {
		this.documentoRepository = documentoRepository;
	}

	@RequestMapping(path = "{id}/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public HttpEntity<byte[]> arquivoBoleto(@PathVariable("id") String id, HttpResponse response) {
		Documento documento = documentoRepository.findOne(Long.valueOf(id));
		return download(documento.getNome(), documento.getArquivo());
	}
}
