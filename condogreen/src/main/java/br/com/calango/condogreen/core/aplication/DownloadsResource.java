package br.com.calango.condogreen.core.aplication;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

public abstract class DownloadsResource {

	protected final HttpEntity<byte[]> download(String nome, byte[] arquivo) {
		HttpHeaders header = new HttpHeaders();
		header.set("Content-Disposition", "attachment; filename=" + nome);
		header.setContentLength(arquivo.length);
		return new HttpEntity<byte[]>(arquivo, header);
	}

	//
	// @ResponseBody
	// @RequestMapping(method = RequestMethod.GET, produces =
	// MediaType.IMAGE_JPEG_VALUE)
	// public byte[] bytes(@RequestParam("id") Long id, @RequestParam("comprir")
	// boolean comprir) throws IOException {
	// InfoBoleto boleto = boletoRepository.findOne(id);
	//
	// byte[] imageBytes = boleto.getArquivo();
	// if (comprir) {
	// imageBytes = comprimir(imageBytes, 0.2f);
	// }
	// return imageBytes;
	// }
	//
	// private static BufferedImage bytesToImage(byte[] imageByte) throws
	// IOException {
	// ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
	// BufferedImage image = ImageIO.read(bis);
	// bis.close();
	// return image;
	// }
	//
	// private static byte[] comprimir(byte[] imageByte, float fator) throws
	// IOException {
	// // Imagem original
	// BufferedImage image = bytesToImage(imageByte);
	//
	// ByteArrayOutputStream baos = new ByteArrayOutputStream();
	//
	// ImageIO.scanForPlugins();
	// Iterator<ImageWriter> writers =
	// ImageIO.getImageWritersByFormatName("jpeg");
	// ImageWriter writer = writers.next();
	//
	// ImageOutputStream ios = ImageIO.createImageOutputStream(baos);
	// writer.setOutput(ios);
	//
	// ImageWriteParam param = writer.getDefaultWriteParam();
	// param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
	// param.setCompressionQuality(fator);
	// writer.write(null, new IIOImage(image, null, null), param);
	//
	// baos.close();
	// ios.close();
	// writer.dispose();
	//
	// // Imagem comprimida
	// return baos.toByteArray();
	// }
}
