package br.com.calango.condogreen.condominio.aplication.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.calango.condogreen.condominio.aplication.repository.NoticiaRepository;
import br.com.calango.condogreen.condominio.domain.Noticia;

@Service
public class NoticiaService {

	private NoticiaRepository noticiaRepository;

	@Autowired
	public NoticiaService(NoticiaRepository noticiaRepository) {
		this.noticiaRepository = noticiaRepository;
	}
	
	public Page<Noticia> findAll(Pageable pageable) {
		return noticiaRepository.findAll(pageable);
	}

	public void save(String nomeImagem, byte[] imagem, Noticia noticia) {
		noticia.setDataCriacao(LocalDate.now());
		noticia.setImagem(imagem);
		noticia.setNomeImagem(nomeImagem);
		noticiaRepository.saveAndFlush(noticia);
	}


	public void delete(long id) {
		noticiaRepository.delete(id);
	}


	public void ativarDesativar(long id) {
		Noticia noticia = noticiaRepository.findOne(id);
		noticia.ativarOuDesativar();
		noticiaRepository.saveAndFlush(noticia);
	}

	public Noticia findById(long id) {
		return noticiaRepository.findOne(id);
	}

	public List<Noticia> getQuatroPrimeiras() {
		return noticiaRepository.getQuatroPrimeiras();
	}

}
