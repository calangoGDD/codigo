package br.com.calango.condogreen.condominio.domain.validacao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.calango.condogreen.condominio.domain.Reserva;
import br.com.calango.condogreen.infrastructure.exception.AbstractValidator;
import br.com.calango.condogreen.infrastructure.exception.BusinessException;

@Component("validadorReserva")
public class ValidadorReserva extends AbstractValidator<Reserva> {

	@Autowired
	private NaoDevePermitirDuasReservasNoMesmoDia naoDevePermitirDuasReservasNoMesmoDia;

	@Override
	public void registerRulesSpecification() throws BusinessException {
		rules.put(naoDevePermitirDuasReservasNoMesmoDia, new String[] { "Já existe uma reserva para o dia selecionado!" });
	}

}
