package br.com.calango.condogreen.boleto.domain.validacao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.calango.condogreen.boleto.aplication.service.GeradorBoleto;
import br.com.calango.condogreen.infrastructure.exception.AbstractValidator;
import br.com.calango.condogreen.infrastructure.exception.BusinessException;

@Component("validadorGeracaoBoletoEmMassa")
public class ValidadorGeracaoBoletoEmMassa extends AbstractValidator<GeradorBoleto> {

	@Autowired
	private DevePossuirResponsavelPorPagarBoletoEmTodosRecebedores devePossuirResponsavelPorPagarBoletoEmTodosRecebedores;

	@Override
	public void registerRulesSpecification() throws BusinessException {

		// TODO descomentar
		// rules.put(devePossuirResponsavelPorPagarBoletoEmTodosRecebedores,
		// new String[] {
		// "Existem apartamentos que não possui responsável pelo pagamento de boleto, por favor atualizar o cadastro."
		// });
	}

}