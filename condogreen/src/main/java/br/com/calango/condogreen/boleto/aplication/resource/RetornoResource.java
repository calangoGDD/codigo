package br.com.calango.condogreen.boleto.aplication.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import br.com.calango.condogreen.boleto.aplication.service.GerenciadorRetorno;

@RepositoryRestController
@RequestMapping("/retornos")
public class RetornoResource {

	private GerenciadorRetorno gerenciadorRetorno;

	@Autowired
	public RetornoResource(GerenciadorRetorno gerenciadorRetorno) {
		this.gerenciadorRetorno = gerenciadorRetorno;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/importar", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ResponseBody
	public byte[] importar(@RequestPart(value = "arquivo") MultipartFile arquivo) {
		byte[] result = new byte[0];
		try {
			result = arquivo.getBytes();
			gerenciadorRetorno.importarRetorno(arquivo.getOriginalFilename(), arquivo.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/migrar", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ResponseBody
	public byte[] migrar(@RequestPart(value = "arquivo") MultipartFile arquivo) {
		byte[] result = new byte[0];
		try {
			result = arquivo.getBytes();
			gerenciadorRetorno.gerarRetornoParaMigracao();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
