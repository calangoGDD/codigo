package br.com.calango.condogreen.condominio.aplication.resource;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.calango.condogreen.boleto.aplication.repository.BoletoRepository;
import br.com.calango.condogreen.financeiro.aplication.repository.DespesaRepository;


@RepositoryRestController
@RequestMapping("/dashboard")
public class DashboardResource {

	@Autowired
	DespesaRepository despesaRepository;
	
	@Autowired
	BoletoRepository boletoRepository;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public Dashboard get() {
		String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		//TODO trocar para 1 mês
		String end = LocalDateTime.now().plusMonths(12).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		
		BigDecimal totalInadimplente = BigDecimal.ZERO;
		BigDecimal totalInadimplentesResult = boletoRepository.getTotalInadimplentes(now, end);
		if(totalInadimplentesResult != null) {
			totalInadimplente = totalInadimplentesResult;
		}
		
		BigDecimal totalDespesa = BigDecimal.ZERO;
		BigDecimal totalDespesaResult = despesaRepository.sumDespesasByDataVencimento(now, end);
		if(totalDespesaResult != null) {
			totalDespesa = totalDespesaResult;
		}
		
		//TODO pensar em receitas
		return new Dashboard(totalDespesa, BigDecimal.ZERO, totalInadimplente);
	}
}
