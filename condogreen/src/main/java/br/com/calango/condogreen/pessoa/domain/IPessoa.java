package br.com.calango.condogreen.pessoa.domain;

import br.com.calango.condogreen.core.domain.Conta;
import br.com.calango.condogreen.core.domain.Endereco;

public interface IPessoa {

	void accept(VisitorPessoa visitor);

	void setCelular(String substring);

	void setCodigoAreaCelular(String substring);

	void setNome(String nome);

	String getNome();

	void setConta(Conta conta);

	void setEndereco(Endereco endereco);

	String getEmail();

	String getCelularWithCodigoArea();

	String getTipoInscricao();

	String getNumeroInscricao();

	String getCidade();

	String getCep();

	String getBairro();

	String getRua();

	String getNumero();

	String getComplemento();

	String getSufixoCep();

	String getUf();

	Conta getConta();
	
	Long getId();
	
}
