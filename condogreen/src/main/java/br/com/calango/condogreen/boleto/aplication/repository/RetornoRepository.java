package br.com.calango.condogreen.boleto.aplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.calango.condogreen.boleto.domain.retorno.Retorno;

@RepositoryRestResource(collectionResourceRel = "retornos", path = "retornos")
public interface RetornoRepository extends JpaRepository<Retorno, Long> {

	Retorno findByNumeroRetorno(@Param(value = "numero_retorno") Long nossoNumero);

}
