package br.com.calango.condogreen.portal.aplication.service;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;

import br.com.calango.condogreen.core.aplication.Notificador;
import br.com.calango.condogreen.portal.aplication.resource.FormularioContato;

@Service
public class PortalService {

	private Notificador notificador;
	
	@Autowired
	public PortalService(Notificador notificador) {
		this.notificador = notificador;
	}
	
	/**
	 * 
	 * @param formularioContato
	 * @throws MessagingException 
	 * @throws MailException 
	 */
	public void enviarEmailFaleConosco(FormularioContato formularioContato) throws MailException, MessagingException {
		notificador.notificarFaleConosco(formularioContato);
	}

}
