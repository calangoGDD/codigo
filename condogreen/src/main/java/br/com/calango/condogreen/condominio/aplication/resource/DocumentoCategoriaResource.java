package br.com.calango.condogreen.condominio.aplication.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.calango.condogreen.condominio.aplication.service.DocumentoService;
import br.com.calango.condogreen.condominio.domain.DocumentoCategoria;

@RepositoryRestController
@RequestMapping("/categorias-documento")
public class DocumentoCategoriaResource {

	@Autowired
	DocumentoService documentoService;
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<DocumentoCategoria> findAll() {
		return documentoService.findCategoriasDocumento();
	}
	
}
