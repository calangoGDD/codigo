package br.com.calango.condogreen.condominio.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.calango.condogreen.core.domain.BasicEntity;
import br.com.calango.condogreen.core.domain.Conta;
import br.com.calango.condogreen.pessoa.domain.Pessoa;
import br.com.calango.condogreen.pessoa.domain.PessoaFisica;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

/**
 * Esta classe representa o condomínio e todos seus itens. Bloco, Itens, Síndico
 * e etc.
 * 
 * @author Guilherme Andrade
 *
 */
@Entity
@Table(name = "condominio")
public class Condominio extends BasicEntity {

	public static final String FK_ID = "condominio_id";
	final static Map<Integer, Character> alfabeto = new HashMap<>();
	static {
		int count = 0;
		for (char ch = 'A'; ch <= 'Z'; ch++) {
			alfabeto.put(++count, Character.valueOf(ch));
		}
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = PessoaJuridica.FK_EMPRESA_ID)
	private PessoaJuridica empresa;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = PessoaFisica.FK_ID_SINDICO)
	private Pessoa sindico;

	@OneToMany(mappedBy = "condominio", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Bloco> blocos;

	@OneToMany(mappedBy = "condominio", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Item> itens;

	public Condominio() {
		this.empresa = new PessoaJuridica();
		this.blocos = new ArrayList<>();
		this.itens = new HashSet<>();
	}

	public Condominio(String nome, Conta conta, int quantidadeDeBlocos, int quantidadeDeApartamentoPorBloco) {
		this();
		this.empresa.setNome(nome);
		this.empresa.setConta(conta);
		construirItens();
		construirBlocos(quantidadeDeBlocos, quantidadeDeApartamentoPorBloco);
	}

	public void setEmpresa(PessoaJuridica empresa) {
		this.empresa = empresa;
	}

	public PessoaJuridica getEmpresa() {
		return empresa;
	}

	/**
	 * 
	 * @return Nome do condomínio
	 */
	public String getNome() {
		return this.empresa.getNome();
	}

	/**
	 * @return the sindico
	 */
	public Pessoa getSindico() {
		return sindico;
	}

	/**
	 * @param sindico
	 *            the sindico to set
	 */
	public void setSindico(Pessoa sindico) {
		this.sindico = sindico;
	}

	public void addItem(Item item) {
		this.itens.add(item);
	}

	private void addBloco(Bloco bloco) {
		this.blocos.add(bloco);
	}

	/**
	 * @return the blocos
	 */
	public List<Bloco> getBlocos() {
		return blocos;
	}

	/**
	 * @param blocos
	 *            the blocos to set
	 */
	public void setBlocos(List<Bloco> blocos) {
		this.blocos = blocos;
	}

	/**
	 * @return the itens
	 */
	public Set<Item> getItens() {
		return itens;
	}

	/**
	 * @param itens
	 *            the itens to set
	 */
	public void setItens(Set<Item> itens) {
		this.itens = itens;
	}

	/**
	 * Método responsável por constuir os itens do condomínio
	 */
	private void construirItens() {
		Item piscina = new Item("Piscina");
		this.addItem(piscina);
		Item quadra = new Item("Quadra");
		this.addItem(quadra);
		Item portaria = new Item("Portaria");
		this.addItem(portaria);
	}

	/**
	 * Método responsável por construir os blocos do condomínio. Ex.: Bloco A,
	 * Bloco B...
	 * 
	 * @param quantidadeDeBlocos
	 * @param quantidadeDeApartamentoPorBloco
	 */
	private void construirBlocos(int quantidadeDeBlocos, int quantidadeDeApartamentoPorBloco) {
		for (int i = 0; i < quantidadeDeBlocos; i++) {
			StringBuilder builder = new StringBuilder();
			builder.append("Bloco ").append(alfabeto.get(i + 1));
			Bloco bloco = new Bloco(builder.toString(), quantidadeDeApartamentoPorBloco);
			this.addBloco(bloco);
		}
	}
}
