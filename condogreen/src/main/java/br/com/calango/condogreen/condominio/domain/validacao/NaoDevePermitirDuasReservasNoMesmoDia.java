package br.com.calango.condogreen.condominio.domain.validacao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.calango.condogreen.condominio.aplication.repository.ReservaRepository;
import br.com.calango.condogreen.condominio.domain.Reserva;
import br.com.calango.condogreen.infrastructure.specification.ISpecification;

@Component("naoDevePermitirDuasReservasNoMesmoDia")
public class NaoDevePermitirDuasReservasNoMesmoDia implements ISpecification<Reserva> {

	@Autowired
	private ReservaRepository reservaRepository;
	
	/**
	 * @see br.com.calango.condogreen.infrastructure.specification.ISpecification#isSatisfiedBy(java.lang.Object)
	 */
	@Override
	public boolean isSatisfiedBy(Reserva reserva) {
		Reserva reservaEncontrada = reservaRepository.findByInicio(reserva.getInicio()); 
		return reservaEncontrada == null;
	}

}
