package br.com.calango.condogreen.boleto.aplication.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.calango.condogreen.boleto.aplication.repository.RemessaRepository;
import br.com.calango.condogreen.boleto.domain.remessa.Remessa;
import br.com.calango.condogreen.core.aplication.DownloadsResource;
import br.com.calango.condogreen.pessoa.aplication.PessoaJurdicaRepository;
import br.com.calango.condogreen.pessoa.domain.PessoaJuridica;

@RestController
@RequestMapping("downloads/remessa")
public class RemessaDownloadResource extends DownloadsResource {

	private RemessaRepository remessaRepository;

	private PessoaJurdicaRepository pessoaJurdicaRepository;

	@Autowired
	public RemessaDownloadResource(RemessaRepository remessaRepository, PessoaJurdicaRepository pessoaJurdicaRepository) {
		this.remessaRepository = remessaRepository;
		this.pessoaJurdicaRepository = pessoaJurdicaRepository;
	}

	@RequestMapping(path = "{id:\\d+}/boletos/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public HttpEntity<byte[]> arquivoRemessaBoletos(@PathVariable("id") long id) {
		Remessa remessa = remessaRepository.findOne(id);
		PessoaJuridica empresa = pessoaJurdicaRepository.findTopByOrderByIdAsc();
		return download(String.format("REM_%s_boletos.pdf", id), remessa.getBoletosConcatenados(empresa));
	}

	@RequestMapping(path = "{id:\\d+}/txt", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public HttpEntity<byte[]> arquivoRemessa(@PathVariable("id") long id) {
		Remessa remessa = remessaRepository.findOne(id);
		return download(remessa.getNome(), remessa.getArquivo());
	}

}
