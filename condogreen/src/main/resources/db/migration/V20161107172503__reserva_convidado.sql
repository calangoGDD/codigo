create table convidado (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        nome varchar(40),
        remessa_id bigint,
        primary key (id)
    );
create table reserva (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        descricao varchar(100),
        diatodo bit,
        fim date,
        inicio date,
        titulo varchar(40),
        morador_id bigint,
        primary key (id)
    );
    
 alter table convidado 
        add constraint FK_2dur2divcb64nc6u8i4xkel66 
        foreign key (remessa_id) 
        references reserva (id);

 alter table reserva 
        add constraint FK_j562vnn08iymirkdhs886tfts 
        foreign key (morador_id) 
        references morador (id);