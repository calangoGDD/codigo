ALTER TABLE `remessa` 
ADD COLUMN `empresa_id` BIGINT(20) NULL AFTER `arquivo`,
ADD INDEX `fk_pessoa_juridica_remessa_idx` (`empresa_id` ASC);
ALTER TABLE `remessa` 
ADD CONSTRAINT `fk_pessoa_juridica_remessa`
  FOREIGN KEY (`empresa_id`)
  REFERENCES `pessoa_juridica` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `remessa` 
DROP COLUMN `boletos_concatenados`;