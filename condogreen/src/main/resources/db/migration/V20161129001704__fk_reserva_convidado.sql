ALTER TABLE `convidado` 
DROP FOREIGN KEY `FK_2dur2divcb64nc6u8i4xkel66`;
ALTER TABLE `convidado` 
CHANGE COLUMN `remessa_id` `reserva_id` BIGINT(20) NULL DEFAULT NULL ;
ALTER TABLE `convidado` 
ADD CONSTRAINT `FK_reserva_convidado`
  FOREIGN KEY (`reserva_id`)
  REFERENCES `reserva` (`id`);