create table noticia (
        id bigint not null auto_increment,
        conteudo varchar(100),
        data_criacao date,
        imagem longblob,
        situacao bit,
        titulo varchar(40),
        versao bigint,
        primary key (id)
    );
    
    
ALTER TABLE `noticia` 
CHANGE COLUMN `conteudo` `conteudo` VARCHAR(4000) NULL DEFAULT NULL ,
CHANGE COLUMN `titulo` `titulo` VARCHAR(125) NULL DEFAULT NULL ,
ADD COLUMN `nomeimagem` VARCHAR(125) NULL AFTER `imagem`;
