ALTER TABLE `convidado` 
DROP FOREIGN KEY `FK_reserva_convidado`;
ALTER TABLE `convidado` 
DROP COLUMN `reserva_id`,
DROP INDEX `FK_reserva_convidado` ;

ALTER TABLE `convidado` 
CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL ,
ADD COLUMN `morador_id` BIGINT(20) NOT NULL AFTER `nome`,
ADD INDEX `convidado_morador_idx` (`morador_id` ASC);
ALTER TABLE `convidado` 
ADD CONSTRAINT `convidado_morador`
  FOREIGN KEY (`morador_id`)
  REFERENCES `morador` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


ALTER TABLE `convidado` 
ADD COLUMN `data_convite` DATE NULL AFTER `morador_id`;


ALTER TABLE `reserva` 
ADD COLUMN `situacao` VARCHAR(3) NOT NULL AFTER `morador_id`;

ALTER TABLE `reserva` 
DROP COLUMN `atualizacao`;


