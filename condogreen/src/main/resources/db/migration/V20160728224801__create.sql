-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: condogreen
-- ------------------------------------------------------
-- Server version	5.7.12-log


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

--
-- Table structure for table `apartamento`
--

DROP TABLE IF EXISTS `apartamento`;
CREATE TABLE `apartamento` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bloco_id` bigint(20) DEFAULT NULL,
  `nome` varchar(30) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_apartamento_bloco` (`bloco_id`),
  CONSTRAINT `FK_apart_receb_boleto` FOREIGN KEY (`id`) REFERENCES `recebe_boleto` (`id`),
  CONSTRAINT `fk_apartamento_bloco` FOREIGN KEY (`bloco_id`) REFERENCES `bloco` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=utf8;



--
-- Table structure for table `apartamento_morador`
--

DROP TABLE IF EXISTS `apartamento_morador`;
CREATE TABLE `apartamento_morador` (
  `morador_id` bigint(20) NOT NULL,
  `apartamento_id` bigint(20) NOT NULL,
  PRIMARY KEY (`morador_id`,`apartamento_id`),
  KEY `idx_apartamento_morador_apartamento` (`apartamento_id`),
  CONSTRAINT `fk_apartamento_morador_apartamento` FOREIGN KEY (`apartamento_id`) REFERENCES `apartamento` (`id`),
  CONSTRAINT `fk_apartamento_morador_morador` FOREIGN KEY (`morador_id`) REFERENCES `morador` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `bloco`
--

DROP TABLE IF EXISTS `bloco`;
CREATE TABLE `bloco` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `condominio_id` bigint(20) DEFAULT NULL,
  `nome` varchar(30) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_bloco_condominio` (`condominio_id`),
  CONSTRAINT `fk_bloco_condominio` FOREIGN KEY (`condominio_id`) REFERENCES `condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Table structure for table `boleto`
--

DROP TABLE IF EXISTS `boleto`;


CREATE TABLE `boleto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cd_movimento` varchar(2) NOT NULL,
  `data_vencimento` date NOT NULL,
  `configurador_id` bigint(20) NOT NULL,
  `situacao` varchar(10) DEFAULT NULL,
  `vl_boleto` decimal(19,2) DEFAULT NULL,
  `sacado_id` bigint(20) DEFAULT NULL,
  `arquivo` longblob,
  `cd_carteira` varchar(100) DEFAULT NULL,
  `dt_emissao` datetime DEFAULT NULL,
  `modalidade` varchar(100) DEFAULT NULL,
  `nosso_numero` varchar(255) DEFAULT NULL,
  `numero_documento` varchar(255) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_boleto_pessoa` (`sacado_id`),
  KEY `fk_boleto_configurador` (`configurador_id`),
  CONSTRAINT `fk_boleto_configurador` FOREIGN KEY (`configurador_id`) REFERENCES `configurador_boleto` (`id`),
  CONSTRAINT `fk_boleto_pessoa` FOREIGN KEY (`sacado_id`) REFERENCES `pessoa` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


--
-- Dumping data for table `boleto`
--

LOCK TABLES `boleto` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `condominio`
--

DROP TABLE IF EXISTS `condominio`;


CREATE TABLE `condominio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `empresa_id` bigint(20) DEFAULT NULL,
  `sindico_id` bigint(20) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_condominio_pessoa` (`sindico_id`),
  KEY `idx_condominio_pessoa_j` (`empresa_id`),
  CONSTRAINT `fk_condominio_pessoa` FOREIGN KEY (`sindico_id`) REFERENCES `pessoa` (`id`),
  CONSTRAINT `fk_condominio_pessoa_j` FOREIGN KEY (`empresa_id`) REFERENCES `pessoa_juridica` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


--
-- Table structure for table `configurador_boleto`
--

DROP TABLE IF EXISTS `configurador_boleto`;


CREATE TABLE `configurador_boleto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `aceite` varchar(255) DEFAULT NULL,
  `cod_descont` varchar(100) DEFAULT NULL,
  `cd_devolucao` varchar(100) DEFAULT NULL,
  `cod_mora` varchar(100) DEFAULT NULL,
  `cod_multa` varchar(100) DEFAULT NULL,
  `cod_protesto` varchar(100) DEFAULT NULL,
  `dia_desconto` int(11) DEFAULT NULL,
  `tp_titulo` varchar(255) DEFAULT NULL,
  `instrucao1` varchar(40) DEFAULT NULL,
  `instrucao2` varchar(40) DEFAULT NULL,
  `instrucao_ao_sacado` varchar(255) DEFAULT NULL,
  `local_pagamento` varchar(255) DEFAULT NULL,
  `prazo_devolucao` int(11) DEFAULT NULL,
  `prazo_protesto` int(11) DEFAULT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  `vl_desc` decimal(19,2) DEFAULT NULL,
  `vl_mora` decimal(19,2) DEFAULT NULL,
  `vl_multa` decimal(19,2) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;




--
-- Table structure for table `conta`
--

DROP TABLE IF EXISTS `conta`;


CREATE TABLE `conta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_conta_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `conta_bancaria`
--

DROP TABLE IF EXISTS `conta_bancaria`;


CREATE TABLE `conta_bancaria` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `agencia` varchar(5) DEFAULT NULL,
  `codigo_beneficiario` varchar(6) DEFAULT NULL,
  `digito_codigo_beneficiario` varchar(1) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


--
-- Table structure for table `endereco`
--

DROP TABLE IF EXISTS `endereco`;


CREATE TABLE `endereco` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bairro` varchar(15) DEFAULT NULL,
  `cep` varchar(5) DEFAULT NULL,
  `cidade` varchar(15) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `numero` varchar(15) DEFAULT NULL,
  `pais` varchar(10) DEFAULT NULL,
  `rua` varchar(40) DEFAULT NULL,
  `sufixo_cep` varchar(3) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2080 DEFAULT CHARSET=utf8;


--
-- Table structure for table `informacao_retorno`
--

DROP TABLE IF EXISTS `informacao_retorno`;


CREATE TABLE `informacao_retorno` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `retorno_id` bigint(20) NOT NULL,
  `boleto_id` bigint(20) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  `valor_pago` decimal(19,2) DEFAULT NULL,
  `valor_multa_juros` decimal(19,2) DEFAULT NULL,
  `valor_efetivo_creditado` decimal(19,2) DEFAULT NULL,
  `valor_tarifa` decimal(19,2) DEFAULT NULL,
  `codigo_movimento` varchar(2) DEFAULT NULL,
  `motivo_ocorrencia` varchar(10) DEFAULT NULL,
  `data_ocorrencia` datetime DEFAULT NULL,
  `data_efeticacao_credito` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_informacao_retorno` (`retorno_id`),
  KEY `idx_informacao_retorno_boleto` (`boleto_id`),
  CONSTRAINT `FK_informacao_retorno_boleto` FOREIGN KEY (`boleto_id`) REFERENCES `boleto` (`id`),
  CONSTRAINT `fk_informacao_retorno` FOREIGN KEY (`retorno_id`) REFERENCES `retorno` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;


CREATE TABLE `item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `condominio_id` bigint(20) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_item_condominio` (`condominio_id`),
  CONSTRAINT `fk_item_condominio` FOREIGN KEY (`condominio_id`) REFERENCES `condominio` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `morador`
--

DROP TABLE IF EXISTS `morador`;


CREATE TABLE `morador` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `responsavel_boleto` bit(1) NOT NULL DEFAULT b'0',
  `pessoa_id` bigint(20) DEFAULT NULL,
  `imagem_perfil` tinyblob,
  `tipo` varchar(255) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_morador_pessoa` (`pessoa_id`),
  CONSTRAINT `fk_morador_pessoa` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2079 DEFAULT CHARSET=utf8;


--
-- Table structure for table `motivo_ocorrencia`
--

DROP TABLE IF EXISTS `motivo_ocorrencia`;


CREATE TABLE `motivo_ocorrencia` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quadro` varchar(2) DEFAULT NULL,
  `codigo` varchar(2) DEFAULT NULL,
  `descricao` varchar(128) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8;


--
-- Dumping data for table `motivo_ocorrencia`
--

LOCK TABLES `motivo_ocorrencia` WRITE;
INSERT INTO `motivo_ocorrencia` VALUES (1,'A','AA','Cód Desconto Preenchido, Obrig Data e Valor/Perc',NULL),(2,'A','AB','Cod Desconto Obrigatório p/ Cód Mov = 7',NULL),(3,'A','AC','Forma de Cadastramento Inválida',NULL),(4,'A','AD','Data de Desconto deve estar em Ordem Crescente',NULL),(5,'A','AE','Data de Desconto é Posterior a Data de Vencimento',NULL),(6,'A','AF','Título não está com situação \"Em Aberto\"',NULL),(7,'A','AG','Título já está Vencido / Vencendo',NULL),(8,'A','AH','Não existe desconto a ser cancelado',NULL),(9,'A','AI','Data solicitada p/ Prot/Dev é anterior a data atual',NULL),(10,'A','AJ','Código do Sacado Inválido',NULL),(11,'A','AK','Número da Parcela Invalida ou Fora de Sequencia',NULL),(12,'A','AL','Estorno de Envio Não Permitido',NULL),(13,'A','AM','Nosso Numero Fora de Sequencia',NULL),(14,'A','VA','Arq.Ret.Inexis. P/ Redisp. Nesta Dt/Nro',NULL),(15,'A','VB','Registro Duplicado',NULL),(16,'A','VC','Cedente deve ser padrão CNAB240',NULL),(17,'A','VD','Ident. Banco Sacado Inválida',NULL),(18,'A','VE','Num Docto Cobr Inválido',NULL),(19,'A','VF','Vlr/Perc a ser concedido inválido',NULL),(20,'A','VG','Data de Inscrição Inválida',NULL),(21,'A','VH','Data Movto Inválida',NULL),(22,'A','VI','Data Inicial Inválida',NULL),(23,'A','VJ','Data Final Inválida',NULL),(24,'A','VK','Banco de Sacado já cadastrado',NULL),(25,'A','VL','Cedente não cadastrado',NULL),(26,'A','VM','Número de Lote Duplicado',NULL),(27,'A','VN','Forma de Emissão de Bloqueto Inválida',NULL),(28,'A','VO','Forma Entrega Bloqueto Inválida p/ Emissão via Banco',NULL),(29,'A','VP','Forma Entrega Bloqueto Invalida p/ Emissão via Cedente',NULL),(30,'A','VQ','Opção para Endosso Inválida',NULL),(31,'A','VR','Tipo de Juros ao Mês Inválido',NULL),(32,'A','VS','Percentual de Juros ao Mês Inválido',NULL),(33,'A','VT','Percentual / Valor de Desconto Inválido',NULL),(34,'A','VU','Prazo de Desconto Inválido',NULL),(35,'A','VV','Preencher Somente Percentual ou Valor',NULL),(36,'A','VW','Prazo de Multa Invalido',NULL),(37,'A','VX','Perc. Desconto tem que estar em ordem decrescente',NULL),(38,'A','VY','Valor Desconto tem que estar em ordem descrescente',NULL),(39,'A','VZ','Dias/Data desconto tem que estar em ordem decrescente',NULL),(40,'A','WA','Vlr Contr p/ aquisição de Bens Inválid',NULL),(41,'A','WB','Vlr Contr p/ Fundo de Reserva Inválid',NULL),(42,'A','WC','Vlr Rend. Aplicações Financ Inválido',NULL),(43,'A','WD','Valor Multa/Juros Monetarios Inválido',NULL),(44,'A','WE','Valor Premios de Seguro Inválido',NULL),(45,'A','WF','Valor Custas Judiciais Inválido',NULL),(46,'A','WG','Valor Reembolso de Despesas Inválido',NULL),(47,'A','WH','Valor Outros Inválido',NULL),(48,'A','WI','Valor de Aquisição de Bens Inválido',NULL),(49,'A','WJ','Valor Devolvido ao Consorciado Inválido',NULL),(50,'A','WK','Vlr Desp. Registro de Contrato Inválido',NULL),(51,'A','WL','Valor de Rendimentos Pagos Inválido',NULL),(52,'A','WM','Data de Descrição Inválida',NULL),(53,'A','WN','Valor do Seguro Inválido',NULL),(54,'A','WO','Data de Vencimento Inválida',NULL),(55,'A','WP','Data de Nascimento Inválida',NULL),(56,'A','WQ','CPF/CNPJ do Aluno Inválido',NULL),(57,'A','WR','Data de Avaliação Inválida',NULL),(58,'A','WS','CPF/CNPJ do Locatario Inválido',NULL),(59,'A','WT','Literal da Remessa Inválida',NULL),(60,'A','WU','Tipo de Registro Inválido',NULL),(61,'A','WV','Modelo Inválido',NULL),(62,'A','WW','Código do Banco de Sacados Inválido',NULL),(63,'A','WX','Banco de Sacados não Cadastrado',NULL),(64,'A','WY','Qtde dias para Protesto tem que estar entre 2 e 90',NULL),(65,'A','WZ','Não existem Sacados para este Banco',NULL),(66,'A','XA','Preço Unitario do Produto Inválido',NULL),(67,'A','XB','Preço Total do Produto Inválido',NULL),(68,'A','XC','Valor Atual do Bem Inválido',NULL),(69,'A','XD','Quantidade de Bens Entregues Inválido',NULL),(70,'A','XE','Quantidade de Bens Distribuidos Inválido',NULL),(71,'A','XF','Quantidade de Bens não Distribuidos Inválido',NULL),(72,'A','XG','Número da Próxima Assembléia Inválido',NULL),(73,'A','XH','Horario da Próxima Assembléia Inválido',NULL),(74,'A','XI','Data da Próxima Assembléia Inválida',NULL),(75,'A','XJ','Número de Ativos Inválido',NULL),(76,'A','XK','Número de Desistentes Excluidos Inválido',NULL),(77,'A','XL','Número de Quitados Inválido',NULL),(78,'A','XM','Número de Contemplados Inválido',NULL),(79,'A','XN','Número de não Contemplados Inválido',NULL),(80,'A','XO','Data da Última Assembléia Inválida',NULL),(81,'A','XP','Quantidade de Prestações Inválida',NULL),(82,'A','XQ','Data de Vencimento da Parcela Inválida',NULL),(83,'A','XR','Valor da Amortização Inválida',NULL),(84,'A','XS','Código do Personalizado Inválido',NULL),(85,'A','XT','Valor da Contribuição Inválida',NULL),(86,'A','XU','Percentual da Contribuição Inválido',NULL),(87,'A','XV','Valor do Fundo de Reserva Inválido',NULL),(88,'A','XW','Número Parcela Inválido ou Fora de Sequência',NULL),(89,'A','XX','Percentual Fundo de Reserva Inválido',NULL),(90,'A','XY','Prz Desc/Multa Preenchido, Obrigat.Perc. ou Valor',NULL),(91,'A','XZ','Valor Taxa de Administração Inválida',NULL),(92,'A','YA','Data de Juros Inválida ou Não Informada',NULL),(93,'A','YB','Data Desconto Inválida ou Não Informada',NULL),(94,'A','YC','E-mail Inválido',NULL),(95,'A','YD','Código de Ocorrência Inválido',NULL),(96,'A','YE','Sacado já Cadastrado (Banco de Sacados)',NULL),(97,'A','YF','Sacado não Cadastrado (Banco de Sacados)',NULL),(98,'A','YG','Remessa Sem Registro Tipo 9',NULL),(99,'A','YH','Identificação da Solicitação Inválida',NULL),(100,'A','YI','Quantidade Bloquetos Solicitada Inválida',NULL),(101,'A','YJ','Trailler do Arquivo não Encontrado',NULL),(102,'A','YK','Tipo Inscrição do Responsable Inválido',NULL),(103,'A','YL','Número Inscrição do Responsable Inválido',NULL),(104,'A','YM','Ajuste de Vencimento Inválido',NULL),(105,'A','YN','Ajuste de Emissão Inválido',NULL),(106,'A','YO','Código de Modelo Inválido',NULL),(107,'A','YP','Vía de Entrega Inválido',NULL),(108,'A','YQ','Espécie Banco de Sacado Inválido',NULL),(109,'A','YR','Aceite Banco de Sacado Inválido',NULL),(110,'A','YS','Sacado já Cadastrado',NULL),(111,'A','YT','Sacado não Cadastrado',NULL),(112,'A','YU','Número do Telefone Inválido',NULL),(113,'A','YV','CNPJ do Condomínio Inválido',NULL),(114,'A','YW','Indicador de Registro de Título Inválido',NULL),(115,'A','YX','Valor da Nota Inválido',NULL),(116,'A','YY','Qtde de dias para Devolução tem que estar entre 5 e 120',NULL),(117,'A','YZ','Quantidade de Produtos Inválida',NULL),(118,'A','ZA','Perc. Taxa de Administração Inválido',NULL),(119,'A','ZB','Valor do Seguro Inválido',NULL),(120,'A','ZC','Percentual do Seguro Inválido',NULL),(121,'A','ZD','Valor da Diferença da Parcela Inválido',NULL),(122,'A','ZE','Perc. Da Diferença da Parcela Inválido',NULL),(123,'A','ZF','Valor Reajuste do Saldo de Caixa Inválido',NULL),(124,'A','ZG','Perc. Reajuste do Saldo de Caixa Inválido',NULL),(125,'A','ZH','Valor Total a Pagar Inválido',NULL),(126,'A','ZI','Percentual ao Total a Pagar Inválido',NULL),(127,'A','ZJ','Valor de Outros Acréscimos Inválido',NULL),(128,'A','ZK','Perc. De Outros Acréscimos Inválido',NULL),(129,'A','ZL','Valor de Outras Deduções Inválido',NULL),(130,'A','ZM','Perc. De Outras Deduções Inválido',NULL),(131,'A','ZN','Valor da Contribuição Inválida',NULL),(132,'A','ZO','Percentual da Contribuição Inválida',NULL),(133,'A','ZP','Valor de Juros/Multa Inválido',NULL),(134,'A','ZQ','Percentual de Juros/Multa Inválido',NULL),(135,'A','ZR','Valor Cobrado Inválido',NULL),(136,'A','ZS','Percentual Cobrado Inválido',NULL),(137,'A','ZT','Valor Disponibilizado em Caixa Inválido',NULL),(138,'A','ZU','Valor Depósito Bancario Inválido',NULL),(139,'A','ZV','Valor Aplicações Financieras Inválido',NULL),(140,'A','ZW','Data/Valor Preenchidos, Obrigatório Dódigo Desconto',NULL),(141,'A','ZX','Valor Cheques em Cobrança Inválido',NULL),(142,'A','ZY','Desconto c/ valor Fixo, Obrigatório Valor do Título',NULL),(143,'A','ZZ','Código Movimento Inválido p/ Segmento Y8',NULL),(144,'A','01','Código do Banco Inválido',NULL),(145,'A','02','Código do Registro Inválido',NULL),(146,'A','03','Código do Segmento Inválido',NULL),(147,'A','04','Código do Movimento não Permitido p/ Carteira',NULL),(148,'A','05','Código do Movimento Inválido',NULL),(149,'A','06','Tipo Número Inscrição Cedente Inválido',NULL),(150,'A','07','Agencia/Conta/DV Inválidos',NULL),(151,'A','08','Nosso Número Inválido',NULL),(152,'A','09','Nosso Número Duplicado',NULL),(153,'A','10','Carteira Inválida',NULL),(154,'A','11','Data de Geração Inválida',NULL),(155,'A','12','Tipo de Documento Inválido',NULL),(156,'A','13','Identif. Da Emissão do Bloqueto Inválida',NULL),(157,'A','14','Identif. Da Distribuição do Bloqueto Inválida',NULL),(158,'A','15','Características Cobrança Incompatíveis',NULL),(159,'A','16','Data de Vencimento Inválida',NULL),(160,'A','17','Data de Vencimento Anterior a Data de Emissão',NULL),(161,'A','18','Vencimento fora do prazo de operação',NULL),(162,'A','19','Título a Cargo de Bco Correspondentes c/ Vencto Inferior a XX Dias',NULL),(163,'A','20','Valor do Título Inválido',NULL),(164,'A','21','Espécie do Título Inválida',NULL),(165,'A','22','Espécie do Título Não Permitida para a Carteira',NULL),(166,'A','23','Aceite Inválido',NULL),(167,'A','24','Data da Emissão Inválida',NULL),(168,'A','25','Data da Emissão Posterior a Data de Entrada',NULL),(169,'A','26','Código de Juros de Mora Inválido',NULL),(170,'A','27','Valor/Taxa de Juros de Mora Inválido',NULL),(171,'A','28','Código do Desconto Inválido',NULL),(172,'A','29','Valor do Desconto Maior ou Igual ao Valor do Título',NULL),(173,'A','30','Desconto a Conceder Não Confere',NULL),(174,'A','31','Concessão de Desconto - Já Existe Desconto Anterior',NULL),(175,'A','32','Valor do IOF Inválido',NULL),(176,'A','33','Valor do Abatimento Inválido',NULL),(177,'A','34','Valor do Abatimento Maior ou Igual ao Valor do Título',NULL),(178,'A','35','Valor Abatimento a Conceder Não Confere',NULL),(179,'A','36','Concessão de Abatimento - Já Existe Abatimento Anterior',NULL),(180,'A','37','Código para Protesto Inválido',NULL),(181,'A','38','Prazo para Protesto Inválido',NULL),(182,'A','39','Pedido de Protesto Não Permitido para o Título',NULL),(183,'A','40','Título com Ordem de Protesto Emitida',NULL),(184,'A','41','Pedido Cancelamento/Sustação p/ Títulos sem Instrução Protesto',NULL),(185,'A','42','Código para Baixa/Devolução Inválido',NULL),(186,'A','43','Prazo para Baixa/Devolução Inválido',NULL),(187,'A','44','Código da Moeda Inválido',NULL),(188,'A','45','Nome do Sacado Não Informado',NULL),(189,'A','46','Tipo/Número de Inscrição do Sacado Inválidos',NULL),(190,'A','47','Endereço do Sacado Não Informado',NULL),(191,'A','48','CEP Inválido',NULL),(192,'A','49','CEP Sem Praça de Cobrança (Não Localizado)',NULL),(193,'A','50','CEP Referente a um Banco Correspondente',NULL),(194,'A','51','CEP incompatível com a Unidade da Federação',NULL),(195,'A','52','Unidade da Federação Inválida',NULL),(196,'A','53','Tipo/Número de Inscrição do Sacador/Avalista Inválidos',NULL),(197,'A','54','Sacador/Avalista Não Informado',NULL),(198,'A','55','Nosso número no Banco Correspondente Não Informado',NULL),(199,'A','56','Código do Banco Correspondente Não Informado',NULL),(200,'A','57','Código da Multa Inválido',NULL),(201,'A','58','Data da Multa Inválida',NULL),(202,'A','59','Valor/Percentual da Multa Inválido',NULL),(203,'A','60','Movimento para Título Não Cadastrado',NULL),(204,'A','61','Alteração da Agência Cobradora/DV Inválida',NULL),(205,'A','62','Tipo de Impressão Inválido',NULL),(206,'A','63','Entrada para Título já Cadastrado',NULL),(207,'A','64','Entrada Inválida para Cobrança Caucionada',NULL),(208,'A','65','CEP do Sacado não encontrado',NULL),(209,'A','66','Agencia Cobradora não encontrada',NULL),(210,'A','67','Agencia Cedente não encontrada',NULL),(211,'A','68','Movimentação inválida para título',NULL),(212,'A','69','Alteração de dados inválida',NULL),(213,'A','70','Apelido do cliente não cadastrado',NULL),(214,'A','71','Erro na composição do arquivo',NULL),(215,'A','72','Lote de serviço inválido',NULL),(216,'A','73','Código do Cedente inválido',NULL),(217,'A','74','Cedente não pertencente a Cobrança Eletrônica',NULL),(218,'A','75','Nome da Empresa inválido',NULL),(219,'A','76','Nome do Banco inválido',NULL),(220,'A','77','Código da Remessa inválido',NULL),(221,'A','78','Data/Hora Geração do arquivo inválida',NULL),(222,'A','79','Número Sequencial do arquivo inválido',NULL),(223,'A','80','Versão do Lay out do arquivo inválido',NULL),(224,'A','81','Literal REMESSA-TESTE - Válido só p/ fase testes',NULL),(225,'A','82','Literal REMESSA-TESTE - Obrigatório p/ fase testes',NULL),(226,'A','83','Tp Número Inscrição Empresa inválido',NULL),(227,'A','84','Tipo de Operação inválido',NULL),(228,'A','85','Tipo de serviço inválido',NULL),(229,'A','86','Forma de lançamento inválido',NULL),(230,'A','87','Número da remessa inválido',NULL),(231,'A','88','Número da remessa menor/igual remessa anterior',NULL),(232,'A','89','Lote de serviço divergente',NULL),(233,'A','90','Número sequencial do registro inválido',NULL),(234,'A','91','Erro seq de segmento do registro detalhe',NULL),(235,'A','92','Cod movto divergente entre grupo de segm',NULL),(236,'A','93','Qtde registros no lote inválido',NULL),(237,'A','94','Qtde registros no lote divergente',NULL),(238,'A','95','Qtde lotes no arquivo inválido',NULL),(239,'A','96','Qtde lotes no arquivo divergente',NULL),(240,'A','97','Qtde registros no arquivo inválido',NULL),(241,'A','98','Qtde registros no arquivo divergente',NULL),(242,'A','99','Código de DDD inválido',NULL);
UNLOCK TABLES;

--
-- Table structure for table `movimento_retorno`
--

DROP TABLE IF EXISTS `movimento_retorno`;


CREATE TABLE `movimento_retorno` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(2) DEFAULT NULL,
  `descricao` varchar(128) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;


--
-- Dumping data for table `movimento_retorno`
--

LOCK TABLES `movimento_retorno` WRITE;
INSERT INTO `movimento_retorno` VALUES (1,'01','Solicitação de Impressão de Títulos Confirmada',NULL),(2,'02','Entrada Confirmada',NULL),(3,'03','Entrada Rejeitada',NULL),(4,'04','Transferência de Carteira/Entrada',NULL),(5,'05','Transferência de Carteira/Baixa',NULL),(6,'06','Liquidação',NULL),(7,'07','Confirmação do Recebimento da Instrução de Desconto',NULL),(8,'08','Confirmação do Recebimento do Cancelamento do Desconto',NULL),(9,'09','Baixa',NULL),(10,'12','Confirmação Recebimento Instrução de Abatimento',NULL),(11,'13','Confirmação Recebimento Instrução de Cancelamento Abatimento',NULL),(12,'14','Confirmação Recebimento Instrução Alteração de Vencimento',NULL),(13,'19','Confirmação Recebimento Instrução de Protesto',NULL),(14,'20','Confirmação Recebimento Instrução de Sustação/Cancelamento de Protesto',NULL),(15,'23','Remessa a Cartório',NULL),(16,'24','Retirada de Cartório',NULL),(17,'25','Protestado e Baixado (Baixa por Ter Sido Protestado)',NULL),(18,'26','Instrução Rejeitada',NULL),(19,'27','Confirmação do Pedido de Alteração de Outros Dados',NULL),(20,'28','Débito de Tarifas/Custas',NULL),(21,'30','Alteração de Dados Rejeitada',NULL),(22,'35','Confirmação de Inclusão Banco de Sacado',NULL),(23,'36','Confirmação de Alteração Banco de Sacado',NULL),(24,'37','Confirmação de Exclusão Banco de Sacado',NULL),(25,'38','Emissão de Bloquetos de Banco de Sacado',NULL),(26,'39','Manutenção de Sacado Rejeitada',NULL),(27,'40','Entrada de Título via Banco de Sacado Rejeitada',NULL),(28,'41','Manutenção de Banco de Sacado Rejeitada',NULL),(29,'44','Estorno de Baixa / Liquidação',NULL),(30,'45','Alteração de Dados',NULL);
UNLOCK TABLES;

ALTER TABLE `conta` 
DROP COLUMN `id`,
CHANGE COLUMN `nome` `nome` VARCHAR(255) NOT NULL ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`nome`);

create table perfil (
        id bigint(20) not null auto_increment,
        atualizacao datetime not null,
        nome varchar(255),
        conta_nome varchar(255),
        primary key (id)
    );
    
ALTER TABLE `perfil` 
ADD INDEX `FK_CONTA_PERFIL_idx` (`conta_nome` ASC);
ALTER TABLE `perfil` 
ADD CONSTRAINT `FK_CONTA_PERFIL`
  FOREIGN KEY (`conta_nome`)
  REFERENCES `conta` (`nome`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


--
-- Table structure for table `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;


CREATE TABLE `pessoa` (
   `id` bigint(20) NOT NULL AUTO_INCREMENT,
   `endereco_id` bigint(20) DEFAULT NULL,
   `celular` varchar(255) DEFAULT NULL,
   `cod_area` varchar(3) DEFAULT NULL,
   `nome` varchar(100) DEFAULT NULL,
   `atualizacao` datetime DEFAULT NULL,
   `codigo_area_celular` varchar(8) DEFAULT NULL,
   `conta_bancaria_id` bigint(20) DEFAULT NULL,
   `conta_nome` VARCHAR(255) DEFAULT NULL,
   PRIMARY KEY (`id`),
   KEY `idx_pessoa_conta_bancaria` (`conta_bancaria_id`),
   KEY `idx_pessoa_endereco` (`endereco_id`),
   KEY `idx_conta_pessoa` (`conta_nome`),
   CONSTRAINT `fk_pessoa_conta_bancaria` FOREIGN KEY (`conta_bancaria_id`) REFERENCES `conta_bancaria` (`id`),
   CONSTRAINT `fk_pessoa_endereco` FOREIGN KEY (`endereco_id`) REFERENCES `endereco` (`id`),
   CONSTRAINT `fk_conta_pessoa` FOREIGN KEY (`conta_nome`) REFERENCES `conta` (`nome`) ON DELETE NO ACTION ON UPDATE NO ACTION
 ) ENGINE=InnoDB AUTO_INCREMENT=2080 DEFAULT CHARSET=utf8;



--
-- Table structure for table `pessoa_fisica`
--

DROP TABLE IF EXISTS `pessoa_fisica`;


CREATE TABLE `pessoa_fisica` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cpf` varchar(11) DEFAULT NULL,
  `data_nascimento` datetime DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2080 DEFAULT CHARSET=utf8;



--
-- Table structure for table `pessoa_juridica`
--

DROP TABLE IF EXISTS `pessoa_juridica`;


CREATE TABLE `pessoa_juridica` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cnpj` varchar(14) DEFAULT NULL,
  `nome_fantasia` varchar(255) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



--
-- Table structure for table `pessoa_migracaco`
--

DROP TABLE IF EXISTS `pessoa_migracaco`;


CREATE TABLE `pessoa_migracaco` (
  `nome` varchar(100) NOT NULL,
  `bloco` varchar(45) DEFAULT NULL,
  `apartamento` varchar(45) DEFAULT NULL,
  `cpf` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;




--
-- Table structure for table `recebe_boleto`
--

DROP TABLE IF EXISTS `recebe_boleto`;


CREATE TABLE `recebe_boleto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=utf8;


--
-- Table structure for table `remessa`
--

DROP TABLE IF EXISTS `remessa`;


CREATE TABLE `remessa` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `boletos_concatenados` longblob,
  `codigo_movimento_remessa` int(11) DEFAULT NULL,
  `nome` varchar(30) DEFAULT NULL,
  `numero_remessa` bigint(20) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  `arquivo` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Table structure for table `remessa_boleto`
--

DROP TABLE IF EXISTS `remessa_boleto`;


CREATE TABLE `remessa_boleto` (
  `remessa_id` bigint(20) NOT NULL,
  `boleto_id` bigint(20) NOT NULL,
  KEY `FK_boleto_remessa` (`boleto_id`),
  KEY `FK_remessa_boleto` (`remessa_id`),
  CONSTRAINT `FK_boleto_remessa` FOREIGN KEY (`boleto_id`) REFERENCES `boleto` (`id`),
  CONSTRAINT `FK_remessa_boleto` FOREIGN KEY (`remessa_id`) REFERENCES `remessa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `retorno`
--

DROP TABLE IF EXISTS `retorno`;


CREATE TABLE `retorno` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `atualizacao` datetime DEFAULT NULL,
  `arquivo` longblob,
  `data_importacao` date DEFAULT NULL,
  `numero_retorno` bigint(20) DEFAULT NULL,
  `valor_total` decimal(19,2) DEFAULT NULL,
  `quantidade_registros` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `transporte`
--

DROP TABLE IF EXISTS `transporte`;


CREATE TABLE `transporte` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `apartamento_id` bigint(20) DEFAULT NULL,
  `placa` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_transporte_apartamento` (`apartamento_id`),
  CONSTRAINT `fk_transporte_apartamento` FOREIGN KEY (`apartamento_id`) REFERENCES `apartamento` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table conta
--

DROP TABLE IF EXISTS conta;


CREATE TABLE conta (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(100) NOT NULL,
  senha varchar(200) NOT NULL,
  `enabled` tinyint(4) DEFAULT '1',
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (nome),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2078 DEFAULT CHARSET=utf8;


--
-- Table structure for table `valor_customizado`
--

DROP TABLE IF EXISTS `valor_customizado`;


CREATE TABLE `valor_customizado` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `atualizacao` datetime DEFAULT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  `configurador_id` bigint(20) DEFAULT NULL,
  `recebedor_boleto_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_valor_cust_config` (`configurador_id`),
  KEY `idx_valor_cust_receb_bolel` (`recebedor_boleto_id`),
  CONSTRAINT `FK_valor_cust_config` FOREIGN KEY (`configurador_id`) REFERENCES `configurador_boleto` (`id`),
  CONSTRAINT `FK_valor_cust_receb_bolel` FOREIGN KEY (`recebedor_boleto_id`) REFERENCES `recebe_boleto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- Dump completed on 2016-09-03 22:43:48
