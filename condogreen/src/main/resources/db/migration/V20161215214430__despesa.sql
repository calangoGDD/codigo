 create table despesa (
    id bigint not null auto_increment,
    categoria varchar(3),
    comprovante longblob,
    data_pagamento date,
    data_vencimento date not null,
    nome varchar(255) not null,
    valor decimal(19,2) not null,
    versao bigint,
    primary key (id)
);