DROP TABLE IF EXISTS `movimento_retorno`;

CREATE TABLE `situacao_boleto` (
  `id` BIGINT(20) NOT NULL,
  `codigo` VARCHAR(2) NOT NULL,
  `descricao` VARCHAR(128) NOT NULL,
  `atualizacao` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `situacao_boleto` VALUES 
(1, '01', 'Solicitação de Impressão de Títulos Confirmada', NOW()),
(2, '02', 'Entrada Confirmada', NOW()),
(3, '03', 'Entrada Rejeitada', NOW()),
(4, '04', 'Transferência de Carteira/Entrada', NOW()),
(5, '05', 'Transferência de Carteira/Baixa', NOW()),
(6, '06', 'Liquidação', NOW()),
(7, '07', 'Confirmação do Recebimento da Instrução de Desconto', NOW()),
(8, '08', 'Confirmação do Recebimento do Cancelamento do Desconto', NOW()),
(9, '09', 'Baixa', NOW()),
(10, '12', 'Confirmação Recebimento Instrução de Abatimento', NOW()),
(11, '13', 'Confirmação Recebimento Instrução de Cancelamento Abatimento', NOW()),
(12, '14', 'Confirmação Recebimento Instrução Alteração de Vencimento', NOW()),
(13, '19', 'Confirmação Recebimento Instrução de Protesto', NOW()),
(14, '20', 'Confirmação Recebimento Instrução de Sustação/Cancelamento de Protesto', NOW()),
(15, '23', 'Remessa a Cartório', NOW()),
(16, '24', 'Retirada de Cartório', NOW()),
(17, '25', 'Protestado e Baixado (Baixa por Ter Sido Protestado)', NOW()),
(18, '26', 'Instrução Rejeitada', NOW()),
(19, '27', 'Confirmação do Pedido de Alteração de Outros Dados', NOW()),
(20, '28', 'Débito de Tarifas/Custas', NOW()),
(21, '30', 'Alteração de Dados Rejeitada', NOW()),
(22, '35', 'Confirmação de Inclusão Banco de Sacado', NOW()),
(23, '36', 'Confirmação de Alteração Banco de Sacado', NOW()),
(24, '37', 'Confirmação de Exclusão Banco de Sacado', NOW()),
(25, '38', 'Emissão de Bloquetos de Banco de Sacado', NOW()),
(26, '39', 'Manutenção de Sacado Rejeitada', NOW()),
(27, '40', 'Entrada de Título via Banco de Sacado Rejeitada', NOW()),
(28, '41', 'Manutenção de Banco de Sacado Rejeitada', NOW()),
(29, '44', 'Estorno de Baixa / Liquidação', NOW()),
(30, '45', 'Alteração de Dados', NOW()),
(31, 'CS', 'Cacelamento Solicitado', NOW()),
(32, 'ER', 'Enviado para registro', NOW()),
(33, 'AC', 'Aguardando confirmação de envio', NOW()),
(34, 'GR', 'Aguardando geração de remessa.', NOW());


ALTER TABLE `boleto` 
DROP COLUMN `situacao`,
ADD COLUMN `situacao_boleto_id` BIGINT(20) NOT NULL AFTER `atualizacao`,
ADD INDEX `fk_situacao_boleto_idx` (`situacao_boleto_id` ASC);
ALTER TABLE `boleto` 
ADD CONSTRAINT `fk_situacao_boleto`
  FOREIGN KEY (`situacao_boleto_id`)
  REFERENCES `situacao_boleto` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
