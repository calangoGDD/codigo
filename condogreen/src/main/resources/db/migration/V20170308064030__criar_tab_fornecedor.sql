CREATE TABLE `fornecedor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
   nome varchar(40),
  `data_criacao` date DEFAULT NULL,
  `imagem` longblob,
  `nomeimagem` varchar(125) DEFAULT NULL,
  `situacao` bit(1) DEFAULT NULL,
  `versao` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;