ALTER TABLE `remessa` 
DROP COLUMN `boletos_concatenados`,
ADD UNIQUE INDEX `numero_remessa_UNIQUE` (`numero_remessa` ASC);
