ALTER TABLE `morador` 
DROP COLUMN `tipo`,
DROP COLUMN `responsavel_boleto`;

ALTER TABLE `apartamento_morador` 
ADD COLUMN `tipo` VARCHAR(45) NULL AFTER `apartamento_id`,
ADD COLUMN `responsavel_boleto` BIT(1) NULL DEFAULT b'0' AFTER `tipo`;