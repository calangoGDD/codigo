ALTER TABLE `apartamento_morador` 
DROP FOREIGN KEY `fk_apartamento_morador_apartamento`,
DROP FOREIGN KEY `fk_apartamento_morador_morador`;
ALTER TABLE `apartamento_morador` 
CHANGE COLUMN `morador_id` `morador_id` BIGINT(20) NULL ,
CHANGE COLUMN `apartamento_id` `apartamento_id` BIGINT(20) NULL ,
DROP PRIMARY KEY;
ALTER TABLE `apartamento_morador` 
ADD CONSTRAINT `fk_apartamento_morador_apartamento`
  FOREIGN KEY (`apartamento_id`)
  REFERENCES `apartamento` (`id`),
ADD CONSTRAINT `fk_apartamento_morador_morador`
  FOREIGN KEY (`morador_id`)
  REFERENCES `morador` (`id`);


ALTER TABLE `apartamento_morador` 
ADD COLUMN `id` BIGINT(20) NOT NULL AUTO_INCREMENT FIRST,
ADD PRIMARY KEY (`id`, `morador_id`, `apartamento_id`);
