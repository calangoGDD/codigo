 create table categoria_documento (
        id bigint not null auto_increment,
        nome varchar(100),
        simbolo varchar(2),
        primary key (id)
    );

create table documento (
        id bigint not null auto_increment,
        arquivo longblob,
        data_criacao date,
        nome varchar(100),
        versao bigint,
        categoria_id bigint,
        primary key (id)
    );

alter table documento 
        add constraint FK_documento_categoria 
        foreign key (categoria_id) 
        references categoria_documento (id);