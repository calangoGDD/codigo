ALTER TABLE `boleto` 
DROP FOREIGN KEY `fk_boleto_pessoa`;
ALTER TABLE `boleto` 
CHANGE COLUMN `sacado_id` `sacado_id` BIGINT(20) NOT NULL ;
ALTER TABLE `boleto` 
ADD CONSTRAINT `fk_boleto_pessoa`
  FOREIGN KEY (`sacado_id`)
  REFERENCES `pessoa` (`id`);