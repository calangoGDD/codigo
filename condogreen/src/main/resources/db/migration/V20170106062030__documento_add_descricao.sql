ALTER TABLE `documento` 
DROP FOREIGN KEY `FK_documento_categoria`;
ALTER TABLE `documento` 
CHANGE COLUMN `nome` `nome` VARCHAR(100) NOT NULL ,
CHANGE COLUMN `categoria_id` `categoria_id` BIGINT(20) NOT NULL ,
ADD COLUMN `descricao` VARCHAR(100) NOT NULL AFTER `categoria_id`;
ALTER TABLE `documento` 
ADD CONSTRAINT `FK_documento_categoria`
  FOREIGN KEY (`categoria_id`)
  REFERENCES `categoria_documento` (`id`);
