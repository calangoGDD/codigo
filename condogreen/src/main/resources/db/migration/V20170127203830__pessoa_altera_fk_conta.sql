ALTER TABLE `pessoa` 
DROP FOREIGN KEY `fk_conta_pessoa`;
ALTER TABLE `pessoa` 
CHANGE COLUMN `conta_nome` `conta_id` BIGINT(20) NULL DEFAULT NULL ;
ALTER TABLE `pessoa` 
ADD CONSTRAINT `fk_conta_pessoa`
  FOREIGN KEY (`conta_id`)
  REFERENCES `conta` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
