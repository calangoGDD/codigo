define([], function() {
	
	var DocumentoCreateController = function($scope, $http, $documentoService, $location, $route, $documentoCategoriaService) {
		ctrl = this;
		ctrl.documento = {};
		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {};
		ctrl.arquivo = {};
		ctrl.documentoCategoria = {};
		
		$documentoCategoriaService.query(function(response) {
			ctrl.documentoCategoria = response;
		});
		
		ctrl.action = function() {
	    	if($scope.form.$valid) {
	    		if(!ctrl.documento.arquivo){
	    			if(!angular.element('#error')[0].firstChild){
	    				angular.element('#error').append('<div id="panelMessages" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><ul id="messages"></ul></div>').show();
	    			}
	    			
	    			angular.element('#messages').append("<li> A imgem não foi adicionada a notícia.</li>");
	    			
	    			return;
	    		}
        		$documentoService.salvar(ctrl.documento, function(response) {
        			$location.path("/documentos");
        		});
        	}
		};
		
	};
	
	return ['$scope', '$http', '$documentoService', '$location', '$route', '$documentoCategoriaService', DocumentoCreateController];
});