define([], function() {

	var CondominioRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/condominio-list', {
			templateUrl: rootContext.concat('/condominio/condominio-list.html'),
			controller: 'condominioListController',
			controllerAs: 'ctrl'
		})
		.when('/condominio-create', {
			templateUrl: rootContext.concat('/condominio/condominio-create.html'),
			controller: 'condominioCreateController',
			controllerAs: 'ctrl'
		})
		.when('/condominio-read/:id', {
			templateUrl: rootContext.concat('/condominio/condominio-read.html'),
			controller: 'condominioReadController',
			controllerAs: 'ctrl'
		})
		.when('/condominio-update/:id', {
			templateUrl: rootContext.concat('/condominio/condominio-update.html'),
			controller: 'condominioUpdateController',
			controllerAs: 'ctrl'
		})
		.when('/condominio-delete/:id', {
			templateUrl: rootContext.concat('/condominio/condominio-delete.html'),
			controller: 'condominioDeleteController',
			controllerAs: 'ctrl'
		});

	};
	return ['CONTEXT', '$routeProvider', CondominioRoute];
});