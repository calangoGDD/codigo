define([], function() {

	var DocumentoRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/documentos', {
			templateUrl: rootContext.concat('/condominio/documento-list.html'),
			controller: 'documentoListController',
			controllerAs: 'ctrl'
		})
		.when('/documentos-create', {
			templateUrl: rootContext.concat('/condominio/documento-create.html'),
			controller: 'documentoCreateController',
			controllerAs: 'ctrl'
		})
		.when('/documentos-update/:id', {
			templateUrl: rootContext.concat('/condominio/documento-create.html'),
			controller: 'documentoUpdateController',
			controllerAs: 'ctrl'
		})
		.when('/documentos-portal-list', {
			templateUrl: rootContext.concat('/condominio/documento-portal-list.html'),
			controller: 'documentoListPortalController',
			controllerAs: 'ctrl'
		});
		
	};
	return ['CONTEXT', '$routeProvider', DocumentoRoute];
});