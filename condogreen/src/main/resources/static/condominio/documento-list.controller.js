define([], function() {
	
	var DocumentoListController = function($location, $documentoService, NgTableParams, CONTEXT, $modalService, $route, $documentoCategoriaService) {

		ctrl = this;
		ctrl.CONTEXT = CONTEXT.root;
		ctrl.tableParams = new NgTableParams({page: 1, count: 10, filter: {}}, {total: 0, counts: [], getData: load});
		function load($defer, params) {
    		var filter = angular.extend({timestamp: new Date().getTime(), page: params.page() - 1, size: params.count(), sort:'dataCriacao,desc'}, params.filter());
    		
    		$documentoService.query(filter, function(response) {
				if(response.content) {
					ctrl.documentos = response.content;
					ctrl.tableParams.total(response.totalElements);
				}
				$defer.resolve(ctrl.documentos);
			});
		}
		
		
		ctrl.createHref = function(id, action) {
			return "#/documento-" + action + "/" + id;
		};
		
		var modalOptions = {
		        bodyText: 'O documento será removido.'
		    };
			
		ctrl.delete = function(idDocumento) {
			$modalService.showModal({}, modalOptions).then(function (result) {
				$documentoService.remover({id:idDocumento}, function(response) {
					$route.reload();
				});
			});
		};
		
		$documentoCategoriaService.query(function(response) {
			ctrl.categorias = response;
		});
		
		
	};
	return ['$location', '$documentoService', 'ngTableParams','CONTEXT', '$modalService', '$route','$documentoCategoriaService', DocumentoListController];
});