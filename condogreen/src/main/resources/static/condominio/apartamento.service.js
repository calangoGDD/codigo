define([], function() {

	var ApartamentoServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/apartamentos/:id'), {app: 'condogreen', id: '@id'},
				{
					query: {
						method: 'GET',
						isArray: false
					},
					update: {
						method: 'PUT',
						isArray: false
					}
				}
			);
	};
	
	return ['CONTEXT', '$resource', ApartamentoServiceFn];
});