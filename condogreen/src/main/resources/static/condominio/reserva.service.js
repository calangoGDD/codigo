define([], function() {

	var ReservaServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/reservas/:id/get'), {app: 'condogreen', timeStamp : new Date().getTime(), id: '@id'},
				{
					query: {
						url: rootContext.concat('/reservas/'),
						method: 'GET',
						isArray: true
					},
					update: {
						url: rootContext.concat('/reservas/atualizar'),
						method: 'PUT',
						isArray: false
					},
					reservar: {
						url: rootContext.concat('/reservas/reservar'),
						method: 'POST',
						isArray: false
					},
					remover: {
						url: rootContext.concat('/reservas/:id/remover'),
						method: 'GET'
					},
					aprovar: {
						url: rootContext.concat('/reservas/:id/aprovar'),
						method: 'GET'
					}
				}
			);
	};
	
	return ['CONTEXT', '$resource', ReservaServiceFn];
});