define([], function() {

	var CondominioUpdateController = function($routeParams, $location, $condominioService) {

		var ctrl = this;
		
		ctrl.condominio = {};
		$condominioService.get($routeParams, function(response) {
			ctrl.condominio = response;
		});
		
		ctrl.save = function(condominio) {
			$condominioService.update($routeParams, condominio, savePedidos);
		};

		savePedidos = function(response) {
			$location.path("/condominio-list");

//			var pedido = {
//				"condominio": response._links.self.href,
//				"entrega" : {
//					"estado" : "PE",
//					"cidade" : "CIDADE",
//					"bairro" : "BBB",
//					"rua" : "RUA",
//					"numero" : 91,
//					"complemento" : "COMP"
//				}
//			};
//			console.log(angular.toJson(pedido));
//			
//			$http({method: 'POST', url: '/pedidos', data: pedido}).success(function(response) {
//				console.log(response);
//			});
		};
	
	};
	return ['$routeParams', '$location', '$condominioService', CondominioUpdateController];
});