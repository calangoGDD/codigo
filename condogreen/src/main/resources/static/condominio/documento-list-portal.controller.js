define([], function() {
	
	var DocumentoListPortalController = function($documentoCategoriaService) {

		ctrl = this;
		
		$documentoCategoriaService.query(function(response) {
			ctrl.categorias = response;
		});
		
		
	};
	return ['$documentoCategoriaService', DocumentoListPortalController];
});