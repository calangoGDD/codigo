define([], function() {
	
	var ReservaController = function($authService, $route, $scope, $http, $reservaService, $modalService, uiCalendarConfig, moment) {
		ctrl = this;
		$scope.SelectedEvent = null;
	    var isFirstTime = true;
	 
	    $scope.events = [];
	    $scope.eventSources = [$scope.events];
	 
	 
	    //Load events from server
	    $reservaService.query(function(response) {
	    	$scope.events.slice(0, $scope.events.length);
	        angular.forEach(response, function (value) {
	            $scope.events.push({
	                title: value.tituloFormatado,
	                id: value.id,
	                description: value.descricao,
	                start: moment(value.inicio, 'DD/MM/YYYY'),
	                end: moment(value.inicio, 'DD/MM/YYYY'),
	                allDay : value.diaTodo,
	                stick: true 
	            });
	        });
		});
	    
	    //configure calendar
	    $scope.uiConfig = {
	        calendar: {
	        	monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
	        	dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
	        	dayNamesShort: ["Dom", "Seg", "Ter", "Quar", "Qui", "Sex", "Sab"],
	        	height: 450,
	            editable: true,
	            displayEventTime: false,
	            header: {
	                left: 'month basicWeek basicDay agendaWeek agendaDay',
	                center: 'title',
	                right:'today prev,next'
	            },
	            eventClick: function (event) {
	                $scope.SelectedEvent = event;
	            },
	            eventAfterAllRender: function () {
	                if ($scope.events.length > 0 && isFirstTime) {
	                    //Focus first event
	                    //uiCalendarConfig.calendars.myCalendar.fullCalendar('gotoDate', $scope.events[0].start);
	                }
	            }
	        }
	    };

	    var modalOptions = {
	    		bodyText: 'A reserva será removida.',
	    };
	    
	    ctrl.delete = function(idReserva) {
	    	$modalService.showModal({}, modalOptions).then(function (result) {
	    		$reservaService.remover({id:idReserva}, function(response) {
	    			$route.reload();
	    		});
	    	});
	    };
	    
	    var modalOptionsAprovar = {
	    		bodyText: 'A reserva será aprovada.'
	    };
	    ctrl.aprovar = function(idReserva) {
	    	$modalService.showModal({}, modalOptionsAprovar).then(function (result) {
	    		$reservaService.aprovar({id:idReserva}, function(response) {
	    			$route.reload();
	    		});
	    	});
	    };
	    
	    ctrl.hasAuthority = function(authority) {
			return $authService.hasAuthority(authority);
		}
	};
	
	
	
	return ['$authService', '$route', '$scope', '$http', '$reservaService', '$modalService', 'uiCalendarConfig', 'moment', ReservaController];
});