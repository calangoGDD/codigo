define([], function() {

	var DocumentoServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/documentos/:id'), {app: 'condogreen', timeStamp : new Date().getTime(), id: '@id'},
				{
					query: {
						url: rootContext.concat('/documentos'),
						method: 'GET',
						isArray: false
					},
					get: {
						url: rootContext.concat('/documentos/get/:id'),
						method: 'GET',
						isArray: false
					},
					documentosByCategoria: {
						url: rootContext.concat('/documentos/byCategoria/:id'),
						method: 'GET',
						isArray: true
					},
					update: {
						url: rootContext.concat('/documentos/atualizar'),
						method: 'PUT',
						isArray: false
					},
					salvar: {
						url: rootContext.concat('/documentos/create'),
						method: 'POST',
						headers: {'Content-Type': undefined},
						transformRequest: function(documento) {
					        var formData = new FormData();
					        
					        if(documento.arquivo) {
					        	formData.append("arquivo", documento.arquivo);
					        }
					        
					        formData.append('documento', new Blob([angular.toJson(documento)], {
					            type: "application/json"
					        }));
					        return formData;
						},
						transformResponse: function(data) {
							return {conteudo: data};
						}
					},
					remover: {
						url: rootContext.concat('/documentos/:id/delete'),
						method: 'GET'
					}
				}
			);
	};
	
	return ['CONTEXT', '$resource', DocumentoServiceFn];
});