define([], function() {
	
	var reservaUpdateController = function($scope, $http, $routeParams, $reservaService, $location) {
		ctrl = this;
		ctrl.reserva = {};
		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {};
		
		$reservaService.get($routeParams, function(response) {
			ctrl.reserva = response;
		});
		ctrl.action = function() {
			if($scope.form.$valid) {
				$reservaService.update($routeParams, ctrl.reserva, function(response) {
					$location.path("/reservas");
				});
			}
		};
	};
	
	return ['$scope', '$http', '$routeParams', '$reservaService', '$location', reservaUpdateController];
});