define([], function() {
	
	var CondominioCreateController = function($location, $condominioService) {

		var ctrl = this;
		
		ctrl.condominio = {};
		ctrl.save = function(condominio) {
			$condominioService.save(condominio, function(response) {
				$location.path("#/home");
			});
		};
	
	};
	return ['$location', '$condominioService', CondominioCreateController];
});