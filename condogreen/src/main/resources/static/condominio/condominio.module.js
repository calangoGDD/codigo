/**
 * 
 */
require.config({
	paths: {
		
		condominioRoute: 'condominio/condominio.route',
		condominioService: 'condominio/condominio.service',
		condominioListController: 'condominio/condominio-list.controller',
		condominioCreateController: 'condominio/condominio-create.controller',
		condominioReadController: 'condominio/condominio-read.controller',
		condominioUpdateController: 'condominio/condominio-update.controller',
		condominioDeleteController: 'condominio/condominio-delete.controller',
		blocoService: 'condominio/bloco.service',
		apartamentoService: 'condominio/apartamento.service',
		
		reservaController: 'condominio/reserva.controller',
		reservaRoute: 'condominio/reserva.route',
		reservaService: 'condominio/reserva.service',
		reservaCreateController: 'condominio/reserva-create.controller',
		reservaUpdateController: 'condominio/reserva-update.controller',
		
		documentoRoute: 'condominio/documento.route',
		documentoService: 'condominio/documento.service',
		documentoListController: 'condominio/documento-list.controller',
		documentoCreateController: 'condominio/documento-create.controller',
		documentoUpdateController:	'condominio/documento-update.controller',
		documentoCategoriaService:	'condominio/documento-categoria.service',
		documentoListPortalController: 'condominio/documento-list-portal.controller',
		
		moradorRoute: 'condominio/morador.route',
		moradorService: 'condominio/morador.service',
		moradorListController: 'condominio/morador-list.controller',
		moradorCreateOrUpdateController: 'condominio/morador-create-update.controller',
	}
});

var _condominioDeps = [
	'angular',
	'condominioRoute',
	'condominioService',
	'condominioListController',
	'condominioCreateController',
	'condominioReadController',
	'condominioUpdateController',
	'condominioDeleteController',
	'blocoService',
	'apartamentoService',
	
	'reservaController',
	'reservaRoute',
	'reservaService',
	'reservaCreateController',
	'reservaUpdateController',
	
	'documentoRoute',
	'documentoService',
	'documentoListController',
	'documentoCreateController',
	'documentoUpdateController',
	'documentoCategoriaService',
	'documentoListPortalController',
	
	'moradorRoute',
	'moradorService',
	'moradorListController',
	'moradorCreateOrUpdateController'
];
define(_condominioDeps, function(
		angular,
		condominioRoute,
		condominioService,
		condominioListController,
		condominioCreateController,
		condominioReadController,
		condominioUpdateController,
		condominioDeleteController,
		blocoService,
		apartamentoService,
		
		reservaController,
		reservaRoute,
		reservaService,
		reservaCreateController,
		reservaUpdateController,
	
		documentoRoute,
		documentoService,
		documentoListController,
		documentoCreateController,
		documentoUpdateController,
		documentoCategoriaService,
		documentoListPortalController,
		
		moradorRoute,
		moradorService,
		moradorListController,
		moradorCreateOrUpdateController) {
	
	var _condominioModule = angular.module('condominioModule', []);
	
	_condominioModule.config(condominioRoute);
	_condominioModule.config(reservaRoute);
	
	_condominioModule.factory('$condominioService', condominioService);
	_condominioModule.factory('$blocoService', blocoService);
	_condominioModule.factory('$apartamentoService', apartamentoService);

	_condominioModule.controller('condominioListController', condominioListController);
	_condominioModule.controller('condominioCreateController', condominioCreateController);
	_condominioModule.controller('condominioReadController', condominioReadController);
	_condominioModule.controller('condominioUpdateController', condominioUpdateController);
	_condominioModule.controller('condominioDeleteController', condominioDeleteController);
	
	_condominioModule.config(documentoRoute);
	_condominioModule.factory('$documentoService', documentoService);
	_condominioModule.controller('documentoListController', documentoListController);
	_condominioModule.controller('documentoCreateController', documentoCreateController);
	_condominioModule.controller('documentoUpdateController', documentoUpdateController);
	_condominioModule.controller('documentoListPortalController', documentoListPortalController);
	
	_condominioModule.factory('$documentoCategoriaService', documentoCategoriaService);
	
	
	_condominioModule.factory('$reservaService', reservaService);
	_condominioModule.controller('reservaController', reservaController);
	_condominioModule.controller('reservaCreateController', reservaCreateController);
	_condominioModule.controller('reservaUpdateController', reservaUpdateController);
	
	
	_condominioModule.config(moradorRoute);
	_condominioModule.factory('$moradorService', moradorService);
	_condominioModule.controller('moradorListController', moradorListController);
	_condominioModule.controller('moradorCreateOrUpdateController', moradorCreateOrUpdateController);
	
	return _condominioModule;
});