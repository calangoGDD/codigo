define([], function() {

	var DocumentoCategoriaServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/categorias-documento/:id'), {app: 'condogreen', timeStamp : new Date().getTime(), id: '@id'},
				{
					query: {
						url: rootContext.concat('/categorias-documento'),
						method: 'GET',
						isArray: true
					}
				}
			);
	};
	
	return ['CONTEXT', '$resource', DocumentoCategoriaServiceFn];
});