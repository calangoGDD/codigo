define([], function() {
	
	var ReservaCreateController = function($scope, $http, $reservaService, $location) {
		ctrl = this;
		ctrl.reserva = {};
		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {};
		
		ctrl.action = function() {
			if($scope.form.$valid) {
				$reservaService.reservar(ctrl.reserva, function(response) {
					$location.path("/reservas");
				});
			}
		};
	};
	
	return ['$scope', '$http', '$reservaService', '$location', ReservaCreateController];
});