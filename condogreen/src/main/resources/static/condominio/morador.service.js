define([], function() {

	var MoradorServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/moradores/:id'), {app: 'condogreen', timeStamp : new Date().getTime(), id: '@id'},
				{
					query: {
						url: rootContext.concat('/moradores'),
						method: 'GET',
						isArray: false
					},
					get: {
						url: rootContext.concat('/moradores/get/:id'),
						method: 'GET',
						isArray: false
					},
					update: {
						url: rootContext.concat('/moradores/atualizar'),
						method: 'PUT',
						isArray: false
					},
					salvar: {
						url: rootContext.concat('/moradores/create'),
						method: 'POST'
					},
					remover: {
						url: rootContext.concat('/moradores/delete/:id'),
						method: 'GET'
					}
				}
			);
	};
	
	return ['CONTEXT', '$resource', MoradorServiceFn];
});