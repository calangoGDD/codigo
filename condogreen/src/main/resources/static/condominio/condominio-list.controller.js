define([], function() {
	
	var CondominioListController = function($location, $condominioService) {

		var ctrl = this;
		
		ctrl.condominios = [];
		$condominioService.query({timestamp: new Date().getTime()}, function(response) {
			if(response._embedded) {
				ctrl.condominios = response._embedded.condominios;
			}
		});
		
		ctrl.getId = function(url) {
			return url.substr(url.lastIndexOf("/") + 1);
		};
		
		ctrl.createHref = function(url, action) {
			return "#/condominio-" + action + "/" + ctrl.getId();
		};
	
	};
	return ['$location', '$condominioService', CondominioListController];
});