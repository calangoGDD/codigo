define([], function() {
	
	var MoradorCreateController = function($scope, $http, $moradorService, $location, $route, $blocoService, $apartamentoService, $routeParams, toastr, $anchorScroll) {
		ctrl = this;
		ctrl.morador = {};
		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {};
		
		if($routeParams.id){
			$moradorService.get($routeParams, function(response) {
				ctrl.morador = response;
			});
		}
		
		ctrl.simNao = [
  			{"true": "Sim"},
  			{"false": "Não"}
  		].reduce(function(memo, obj) {
  		    return angular.extend(memo, obj);
  		}, {});
		
		ctrl.tipoMorador = [
			{"IQ": "Inquilino"},
  			{"PR": "Proprietário"}
		].reduce(function(memo, obj) {
  		    return angular.extend(memo, obj);
  		}, {});
		
		$blocoService.query(function(response) {
			if(response._embedded) {
				ctrl.blocos = response._embedded.blocos;
			}
		});
	
		$scope.linkApartamentos = {};
		$scope.apts = {};
		
		ctrl.getApartamentos = function(linkApartamentos){
			$http.get(linkApartamentos).then(function(response){
		          $scope.apts = response.data._embedded.apartamentos;                
			});
		}
		
		ctrl.action = function() {
	    	if($scope.form.$valid) {
        		$moradorService.salvar(ctrl.morador, function(response) {
        			$location.path("/moradores");
        		});
        	}
		};
		
		ctrl.getId = function(url) {
			return url.substr(url.lastIndexOf("/") + 1);
		};
		
		ctrl.morador.apartamentos = [];
		$scope.addApartamento = function() {
			var apartamentoId = ctrl.getId(ctrl.morador.uriApartamento);
			if(naoExiste(apartamentoId)){
				var apartamento = null;
				$apartamentoService.get({id: apartamentoId}, function (response) {
					var identificacaoRecebedorBoleto = response.identificacaoRecebedorBoleto;
					ctrl.morador.apartamentos.push({
						nome: identificacaoRecebedorBoleto,
						apartamentoId: apartamentoId,
						tipo: ctrl.morador.tipo,
						responsavelBoleto: ctrl.morador.responsavelBoleto
					});
				});
			} else {
				toastr.error("Apartamento já adicionado ao morador!", "Error");
			}
		};
		
		naoExiste = function(apartamentoId) {
			var result = true;
			angular.forEach(ctrl.morador.apartamentos, function(apartamento) {
				if(apartamento.apartamentoId == apartamentoId) {
					result = false;
				}
			})
			
			return result;
		};
		
		ctrl.deleteApartamento = function(apartamento) {
			 var index = ctrl.morador.apartamentos.indexOf(apartamento);
			 if (index != -1) {
				 ctrl.morador.apartamentos.splice(index, 1);
			 }
			
		}
		
	};
	
	return ['$scope', '$http', '$moradorService', '$location', '$route', '$blocoService', '$apartamentoService', '$routeParams', 'toastr','$anchorScroll', MoradorCreateController];
});