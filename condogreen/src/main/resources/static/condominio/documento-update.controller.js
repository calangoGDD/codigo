define([], function() {
	
	var DocumentoUpdateController = function($scope, $http, $routeParams, $documentoService, $location) {
		ctrl = this;
		ctrl.documento = {};
		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {};
		
		ctrl.arquivo = {};
		$documentoService.get($routeParams, function(response) {
			ctrl.documento = response;
		});
		
		ctrl.action = function() {
	    	if($scope.form.$valid) {
	    		
	    		if(!ctrl.documento.imagem){
	    			if(!angular.element('#error')[0].firstChild){
	    				angular.element('#error').append('<div id="panelMessages" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><ul id="messages"></ul></div>').show();
	    			}
	    			
	    			angular.element('#messages').append("<li> O documneto não foi anexado.</li>");
	    			
	    			return;
	    		}
	    		
        		$documentoService.salvar(ctrl.documento, function(response) {
        			$location.path("/documentos");
        		});
        	}
			
		};
	};
	
	return ['$scope', '$http', '$routeParams', '$documentoService', '$location', DocumentoUpdateController];
});