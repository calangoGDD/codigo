define([], function() {

	var ReservaRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/reservas', {
			templateUrl: rootContext.concat('/condominio/reserva-list.html'),
			controller: 'reservaController',
			controllerAs: 'ctrl'
		})
		.when('/reservas-create', {
			templateUrl: rootContext.concat('/condominio/reserva-create.html'),
			controller: 'reservaCreateController',
			controllerAs: 'ctrl'
		})
		.when('/reservas-update/:id', {
			templateUrl: rootContext.concat('/condominio/reserva-create.html'),
			controller: 'reservaUpdateController',
			controllerAs: 'ctrl'
		});

	};
	return ['CONTEXT', '$routeProvider', ReservaRoute];
});