define([], function() {

	var CondominioReadController = function($routeParams, $condominioService) {

		var ctrl = this;
		var ctrl = this;
		
		$condominioService.get($routeParams, function(response) {
			ctrl.condominio = response;
		});
	
	};
	return ['$routeParams', '$condominioService', CondominioReadController];
});