define([], function() {

	var BlocoServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/blocos/:id'), {app: 'condogreen', id: '@id'},
				{
					query: {
						method: 'GET',
						isArray: false
					},
					update: {
						method: 'PUT',
						isArray: false
					}
				}
			);
	};
	
	return ['CONTEXT', '$resource', BlocoServiceFn];
});