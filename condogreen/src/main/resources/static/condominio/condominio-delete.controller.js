define([], function() {

	var DespesaDeleteController = function($routeParams, $location, $despesaService) {

		var ctrl = this;
		
		ctrl.despesa = {};
		$despesaService.get($routeParams, function(response) {
			ctrl.despesa = response;
		});
		
		ctrl.confirm = function(despesa) {
			$despesaService.delete($routeParams, function(response) {
				$location.path("/despesa-list");
			});
		};
	
	};
	return ['$routeParams', '$location', '$despesaService', DespesaDeleteController];
});