define([], function() {

	var MoradorRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/moradores', {
			templateUrl: rootContext.concat('/condominio/morador-list.html'),
			controller: 'moradorListController',
			controllerAs: 'ctrl'
		})
		.when('/moradores-create', {
			templateUrl: rootContext.concat('/condominio/morador-create.html'),
			controller: 'moradorCreateOrUpdateController',
			controllerAs: 'ctrl'
		})
		.when('/moradores-update/:id', {
			templateUrl: rootContext.concat('/condominio/morador-update.html'),
			controller: 'moradorCreateOrUpdateController',
			controllerAs: 'ctrl'
		});
		
	};
	return ['CONTEXT', '$routeProvider', MoradorRoute];
});