define([], function() {
	
	var MoradorListController = function($location, $moradorService, NgTableParams, CONTEXT, $modalService, $route) {

		ctrl = this;
		ctrl.CONTEXT = CONTEXT.root;
		ctrl.tableParams = new NgTableParams({page: 1, count: 10, filter: {}}, {total: 0, counts: [], getData: load});
		function load($defer, params) {
    		var filter = angular.extend({timestamp: new Date().getTime(), page: params.page() - 1, size: params.count(), sort:'id,desc'}, params.filter());
    		
    		$moradorService.query(filter, function(response) {
				if(response.content) {
					ctrl.moradores = response.content;
					ctrl.tableParams.total(response.totalElements);
				}
				$defer.resolve(ctrl.moradores);
			});
		}
		
		
		ctrl.createHref = function(id, action) {
			return "#/morador-" + action + "/" + id;
		};
		
		var modalOptions = {
		        bodyText: 'O morador será removido.'
		    };
			
		ctrl.delete = function(idMorador) {
			$modalService.showModal({}, modalOptions).then(function (result) {
				$moradorService.remover({id:idMorador}, function(response) {
					$route.reload();
				});
			});
		};
		
	};
	return ['$location', '$moradorService', 'ngTableParams','CONTEXT', '$modalService', '$route', MoradorListController];
});