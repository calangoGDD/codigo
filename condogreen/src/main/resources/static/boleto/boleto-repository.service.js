define([], function() {

	var BoletoRepositoryFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/boletos/:id'), {app: 'condogreen', id: '@id'}, {
			query: {
				method: 'GET',
				isArray: false
			},
			searchBoletosAguardandoRemessa: {
				url: rootContext.concat('/boletos/search/findBySituacao/'),
				params:{situacao:'GR'},
				method: 'GET'
			},
			findAllByOrderByAtualizacaoDesc: {
				url: rootContext.concat('/boletos/search/findAllByOrderByAtualizacaoDesc/'),
				method: 'GET'
			}
		});
	};
	
	return ['CONTEXT', '$resource', BoletoRepositoryFn];
});