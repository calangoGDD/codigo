define([], function() {

	var RetornoServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		
		return $resource(rootContext.concat('/retornos/:id'), {app: 'condogreen', timeStamp : new Date().getTime(), id: '@id'}, {
			query: {
				method: 'GET',
				isArray: false
			},
			update: {
				method: 'PUT',
				isArray: false
			},
			importar: {
				url: rootContext.concat('/retornos/importar'),
				method: 'POST',
				headers: {'Content-Type': undefined},
				transformRequest: function(arquivo) {
			        var formData = new FormData();
			        formData.append("arquivo", arquivo);
			        return formData;
				},
				transformResponse: function(data) {
					return {conteudo: data};
				}
			},
			migrar: {
				url: rootContext.concat('/retornos/migrar'),
				method: 'POST',
				headers: {'Content-Type': undefined},
				transformRequest: function(arquivo) {
			        var formData = new FormData();
			        formData.append("arquivo", arquivo);
			        return formData;
				},
				transformResponse: function(data) {
					return {conteudo: data};
				}
			},
			getInformacoesRetorno: {
				url: rootContext.concat('/retornos/:id/informacoesRetorno'),
				method: 'GET'
			},
			getConfigurador: {
				url: rootContext.concat('/retornos/:id/configurador'),
				method: 'GET',
				isArray: false
			}
		});
	};
	
	return ['CONTEXT', '$resource', RetornoServiceFn];
});