define([], function() {
	
	var ConfiguradorBoletoListController = function($scope, NgTableParams, $configuradorBoletoService) {

		ctrl = this;
		ctrl.tableParams = new NgTableParams({page: 1, count: 10, filter: {nome: ""}}, {total: 0, counts: [], getData: load});
		function load($defer, params) {
    		var filter = angular.extend({page: params.page() - 1, size: params.count()}, params.filter());
			
    		return $configuradorBoletoService.search(filter, function(response) {
				if(response._embedded) {
					ctrl.configuradorBoletos = response._embedded.configuradorboletos;
					ctrl.tableParams.total(response.page.totalElements);
				}
				$defer.resolve(ctrl.configuradorBoletos);
			});
		}

		ctrl.showFilter = true;
		ctrl.toggleFilter = function() {
			ctrl.showFilter = !ctrl.showFilter;
			if(!ctrl.showFilter) {
				ctrl.tableParams.filter({nome: ""});
			}
		};
		
		ctrl.getId = function(url) {
			return url.substr(url.lastIndexOf("/") + 1);
		};
		
		ctrl.createHref = function(url, action) {
			return "#/configurador-boleto-" + action + "/" + ctrl.getId(url);
		};
		
		ctrl.especieDoTituloList = [
    			{"CH_CHEQUE": "01 CH Cheque"},
    			{"DM_DUPLICATA_MERCANTIL": "02 DM Duplicata Mercantil"},
    			{"DMI_DUPLICATA_MERCANTIL_PARA_INDICACAO": "03 DMI Duplicata Mercantil p/ Indicação"},
    			{"DS_DUPLICATA_DE_SERVICO": "04 DS Duplicata de Serviço"},
    			{"DSI_DUPLICATA_DE_SERVICO_PARA_INDICACAO": "05 DSI Duplicata de Serviço p/ Indicação"},
    			{"DR_DUPLICATA_RURAL": "06 DR Duplicata Rural"},
    			{"LC_LETRA_DE_CAMBIO": "07 LC Letra de Câmbio"},
    			{"NCC_NOTA_DE_CREDITO_COMERCIAL": "08 NCC Nota de Crédito Comercial"},
    			{"NCE_NOTA_DE_CREDITO_A_EXPORTACAO": "09 NCE Nota de Crédito a Exportação"},
    			{"NCI_NOTA_DE_CREDITO_INDUSTRIAL": "10 NCI Nota de Crédito Industrial"},
    			{"NCR_NOTA_DE_CREDITO_RURAL": "11 NCR Nota de Crédito Rural"},
    			{"NP_NOTA_PROMISSORIA": "12 NP Nota Promissória"},
    			{"NPR_NOTA_PROMISSORIA_RURAL": "13 NPR Nota Promissória Rural"},
    			{"TM_TRIPLICATA_MERCANTIL": "14 TM Triplicata Mercantil"},
    			{"TS_TRIPLICATA_DE_SERVICO": "15 TS Triplicata de Serviço"},
    			{"NS_NOTA_DE_SEGURO": "16 NS Nota de Seguro"},
    			{"RC_RECIBO": "17 RC Recibo"},
    			{"FAT_FATURA": "18 FAT Fatura"},
    			{"ND_NOTA_DE_DEBITO": "19 ND Nota de Débito"},
    			{"AP_APOLICE_DE_SEGURO": "20 AP Apólice de Seguro"},
    			{"ME_MENSALIDADE_ESCOLAR": "21 ME Mensalidade Escolar"},
    			{"PC_PARCELA_DE_CONSORCIO": "22 PC Parcela de Consórcio"},
    			{"NF_NOTA_FISCAL": "23 NF Nota Fiscal"},
    			{"DD_DOCUMENTO_DE_DIVIDA": "24 DD Documento de Dúvida"},
    			{"CEDULA_DE_PRODUTO_RURAL": "25 CPR Cédula de Produto Rural"},
    			{"OUTROS": "99 OU Outros"}
    		].reduce(function(memo, obj) {
    		    return angular.extend(memo, obj);
    		}, {});

	};
	return ['$scope', 'ngTableParams', '$configuradorBoletoService', ConfiguradorBoletoListController];
});
