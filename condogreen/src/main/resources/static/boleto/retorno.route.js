define([], function() {

	var RetornoRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/retorno-list', {
			templateUrl: rootContext.concat('/boleto/retorno-list.html'),
			controller: 'retornoListController',
			controllerAs: 'ctrl'
		})
		.when('/retorno-generate', {
			templateUrl: rootContext.concat('/boleto/retorno-generate.html'),
			controller: 'retornoGenerateController',
			controllerAs: 'ctrl'
		})
		.when('/retorno-read/:id', {
			templateUrl: rootContext.concat('/boleto/retorno-read.html'),
			controller: 'retornoReadController',
			controllerAs: 'ctrl'
		});
	};
	
	return ['CONTEXT', '$routeProvider', RetornoRoute];
});