define([], function() {

	var ConfiguradorBoletoRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/configurador-boleto-list', {
			templateUrl: rootContext.concat('/boleto/configurador-boleto-list.html'),
			controller: 'configuradorBoletoListController',
			controllerAs: 'ctrl'
		})
		.when('/configurador-boleto-create', {
			templateUrl: rootContext.concat('/boleto/configurador-boleto-create.html'),
			controller: 'configuradorBoletoCreateController',
			controllerAs: 'ctrl'
		})
		.when('/configurador-boleto-read/:id', {
			templateUrl: rootContext.concat('/boleto/configurador-boleto-read.html'),
			controller: 'configuradorBoletoReadController',
			controllerAs: 'ctrl'
		})
		.when('/configurador-boleto-update/:id', {
			templateUrl: rootContext.concat('/boleto/configurador-boleto-update.html'),
			controller: 'configuradorBoletoUpdateController',
			controllerAs: 'ctrl'
		})
		.when('/configurador-boleto-delete/:id', {
			templateUrl: rootContext.concat('/boleto/configurador-boleto-delete.html'),
			controller: 'configuradorBoletoDeleteController',
			controllerAs: 'ctrl'
		});
	};
	return ['CONTEXT', '$routeProvider', ConfiguradorBoletoRoute];
});