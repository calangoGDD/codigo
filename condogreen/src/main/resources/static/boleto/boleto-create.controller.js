define([], function() {
	
	var BoletoCreateController = function($scope, $location, $configuradorBoletoService, $boletoService, $apartamentoService, $blocoService, $http) {
		ctrl = this;

		ctrl.boletoDTO = {};
		
		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {};
		
		$configuradorBoletoService.query(function(response) {
			if(response._embedded) {
				ctrl.configuradores = response._embedded.configuradorboletos;
			}
		});
		
		$scope.linkApartamentos = {};
		
		$blocoService.query(function(response) {
			if(response._embedded) {
				ctrl.blocos = response._embedded.blocos;
			}
		});
	
		$scope.apartamentos = {};
		ctrl.getApartamentos = function(linkApartamentos){
			$http.get(linkApartamentos).then(function(response){
		          $scope.apartamentos = response.data._embedded.apartamentos;                
			});
		}
		
		ctrl.create = function() {
			if($scope.form.$valid) {
				$boletoService.save(ctrl.boletoDTO, function(response) {
					$location.path("/boleto-list");
				});
			}
		};
		
		ctrl.limparValor = function(field) {
			delete ctrl.boletoDTO[field];
		};

		ctrl.gerarBoletosEmMassa = function() {
			if($scope.form.$valid) {
				$boletoService.gerarBoletosEmMassa(ctrl.boletoDTO, function(response) {
					$location.path("/boleto-list");
				}, function(error) {
					angular.element('#error').append('<div id="panelMessages" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><ul id="messages"></ul></div>').show();
					error.data.forEach(function(msg, i) {
						angular.element('#messages').append("<li>" + msg + "</li>");
					});
				});
			}
		};
	};

	
	return ['$scope', '$location', '$configuradorBoletoService', '$boletoService', '$apartamentoService', '$blocoService','$http', BoletoCreateController];
});
