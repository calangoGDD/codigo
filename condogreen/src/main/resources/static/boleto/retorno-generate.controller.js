define([], function() {
	
	var RetornoGenerateController = function($scope, $location, $filter, $configuradorBoletoService, $retornoService) {
		ctrl = this;

		ctrl.retorno = {};
		
		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {
				minDate: new Date(),
		};
		
		$configuradorBoletoService.query(function(response) {
			if(response._embedded) {
				ctrl.configuradores = response._embedded.configuradorboletos;
			}
			
		});
		
		ctrl.create = function() {
			if($scope.form.$valid) {
				console.log(ctrl.retorno);
				$retornoService.save(ctrl.retorno, function(response) {
					$location.path("/retorno-list");
				}, function(error) {
					angular.element('#error').append('<div id="panelMessages" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><ul id="messages"></ul></div>').show();
					error.data.forEach(function(msg, i) {
						angular.element('#messages').append("<li>" + msg + "</li>");
					});
				});
			}
		};
		
		ctrl.limparValor = function(field) {
			delete ctrl.retorno[field];
		};
		
	};

	return ['$scope', '$location', '$filter', '$configuradorBoletoService', '$retornoService', RetornoGenerateController];
});
