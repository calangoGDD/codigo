define([], function() {
	
	var RetornoListController = function($scope, NgTableParams, $http, $retornoService) {
		
		ctrl = this;
		ctrl.tableParams = new NgTableParams({page: 1, count: 10, filter: {}}, {total: 0, counts: [], getData: load});
		function load($defer, params) {
    		var filter = angular.extend({timestamp: new Date().getTime(), page: params.page() - 1, size: params.count()}, params.filter());
			
    		return $retornoService.query(filter, function(response) {
				if(response._embedded) {
					ctrl.retornos = response._embedded.retornos;
					ctrl.tableParams.total(response.page.totalElements);
				}
				$defer.resolve(ctrl.retornos);
			});
		}

		ctrl.showFilter = true;
		ctrl.toggleFilter = function() {
			ctrl.showFilter = !ctrl.showFilter;
			if(!ctrl.showFilter) {
				ctrl.tableParams.filter();
			}
		};
		
		ctrl.getId = function(url) {
			return url.substr(url.lastIndexOf("/") + 1);
		};
		
		ctrl.createHref = function(url, action) {
			return "#/retorno-" + action + "/" + ctrl.getId(url);
		};
		
		ctrl.arquivo = {};
		ctrl.upload = function() {
	        $retornoService.migrar(ctrl.arquivo, function(data) {
	        	ctrl.tableParams.reload();
	        });
		};
		
		ctrl.downloadFile = function(arquivo, nome) {
			//The atob function will decode a base64-encoded string into a new string with a character for each byte of the binary data.
			var byteCharacters = atob(arquivo);
			var byteNumbers = new Array(byteCharacters.length);
			for (var i = 0; i < byteCharacters.length; i++) {
			    byteNumbers[i] = byteCharacters.charCodeAt(i);
			}
			
			var byteArray = new Uint8Array(byteNumbers);
	        var blob = new Blob([byteArray], {type: "text/plain;charset=utf-8"});
	        saveAs(blob, nome);
		}
	};
	return ['$scope', 'ngTableParams', '$http', '$retornoService', RetornoListController];
});
