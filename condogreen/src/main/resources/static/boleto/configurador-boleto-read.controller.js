define([], function() {

	var ConfiguradorBoletoReadController = function($routeParams, $configuradorBoletoService) {

		var ctrl = this;
		ctrl.disabled = true;
		
		$configuradorBoletoService.get($routeParams, function(response) {
			ctrl.configuradorBoleto = response;
		});

		ctrl.dias = [];
		for(i = 1; i <= 31; i++) {
			if(i < 10) {
				ctrl.dias.push("0" + i);
			} else {
				ctrl.dias.push("" + i);
			}
		}

		ctrl.especieDoTituloList = [
			{"CH_CHEQUE": "01 CH Cheque"},
			{"DM_DUPLICATA_MERCANTIL": "02 DM Duplicata Mercantil"},
			{"DMI_DUPLICATA_MERCANTIL_PARA_INDICACAO": "03 DMI Duplicata Mercantil p/ Indicação"},
			{"DS_DUPLICATA_DE_SERVICO": "04 DS Duplicata de Serviço"},
			{"DSI_DUPLICATA_DE_SERVICO_PARA_INDICACAO": "05 DSI Duplicata de Serviço p/ Indicação"},
			{"DR_DUPLICATA_RURAL": "06 DR Duplicata Rural"},
			{"LC_LETRA_DE_CAMBIO": "07 LC Letra de Câmbio"},
			{"NCC_NOTA_DE_CREDITO_COMERCIAL": "08 NCC Nota de Crédito Comercial"},
			{"NCE_NOTA_DE_CREDITO_A_EXPORTACAO": "09 NCE Nota de Crédito a Exportação"},
			{"NCI_NOTA_DE_CREDITO_INDUSTRIAL": "10 NCI Nota de Crédito Industrial"},
			{"NCR_NOTA_DE_CREDITO_RURAL": "11 NCR Nota de Crédito Rural"},
			{"NP_NOTA_PROMISSORIA": "12 NP Nota Promissória"},
			{"NPR_NOTA_PROMISSORIA_RURAL": "13 NPR Nota Promissória Rural"},
			{"TM_TRIPLICATA_MERCANTIL": "14 TM Triplicata Mercantil"},
			{"TS_TRIPLICATA_DE_SERVICO": "15 TS Triplicata de Serviço"},
			{"NS_NOTA_DE_SEGURO": "16 NS Nota de Seguro"},
			{"RC_RECIBO": "17 RC Recibo"},
			{"FAT_FATURA": "18 FAT Fatura"},
			{"ND_NOTA_DE_DEBITO": "19 ND Nota de Débito"},
			{"AP_APOLICE_DE_SEGURO": "20 AP Apólice de Seguro"},
			{"ME_MENSALIDADE_ESCOLAR": "21 ME Mensalidade Escolar"},
			{"PC_PARCELA_DE_CONSORCIO": "22 PC Parcela de Consórcio"},
			{"NF_NOTA_FISCAL": "23 NF Nota Fiscal"},
			{"DD_DOCUMENTO_DE_DIVIDA": "24 DD Documento de Dúvida"},
			{"CEDULA_DE_PRODUTO_RURAL": "25 CPR Cédula de Produto Rural"},
			{"OUTROS": "99 OU Outros"}
		].reduce(function(memo, obj) {
		    return angular.extend(memo, obj);
		}, {});
		
		ctrl.codMultaList = [
            {"SEM_MULTA": "Sem Multa"},
            {"VALOR_FIXO": "Valor Fixo"},
            {"PERCENTUAL": "Percentual"}
        ].reduce(function(memo, obj) {
		    return angular.extend(memo, obj);
		}, {});		
		
		ctrl.codDescontoList = [
            {"SEM_DESCONTO": "Sem Desconto"},
            {"VALOR_FIXO": "Valor Fixo"},
            {"PERCENTUAL": "Percentual"}
        ].reduce(function(memo, obj) {
		    return angular.extend(memo, obj);
		}, {});		
		
		ctrl.codMoraList = [
        	{"VALOR_POR_DIA": "Valor por Dia"},
        	{"TAXA_MENSAL": "Taxa Mensal"},
        	{"ISENTO": "Isento"}
        ].reduce(function(memo, obj) {
		    return angular.extend(memo, obj);
		}, {});		
		
		ctrl.codProtestoList = [
			{"PROTESTAR": "Protestar"},
			{"NAO_PROTESTAR": "Não Protestar"},
			{"CANCELAMENTO_AUTOMATICO": "Cancelamento Automático"}
		].reduce(function(memo, obj) {
		    return angular.extend(memo, obj);
		}, {});		
		
		ctrl.codDevolucaoList = [
	        {"BAIXAR_DEVOLVER": "Baixar / Devolver"},
	        {"NAO_BAIXAR_NAO_DEVOLVER": "Não Baixar / Não Devolver"}
        ].reduce(function(memo, obj) {
		    return angular.extend(memo, obj);
		}, {});
		
	};
	return ['$routeParams', '$configuradorBoletoService', ConfiguradorBoletoReadController];
});