define([], function() {

	var RemessaServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		
		return $resource(rootContext.concat('/remessas/:id'), {app: 'condogreen', timeStamp : new Date().getTime(), id: '@id'}, {
			query: {
				method: 'GET',
				isArray: false
			},
			update: {
				method: 'PUT',
				isArray: false
			},
			gerar: {
				url: rootContext.concat('/remessas/gerar'),
				method: 'POST',
				isArray: false
			},
			getBoletos: {
				url: rootContext.concat('/remessas/:id/boletos'),
				method: 'GET'
			},
			getConfigurador: {
				url: rootContext.concat('/remessas/:id/configurador'),
				method: 'GET',
				isArray: false
			},
			notificar: {
				url: rootContext.concat('/remessas/:numeroRemessa/notificar'),
				method: 'GET'
			},
			findAllByOrderByAtualizacaoDesc: {
				url: rootContext.concat('/remessas/findAllByOrderByAtualizacaoDesc'),
				method: 'GET',
				isArray: false
			}
			
		});
	};
	
	return ['CONTEXT', '$resource', RemessaServiceFn];
});