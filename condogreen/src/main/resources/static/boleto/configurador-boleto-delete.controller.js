define([], function() {

	var ConfiguradorBoletoDeleteController = function($routeParams, $location, $configuradorBoletoService) {
		ctrl = this;
		
		ctrl.configuradorBoleto = {};
		$configuradorBoletoService.get($routeParams, function(response) {
			ctrl.disabled = true;
			ctrl.configuradorBoleto = response;
		});
		
		ctrl.confirm = function() {
			$configuradorBoletoService.delete($routeParams, function(response) {
				$location.path("/configurador-boleto-list");
			});
		};
	
	};
	return ['$routeParams', '$location', '$configuradorBoletoService', ConfiguradorBoletoDeleteController];
});