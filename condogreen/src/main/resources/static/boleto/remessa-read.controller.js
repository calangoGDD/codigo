define([], function() {
	
	var RemessaReadController = function(CONTEXT, $routeParams, NgTableParams, $remessaService) {
		ctrl = this;
		ctrl.CONTEXT = CONTEXT.root;
		
		ctrl.disabled = true;
		ctrl.remessa = {};
		
		
		ctrl.tableParams = new NgTableParams({page: 1, count: 10, filter: {nossoNumero:""}}, {total: 0, counts: [], getData: load});
		function load($defer, params) {
    		var filter = angular.extend({page: params.page() - 1, size: params.count()}, params.filter());
			
			ctrl.boletos = [];
    		return $remessaService.getBoletos($routeParams, function(responseBoletos) {
				$defer.resolve(responseBoletos._embedded.boletos);
			});
		}
		
		
		$remessaService.get($routeParams, function(remessa) {
			ctrl.remessa = remessa;
		});

		
		ctrl.downloadFile = function(arquivo, nome) {
			var byteCharacters = atob(arquivo);
			var byteNumbers = new Array(byteCharacters.length);
			for (var i = 0; i < byteCharacters.length; i++) {
			    byteNumbers[i] = byteCharacters.charCodeAt(i);
			}
			
			var byteArray = new Uint8Array(byteNumbers);
	        var blob = new Blob([byteArray], {type: "application/pdf;charset=utf-8"});
	        saveAs(blob, nome);
		}
		
	};

	return ['CONTEXT', '$routeParams', 'ngTableParams', '$remessaService', RemessaReadController];
});
