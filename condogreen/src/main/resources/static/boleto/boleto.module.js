/**
 * 
 */
require.config({
	paths: {
		configuradorBoletoRoute: 'boleto/configurador-boleto.route',
		configuradorBoletoService: 'boleto/configurador-boleto.service',
		configuradorBoletoListController: 'boleto/configurador-boleto-list.controller',
		configuradorBoletoCreateController: 'boleto/configurador-boleto-create.controller',
		configuradorBoletoReadController: 'boleto/configurador-boleto-read.controller',
		configuradorBoletoUpdateController: 'boleto/configurador-boleto-update.controller',
		configuradorBoletoDeleteController: 'boleto/configurador-boleto-delete.controller',
		remessaRoute: 'boleto/remessa.route',
		remessaService: 'boleto/remessa.service',
		remessaListController: 'boleto/remessa-list.controller',
		remessaReadController: 'boleto/remessa-read.controller',
		retornoRoute: 'boleto/retorno.route',
		retornoService: 'boleto/retorno.service',
		retornoListController: 'boleto/retorno-list.controller',
		retornoGenerateController: 'boleto/retorno-generate.controller',
		retornoReadController: 'boleto/retorno-read.controller',
		valorCustomizadoRoute: 'boleto/valor-customizado.route',
		valorCustomizadoService: 'boleto/valor-customizado.service',
		valorCustomizadoListController: 'boleto/valor-customizado-list.controller',
		valorCustomizadoCreateController: 'boleto/valor-customizado-create.controller',
		valorCustomizadoReadController: 'boleto/valor-customizado-read.controller',
		valorCustomizadoUpdateController: 'boleto/valor-customizado-update.controller',
		boletoRoute: 'boleto/boleto.route',
		boletoService: 'boleto/boleto.service',
		boletoRepositoryService	: 'boleto/boleto-repository.service',
		boletoCreateController: 'boleto/boleto-create.controller',
		meusBoletosController: 'boleto/meus-boletos.controller',
		boletoListController: 'boleto/boleto-list.controller',
		modalService: 'core/modalService.service',
		datePickerDirective: 'core/datePickerDirective.directive',
		datepickerLocaldateDirective: 'core/datepickerLocaldateDirective.directive'
	}
});

var _boletoDeps = [
	'angular',
	'configuradorBoletoRoute',
	'configuradorBoletoService',
	'configuradorBoletoListController',
	'configuradorBoletoCreateController',
	'configuradorBoletoReadController',
	'configuradorBoletoUpdateController',
	'configuradorBoletoDeleteController',
	'remessaRoute',
	'remessaService',
	'remessaListController',
	'remessaReadController',
	'retornoRoute',
	'retornoService',
	'retornoListController',
	'retornoGenerateController',
	'retornoReadController',
	'valorCustomizadoRoute',
	'valorCustomizadoService',
	'valorCustomizadoListController',
	'valorCustomizadoCreateController',
	'valorCustomizadoUpdateController',
	'valorCustomizadoReadController',
	'boletoRoute',
	'boletoService',
	'boletoRepositoryService',
	'boletoCreateController',
	'meusBoletosController',
	'boletoListController',
	'modalService',
	'datePickerDirective',
	'datepickerLocaldateDirective'
];

define(_boletoDeps, function(
		angular,
		configuradorBoletoRoute,
		configuradorBoletoService,
		configuradorBoletoListController,
		configuradorBoletoCreateController,
		configuradorBoletoReadController,
		configuradorBoletoUpdateController,
		configuradorBoletoDeleteController,
		remessaRoute,
		remessaService,
		remessaListController,
		remessaReadController,
		retornoRoute,
		retornoService,
		retornoListController,
		retornoGenerateController,
		retornoReadController,
		valorCustomizadoRoute,
		valorCustomizadoService,
		valorCustomizadoListController,
		valorCustomizadoCreateController,
		valorCustomizadoUpdateController,
		valorCustomizadoReadController,
		boletoRoute,
		boletoService,
		boletoRepositoryService,
		boletoCreateController,
		meusBoletosController,
		boletoListController,
		modalService,
		datePickerDirective,
		datepickerLocaldateDirective) {
	
	var _boletoModule = angular.module('boletoModule', []);
	
	_boletoModule.config(configuradorBoletoRoute);
	_boletoModule.config(remessaRoute);
	_boletoModule.config(retornoRoute);
	_boletoModule.config(valorCustomizadoRoute);
	_boletoModule.config(boletoRoute);
	
	_boletoModule.directive('mydatepicker', datePickerDirective);
	_boletoModule.directive('datepickerLocaldate', datepickerLocaldateDirective);
	
	_boletoModule.factory('$configuradorBoletoService', configuradorBoletoService);
	_boletoModule.factory('$remessaService', remessaService);
	_boletoModule.factory('$retornoService', retornoService);
	_boletoModule.factory('$valorCustomizadoService', valorCustomizadoService);
	_boletoModule.factory('$boletoService', boletoService);
	_boletoModule.factory('$boletoRepositoryService', boletoRepositoryService);
	_boletoModule.service('$modalService', modalService);
	
	_boletoModule.controller('configuradorBoletoListController', configuradorBoletoListController);
	_boletoModule.controller('configuradorBoletoCreateController', configuradorBoletoCreateController);
	_boletoModule.controller('configuradorBoletoReadController', configuradorBoletoReadController);
	_boletoModule.controller('configuradorBoletoUpdateController', configuradorBoletoUpdateController);
	_boletoModule.controller('configuradorBoletoDeleteController', configuradorBoletoDeleteController);

	_boletoModule.controller('remessaListController', remessaListController);
	_boletoModule.controller('remessaReadController', remessaReadController);

	_boletoModule.controller('retornoListController', retornoListController);
	_boletoModule.controller('retornoGenerateController', retornoGenerateController);
	_boletoModule.controller('retornoReadController', retornoReadController);

	_boletoModule.controller('valorCustomizadoListController', valorCustomizadoListController);
	_boletoModule.controller('valorCustomizadoCreateController', valorCustomizadoCreateController);
	_boletoModule.controller('valorCustomizadoUpdateController', valorCustomizadoUpdateController);
	_boletoModule.controller('valorCustomizadoReadController', valorCustomizadoReadController);

	_boletoModule.controller('boletoCreateController', boletoCreateController);
	_boletoModule.controller('meusBoletosController', meusBoletosController);
	_boletoModule.controller('boletoListController', boletoListController);
	
	return _boletoModule;
});