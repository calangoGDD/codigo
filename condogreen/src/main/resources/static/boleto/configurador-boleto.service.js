define([], function() {

	var ConfiguradorBoletoServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/configuradorboletos/:id'), {app: 'condogreen', timeStamp : new Date().getTime(), id: '@id'},
				{
					query: {
						method: 'GET',
						isArray: false
					},
					search: {
						url: rootContext.concat('/configuradorboletos/search/findByNomeContaining'),
						method: 'GET'
					},
					update: {
						method: 'PUT',
						isArray: false
					}
				}
			);
	};
	
	return ['CONTEXT', '$resource', ConfiguradorBoletoServiceFn];
});