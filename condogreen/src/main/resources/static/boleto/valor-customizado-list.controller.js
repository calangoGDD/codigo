define([], function() {
	
	var ValorCustomizadoListController = function($scope, NgTableParams, $valorCustomizadoService, $modalService, $route) {
		
		ctrl = this;
		ctrl.tableParams = new NgTableParams({page: 1, count: 10, filter: {}}, {total: 0, counts: [], getData: load});
		function load($defer, params) {
			var filter = angular.extend({timestamp: new Date().getTime(), page: params.page() - 1, size: params.count()}, params.filter());
			
    		return $valorCustomizadoService.query(filter, function(response) {
    			if(response._embedded) {
					ctrl.valorescustomizados = response._embedded.valorescustomizados;
					ctrl.tableParams.total(response.page.totalElements);
				}
				$defer.resolve(ctrl.valorescustomizados);
			});
		}

		ctrl.showFilter = true;
		ctrl.toggleFilter = function() {
			ctrl.showFilter = !ctrl.showFilter;
			if(!ctrl.showFilter) {
				ctrl.tableParams.filter();
			}
		};
		
		ctrl.getId = function(url) {
			return url.substr(url.lastIndexOf("/") + 1);
		};
		
		ctrl.createHref = function(url, action) {
			return "#/valor-customizado-" + action + "/" + ctrl.getId(url);
		};
		
		var modalOptions = {
	        bodyText: 'O valor personalizado para este apartamento será removido do configurador.'
	    };
		
		ctrl.delete = function(url) {
			$modalService.showModal({}, modalOptions).then(function (result) {
				var idValor = ctrl.getId(url);
				$valorCustomizadoService.delete({id:idValor}, function(response) {
					$route.reload();
				});
			});
		};
	};
	return ['$scope', 'ngTableParams', '$valorCustomizadoService','$modalService','$route', ValorCustomizadoListController];
});
