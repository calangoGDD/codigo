define([], function() {

	var BoletoRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/boleto-create', {
			templateUrl: rootContext.concat('/boleto/boleto-create.html'),
			controller: 'boletoCreateController',
			controllerAs: 'ctrl'
		})
		.when('/boleto-list', {
			templateUrl: rootContext.concat('/boleto/boleto-list.html'),
			controller: 'boletoListController',
			controllerAs: 'ctrl'
		})
		.when('/gerarBoletosEmMassa', {
			templateUrl: rootContext.concat('/boleto/boleto-em-massa-generate.html'),
			controller: 'boletoCreateController',
			controllerAs: 'ctrl'
		})
		.when('/meus-boletos', {
			templateUrl: rootContext.concat('/web-basico/boleto/meus-boletos-list.html'),
			controller: 'meusBoletosController',
			controllerAs: 'ctrl'
		});
	};
	
	return ['CONTEXT', '$routeProvider', BoletoRoute];
});