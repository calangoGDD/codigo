define([], function() {

	var ValorCustomizadoReadController = function($routeParams, $valorCustomizadoService) {

		var ctrl = this;
		ctrl.disabled = true;
		
		$valorCustomizadoService.get($routeParams, function(response) {
			ctrl.valorCustomizado = response;
		});
	
	};
	return ['$routeParams', '$valorCustomizadoService', ValorCustomizadoReadController];
});