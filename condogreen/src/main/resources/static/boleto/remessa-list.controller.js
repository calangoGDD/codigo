define([], function() {
	
	var RemessaListController = function(CONTEXT, $scope, NgTableParams, $remessaService, $route) {
		
		ctrl = this;
		ctrl.CONTEXT = CONTEXT.root;
		ctrl.tableParams = new NgTableParams({page: 1, count: 10, filter: {}}, {total: 0, counts: [], getData: load});
		function load($defer, params) {
    		var filter = angular.extend({timestamp: new Date().getTime(), page: params.page() - 1, size: params.count(), sort:'atualizacao,desc'}, params.filter());
			
    		return $remessaService.query(filter, function(response) {
				if(response._embedded) {
					ctrl.remessas = response._embedded.remessas;
					ctrl.tableParams.total(response.page.totalElements);
				}
				$defer.resolve(ctrl.remessas);
			});
		}

		ctrl.showFilter = true;
		ctrl.toggleFilter = function() {
			ctrl.showFilter = !ctrl.showFilter;
			if(!ctrl.showFilter) {
				ctrl.tableParams.filter();
			}
		};
		
		ctrl.getId = function(url) {
			return url.substr(url.lastIndexOf("/") + 1);
		};
		
		ctrl.createHref = function(id, action) {
			return "#/remessa-" + action + "/" + id;
		};
		
		ctrl.downloadFile = function(arquivo, nome) {
			//The atob function will decode a base64-encoded string into a new string with a character for each byte of the binary data.
			var byteCharacters = atob(arquivo);
			var byteNumbers = new Array(byteCharacters.length);
			for (var i = 0; i < byteCharacters.length; i++) {
			    byteNumbers[i] = byteCharacters.charCodeAt(i);
			}
			
			var byteArray = new Uint8Array(byteNumbers);
	        var blob = new Blob([byteArray], {type: "text/plain;charset=utf-8"});
	        saveAs(blob, nome);
		}
		
		ctrl.gerarRemessa = function() {
			$remessaService.gerar(function(response) {
				$route.reload();
			} , function(error) {
				angular.element('#error').append('<div id="panelMessages" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><ul id="messages"></ul></div>').show();
				error.data.forEach(function(msg, i) {
					angular.element('#messages').append("<li>" + msg + "</li>");
				});
			});
		};

		ctrl.notificar = function(numeroRemessa) {
			$remessaService.notificar({numeroRemessa: numeroRemessa}, 
			function(response) {
				$route.reload();
			}, function(error) {
				angular.element('#error').append('<div id="panelMessages" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><ul id="messages"></ul></div>').show();
				error.data.forEach(function(msg, i) {
					angular.element('#messages').append("<li>" + msg + "</li>");
				});
			});
		};
	};
	
	return ['CONTEXT', '$scope', 'ngTableParams', '$remessaService', '$route', RemessaListController];
});
