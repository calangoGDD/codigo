define([], function() {

	var ValorCustomizadoServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/valorescustomizados/:id'), {app: 'condogreen', id: '@id'}, {
			query: {
				method: 'GET',
				isArray: false
			},
			search: {
				url: rootContext.concat('/valorescustomizados/search/findByNomeContaining'),
				method: 'GET'
			},
			update: {
				method: 'PUT',
				isArray: false
			}
		});
	};
	
	return ['CONTEXT', '$resource', ValorCustomizadoServiceFn];
});