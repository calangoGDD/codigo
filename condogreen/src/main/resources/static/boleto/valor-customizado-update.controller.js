define([], function() {
	
	var ValorCustomizadoUpdateController = function($scope, $routeParams, $location, $configuradorBoletoService, $valorCustomizadoService, $apartamentoService, $blocoService, $http) {
		ctrl = this;

		ctrl.valorCustomizado = {};
		$valorCustomizadoService.get($routeParams, function(response) {
			ctrl.valorCustomizado = response;
		});
		
		$configuradorBoletoService.query(function(response) {
			if(response._embedded) {
				ctrl.configuradores = response._embedded.configuradorboletos;
			}
			
		});
		
		$scope.linkApartamentos = {};
		
		$blocoService.query(function(response) {
			if(response._embedded) {
				ctrl.blocos = response._embedded.blocos;
			}
		});
	
		$scope.apartamentos = {};
		ctrl.getApartamentos = function(linkApartamentos){
			$http.get(linkApartamentos).then(function(response){
		          $scope.apartamentos = response.data._embedded.apartamentos;                
			});
		}
		
		
		ctrl.update = function() {
			if($scope.form.$valid) {
				$valorCustomizadoService.update($routeParams, ctrl.valorCustomizado, function() {
					$location.path("/valor-customizado-list");
				}, function(error) {
					angular.element('#error').append('<div id="panelMessages" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><ul id="messages"></ul></div>').show();
					error.data.forEach(function(msg, i) {
						angular.element('#messages').append("<li>" + msg + "</li>");
					});
				});
			}
		};
	
		
		ctrl.limparValor = function(field) {
			delete ctrl.valorCustomizado[field];
		};
	};

	return ['$scope', '$routeParams', '$location', '$configuradorBoletoService', '$valorCustomizadoService', '$apartamentoService', '$blocoService','$http', ValorCustomizadoUpdateController];
});
