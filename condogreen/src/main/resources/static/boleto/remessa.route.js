define([], function() {

	var RemessaRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/remessa-list', {
			templateUrl: rootContext.concat('/boleto/remessa-list.html'),
			controller: 'remessaListController',
			controllerAs: 'ctrl'
		})
		.when('/remessa-read/:id', {
			templateUrl: rootContext.concat('/boleto/remessa-read.html'),
			controller: 'remessaReadController',
			controllerAs: 'ctrl'
		});
	};
	
	return ['CONTEXT', '$routeProvider', RemessaRoute];
});