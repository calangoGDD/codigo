define([], function() {

	var BoletoServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/boleto/:id'), {app: 'condogreen', id: '@id'}, {
			update: {
				method: 'PUT',
				isArray: false
			},
			save: {
				url: rootContext.concat('/boleto/gerar'),
				method: 'POST',
				isArray: false
			},
			gerarBoletosEmMassa: {
				url: rootContext.concat('/boleto/gerarBoletosEmMassa'),
				method: 'POST',
				isArray: false
			},
			cancelar: {
				url:rootContext.concat('/boleto/cancelar'),
				method: 'POST',
				isArray: false
			},
			meusBoletos: {
				url:rootContext.concat('/basic/boleto/meusBoletos'),
				method: 'GET',
				isArray: true
			}
		});
	};
	
	return ['CONTEXT', '$resource', BoletoServiceFn];
});