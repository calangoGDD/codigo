define([], function() {
	
	var BoletoListController = function(CONTEXT, $scope, NgTableParams, $boletoService, $modalService, $boletoRepositoryService, $route, $remessaService) {
		
		ctrl = this;
		ctrl.CONTEXT = CONTEXT.root;
		ctrl.tableParams = new NgTableParams({page: 1, count: 10, filter: {}}, {total: 0, counts: [], getData: load});
		function load($defer, params) {
    		var filter = angular.extend({timestamp: new Date().getTime(), page: params.page() - 1, size: params.count()}, params.filter());
			
    		return $boletoRepositoryService.findAllByOrderByAtualizacaoDesc(filter, function(response) {
				if(response._embedded) {
					ctrl.boletos = response._embedded.boletos;
					ctrl.tableParams.total(response.page.totalElements);
				}
				$defer.resolve(ctrl.boletos);
			});
		}

		ctrl.showFilter = true;
		ctrl.toggleFilter = function() {
			ctrl.showFilter = !ctrl.showFilter;
			if(!ctrl.showFilter) {
				ctrl.tableParams.filter();
			}
		};
		
		ctrl.getId = function(url) {
			return url.substr(url.lastIndexOf("/") + 1);
		};
		
		
		ctrl.delete = function(url) {
			var modalOptions = {
			        bodyText: 'O Boleto será removido.'
			};
			$modalService.showModal({}, modalOptions).then(function (result) {
				var idBoleto = ctrl.getId(url);
				$boletoRepositoryService.delete({id:idBoleto}, function(response) {
					$route.reload();
				});
			});
		};
		
		ctrl.cancelar = function(url) {
			var modalOptions = {
			        bodyText: 'O Boleto será cancelado.',
			        actionButtonText: 'Cancelar Boleto'
			    };
			$modalService.showModal({}, modalOptions).then(function (result) {
				var idBoleto = ctrl.getId(url);
				$boletoService.cancelar({id:idBoleto}, function(response) {
					$route.reload();
				});
			});
		};
		
		ctrl.downloadFile = function(arquivo, nome) {
			//The atob function will decode a base64-encoded string into a new string with a character for each byte of the binary data.
			var byteCharacters = atob(arquivo);
			var byteNumbers = new Array(byteCharacters.length);
			for (var i = 0; i < byteCharacters.length; i++) {
			    byteNumbers[i] = byteCharacters.charCodeAt(i);
			}
			
			var byteArray = new Uint8Array(byteNumbers);
	        var blob = new Blob([byteArray], {type: "text/plain;charset=utf-8"});
	        saveAs(blob, nome);
		}
	};
	
	
	return ['CONTEXT', '$scope', 'ngTableParams', '$boletoService','$modalService','$boletoRepositoryService','$route', '$remessaService',BoletoListController];
});
