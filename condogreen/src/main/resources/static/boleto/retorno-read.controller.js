define([], function() {
	
	var RetornoReadController = function($routeParams, NgTableParams, $retornoService) {
		ctrl = this;

		ctrl.disabled = true;
		ctrl.retorno = {};
		
		
		ctrl.tableParams = new NgTableParams({page: 1, count: 10, filter: {nossoNumero:""}}, {total: 0, counts: [], getData: load});
		function load($defer, params) {
    		var filter = angular.extend({page: params.page() - 1, size: params.count()}, params.filter());
			
			ctrl.informacoesRetorno = [];
    		return $retornoService.getInformacoesRetorno($routeParams, function(response) {
				$defer.resolve(response._embedded.informacoesRetorno);
			});
		}
		
		
		$retornoService.get($routeParams, function(retorno) {
			ctrl.retorno = retorno;
		});

		
		ctrl.downloadFile = function(arquivo, nome) {
			var byteCharacters = atob(arquivo);
			var byteNumbers = new Array(byteCharacters.length);
			for (var i = 0; i < byteCharacters.length; i++) {
			    byteNumbers[i] = byteCharacters.charCodeAt(i);
			}
			
			var byteArray = new Uint8Array(byteNumbers);
	        var blob = new Blob([byteArray], {type: "application/pdf;charset=utf-8"});
	        saveAs(blob, nome);
		}
		
	};

	return ['$routeParams', 'ngTableParams', '$retornoService', RetornoReadController];
});
