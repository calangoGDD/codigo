define([], function() {
	
	var MeusBoletosController = function(CONTEXT, $boletoService, NgTableParams, $routeParams) {
		ctrl = this;
		ctrl.CONTEXT = CONTEXT.root;
		
		ctrl.tableParams = new NgTableParams({page: 1, count: 10}, {total: 0, counts: [], getData: load});
		function load($defer, params) {
    		var filter = angular.extend({page: params.page() - 1, size: params.count()}, params.filter());
			
			ctrl.boletos = [];
    		return $boletoService.meusBoletos($routeParams, function(responseBoletos) {
				$defer.resolve(responseBoletos);
			});
		}
	};

	
	return ['CONTEXT', '$boletoService', 'ngTableParams','$routeParams', MeusBoletosController];
});
