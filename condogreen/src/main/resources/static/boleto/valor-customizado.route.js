define([], function() {

	var ValorCustomizadoRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/valor-customizado-list', {
			templateUrl: rootContext.concat('/boleto/valor-customizado-list.html'),
			controller: 'valorCustomizadoListController',
			controllerAs: 'ctrl'
		})
		.when('/valor-customizado-create', {
			templateUrl: rootContext.concat('/boleto/valor-customizado-create.html'),
			controller: 'valorCustomizadoCreateController',
			controllerAs: 'ctrl'
		})
		.when('/valor-customizado-read/:id', {
			templateUrl: rootContext.concat('/boleto/valor-customizado-read.html'),
			controller: 'valorCustomizadoReadController',
			controllerAs: 'ctrl'
		})
		.when('/valor-customizado-update/:id', {
			templateUrl: rootContext.concat('/boleto/valor-customizado-update.html'),
			controller: 'valorCustomizadoUpdateController',
			controllerAs: 'ctrl'
		});
	};
	
	return ['CONTEXT', '$routeProvider', ValorCustomizadoRoute];
});