/**
 * 
 */
var _require = require;
require.config({
	paths: {
		'angular': 'bower_components/angular/angular',
		'ngRoute': 'bower_components/angular-route/angular-route',
		'ngResource': 'bower_components/angular-resource/angular-resource',
		'ngCookies': 'bower_components/angular-cookies/angular-cookies.min',
		'jquery': 'bower_components/jquery/dist/jquery',
		'ng-table': 'bower_components/ng-table/ng-table',
		'toastr': 'bower_components/angular-toastr/dist/angular-toastr.tpls',
		'ngSanitize': 'bower_components/angular-sanitize/angular-sanitize',
		'angular-input-masks': "bower_components/angular-angular-input-masks/dist/mask.min",
		'blobjs':"bower_components/Blob/Blob",
		'filesaver': "bower_components/angular-filesaver/angular-filesaver",
		'moment':'bower_components/moment/min/moment-with-locales',
		'angular-moment': 'bower_components/angular-moment/angular-moment',
		'calendar':'bower_components/angular-ui-calendar/src/calendar',
		'fullcalendar':'bower_components/fullcalendar/dist/fullcalendar',
		'gcal':'bower_components/fullcalendar/dist/gcal',
		'ckeditor': 'bower_components/angular-ckeditor/angular-ckeditor',
		'ui-bootstrap': 'bower_components/angular-bootstrap/ui-bootstrap-tpls.min',
		'string-mask': 'bower_components/string-mask/src/string-mask',
		'br-validations': 'bower_components/br-validations/releases/br-validations',
		'angular-input-masks' : 'bower_components/angular-input-masks/angular-input-masks',
		'contextoModule':"contexto/contexto.module",
		'authModule': 'auth/auth.module',
		'condominioModule': 'condominio/condominio.module',
		'boletoModule': 'boleto/boleto.module',
		'portalModule': 'portal/portal.module',
		'financeiroModule': 'financeiro/financeiro.module',
		'fileUploadModule': 'core/file-upload/file-upload.module',
	},
	shim: {
		'angular': {
			exports: 'angular'
		},
		'ngRoute': {
			deps: ['angular'],
			exports: 'ngRoute'
		},
		'ngCookies': {
			deps: ['angular'],
			exports: 'ngCookies'
		},
		'ngResource': {
			deps: ['angular'],
			exports: 'ngResource'
		},
		'ng-table': {
			deps: ['angular'],
			exports: 'ng-table'
		},
		'toastr': {
			deps: ['angular'],
			exports: 'toastr'
		},
		'moment': {
			deps:['angular'],
			exports: 'moment'
		},
		'calendar': {
			deps:['angular', 'moment'],
			exports: 'calendar',
			
		},
		'angular-moment': {
			deps:['angular'],
			exports: 'angularMoment'
		},
		'string-mask': {
			deps:['angular'],
			exports: 'string-mask'
		},
		'br-validations': {
			deps:['angular'],
			exports: 'br-validations'
		},
		'angular-input-masks': {
			deps: ['angular', 'string-mask', 'moment', 'br-validations'],
			init: function () {
                require = _require;
            },
			exports: 'angular-input-masks'
		}
		,
		'filesaver': {
			deps:['angular'],
			exports: 'filesaver'
		},
		'fullcalendar': {
			deps:['angular', 'jquery'],
			exports: 'fullcalendar'
		},
		'gcal': {
			deps:['angular', 'fullcalendar'],
			exports: 'gcal'
		},
		'ngSanitize': {
			deps:['angular'],
			exports: 'ngSanitize'
		},
		'ui-bootstrap': {
			deps:['angular'],
			exports: 'ui-bootstrap'
		}
		
	}
});

var appController = ['$authService', '$location', function($authService, $location) {
	var appCtrl = this;
	
	appCtrl.authenticated = isAuthenticated;
	appCtrl.hasAuthority = hasAuthority;
	appCtrl.isPerfilBasico = isPerfilBasico;

	appCtrl.home = function() {
		if(isPerfilBasico()) {
			$location.path("/homeb");
		} else {
			$location.path("/home");
		}
	}
	
	appCtrl.isActive = function (viewLocation) { 
        return viewLocation === $location.path();
    };
	
	function hasAuthority(authority) {
		return $authService.hasAuthority(authority);
	}
	
	function isAuthenticated() {
    	return $authService.isAuthenticated();
    }
	
	function isPerfilBasico() {
		return isAuthenticated() && hasAuthority('BASICO');
	}
}];

var appRoute = ['CONTEXT','$routeProvider', '$httpProvider','$locationProvider', function(CONTEXT, $routeProvider, $httpProvider, $locationProvider) {
	var rootContext = CONTEXT.root;
	$locationProvider.hashPrefix('');
	$routeProvider
		.when('/login', {
			templateUrl: rootContext.concat('/login.html'),
			controller: 'authController',
			controllerAs: 'authCtrl'
		})
		.when('/', {
			templateUrl: rootContext.concat('/portal/portal-sitio-viver2.html'),
			controller: 'portalController',
			controllerAs: 'portalCtrl'
		})
		.when('/evolucao-de-obras', {
			templateUrl: rootContext.concat('/portal/evolucao-de-obras.html')
		})
		.when('/regimento-interno', {
			templateUrl: rootContext.concat('/portal/regimento-interno-sv2.html')
		})
		.when('/logout', {
			template: '',
			controller: ['$authService', '$location', function($authService, $location) {
				$authService.logout().then(function() {
					$location.path("/");
				});
			}]
		})
		.when('/home', {
			templateUrl: rootContext.concat('/home.html'),
			controller: 'dashboardController',
			controllerAs: 'dashboardCtrl'
		})
		.when('/homeb', {
			templateUrl: rootContext.concat('/web-basico/home.html'),
			controller: ['$boletoService','$authService', '$location', function($boletoService, $authService, $location) {
				ctrl = this;
				if(!$authService.isAuthenticated()) {
					$location.path("/login");
				}
				
				ctrl.meusBoletos = function(){
					return $boletoService.meusBoletos();
				};
			}],
			controllerAs: 'ctrl'
		})
		.otherwise('/');
	
	$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
	$httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('XSRFInterceptor', 'errorHttpInterceptor');
}];

var disableOnRequest = ['$http', function($http) {
	return function(scope, element, attrs) {
		var oldText = element.text();
		scope.$watch(function() {
			return $http.pendingRequests.length > 0;
		},
		function(request) {
			if (request) {
				element.prop("disabled", true).text("Aguarde...");
			} else {
				element.prop("disabled", false).text(oldText);
			}
		});
	};
}];

define('app', ['angular',
               'ng-table',
               'ngRoute',
               'ngResource',
               'ngCookies',
               'toastr',
               'contextoModule',
               'authModule',
               'condominioModule',
               'boletoModule',
               'portalModule',
               'fileUploadModule',
               'financeiroModule',
               'blobjs',
               'ckeditor',
               'filesaver',
               'moment',
               'calendar',
               'angular-moment',
               'fullcalendar',
               'gcal',
               'ngSanitize',
               'ui-bootstrap',
               'angular-input-masks'], function() {
	
	var appName = 'condogreen';
	var app = angular.module(appName, ['ngRoute',
	                                   'ngResource',
	                                   'ngCookies',
	                                   'ngTable',
	                                   'toastr',
	                                   'contextoModule',
	                                   'authModule',
	                                   'condominioModule',
	                                   'boletoModule',
	                                   'portalModule',
	                                   'fileUploadModule',
	                                   'financeiroModule',
	                                   'ui.calendar',
	                                   'angularMoment',
	                                   'ngSanitize',
	                                   'ui.bootstrap',
	                                   'ui.utils.masks']);
	
	app.config(appRoute);
	app.controller('appController', appController);
	app.directive('disableOnRequest', disableOnRequest);
	app.factory('errorHttpInterceptor', ['$q', 'toastr', function ($q, toastr) {
        return {
            responseError: function responseError(rejection) {
            	angular.forEach(rejection.data, function(message, i) {
            		toastr.error(message, "Erro");
            	});
                return $q.reject(rejection);
            }
        };
    }]);
	

	angular.bootstrap(document, [appName]);

	return app;
});
