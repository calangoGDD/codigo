define([], function() {

	var FaleConoscoRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/fale-conosco',{
			templateUrl: rootContext.concat('/portal/fale-conosco.html'),
			controller: 'faleConoscoController',
			controllerAs: 'ctrl'
		});
	};
	
	return ['CONTEXT', '$routeProvider', FaleConoscoRoute];
});