define([], function() {
	
	var FornecedorPortalController = function($fornecedorService) {

		ctrl = this;
		ctrl.fornecedors = [];
		
		$fornecedorService.quatroPrimeiras(function(response) {
			if(response.content) {
				ctrl.fornecedors = response.content;
			}
		});
		
	};
	return ['$fornecedorService', FornecedorPortalController];
});