define([], function() {

	var NoticiaserviceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/noticias/:id'), {app: 'condogreen', timeStamp : new Date().getTime(), id: '@id'},
				{
					query: {
						url: rootContext.concat('/noticias'),
						method: 'GET',
						isArray: false
					},
					get: {
						url: rootContext.concat('/noticias/get/:id'),
						method: 'GET',
						isArray: false
					},
					getQuatroPrimeiras: {
						url: rootContext.concat('/noticias/getQuatroPrimeiras'),
						method: 'GET',
						isArray: true
					},
					update: {
						url: rootContext.concat('/noticias/atualizar'),
						method: 'PUT',
						isArray: false
					},
					salvar: {
						url: rootContext.concat('/noticias/create'),
						method: 'POST',
						headers: {'Content-Type': undefined},
						transformRequest: function(noticia) {
					        var formData = new FormData();
					        
					        if(noticia.imagem) {
					        	formData.append("arquivo", noticia.imagem);
					        }
					        formData.append('noticia', new Blob([angular.toJson(noticia)], {
					            type: "application/json"
					        }));
					        return formData;
						},
						transformResponse: function(data) {
							return {conteudo: data};
						}
					},
					remover: {
						url: rootContext.concat('/noticias/:id/delete'),
						method: 'GET'
					},
					ativarDesativar: {
						url: rootContext.concat('/noticias/ativar-desativar/:id'),
						method: 'PUT'
					}
				}
			);
	};
	
	return ['CONTEXT', '$resource', NoticiaserviceFn];
});