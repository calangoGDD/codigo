define([], function() {
	
	var noticiaVerMaisController = function($noticiaService, $routeParams) {
		ctrl = this;
		ctrl.noticia = {};
		
		$noticiaService.get($routeParams, function(response) {
			ctrl.noticia = response;
		});
	};
	
	return ['$noticiaService', '$routeParams', noticiaVerMaisController];
});