define([], function() {
	
	var noticiaUpdateController = function($scope, $http, $routeParams, $noticiaService, $location, toastr) {
		ctrl = this;
		ctrl.noticia = {};
		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {};
		
		ctrl.arquivo = {};
		$noticiaService.get($routeParams, function(response) {
			ctrl.noticia = response;
		});
		
		ctrl.action = function() {
	    	if($scope.form.$valid) {
	    		
	    		if(!ctrl.noticia.imagem){
	    			toastr.error("A imgem não foi adicionada a notícia.", "Error");
	    			return;
	    		}
	    		
        		$noticiaService.salvar(ctrl.noticia, function(response) {
        			$location.path("/noticias");
        		});
        	}
			
		};
	};
	
	return ['$scope', '$http', '$routeParams', '$noticiaService', '$location', 'toastr', noticiaUpdateController];
});