define([], function() {
	
	var PortalController = function($cookies, $noticiaService) {
			$cookies.remove('authenticated');
			
			portalCtrl = this;
			portalCtrl.noticias = {};
			
			$noticiaService.getQuatroPrimeiras(function(response) {
				if(response) {
					portalCtrl.noticias = response;
				}
				
			});
	};
	return ['$cookies', '$noticiaService', PortalController];
});