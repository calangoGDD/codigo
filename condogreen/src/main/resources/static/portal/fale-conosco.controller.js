define([], function() {
	
	var FaleConoscoController = function($scope, $faleConoscoService) {
		ctrl = this;

		ctrl.formularioContato = {};
		
		ctrl.submit = function() {
			if($scope.form.$valid) {
				$faleConoscoService.submit(ctrl.formularioContato, function(response) {
					angular.element('#error').append('<div id="panelMessages" class="alert alert-success fade in alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><ul id="messages"></ul></div>').show();
					angular.element('#messages').append("<li> Mensagem enviada com sucesso!</li>");
					ctrl.limparValor('nome');
					ctrl.limparValor('email');
					ctrl.limparValor('assunto');
					ctrl.limparValor('mensagem');
				}, function(error) {
					angular.element('#error').append('<div id="panelMessages" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><ul id="messages"></ul></div>').show();
					error.data.forEach(function(msg, i) {
						angular.element('#messages').append("<li>" + msg + "</li>");
					});
				});
			}
		};
		
		
		ctrl.limparValor = function(field) {
			delete ctrl.formularioContato[field];
		};
	};

	
	return ['$scope','$faleConoscoService', FaleConoscoController];
});
