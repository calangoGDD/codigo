define([], function() {

	var NoticiaRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/noticias', {
			templateUrl: rootContext.concat('/portal/noticia-list.html'),
			controller: 'noticiaListController',
			controllerAs: 'ctrl'
		})
		.when('/noticias-create', {
			templateUrl: rootContext.concat('/portal/noticia-create.html'),
			controller: 'noticiaCreateController',
			controllerAs: 'ctrl'
		})
		.when('/noticias-update/:id', {
			templateUrl: rootContext.concat('/portal/noticia-create.html'),
			controller: 'noticiaUpdateController',
			controllerAs: 'ctrl'
		})
		.when('/noticia-ver-mais/:id', {
			templateUrl: rootContext.concat('/portal/noticia-ver-mais.html'),
			controller: 'noticiaVerMaisController',
			controllerAs: 'ctrl'
		});

	};
	return ['CONTEXT', '$routeProvider', NoticiaRoute];
});