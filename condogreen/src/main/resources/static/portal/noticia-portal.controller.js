define([], function() {
	
	var NoticiaPortalController = function($noticiaService) {

		ctrl = this;
		ctrl.noticias = [];
		
		$noticiaService.quatroPrimeiras(function(response) {
			if(response.content) {
				ctrl.noticias = response.content;
			}
		});
		
	};
	return ['$noticiaService', NoticiaPortalController];
});