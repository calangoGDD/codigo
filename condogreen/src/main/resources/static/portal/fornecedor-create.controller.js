define([], function() {
	
	var FornecedorCreateController = function($scope, $http, $fornecedorService, $location, $route, toastr) {
		ctrl = this;
		ctrl.fornecedor = {};
		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {};
		ctrl.arquivo = {};
		
		
		ctrl.action = function() {
	    	if($scope.form.$valid) {
	    		if(!ctrl.fornecedor.imagem){
	    			toastr.error("A imagem não foi adicionada a notícia.", "Error");
	    			return;
	    		}
        		$fornecedorService.salvar(ctrl.fornecedor, function(response) {
        			$location.path("/fornecedors");
        		});
        	}
		};
		
	};
	
	return ['$scope', '$http', '$fornecedorService', '$location', '$route', 'toastr', FornecedorCreateController];
});