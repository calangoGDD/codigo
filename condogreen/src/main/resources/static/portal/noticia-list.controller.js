define([], function() {
	
	var NoticiaListController = function($location, $noticiaService, NgTableParams, CONTEXT, $modalService, $route) {

		ctrl = this;
		ctrl.CONTEXT = CONTEXT.root;
		ctrl.tableParams = new NgTableParams({page: 1, count: 10, filter: {}}, {total: 0, counts: [], getData: load});
		function load($defer, params) {
    		var filter = angular.extend({timestamp: new Date().getTime(), page: params.page() - 1, size: params.count(), sort:'dataCriacao,desc'}, params.filter());
    		
    		$noticiaService.query(filter, function(response) {
				if(response.content) {
					ctrl.noticias = response.content;
					ctrl.tableParams.total(response.totalElements);
				}
				$defer.resolve(ctrl.noticias);
			});
		}
		
		
		ctrl.createHref = function(id, action) {
			return "#/noticia-" + action + "/" + id;
		};
		
		var modalOptions = {
		        bodyText: 'A notícia será removida.'
		    };
			
		ctrl.delete = function(idNoticia) {
			$modalService.showModal({}, modalOptions).then(function (result) {
				$noticiaService.remover({id:idNoticia}, function(response) {
					$route.reload();
				});
			});
		};
		
		ctrl.ativarDesativar = function(idNoticia) {
			$noticiaService.ativarDesativar({id:idNoticia}, function(response) {
				$route.reload();
			});
		}
		
	};
	return ['$location', '$noticiaService', 'ngTableParams','CONTEXT', '$modalService', '$route', NoticiaListController];
});