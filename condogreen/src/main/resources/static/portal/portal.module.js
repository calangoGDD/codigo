/**
 * 
 */
require.config({
	paths: {
		faleConoscoRoute: 'portal/fale-conosco.route',
		faleConoscoService: 'portal/fale-conosco.service',
		faleConoscoController: 'portal/fale-conosco.controller',
		noticiaRoute: 'portal/noticia.route',
		noticiaService: 'portal/noticia.service',
		noticiaListController: 'portal/noticia-list.controller',
		noticiaCreateController: 'portal/noticia-create.controller',
		noticiaUpdateController:	'portal/noticia-update.controller',
		noticiaVerMaisController: 'portal/noticia-ver-mais.controller',
		portalController: 'portal/portal.controller',
		editorDirective: 'core/editorDirective.directive',
	}
});

var _portalDeps = [
	'angular',
	'faleConoscoRoute',
	'faleConoscoService',
	'faleConoscoController',
	'noticiaRoute',
	'noticiaService',
	'noticiaListController',
	'noticiaCreateController',
	'noticiaUpdateController',
	'noticiaVerMaisController',
	'portalController',
	'editorDirective'
];

define(_portalDeps, function(
		angular,
		faleConoscoRoute,
		faleConoscoService,
		faleConoscoController,
		noticiaRoute,
		noticiaService,
		noticiaListController,
		noticiaCreateController,
		noticiaUpdateController,
		noticiaVerMaisController,
		portalController,
		editorDirective) {
	
	var _portalModule = angular.module('portalModule', []);
	
	_portalModule.config(faleConoscoRoute);
	_portalModule.factory('$faleConoscoService', faleConoscoService);
	_portalModule.controller('faleConoscoController', faleConoscoController);

	_portalModule.config(noticiaRoute);
	_portalModule.factory('$noticiaService', noticiaService);
	_portalModule.controller('noticiaListController', noticiaListController);
	_portalModule.controller('noticiaCreateController', noticiaCreateController);
	_portalModule.controller('noticiaUpdateController', noticiaUpdateController);
	_portalModule.controller('noticiaVerMaisController', noticiaVerMaisController);
	
	_portalModule.controller('portalController', portalController);
	
	_portalModule.directive('ckedit', editorDirective);
	
	_portalModule;
});