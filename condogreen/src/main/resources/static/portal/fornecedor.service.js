define([], function() {

	var FornecedorserviceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/fornecedors/:id'), {app: 'condogreen', timeStamp : new Date().getTime(), id: '@id'},
				{
					query: {
						url: rootContext.concat('/fornecedors'),
						method: 'GET',
						isArray: false
					},
					get: {
						url: rootContext.concat('/fornecedors/get/:id'),
						method: 'GET',
						isArray: false
					},
					getQuatroPrimeiras: {
						url: rootContext.concat('/fornecedors/getQuatroPrimeiras'),
						method: 'GET',
						isArray: true
					},
					update: {
						url: rootContext.concat('/fornecedors/atualizar'),
						method: 'PUT',
						isArray: false
					},
					salvar: {
						url: rootContext.concat('/fornecedors/create'),
						method: 'POST',
						headers: {'Content-Type': undefined},
						transformRequest: function(fornecedor) {
					        var formData = new FormData();
					        
					        if(fornecedor.imagem) {
					        	formData.append("arquivo", fornecedor.imagem);
					        }
					        formData.append('fornecedor', new Blob([angular.toJson(fornecedor)], {
					            type: "application/json"
					        }));
					        return formData;
						},
						transformResponse: function(data) {
							return {conteudo: data};
						}
					},
					remover: {
						url: rootContext.concat('/fornecedors/:id/delete'),
						method: 'GET'
					},
					ativarDesativar: {
						url: rootContext.concat('/fornecedors/ativar-desativar/:id'),
						method: 'PUT'
					}
				}
			);
	};
	
	return ['CONTEXT', '$resource', FornecedorserviceFn];
});