define([], function() {

	var FaleConoscoServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/fale-conosco/'), null, {
			submit: {
				url: rootContext.concat('/fale-conosco/submit'),
				method: 'POST',
				isArray: false
			}
		});
	};
	
	return ['CONTEXT', '$resource', FaleConoscoServiceFn];
});