define([], function() {
	
	var fornecedorUpdateController = function($scope, $http, $routeParams, $fornecedorService, $location, toastr) {
		ctrl = this;
		ctrl.fornecedor = {};
		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {};
		
		ctrl.arquivo = {};
		$fornecedorService.get($routeParams, function(response) {
			ctrl.fornecedor = response;
		});
		
		ctrl.action = function() {
	    	if($scope.form.$valid) {
	    		
	    		if(!ctrl.fornecedor.imagem){
	    			toastr.error("A imgem não foi adicionada a notícia.", "Error");
	    			return;
	    		}
	    		
        		$fornecedorService.salvar(ctrl.fornecedor, function(response) {
        			$location.path("/fornecedors");
        		});
        	}
			
		};
	};
	
	return ['$scope', '$http', '$routeParams', '$fornecedorService', '$location', 'toastr', fornecedorUpdateController];
});