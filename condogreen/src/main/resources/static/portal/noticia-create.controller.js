define([], function() {
	
	var NoticiaCreateController = function($scope, $http, $noticiaService, $location, $route, toastr) {
		ctrl = this;
		ctrl.noticia = {};
		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {};
		ctrl.arquivo = {};
		
		
		ctrl.action = function() {
	    	if($scope.form.$valid) {
	    		if(!ctrl.noticia.imagem){
	    			toastr.error("A imagem não foi adicionada a notícia.", "Error");
	    			return;
	    		}
        		$noticiaService.salvar(ctrl.noticia, function(response) {
        			$location.path("/noticias");
        		});
        	}
		};
		
	};
	
	return ['$scope', '$http', '$noticiaService', '$location', '$route', 'toastr', NoticiaCreateController];
});