define([], function() {
	
	var FornecedorListController = function($location, $fornecedorService, NgTableParams, CONTEXT, $modalService, $route) {

		ctrl = this;
		ctrl.CONTEXT = CONTEXT.root;
		ctrl.tableParams = new NgTableParams({page: 1, count: 10, filter: {}}, {total: 0, counts: [], getData: load});
		function load($defer, params) {
    		var filter = angular.extend({timestamp: new Date().getTime(), page: params.page() - 1, size: params.count(), sort:'dataCriacao,desc'}, params.filter());
    		
    		$fornecedorService.query(filter, function(response) {
				if(response.content) {
					ctrl.fornecedors = response.content;
					ctrl.tableParams.total(response.totalElements);
				}
				$defer.resolve(ctrl.fornecedors);
			});
		}
		
		
		ctrl.createHref = function(id, action) {
			return "#/fornecedor-" + action + "/" + id;
		};
		
		var modalOptions = {
		        bodyText: 'O fornecedor será removido.'
		    };
			
		ctrl.delete = function(idFornecedor) {
			$modalService.showModal({}, modalOptions).then(function (result) {
				$fornecedorService.remover({id:idFornecedor}, function(response) {
					$route.reload();
				});
			});
		};
		
		ctrl.ativarDesativar = function(idFornecedor) {
			$fornecedorService.ativarDesativar({id:idFornecedor}, function(response) {
				$route.reload();
			});
		}
		
	};
	return ['$location', '$fornecedorService', 'ngTableParams','CONTEXT', '$modalService', '$route', FornecedorListController];
});