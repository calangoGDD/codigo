define([], function() {

	var FornecedorRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/fornecedors', {
			templateUrl: rootContext.concat('/portal/fornecedor-list.html'),
			controller: 'fornecedorListController',
			controllerAs: 'ctrl'
		})
		.when('/fornecedors-create', {
			templateUrl: rootContext.concat('/portal/fornecedor-create.html'),
			controller: 'fornecedorCreateController',
			controllerAs: 'ctrl'
		})
		.when('/fornecedors-update/:id', {
			templateUrl: rootContext.concat('/portal/fornecedor-create.html'),
			controller: 'fornecedorUpdateController',
			controllerAs: 'ctrl'
		})
		.when('/fornecedor-ver-mais/:id', {
			templateUrl: rootContext.concat('/portal/fornecedor-ver-mais.html'),
			controller: 'fornecedorVerMaisController',
			controllerAs: 'ctrl'
		});

	};
	return ['CONTEXT', '$routeProvider', FornecedorRoute];
});