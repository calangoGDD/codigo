define([], function() {

	var DespesaReadController = function($routeParams, $despesaService) {

		var ctrl = this;
		ctrl.disabled = true;
		
		$despesaService.get($routeParams, function(response) {
			ctrl.despesa = response;
		});
		

		ctrl.categoriaList = [
			{"man": "Manutenção"},
			{"fun": "Funcionário"},
			{"ser": "Serviço"},
			{"out": "Outros"}
		].reduce(function(memo, obj) {
		    return angular.extend(memo, obj);
		}, {});
	
	};
	return ['$routeParams', '$despesaService', DespesaReadController];
});