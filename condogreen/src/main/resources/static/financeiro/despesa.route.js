define([], function() {

	var DespesaRoute = function(CONTEXT, $routeProvider) {
		var rootContext = CONTEXT.root;

		$routeProvider
		.when('/despesa-list', {
			templateUrl: rootContext.concat('/financeiro/despesa-list.html'),
			controller: 'despesaListController',
			controllerAs: 'ctrl'
		})
		.when('/despesa-create', {
			templateUrl: rootContext.concat('/financeiro/despesa-create.html'),
			controller: 'despesaCreateController',
			controllerAs: 'ctrl'
		})
		.when('/despesa-read/:id', {
			templateUrl: rootContext.concat('/financeiro/despesa-read.html'),
			controller: 'despesaReadController',
			controllerAs: 'ctrl'
		})
		.when('/despesa-update/:id', {
			templateUrl: rootContext.concat('/financeiro/despesa-update.html'),
			controller: 'despesaUpdateController',
			controllerAs: 'ctrl'
		})
		.when('/despesa-delete/:id', {
			templateUrl: rootContext.concat('/financeiro/despesa-delete.html'),
			controller: 'despesaDeleteController',
			controllerAs: 'ctrl'
		});

	};
	return ['CONTEXT', '$routeProvider', DespesaRoute];
});