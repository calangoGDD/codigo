define([], function() {
	
	var despesaCreateController = function($scope, $http, $despesaService, $location, $route) {

		ctrl = this;
		ctrl.despesa = {};
		
		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {};
		ctrl.comprovante = {};
		
		ctrl.action = function() {
	    	if($scope.form.$valid) {
        		$despesaService.salvar(ctrl.despesa, function(response) {
        			$location.path("/despesa-list");
        		});
        	}
		};
		
		ctrl.categoriaList = [
  			{"man": "Manutenção"},
  			{"fun": "Funcionário"},
  			{"ser": "Serviço"},
  			{"out": "Outros"}
  		].reduce(function(memo, obj) {
  		    return angular.extend(memo, obj);
  		}, {});
		
	};
	
	return ['$scope', '$http', '$despesaService', '$location', '$route', despesaCreateController];
});