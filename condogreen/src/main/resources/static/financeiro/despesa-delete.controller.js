define([], function() {

	var CondominioDeleteController = function($routeParams, $location, $condominioService) {

		var ctrl = this;
		
		ctrl.condominio = {};
		$condominioService.get($routeParams, function(response) {
			ctrl.condominio = response;
		});
		
		ctrl.confirm = function(condominio) {
			$condominioService.delete($routeParams, function(response) {
				$location.path("/condominio-list");
			});
		};
	
	};
	return ['$routeParams', '$location', '$condominioService', CondominioDeleteController];
});