define([], function() {
	
	var DespesaListController = function($location, $despesaService, NgTableParams, CONTEXT, $modalService, $route) {

		ctrl = this;
		ctrl.CONTEXT = CONTEXT.root;
		ctrl.showFilter = true;
		
		ctrl.tableParams = new NgTableParams({page: 1, count: 10, filter: {nome: ""}}, {total: 0, counts: [], getData: load});
		
		function load($defer, params) {
    		var filter = angular.extend({page: params.$params.page-1, size: params.$params.count}, params.$params.filter);
			
    		$despesaService.query(filter, function(response) {

    			if(response.content) {
					ctrl.despesas = response.content;
					ctrl.tableParams.total(response.totalElements);
				}
				
    			$defer.resolve(ctrl.despesas);
			});
		}
		
		var modalOptions = {
	        bodyText: 'A despesa será removida.'
	    };
			
		ctrl.apagar = function(despesaId) {
			$modalService.showModal({}, modalOptions).then(function (result) {
				$despesaService.apagar({id:despesaId}, function(response) {
					$route.reload();
				});
			});
		};
		
		ctrl.categoriaList = [
			{"man": "Manutenção"},
			{"fun": "Funcionário"},
			{"ser": "Serviço"},
			{"out": "Outros"}
		].reduce(function(memo, obj) {
		    return angular.extend(memo, obj);
		}, {});
		
	};
	return ['$location', '$despesaService', 'ngTableParams','CONTEXT', '$modalService', '$route', DespesaListController];
});