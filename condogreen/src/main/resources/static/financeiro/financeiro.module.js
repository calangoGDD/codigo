/**
 * 
 */
require.config({
	paths: {
		despesaRoute: 'financeiro/despesa.route',
		despesaService: 'financeiro/despesa.service',
		despesaListController: 'financeiro/despesa-list.controller',
		despesaCreateController: 'financeiro/despesa-create.controller',
		despesaReadController: 'financeiro/despesa-read.controller',
		despesaUpdateController: 'financeiro/despesa-update.controller',
		despesaDeleteController: 'financeiro/despesa-delete.controller',
		dashboardController : 'financeiro/dashboard.controller',
		dashboardService : 'financeiro/dashboard.service'
	}
});

var _portalDeps = [
	'angular',
	'despesaRoute',
	'despesaService',
	'despesaListController',
	'despesaCreateController',
	'despesaReadController',
	'despesaUpdateController',
	'despesaDeleteController',
	'dashboardController',
	'dashboardService'
	
];

define(_portalDeps, function(
		angular,
		despesaRoute,
		despesaService,
		despesaListController,
		despesaCreateController,
		despesaReadController,
		despesaUpdateController,
		despesaDeleteController,
		dashboardController,
		dashboardService) {
	
	var _financeiroModule = angular.module('financeiroModule', []);
	
	_financeiroModule.config(despesaRoute);
	_financeiroModule.factory('$despesaService', despesaService);
	_financeiroModule.controller('despesaListController', despesaListController);
	_financeiroModule.controller('despesaCreateController', despesaCreateController);
	_financeiroModule.controller('despesaReadController', despesaReadController);
	_financeiroModule.controller('despesaUpdateController', despesaUpdateController);
	_financeiroModule.controller('despesaDeleteController', despesaDeleteController);
	
	_financeiroModule.factory('$dashboardService', dashboardService);
	_financeiroModule.controller('dashboardController', dashboardController);
	
	_financeiroModule;
});