define([], function() {

	var DespesaServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/despesas/:id'), {app: 'condogreen', timeStamp : new Date().getTime(), id: '@id'},
				{
					query: {
						url: rootContext.concat('/despesas'),
						method: 'GET',
						isArray: false
					},
					get: {
						url: rootContext.concat('/despesas/get/:id'),
						method: 'GET',
						isArray: false
					},
					update: {
						url: rootContext.concat('/despesas/update'),
						method: 'PUT',
						isArray: false
					},
					salvar: {
						url: rootContext.concat('/despesas/create'),
						method: 'POST',
						headers: {'Content-Type': undefined},
						transformRequest: function(despesa) {
					        var formData = new FormData();
					        
					        if(despesa.comprovante) {
					        	formData.append("comprovante", despesa.comprovante);
					        }
					        formData.append('despesa', new Blob([angular.toJson(despesa)], {
					            type: "application/json"
					        }));
					        return formData;
						},
						transformResponse: function(data) {
							return {conteudo: data};
						}
					},
					apagar: {
						url: rootContext.concat('/despesas/:id/delete'),
						method: 'GET',
						isArray: false
					}
				}
			);
	};
	
	return ['CONTEXT', '$resource', DespesaServiceFn];
});