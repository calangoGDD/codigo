define([], function() {

	var DashBoardController = function($scope, $dashboardService, $authService, $location) {
		dashboardCtrl = this;
		if(!$authService.isAuthenticated()) {
			$location.path("/");
		}
		dashboardCtrl.dashboard = {};
		$dashboardService.get({}, function(response) {
			dashboardCtrl.dashboard = response;
		});

	};
	return ['$scope', '$dashboardService', '$authService', '$location', DashBoardController];
});