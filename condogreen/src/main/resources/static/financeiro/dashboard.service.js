define([], function() {

	var DashBoardServiceFn = function(CONTEXT, $resource) {
		var rootContext = CONTEXT.root;
		return $resource(rootContext.concat('/dashboard'), {app: 'condogreen', timeStamp : new Date().getTime()},
				{
					//
				}
			);
	};
	
	return ['CONTEXT', '$resource', DashBoardServiceFn];
});