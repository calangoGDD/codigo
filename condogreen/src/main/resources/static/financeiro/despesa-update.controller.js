define([], function() {
	
	var despesaUpdateController = function($scope, $http, $routeParams, $despesaService, $location) {
		ctrl = this;
		ctrl.despesa = {};

		//Datepicker
		ctrl.opened = false;
		ctrl.dateOptions = {};
		
		ctrl.arquivo = {};
		$despesaService.get($routeParams, function(response) {
			ctrl.despesa = response;
		});
		
		ctrl.action = function() {
	    	if($scope.form.$valid) {
	    		
        		$despesaService.salvar(ctrl.despesa, function(response) {
        			$location.path("/despesa-list");
        		});
        	}
			
		};
		
		ctrl.categoriaList = [
  			{"man": "Manutenção"},
  			{"fun": "Funcionário"},
  			{"ser": "Serviço"},
  			{"out": "Outros"}
  		].reduce(function(memo, obj) {
  		    return angular.extend(memo, obj);
  		}, {});

	};
	
	return ['$scope', '$http', '$routeParams', '$despesaService', '$location', despesaUpdateController];
});