/**
 * 
 */
require.config({
	paths: {
		config: 'contexto/contexto.constant'
	}
});

var _contextoDeps = ['angular', 'config'];
define(_contextoDeps, function(angular, config) {
	
	var _contextoModule = angular.module('contextoModule', []);
	
	_contextoModule.constant('CONTEXT', config.CONTEXT);

	return _contextoModule;
});