define([], function() {
	
	var fileUploadDirective = function($parse, toastr) {
		return {
			restrict: 'E',
			scope: {
				id: '@ngId',
				name: '@ngName',
				maxfilesizemb: '@',
				showImportButton: '=',
				bindModel: '=ngModel',
				validatedimensionpicture:'@',
				withmax:'@',
				heightmax:'@',
				validextensions:'@'
			},
			template:
				'<div class="input-group">' + 
				' 	<span class="input-group-btn" ng-show="hasFile()">' +
				'		<button type="button" class="btn btn-info btn-file"><i class="glyphicon glyphicon-download"></i></button>' +
				'	</span>' +
				'	<span class="input-group-btn" ng-show="!hasFile()">' + 
				'		<label class="btn btn-info btn-file"><i class="glyphicon glyphicon-search"></i><input type="file" id="{{id}}" name="{{name}}" style="display: none;"></label>' +
				'	</span>' +
				'	<input type="text" class="form-control" ng-model="fileName" readonly="readonly">' + 
				' 	<span class="input-group-btn" ng-show="hasFile()">' +
				'		<button type="button" class="btn btn-danger btn-file" ng-click="clear()"><i class="glyphicon glyphicon-erase"></i></button>' +
				'	</span>' +
				'</div>',
			replace: true,
			link: function(scope, element, attrs) {
				var inputFile = element.find('input[type="file"]');
				
				$(inputFile).bind('change', function() {
					var objeto = angular.element(this);
					var file = objeto[0].files[0];
					
					if(!scope.maxfilesizemb) {
						scope.maxfilesizemb = 50;
					}
					
					if (file.size > scope.maxfilesizemb * 1048576) {
						toastr.error("Tamanho máximo permitido (" + (scope.maxfilesizemb * 1024) + "KB).", "Error");
						clear();
						return;
			        }
					
					var extension = file.name.split('.').pop();
					var validextensionsArray = scope.validextensions.split(",")
					if (scope.validextensions &&   validextensionsArray.indexOf(extension) <= -1) {
						toastr.error("Apenas as extensões "+ scope.validextensions.replace(',', ', ').replace(/,(?=[^,]*$)/, ' e ') +" serão aceitas.", "Error");
						clear();
						return;
					}
					
			        if(scope.validatedimensionpicture == 'true') {
						var img = new Image();
				        img.src = window.URL.createObjectURL(file);
				        img.onload = function() {
				            var width = img.naturalWidth,
				                height = img.naturalHeight;
				            window.URL.revokeObjectURL(img.src);
				            
				            if(!scope.withmax) {
				            	scope.withmax = 320;
				            }
				            
				            if(!scope.heightmax) {
				            	scope.heightmax = 200;
				            }
				            
				            if(!(width == scope.withmax && height == scope.heightmax)) {
				            	toastr.error("O tamanho da imagem deve ser "+ scope.withmax+"x"+scope.heightmax+". A imagem atual possui: "+width+"x"+ height, "Error");
				            	clear();
				            	return;
				            }
					        
				        };
			        }
					
		        });
				
				scope.$$postDigest(function() {
					inputFile.on('change', function(event) {
						var objeto = angular.element(this);
						scope.$apply(function() {
							var file = objeto[0].files[0];
							scope.bindModel = file;
							if(file){
								scope.fileName = file.name;
							}
						});
					});
				});
				
				scope.hasFile = hasFile;
				function hasFile() {
					return inputFile[0].files.length > 0;
				}
				
				scope.clear = clear; 
				function clear() {
					if(hasFile()) {
						element.find("#" + scope.id).val(null);
						scope.fileName = "";
					}
				}
				
				scope.$watch('fileName', function(n, o) {
					if(n == "") {
						clear();
					}
				});
		    }
		};
	}

	return ['$parse', 'toastr', fileUploadDirective];
});