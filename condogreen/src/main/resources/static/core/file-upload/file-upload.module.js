/**
 * 
 */
require.config({
	paths: {
		fileUploadDirective: 'core/file-upload/file-upload.directive'
	}
});

var _fileUploadDeps = ['angular', 'fileUploadDirective'];

define(_fileUploadDeps, function(angular, fileUploadDirective) {
	
	var _fileUploadModule = angular.module('fileUploadModule', []);
	
	_fileUploadModule.directive('fileUpload', fileUploadDirective);
	
	return _fileUploadModule;
});