define([], function() {

	var DatePickerLocalDateDirectiveFn = function ($parse) {
        var directive = {
                restrict: 'A',
                require: 'ngModel',
                link: link
            };
            return directive;

            function link(scope, element, attr, ngModelController) {
                ngModelController.$parsers.push(function (viewValue) {
                	console.log(viewValue);
                    return viewValue.toLocaleDateString('pt-BR');
                });

                ngModelController.$formatters.push(function (modelValue) {
                	if (!modelValue) {
                        return undefined;
                    }

					var dateParts = modelValue.split("/");

					return new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                });
            }
        }
	
	return [DatePickerLocalDateDirectiveFn];
});