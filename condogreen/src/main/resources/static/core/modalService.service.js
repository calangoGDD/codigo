define([], function() {

	var ModalServiceFn = function (CONTEXT, $uibModal, $rootScope, $uibModalInstance) {
		var rootContext = CONTEXT.root;

	    var modalDefaults = {
	        backdrop: true,
	        keyboard: true,
	        modalFade: true,
	        templateUrl: rootContext.concat('/core/modal.html')
	    };

	    var modalOptions = {
	        closeButtonText: 'Cancelar',
	        actionButtonText: 'Confirmar',
	        headerText: 'Deseja Prosseguir?',
	        bodyText: 'action'
	    };

	    this.showModal = function (customModalDefaults, customModalOptions) {
	        if (!customModalDefaults) customModalDefaults = {};
	        customModalDefaults.backdrop = 'static';
	        return this.show(customModalDefaults, customModalOptions);
	    };

	    this.show = function (customModalDefaults, customModalOptions) {
	        //Create temp objects to work with since we're in a singleton service
	        var tempModalDefaults = {};
	        var tempModalOptions = {};

	        //Map angular-ui modal custom defaults to modal defaults defined in service
	        angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

	        //Map modal.html $rootScope custom properties to defaults defined in service
	        angular.extend(tempModalOptions, modalOptions, customModalOptions);

	        if (!tempModalDefaults.controller) {
	            tempModalDefaults.controller = function ($rootScope, $uibModalInstance) {
	                $rootScope.modalOptions = tempModalOptions;
	                $rootScope.modalOptions.ok = function (result) {
	                    $uibModalInstance.close(result);
	                };
	                $rootScope.modalOptions.close = function (result) {
	                    $uibModalInstance.dismiss('cancel');
	                };
	            }
	        }

	        return $uibModal.open(tempModalDefaults).result;
	    };

	};
	
	return ['CONTEXT', '$uibModal', '$rootScope', ModalServiceFn];
});