define([], function() {

	var DatePickerDirectiveFn = function(CONTEXT) {
		var rootContext = CONTEXT.root;

		return {
			restrict : "E",
			scope : {
				ngModel : "=",
				dateOptions : "=",
				opened : "="
			},
			link : function($scope, element, attrs) {

				$scope.dateOptions.showWeeks = false
				
				$scope.open = function(event) {
					event.preventDefault();
					event.stopPropagation();
					$scope.opened = true;
				};

				$scope.clear = function() {
					$scope.ngModel = null;
				};
			},
			templateUrl : rootContext.concat('/core/datepicker.html')
		}
	};

	return [ 'CONTEXT', DatePickerDirectiveFn ];
});