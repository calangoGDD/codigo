define([], function() {
	
	
	var XSRFInterceptor = function($q, $cookies, $location) {
		return  {
			request: function(config) {
				var token = $cookies.get('XSRF-TOKEN');
				if (token) {
					config.headers['XSRF-TOKEN'] = token;
				}
				return config;
			},
			requestError: function(config) {
				return $q.reject(rejection);
			},
			responseError: function(rejection) {
				if(rejection.status == '401') {
					$location.path("/");
				}
				return $q.reject(rejection);
			}
		};
	};
	
	return ['$q', '$cookies', '$location', XSRFInterceptor];
});