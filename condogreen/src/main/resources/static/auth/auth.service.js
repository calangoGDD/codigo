define([], function() {

	var AtuhServiceFn = function($cookies, $http) {
		return {
			setPrincipal: function(principal) {
				$cookies.putObject('principal', principal);
			},
			getPrincipal: function() {
				return $cookies.getObject('principal');
			},
			hasAuthority: function(authority) {
				var authenticated = $cookies.get('authenticated');
				var searchPerfil = [];
				if(authenticated) {
					var principal = $cookies.getObject('principal');
					searchPerfil = principal.authorities.filter(function(o, i) {
						return o.authority == authority;
					});
				}
				return searchPerfil.length > 0;
			},
			isAuthenticated : function() {
				return $cookies.get('authenticated');
			},
			authenticate: function(credentials, callback) {
		    	var headers = credentials ? {authorization : "Basic " + btoa(credentials.username + ":" + credentials.password)} : {};
		    	var setPrincipal = this.setPrincipal;
		    	var hasAuthority = this.hasAuthority;
		    	$http.get('user', {headers : headers})
					.success(function(data) {
						if (data.name) {
							setPrincipal(data.principal);
							$cookies.put('authenticated', true);
						} else {
							$cookies.remove('authenticated');
						}
						var redirectUrl = hasAuthority('ADMIN') ? "/home" : "/homeb";
						callback && callback({authenticated: $cookies.get('authenticated'), redirectUrl: redirectUrl});
					})
					.error(function() {
						$cookies.remove('authenticated');
						callback && callback({authenticated: false});
					});
			},
			logout: function() {
				return $http.get('logout')
					.success(function() {
						$cookies.remove('principal');
						$cookies.remove('authenticated');
						return false;
					})
					.error(function(data) {
						$cookies.remove('authenticated');
						return false;
					});
			}
		};
	};
	
	return ['$cookies', '$http', AtuhServiceFn];
});