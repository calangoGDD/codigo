define([], function() {
	
	var AuthController = function($authService, $location) {
		var authCtrl = this;

		authCtrl.error = false;
		
		$authService.authenticate();
		authCtrl.credentials = {};
		authCtrl.login = function() {
			$authService.authenticate(authCtrl.credentials, function(response) {
				if (response.authenticated) {
					$location.path(response.redirectUrl);
				} else {
					authCtrl.error = true;
					authCtrl.credentials = {};
					$location.path("/login");
				}
			});
		};
		
		authCtrl.logout = function() {
			$authService.logout().then(function() {
				$location.path("/login");
			});
		};
	}

	return ['$authService', '$location', AuthController];
});
