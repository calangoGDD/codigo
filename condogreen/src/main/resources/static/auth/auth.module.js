/**
 * 
 */
require.config({
	paths: {
		authController: 'auth/auth.controller',
		authService: 'auth/auth.service',
		xsrfInterceptor: 'auth/xsrf.interceptor'
	}
});

var _authDeps = ['angular', 'authController', 'authService', 'xsrfInterceptor'];

define(_authDeps, function(angular, authController, authService, xsrfInterceptor) {
	
	var _authModule = angular.module('authModule', []);
	
	_authModule.controller('authController', authController);
	_authModule.service('$authService', authService);
	_authModule.factory('XSRFInterceptor', xsrfInterceptor);
	
	return _authModule;
});