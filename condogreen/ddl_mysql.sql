
    alter table Perfil 
        drop 
        foreign key FK_cx7a3iu75anw3qtsl73ix3osc;

    alter table apartamento 
        drop 
        foreign key FK_oku3j5160x9rb6fe8o8db5wat;

    alter table apartamento 
        drop 
        foreign key FK_cjuroe203pol5bttugwdam2jm;

    alter table apartamento_morador 
        drop 
        foreign key FK_n9h5cgr7i9aco5smu1jnfnju5;

    alter table apartamento_morador 
        drop 
        foreign key FK_2o2s9usk0xlbta8o22022r4lw;

    alter table bloco 
        drop 
        foreign key FK_7sjna2g699pjqwqq8x8cqwlj2;

    alter table boleto 
        drop 
        foreign key FK_lhrgffvqscqlg3503lrskmw1l;

    alter table boleto 
        drop 
        foreign key FK_4qqp39nrjj01k3q7h5enl9j3r;

    alter table boleto 
        drop 
        foreign key FK_12svkxvsjk8nkitxfydmao022;

    alter table condominio 
        drop 
        foreign key FK_6sdcpo24j6djmk3m7ikahdvbi;

    alter table condominio 
        drop 
        foreign key FK_j0uy3l2y2270d3wvhfg7iwwug;

    alter table convidado 
        drop 
        foreign key FK_1dbndl655art6rdpqd2edmumc;

    alter table documento 
        drop 
        foreign key FK_9yjumoit571whf11qqwb8fr1b;

    alter table informacao_retorno 
        drop 
        foreign key FK_idwhaprsc1vjvy547qcf385x9;

    alter table informacao_retorno 
        drop 
        foreign key FK_7s5adtv98hcpaib9vqeosd381;

    alter table item 
        drop 
        foreign key FK_ci73t0elgs0pvrd6t1opc5nrx;

    alter table morador 
        drop 
        foreign key FK_hhd8i9nhsxi8mykuportmxihi;

    alter table pessoa 
        drop 
        foreign key FK_fd9cwlj8jd3cj2kbajkoji2v3;

    alter table pessoa_fisica 
        drop 
        foreign key FK_1xbamx9axtft6c9hfxmaumr0k;

    alter table pessoa_juridica 
        drop 
        foreign key FK_5g7aqbsr2okfs904x38li5px4;

    alter table remessa 
        drop 
        foreign key FK_thcu587tboyjaejr9o49pj8hb;

    alter table remessa_boleto 
        drop 
        foreign key FK_a7wixlen6ydnpo4jmnvfovm5a;

    alter table remessa_boleto 
        drop 
        foreign key FK_1kw0ln88los08mut2xb36pion;

    alter table reserva 
        drop 
        foreign key FK_jfl3pli7sslxrutmuaqrkcvhd;

    alter table transporte 
        drop 
        foreign key FK_i5h8ohj6s3bq59jqcyo22wxc8;

    alter table valor_customizado 
        drop 
        foreign key FK_sfnd7nk34010k1to4hihiq4fs;

    alter table valor_customizado 
        drop 
        foreign key FK_4183sii72au2o2ex7orpnsas1;

    drop table if exists Conta;

    drop table if exists Perfil;

    drop table if exists apartamento;

    drop table if exists apartamento_morador;

    drop table if exists bloco;

    drop table if exists boleto;

    drop table if exists categoria_documento;

    drop table if exists condominio;

    drop table if exists configurador_boleto;

    drop table if exists conta_bancaria;

    drop table if exists convidado;

    drop table if exists despesa;

    drop table if exists documento;

    drop table if exists endereco;

    drop table if exists informacao_retorno;

    drop table if exists item;

    drop table if exists morador;

    drop table if exists motivo_ocorrencia;

    drop table if exists noticia;

    drop table if exists pessoa;

    drop table if exists pessoa_fisica;

    drop table if exists pessoa_juridica;

    drop table if exists recebe_boleto;

    drop table if exists remessa;

    drop table if exists remessa_boleto;

    drop table if exists reserva;

    drop table if exists retorno;

    drop table if exists situacao_boleto;

    drop table if exists transporte;

    drop table if exists valor_customizado;

    create table Conta (
        nome varchar(255) not null,
        email varchar(255),
        senha varchar(255),
        primary key (nome)
    );

    create table Perfil (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        nome varchar(255),
        conta_nome varchar(255),
        primary key (id)
    );

    create table apartamento (
        nome varchar(30),
        id bigint not null,
        bloco_id bigint,
        primary key (id)
    );

    create table apartamento_morador (
        morador_id bigint not null,
        apartamento_id bigint not null,
        primary key (morador_id, apartamento_id)
    );

    create table bloco (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        nome varchar(30),
        condominio_id bigint,
        primary key (id)
    );

    create table boleto (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        cd_movimento varchar(255),
        cd_carteira varchar(255),
        dt_emissao tinyblob,
        data_vencimento tinyblob,
        modalidade varchar(255),
        nosso_numero varchar(255),
        numero_documento varchar(255),
        vl_boleto decimal(19,2),
        configurador_id bigint,
        sacado_id bigint,
        situacao_boleto_id bigint,
        primary key (id)
    );

   

    create table condominio (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        empresa_id bigint,
        sindico_id bigint,
        primary key (id)
    );

    create table configurador_boleto (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        aceite varchar(255),
        cod_descont varchar(255),
        cd_devolucao varchar(100),
        cod_mora varchar(255),
        cod_multa varchar(255),
        cod_protesto varchar(100),
        dia_desconto integer,
        tp_titulo varchar(255),
        instrucao1 varchar(40),
        instrucao2 varchar(40),
        instrucao_ao_sacado varchar(255),
        local_pagamento varchar(255),
        nome varchar(255),
        prazo_devolucao integer,
        prazo_protesto integer,
        valor Decimal(15,2) default '0.00',
        vl_desc Decimal(15,2) default '0.00',
        vl_mora Decimal(15,2) default '0.00',
        vl_multa Decimal(15,2) default '0.00',
        primary key (id)
    );

    create table conta_bancaria (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        agencia varchar(255),
        codigo_beneficiario varchar(255),
        digito_codigo_beneficiario varchar(255),
        primary key (id)
    );

    create table convidado (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        data_convite tinyblob,
        nome varchar(40),
        morador_id bigint,
        primary key (id)
    );

    create table despesa (
        id bigint not null auto_increment,
        categoria varchar(255),
        comprovante longblob,
        data_pagamento tinyblob,
        data_vencimento tinyblob not null,
        nome varchar(255) not null,
        valor decimal(19,2) not null,
        versao bigint,
        primary key (id)
    );

    

    create table endereco (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        bairro varchar(15),
        cep varchar(5),
        cidade varchar(15),
        complemento varchar(100),
        numero varchar(15),
        pais varchar(10),
        rua varchar(40),
        sufixo_cep varchar(3),
        uf varchar(2),
        primary key (id)
    );

    create table informacao_retorno (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        codigo_movimento varchar(255),
        data_efeticacao_credito tinyblob,
        data_ocorrencia tinyblob,
        motivo_ocorrencia varchar(255),
        ordem integer,
        valor_efetivo_creditado decimal(19,2),
        valor_multa_juros decimal(19,2),
        valor_pago decimal(19,2),
        valor_tarifa decimal(19,2),
        boleto_id bigint,
        retorno_id bigint,
        primary key (id)
    );

    create table item (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        nome varchar(255),
        condominio_id bigint,
        primary key (id)
    );

    create table morador (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        imagem_perfil tinyblob,
        responsavel_boleto bit,
        tipo varchar(255),
        pessoa_id bigint,
        primary key (id)
    );

    create table motivo_ocorrencia (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        codigo varchar(255),
        descricao varchar(255),
        quadro varchar(255),
        primary key (id)
    );

    create table noticia (
        id bigint not null auto_increment,
        conteudo varchar(4000),
        data_criacao tinyblob,
        imagem longblob,
        nomeimagem varchar(255),
        situacao bit,
        titulo varchar(40),
        versao bigint,
        primary key (id)
    );

    create table pessoa (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        celular varchar(255),
        codigo_area_celular varchar(3),
        nome varchar(30),
        conta_nome varchar(255),
        primary key (id)
    );

    create table pessoa_fisica (
        cpf varchar(11),
        data_nascimento datetime,
        id bigint not null,
        primary key (id)
    );

    create table pessoa_juridica (
        cnpj varchar(14),
        nome_fantasia varchar(30),
        id bigint not null,
        primary key (id)
    );

    create table recebe_boleto (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        primary key (id)
    );

    create table remessa (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        arquivo longblob,
        nome varchar(30),
        numero_remessa bigint,
        qtd_boletos integer not null,
        empresa_id bigint,
        primary key (id)
    );

    create table remessa_boleto (
        remessa_id bigint not null,
        boleto_id bigint not null
    );

    create table reserva (
        id bigint not null auto_increment,
        descricao varchar(100),
        diatodo bit,
        fim tinyblob,
        inicio tinyblob,
        situacao varchar(255) not null,
        titulo varchar(40),
        morador_id bigint,
        primary key (id)
    );

    create table retorno (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        arquivo longblob,
        data_importacao tinyblob,
        numero_retorno bigint,
        quantidade_registros bigint,
        valor_total decimal(19,2),
        primary key (id)
    );

    create table situacao_boleto (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        codigo varchar(255),
        descricao varchar(255),
        primary key (id)
    );

    create table transporte (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        placa varchar(255),
        tipo varchar(255),
        apartamento_id bigint,
        primary key (id)
    );

    create table valor_customizado (
        id bigint not null auto_increment,
        atualizacao datetime not null,
        valor decimal(19,2),
        configurador_id bigint,
        recebedor_boleto_id bigint,
        primary key (id)
    );

    alter table Conta 
        add constraint UK_t0ri4tv9tcaagrwkm86qywixk unique (email);

    alter table Perfil 
        add constraint FK_cx7a3iu75anw3qtsl73ix3osc 
        foreign key (conta_nome) 
        references Conta (nome);

    alter table apartamento 
        add constraint FK_oku3j5160x9rb6fe8o8db5wat 
        foreign key (bloco_id) 
        references bloco (id);

    alter table apartamento 
        add constraint FK_cjuroe203pol5bttugwdam2jm 
        foreign key (id) 
        references recebe_boleto (id);

    alter table apartamento_morador 
        add constraint FK_n9h5cgr7i9aco5smu1jnfnju5 
        foreign key (apartamento_id) 
        references apartamento (id);

    alter table apartamento_morador 
        add constraint FK_2o2s9usk0xlbta8o22022r4lw 
        foreign key (morador_id) 
        references morador (id);

    alter table bloco 
        add constraint FK_7sjna2g699pjqwqq8x8cqwlj2 
        foreign key (condominio_id) 
        references condominio (id);

    alter table boleto 
        add constraint FK_lhrgffvqscqlg3503lrskmw1l 
        foreign key (configurador_id) 
        references configurador_boleto (id);

    alter table boleto 
        add constraint FK_4qqp39nrjj01k3q7h5enl9j3r 
        foreign key (sacado_id) 
        references pessoa (id);

    alter table boleto 
        add constraint FK_12svkxvsjk8nkitxfydmao022 
        foreign key (situacao_boleto_id) 
        references situacao_boleto (id);

    alter table condominio 
        add constraint FK_6sdcpo24j6djmk3m7ikahdvbi 
        foreign key (empresa_id) 
        references pessoa_juridica (id);

    alter table condominio 
        add constraint FK_j0uy3l2y2270d3wvhfg7iwwug 
        foreign key (sindico_id) 
        references pessoa (id);

    alter table convidado 
        add constraint FK_1dbndl655art6rdpqd2edmumc 
        foreign key (morador_id) 
        references morador (id);

    

    alter table informacao_retorno 
        add constraint FK_idwhaprsc1vjvy547qcf385x9 
        foreign key (boleto_id) 
        references boleto (id);

    alter table informacao_retorno 
        add constraint FK_7s5adtv98hcpaib9vqeosd381 
        foreign key (retorno_id) 
        references retorno (id);

    alter table item 
        add constraint FK_ci73t0elgs0pvrd6t1opc5nrx 
        foreign key (condominio_id) 
        references condominio (id);

    alter table morador 
        add constraint FK_hhd8i9nhsxi8mykuportmxihi 
        foreign key (pessoa_id) 
        references pessoa (id);

    alter table pessoa 
        add constraint FK_fd9cwlj8jd3cj2kbajkoji2v3 
        foreign key (conta_nome) 
        references Conta (nome);

    alter table pessoa_fisica 
        add constraint FK_1xbamx9axtft6c9hfxmaumr0k 
        foreign key (id) 
        references pessoa (id);

    alter table pessoa_juridica 
        add constraint FK_5g7aqbsr2okfs904x38li5px4 
        foreign key (id) 
        references pessoa (id);

    alter table remessa 
        add constraint FK_thcu587tboyjaejr9o49pj8hb 
        foreign key (empresa_id) 
        references pessoa_juridica (id);

    alter table remessa_boleto 
        add constraint FK_a7wixlen6ydnpo4jmnvfovm5a 
        foreign key (boleto_id) 
        references boleto (id);

    alter table remessa_boleto 
        add constraint FK_1kw0ln88los08mut2xb36pion 
        foreign key (remessa_id) 
        references remessa (id);

    alter table reserva 
        add constraint FK_jfl3pli7sslxrutmuaqrkcvhd 
        foreign key (morador_id) 
        references morador (id);

    alter table transporte 
        add constraint FK_i5h8ohj6s3bq59jqcyo22wxc8 
        foreign key (apartamento_id) 
        references apartamento (id);

    alter table valor_customizado 
        add constraint FK_sfnd7nk34010k1to4hihiq4fs 
        foreign key (configurador_id) 
        references configurador_boleto (id);

    alter table valor_customizado 
        add constraint FK_4183sii72au2o2ex7orpnsas1 
        foreign key (recebedor_boleto_id) 
        references recebe_boleto (id);
